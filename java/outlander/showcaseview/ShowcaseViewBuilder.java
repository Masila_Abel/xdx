package outlander.showcaseview;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.widget.LinearLayout;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ShowcaseViewBuilder extends View implements OnTouchListener {
    private static final String TAG = "SHOWCASE_VIEW";
    private int backgroundOverlayColor;
    private Paint backgroundPaint;
    private HashMap<Integer, OnClickListener> idsClickListenerMap = new HashMap();
    private HashMap<Rect, Integer> idsRectMap = new HashMap();
    private Activity mActivity;
    private float mCenterX;
    private float mCenterY;
    private List<View> mCustomView = new ArrayList();
    private List<Integer> mCustomViewGravity = new ArrayList();
    private int mCustomViewMargin;
    private boolean mHideOnTouchOutside;
    private Drawable mMarkerDrawable;
    private float mMarkerDrawableBottomMargin = 0.0f;
    private int mMarkerDrawableGravity;
    private float mMarkerDrawableLeftMargin = 0.0f;
    private float mMarkerDrawableRightMargin = 0.0f;
    private float mMarkerDrawableTopMargin = 0.0f;
    private float mRadius;
    private float mRingWidth = 10.0f;
    private View mTargetView;
    private int ringColor;
    private Paint ringPaint;
    private Canvas tempCanvas;
    private Paint transparentPaint;

    private ShowcaseViewBuilder(Context context) {
        super(context);
    }

    private ShowcaseViewBuilder(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public static ShowcaseViewBuilder init(Activity activity) {
        ShowcaseViewBuilder showcaseViewBuilder = new ShowcaseViewBuilder(activity);
        showcaseViewBuilder.mActivity = activity;
        showcaseViewBuilder.setClickable(true);
        return showcaseViewBuilder;
    }

    public ShowcaseViewBuilder setTargetView(View view) {
        this.mTargetView = view;
        return this;
    }

    private void calculateRadiusAndCenter() {
        int width = this.mTargetView.getMeasuredWidth();
        int height = this.mTargetView.getMeasuredHeight();
        int[] xy = new int[]{0, 0};
        this.mTargetView.getLocationInWindow(xy);
        this.mCenterX = (float) (xy[0] + (width / 2));
        this.mCenterY = (float) (xy[1] + (height / 2));
        if (width > height) {
            this.mRadius = (float) ((width * 7) / 12);
        } else {
            this.mRadius = (float) ((height * 7) / 12);
        }
    }

    public ShowcaseViewBuilder setHideOnTouchOutside(boolean value) {
        this.mHideOnTouchOutside = value;
        return this;
    }

    public ShowcaseViewBuilder setMarkerDrawable(Drawable drawable, int gravity) {
        this.mMarkerDrawable = drawable;
        this.mMarkerDrawableGravity = gravity;
        return this;
    }

    public ShowcaseViewBuilder setDrawableLeftMargin(float margin) {
        this.mMarkerDrawableLeftMargin = margin;
        return this;
    }

    public ShowcaseViewBuilder setDrawableRightMargin(float margin) {
        this.mMarkerDrawableRightMargin = margin;
        return this;
    }

    public ShowcaseViewBuilder setDrawableTopMargin(float margin) {
        this.mMarkerDrawableTopMargin = margin;
        return this;
    }

    public ShowcaseViewBuilder setDrawableBottomMargin(float margin) {
        this.mMarkerDrawableBottomMargin = margin;
        return this;
    }

    public ShowcaseViewBuilder addCustomView(int layoutId, int gravity) {
        View view = LayoutInflater.from(this.mActivity).inflate(layoutId, null);
        LinearLayout linearLayout = new LinearLayout(this.mActivity);
        linearLayout.addView(view);
        linearLayout.setGravity(17);
        DisplayMetrics metrics = new DisplayMetrics();
        this.mActivity.getWindowManager().getDefaultDisplay().getMetrics(metrics);
        Rect rect = new Rect();
        rect.set(0, 0, metrics.widthPixels, metrics.heightPixels);
        linearLayout.measure(MeasureSpec.makeMeasureSpec(rect.width(), 1073741824), MeasureSpec.makeMeasureSpec(rect.height(), 1073741824));
        this.mCustomView.add(linearLayout);
        this.mCustomViewGravity.add(Integer.valueOf(gravity));
        return this;
    }

    public ShowcaseViewBuilder addCustomView(int layoutId) {
        View view = LayoutInflater.from(this.mActivity).inflate(layoutId, null);
        DisplayMetrics metrics = new DisplayMetrics();
        this.mActivity.getWindowManager().getDefaultDisplay().getMetrics(metrics);
        Rect rect = new Rect();
        rect.set(0, 0, metrics.widthPixels, metrics.heightPixels);
        view.measure(MeasureSpec.makeMeasureSpec(rect.width(), 1073741824), MeasureSpec.makeMeasureSpec(rect.height(), 1073741824));
        this.mCustomView.add(view);
        this.mCustomViewGravity.add(Integer.valueOf(0));
        return this;
    }

    public ShowcaseViewBuilder setCustomViewMargin(int margin) {
        this.mCustomViewMargin = margin;
        return this;
    }

    public ShowcaseViewBuilder setRingColor(int color) {
        this.ringColor = color;
        return this;
    }

    public ShowcaseViewBuilder setRingWidth(float ringWidth) {
        this.mRingWidth = ringWidth;
        return this;
    }

    public ShowcaseViewBuilder setBackgroundOverlayColor(int color) {
        this.backgroundOverlayColor = color;
        return this;
    }

    public void show() {
        this.transparentPaint = new Paint();
        this.ringPaint = new Paint();
        this.backgroundPaint = new Paint();
        if (this.mTargetView != null) {
            if (this.mTargetView.getWidth() == 0 || this.mTargetView.getHeight() == 0) {
                this.mTargetView.getViewTreeObserver().addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
                    public void onGlobalLayout() {
                        ShowcaseViewBuilder.this.invalidate();
                        ShowcaseViewBuilder.this.addShowcaseView();
                        ShowcaseViewBuilder.this.mTargetView.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                    }
                });
            } else {
                invalidate();
                addShowcaseView();
            }
        }
        setOnTouchListener(this);
    }

    private void addShowcaseView() {
        ((ViewGroup) this.mActivity.getWindow().getDecorView()).addView(this);
    }

    public void hide() {
        this.mCustomView.clear();
        this.mCustomViewGravity.clear();
        this.idsClickListenerMap.clear();
        this.idsRectMap.clear();
        this.mHideOnTouchOutside = false;
        ((ViewGroup) this.mActivity.getWindow().getDecorView()).removeView(this);
    }

    protected void onDraw(Canvas canvas) {
        if (this.mTargetView != null) {
            setShowcase(canvas);
            drawMarkerDrawable(canvas);
            addCustomView(canvas);
        }
        super.onDraw(canvas);
    }

    private void drawMarkerDrawable(Canvas canvas) {
        if (this.mMarkerDrawable != null) {
            switch (this.mMarkerDrawableGravity) {
                case R.styleable.View_paddingEnd /*3*/:
                    this.mMarkerDrawable.setBounds((int) (((((this.mCenterX + this.mMarkerDrawableLeftMargin) - this.mRadius) - ((float) this.mMarkerDrawable.getMinimumWidth())) - this.mRingWidth) - 10.0f), (int) ((this.mCenterY + this.mMarkerDrawableTopMargin) - ((float) this.mMarkerDrawable.getMinimumHeight())), (int) ((((this.mCenterX + this.mMarkerDrawableLeftMargin) - this.mRadius) - this.mRingWidth) - 10.0f), (int) (this.mCenterY + this.mMarkerDrawableTopMargin));
                    break;
                case R.styleable.Toolbar_contentInsetStart /*5*/:
                    this.mMarkerDrawable.setBounds((int) ((((this.mCenterX + this.mMarkerDrawableLeftMargin) + this.mRadius) + this.mRingWidth) + 10.0f), (int) ((this.mCenterY + this.mMarkerDrawableTopMargin) - ((float) this.mMarkerDrawable.getMinimumHeight())), (int) (((((this.mCenterX + this.mMarkerDrawableLeftMargin) + this.mRadius) + ((float) this.mMarkerDrawable.getMinimumWidth())) + this.mRingWidth) + 10.0f), (int) (this.mCenterY + this.mMarkerDrawableTopMargin));
                    break;
                case R.styleable.AppCompatTheme_homeAsUpIndicator /*48*/:
                    this.mMarkerDrawable.setBounds((int) ((this.mCenterX + this.mMarkerDrawableLeftMargin) - ((float) this.mMarkerDrawable.getMinimumWidth())), (int) (((((this.mCenterY + this.mMarkerDrawableTopMargin) - this.mRadius) - ((float) this.mMarkerDrawable.getMinimumHeight())) - this.mRingWidth) - 10.0f), (int) (this.mCenterX + this.mMarkerDrawableLeftMargin), (int) ((((this.mCenterY + this.mMarkerDrawableTopMargin) - this.mRadius) - this.mRingWidth) - 10.0f));
                    break;
                case R.styleable.AppCompatTheme_panelMenuListTheme /*80*/:
                    this.mMarkerDrawable.setBounds((int) ((this.mCenterX + this.mMarkerDrawableLeftMargin) - ((float) this.mMarkerDrawable.getMinimumWidth())), (int) ((((this.mCenterY + this.mMarkerDrawableTopMargin) + this.mRadius) + this.mRingWidth) + 10.0f), (int) (this.mCenterX + this.mMarkerDrawableLeftMargin), (int) (((((this.mCenterY + this.mMarkerDrawableTopMargin) + this.mRadius) + ((float) this.mMarkerDrawable.getMinimumHeight())) + this.mRingWidth) + 10.0f));
                    break;
            }
            this.mMarkerDrawable.draw(canvas);
            return;
        }
        Log.d(TAG, "No marker drawable defined");
    }

    private void addCustomView(Canvas canvas) {
        if (this.mCustomView.size() != 0) {
            for (int i = 0; i < this.mCustomView.size(); i++) {
                float cy = (float) (((View) this.mCustomView.get(i)).getMeasuredHeight() / 2);
                float cx = (float) (((View) this.mCustomView.get(i)).getMeasuredWidth() / 2);
                switch (((Integer) this.mCustomViewGravity.get(i)).intValue()) {
                    case R.styleable.View_paddingEnd /*3*/:
                        float viewHeight = (float) ((ViewGroup) this.mCustomView.get(i)).getChildAt(0).getMeasuredHeight();
                        ((View) this.mCustomView.get(i)).layout(0, 0, (int) ((this.mCenterX - (2.0f * this.mRadius)) - ((float) (this.mCustomViewMargin * 2))), (int) ((((float) ((View) this.mCustomView.get(i)).getMeasuredHeight()) - viewHeight) + (2.0f * (this.mCenterY - cy))));
                        break;
                    case R.styleable.Toolbar_contentInsetStart /*5*/:
                        float marginX = (cx - this.mCenterX) - (2.0f * (this.mRadius + ((float) this.mCustomViewMargin)));
                        float viewWidth = (float) ((ViewGroup) this.mCustomView.get(i)).getChildAt(0).getMeasuredWidth();
                        ((View) this.mCustomView.get(i)).layout(0, 0, (int) ((((float) ((View) this.mCustomView.get(i)).getMeasuredWidth()) - (2.0f * marginX)) + viewWidth), (int) ((((float) ((View) this.mCustomView.get(i)).getMeasuredHeight()) - ((float) ((ViewGroup) this.mCustomView.get(i)).getChildAt(0).getMeasuredHeight())) + (this.mCenterY - cy)));
                        break;
                    case R.styleable.AppCompatTheme_homeAsUpIndicator /*48*/:
                        ((View) this.mCustomView.get(i)).layout(0, 0, ((View) this.mCustomView.get(i)).getMeasuredWidth(), (int) (((float) ((View) this.mCustomView.get(i)).getMeasuredHeight()) + (2.0f * ((this.mCenterY - cy) - (2.0f * (this.mRadius + ((float) this.mCustomViewMargin)))))));
                        break;
                    case R.styleable.AppCompatTheme_panelMenuListTheme /*80*/:
                        ((View) this.mCustomView.get(i)).layout(0, 0, ((View) this.mCustomView.get(i)).getMeasuredWidth(), (int) (((float) ((View) this.mCustomView.get(i)).getMeasuredHeight()) - (2.0f * ((((float) (((View) this.mCustomView.get(i)).getMeasuredHeight() / 2)) - this.mCenterY) - (2.0f * (this.mRadius + ((float) this.mCustomViewMargin)))))));
                        break;
                    default:
                        ((View) this.mCustomView.get(i)).layout(0, 0, ((View) this.mCustomView.get(i)).getMeasuredWidth(), ((View) this.mCustomView.get(i)).getMeasuredHeight());
                        break;
                }
                ((View) this.mCustomView.get(i)).draw(canvas);
            }
            return;
        }
        Log.d(TAG, "No Custom View defined");
    }

    private ArrayList<View> getAllChildren(View v) {
        if (v instanceof ViewGroup) {
            ArrayList<View> result = new ArrayList();
            ViewGroup viewGroup = (ViewGroup) v;
            for (int i = 0; i < viewGroup.getChildCount(); i++) {
                View child = viewGroup.getChildAt(i);
                ArrayList<View> viewArrayList = new ArrayList();
                viewArrayList.add(v);
                viewArrayList.addAll(getAllChildren(child));
                result.addAll(viewArrayList);
            }
            return result;
        }
        viewArrayList = new ArrayList();
        viewArrayList.add(v);
        return viewArrayList;
    }

    private void setShowcase(Canvas canvas) {
        calculateRadiusAndCenter();
        Bitmap bitmap = Bitmap.createBitmap(canvas.getWidth(), canvas.getHeight(), Config.ARGB_8888);
        this.tempCanvas = new Canvas(bitmap);
        this.backgroundPaint.setColor(this.backgroundOverlayColor);
        this.backgroundPaint.setAntiAlias(true);
        this.transparentPaint.setColor(getResources().getColor(17170445));
        this.transparentPaint.setXfermode(new PorterDuffXfermode(Mode.CLEAR));
        this.transparentPaint.setAntiAlias(true);
        this.ringPaint.setColor(this.ringColor);
        this.ringPaint.setXfermode(new PorterDuffXfermode(Mode.ADD));
        this.ringPaint.setAntiAlias(true);
        this.tempCanvas.drawRect(0.0f, 0.0f, (float) this.tempCanvas.getWidth(), (float) this.tempCanvas.getHeight(), this.backgroundPaint);
        this.tempCanvas.drawCircle(this.mCenterX, this.mCenterY, this.mRadius + this.mRingWidth, this.ringPaint);
        this.tempCanvas.drawCircle(this.mCenterX, this.mCenterY, this.mRadius, this.transparentPaint);
        canvas.drawBitmap(bitmap, 0.0f, 0.0f, new Paint());
    }

    public void setClickListenerOnView(int id, OnClickListener clickListener) {
        this.idsClickListenerMap.put(Integer.valueOf(id), clickListener);
    }

    private int getAbsoluteLeft(View myView) {
        if (myView == null) {
            return 0;
        }
        if (myView.getParent() == myView.getRootView()) {
            return myView.getLeft();
        }
        return getAbsoluteLeft((View) myView.getParent()) + myView.getLeft();
    }

    private int getAbsoluteTop(View myView) {
        if (myView == null) {
            return 0;
        }
        if (myView.getParent() == myView.getRootView()) {
            return myView.getTop();
        }
        return getAbsoluteTop((View) myView.getParent()) + myView.getTop();
    }

    public boolean onTouch(View v, MotionEvent event) {
        if (this.idsRectMap.isEmpty()) {
            for (View parentView : this.mCustomView) {
                for (View view : getAllChildren(parentView)) {
                    Rect rect = new Rect();
                    rect.set(getAbsoluteLeft(view), getAbsoluteTop(view), getAbsoluteLeft(view) + view.getMeasuredWidth(), getAbsoluteTop(view) + view.getMeasuredHeight());
                    if (view.getId() > 0) {
                        this.idsRectMap.put(rect, Integer.valueOf(view.getId()));
                    }
                }
            }
        }
        if (event.getAction() == 1) {
            float X = event.getX();
            float Y = event.getY();
            Object[] keys = this.idsRectMap.keySet().toArray();
            for (int i = 0; i < this.idsRectMap.size(); i++) {
                Rect r = keys[i];
                if (r.contains((int) X, (int) Y)) {
                    int id = ((Integer) this.idsRectMap.get(r)).intValue();
                    if (this.idsClickListenerMap.get(Integer.valueOf(id)) != null) {
                        ((OnClickListener) this.idsClickListenerMap.get(Integer.valueOf(id))).onClick(v);
                        return true;
                    }
                }
            }
            if (this.mHideOnTouchOutside) {
                hide();
                return true;
            }
        }
        return false;
    }
}
