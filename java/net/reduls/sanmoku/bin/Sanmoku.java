package net.reduls.sanmoku.bin;

import java.io.IOException;
import net.reduls.sanmoku.Morpheme;
import net.reduls.sanmoku.Tagger;
import net.reduls.sanmoku.util.ReadLine;
import outlander.showcaseview.BuildConfig;

public final class Sanmoku {
    public static void main(String[] strArr) throws IOException {
        int i = 1;
        if (!(strArr.length == 0 || (strArr.length == 1 && strArr[0].equals("-wakati")))) {
            System.err.println("Usage: java net.reduls.igo.bin.Sanmoku [-wakati]");
            System.exit(1);
        }
        if (strArr.length != 1) {
            i = 0;
        }
        ReadLine readLine = new ReadLine(System.in);
        String read;
        if (i != 0) {
            for (read = readLine.read(); read != null; read = readLine.read()) {
                for (String read2 : Tagger.wakati(read2)) {
                    System.out.print(read2 + " ");
                }
                System.out.println(BuildConfig.FLAVOR);
            }
            return;
        }
        for (read2 = readLine.read(); read2 != null; read2 = readLine.read()) {
            for (Morpheme morpheme : Tagger.parse(read2)) {
                System.out.println(morpheme.surface + "\t" + morpheme.feature);
            }
            System.out.println("EOS");
        }
    }
}
