package net.reduls.sanmoku.dic;

import java.util.Iterator;
import net.reduls.sanmoku.util.Misc;

public final class Morpheme {
    private static final byte[] leafAccCounts = Misc.readBytesFromFile("morp.leaf.cnt.bin", 2);
    private static final byte[] leafs = Misc.readBytesFromFile("morp.leaf.bin", 8);
    private static final byte[] morpMap = Misc.readBytesFromFile("morp.info.map", 4);
    private static final byte[] morps = Misc.readBytesFromFile("morp.info.bin", 2);
    private static final int nextBase = Misc.readIntFromFile("morp.base.bin");

    static class Entry {
        public final short cost;
        public final int morphemeId;
        public final short posId;

        private Entry(int i) {
            int i2 = ((Morpheme.morps[(i * 2) + 0] & 255) << 8) | (Morpheme.morps[(i * 2) + 1] & 255);
            this.posId = (short) (((short) (Morpheme.morpMap[(i2 * 4) + 0] << 8)) | ((short) (Morpheme.morpMap[(i2 * 4) + 1] & 255)));
            this.cost = (short) (((short) (Morpheme.morpMap[(i2 * 4) + 3] & 255)) | ((short) (Morpheme.morpMap[(i2 * 4) + 2] << 8)));
            this.morphemeId = i;
        }
    }

    static class MorphemeIterator implements Iterator<Entry> {
        private int node;

        public MorphemeIterator(int i) {
            this.node = i;
        }

        public boolean hasNext() {
            return this.node != -1;
        }

        public Entry next() {
            Entry entry = new Entry(this.node);
            this.node = Morpheme.nextNode(this.node);
            return entry;
        }

        public void remove() {
            throw new UnsupportedOperationException();
        }
    }

    private static final long getLeaf(int i) {
        int i2 = i / 64;
        return ((long) (leafs[(i2 * 8) + 7] & 255)) | (((((((((long) leafs[(i2 * 8) + 0]) << 56) | (((long) (leafs[(i2 * 8) + 1] & 255)) << 48)) | (((long) (leafs[(i2 * 8) + 2] & 255)) << 40)) | (((long) (leafs[(i2 * 8) + 3] & 255)) << 32)) | (((long) (leafs[(i2 * 8) + 4] & 255)) << 24)) | (((long) (leafs[(i2 * 8) + 5] & 255)) << 16)) | (((long) (leafs[(i2 * 8) + 6] & 255)) << 8));
    }

    public static Iterable<Entry> getMorphemes(final int i) {
        return new Iterable<Entry>() {
            public Iterator<Entry> iterator() {
                return new MorphemeIterator(i);
            }
        };
    }

    private static final boolean hasNext(long j, int i) {
        return ((1 << (i % 64)) & j) != 0;
    }

    private static final int nextNode(int i) {
        long leaf = getLeaf(i);
        return !hasNext(leaf, i) ? -1 : nextNode(leaf, i);
    }

    private static final int nextNode(long j, int i) {
        int i2 = i / 64;
        return (((leafAccCounts[(i2 * 2) + 1] & 255) | ((leafAccCounts[(i2 * 2) + 0] & 255) << 8)) + nextBase) + Long.bitCount(((1 << (i % 64)) - 1) & j);
    }
}
