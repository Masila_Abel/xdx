package net.reduls.sanmoku.dic;

import net.reduls.sanmoku.dic.Char.Category;
import net.reduls.sanmoku.dic.WordDic.Callback;

public final class Unknown {
    private static final Category space = Char.category(' ');

    public static void search(String str, int i, Callback callback) {
        char charAt = str.charAt(i);
        Category category = Char.category(charAt);
        if (callback.isEmpty() || category.invoke) {
            boolean z = category == space;
            int min = Math.min(str.length(), category.length + i);
            int i2 = i;
            while (i2 < min) {
                WordDic.eachViterbiNode(callback, category.id, i, (i2 - i) + 1, z);
                if (i2 + 1 == min || Char.isCompatible(charAt, str.charAt(i2 + 1))) {
                    i2++;
                } else {
                    return;
                }
            }
            if (category.group && i2 < str.length()) {
                while (i2 < str.length()) {
                    if (Char.isCompatible(charAt, str.charAt(i2))) {
                        i2++;
                    } else {
                        WordDic.eachViterbiNode(callback, category.id, i, i2 - i, z);
                        return;
                    }
                }
                WordDic.eachViterbiNode(callback, category.id, i, str.length() - i, z);
            }
        }
    }
}
