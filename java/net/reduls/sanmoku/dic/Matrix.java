package net.reduls.sanmoku.dic;

import java.io.Closeable;
import net.reduls.sanmoku.util.Misc;

public final class Matrix {
    private static final int leftNum;
    private static final byte[] matrix;
    private static final byte[] posid_map = Misc.readBytesFromFile("posid-map.bin", 2);
    private static final byte[] val = Misc.readBytesFromFile("matrix.map", 2);

    static {
        Closeable openDictionaryDataAsDIS = Misc.openDictionaryDataAsDIS("matrix.bin");
        int readInt = Misc.readInt(openDictionaryDataAsDIS);
        leftNum = Misc.readInt(openDictionaryDataAsDIS);
        matrix = new byte[(readInt * 7)];
        try {
            openDictionaryDataAsDIS.readFully(matrix, 0, matrix.length);
            Misc.close(openDictionaryDataAsDIS);
        } catch (Throwable e) {
            throw new RuntimeException(e);
        }
    }

    public static short linkCost(short s, short s2) {
        int posid = (posid(s) * leftNum) + posid(s2);
        posid = ((int) (node(posid / 4) >> ((posid % 4) * 14))) & 16383;
        return (short) ((val[(posid * 2) + 1] & 255) | (val[posid * 2] << 8));
    }

    private static long node(int i) {
        return ((((((((long) (matrix[(i * 7) + 0] & 255)) << 48) | (((long) (matrix[(i * 7) + 1] & 255)) << 40)) | (((long) (matrix[(i * 7) + 2] & 255)) << 32)) | (((long) (matrix[(i * 7) + 3] & 255)) << 24)) | (((long) (matrix[(i * 7) + 4] & 255)) << 16)) | (((long) (matrix[(i * 7) + 5] & 255)) << 8)) | ((long) (matrix[(i * 7) + 6] & 255));
    }

    private static short posid(short s) {
        return (short) ((posid_map[s * 2] << 8) | (posid_map[(s * 2) + 1] & 255));
    }
}
