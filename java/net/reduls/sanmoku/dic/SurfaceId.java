package net.reduls.sanmoku.dic;

import net.reduls.sanmoku.dic.WordDic.Callback;
import net.reduls.sanmoku.util.Misc;
import outlander.showcaseview.R;

public final class SurfaceId {
    private static final byte[] char_to_chck = Misc.readBytesFromFile("surface-id.bin.char", 256, 1);
    private static final byte[] exts = Misc.readBytesFromFile("surface-id.bin.ext", 1);
    private static final int idOffset = Misc.readIntFromFile("category.bin");
    private static final byte[] nodes = Misc.readBytesFromFile("surface-id.bin.node", 1);

    private static int base(long j) {
        return (int) (524287 & j);
    }

    private static char chck(long j) {
        return (char) ((int) ((j >> 20) & 127));
    }

    private static boolean checkEC(CodeStream codeStream, long j) {
        char c = (char) ((int) ((j >> 27) & 127));
        return c == '\u0000' || (read(codeStream) == c && !codeStream.isEos());
    }

    private static boolean checkEncodedChildren(CodeStream codeStream, long j) {
        switch (type(j)) {
            case R.styleable.View_android_theme /*0*/:
                return checkEC(codeStream, j);
            default:
                return true;
        }
    }

    public static void eachCommonPrefix(String str, int i, Callback callback) {
        long node = getNode(0);
        int i2 = idOffset;
        CodeStream codeStream = new CodeStream(str, i);
        while (true) {
            if (isTerminal(node)) {
                int i3 = i2 + 1;
                WordDic.eachViterbiNode(callback, i2, i, codeStream.position() - i, false);
                i2 = i3;
            }
            if (!codeStream.isEos() && checkEncodedChildren(codeStream, node)) {
                char read = read(codeStream);
                node = getNode(base(node) + read);
                if (chck(node) == read) {
                    i2 += siblingTotal(node);
                } else {
                    return;
                }
            }
            return;
        }
    }

    private static long getNode(int i) {
        return ((((((long) (nodes[(i * 5) + 0] & 255)) << 32) | (((long) (nodes[(i * 5) + 1] & 255)) << 24)) | (((long) (nodes[(i * 5) + 2] & 255)) << 16)) | (((long) (nodes[(i * 5) + 3] & 255)) << 8)) | ((long) (nodes[(i * 5) + 4] & 255));
    }

    private static boolean isTerminal(long j) {
        return ((j >> 19) & 1) == 1;
    }

    private static char read(CodeStream codeStream) {
        return (char) (char_to_chck[codeStream.read()] & 255);
    }

    private static int siblingTotal(long j) {
        switch (type(j)) {
            case R.styleable.View_android_theme /*0*/:
                return (int) ((j >> 34) & 31);
            case R.styleable.View_paddingStart /*2*/:
                return (int) ((j >> 27) & 2047);
            default:
                int i = (int) ((j >> 27) & 2047);
                return ((exts[(i * 4) + 3] & 255) << 0) | ((((exts[(i * 4) + 0] & 255) << 24) | ((exts[(i * 4) + 1] & 255) << 16)) | ((exts[(i * 4) + 2] & 255) << 8));
        }
    }

    private static int type(long j) {
        return ((j >> 39) & 1) == 1 ? ((int) ((j >> 38) & 1)) + 2 : 0;
    }
}
