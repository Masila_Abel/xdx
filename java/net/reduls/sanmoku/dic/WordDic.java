package net.reduls.sanmoku.dic;

public final class WordDic {

    public interface Callback {
        void call(ViterbiNode viterbiNode);

        boolean isEmpty();
    }

    public static void eachViterbiNode(Callback callback, int i, int i2, int i3, boolean z) {
        for (Entry entry : Morpheme.getMorphemes(i)) {
            callback.call(new ViterbiNode(i2, (short) i3, entry.cost, entry.posId, z, entry.morphemeId));
        }
    }

    public static void search(String str, int i, Callback callback) {
        SurfaceId.eachCommonPrefix(str, i, callback);
    }
}
