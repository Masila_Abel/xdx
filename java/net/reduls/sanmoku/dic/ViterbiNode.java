package net.reduls.sanmoku.dic;

public final class ViterbiNode {
    public int cost;
    private final int length_posId_isSpace;
    public final int morphemeId;
    public ViterbiNode prev = null;
    public final int start;

    public ViterbiNode(int i, short s, short s2, short s3, boolean z, int i2) {
        this.cost = s2;
        this.start = i;
        this.length_posId_isSpace = (z ? 1 : 0) + ((s3 << 1) + (s << 17));
        this.morphemeId = i2;
    }

    public static ViterbiNode makeBOSEOS() {
        return new ViterbiNode(0, (short) 0, (short) 0, (short) 0, false, 0);
    }

    public boolean isSpace() {
        return (this.length_posId_isSpace & 1) == 1;
    }

    public short length() {
        return (short) (this.length_posId_isSpace >> 17);
    }

    public short posId() {
        return (short) ((this.length_posId_isSpace >> 1) & 65535);
    }
}
