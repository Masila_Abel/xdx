package net.reduls.sanmoku.dic;

import java.io.Closeable;
import java.util.ArrayList;
import net.reduls.sanmoku.util.Misc;

public final class PartsOfSpeech {
    private static final String[] posArray;

    static {
        Closeable openDictionaryDataAsBR = Misc.openDictionaryDataAsBR("pos.bin");
        ArrayList arrayList = new ArrayList();
        for (Object readLine = Misc.readLine(openDictionaryDataAsBR); readLine != null; readLine = Misc.readLine(openDictionaryDataAsBR)) {
            arrayList.add(readLine);
        }
        Misc.close(openDictionaryDataAsBR);
        posArray = new String[arrayList.size()];
        for (int i = 0; i < posArray.length; i++) {
            posArray[i] = (String) arrayList.get(i);
        }
    }

    public static final String get(int i) {
        return posArray[i];
    }
}
