package net.reduls.sanmoku.dic;

import outlander.showcaseview.R;

public final class CodeStream {
    private int code;
    private final int end = this.src.length();
    private int octetLen;
    private int octetPos;
    private int pos;
    private final CharSequence src;

    public CodeStream(CharSequence charSequence, int i) {
        this.src = charSequence;
        this.pos = i;
        this.code = i == this.end ? 0 : charSequence.charAt(i);
        this.octetLen = octetLength(this.code);
        this.octetPos = this.octetLen;
    }

    private void eat() {
        this.octetPos--;
        if (this.octetPos == 0) {
            this.pos++;
            if (!isEos()) {
                this.code = this.src.charAt(this.pos);
                this.octetLen = octetLength(this.code);
                this.octetPos = this.octetLen;
            }
        }
    }

    private int octetLength(int i) {
        return i < 128 ? 1 : i < 2048 ? 2 : i < 65536 ? 3 : 4;
    }

    private char peek() {
        if (this.octetPos == this.octetLen) {
            switch (this.octetLen) {
                case R.styleable.View_android_focusable /*1*/:
                    return (char) this.code;
                case R.styleable.View_paddingStart /*2*/:
                    return (char) (((byte) ((this.code >> 6) & 31)) + 192);
                case R.styleable.View_paddingEnd /*3*/:
                    return (char) (((byte) ((this.code >> 12) & 15)) + 224);
                default:
                    return (char) (((byte) ((this.code >> 18) & 7)) + 240);
            }
        }
        return (char) (((byte) ((this.code >> ((this.octetPos - 1) * 6)) & 63)) + 128);
    }

    public boolean isEos() {
        return this.pos == this.end;
    }

    public int position() {
        return this.pos;
    }

    public char read() {
        char peek = peek();
        eat();
        return peek;
    }
}
