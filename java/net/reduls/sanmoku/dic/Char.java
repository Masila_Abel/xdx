package net.reduls.sanmoku.dic;

import net.reduls.sanmoku.util.Misc;

public final class Char {
    private static final Category[] charCategorys;
    private static final byte[] charInfos = Misc.readBytesFromFile("code.bin", 6);

    public static final class Category {
        public final boolean group;
        public final int id;
        public final boolean invoke;
        public final byte length;

        public Category(int i, boolean z, boolean z2, byte b) {
            this.id = i;
            this.invoke = z;
            this.group = z2;
            this.length = b;
        }
    }

    static {
        Object openDictionaryDataAsDIS = Misc.openDictionaryDataAsDIS("category.bin");
        int readInt = Misc.readInt(openDictionaryDataAsDIS);
        charCategorys = new Category[readInt];
        for (int i = 0; i < readInt; i++) {
            charCategorys[i] = new Category(i, Misc.readByte(openDictionaryDataAsDIS) == (byte) 1, Misc.readByte(openDictionaryDataAsDIS) == (byte) 1, Misc.readByte(openDictionaryDataAsDIS));
        }
        Misc.close(openDictionaryDataAsDIS);
    }

    public static final Category category(char c) {
        return charCategorys[findNode(c) >> 16];
    }

    private static final int compatibleMask(char c) {
        return findNode(c) & 65535;
    }

    public static final int findNode(char c) {
        int i = 0;
        int length = charInfos.length / 6;
        while (true) {
            int i2 = ((length - i) / 2) + i;
            if (length - i == 1) {
                return nodeValue(i);
            }
            if (c < nodeCode(i2)) {
                length = i2;
            } else if (c >= nodeCode(i2)) {
                i = i2;
            }
        }
    }

    public static final boolean isCompatible(char c, char c2) {
        return (compatibleMask(c) & compatibleMask(c2)) != 0;
    }

    public static final int nodeCode(int i) {
        return (((charInfos[(i * 6) + 0] & 255) << 16) | ((charInfos[(i * 6) + 1] & 255) << 8)) | ((charInfos[(i * 6) + 2] & 255) << 0);
    }

    public static final int nodeValue(int i) {
        return (((charInfos[(i * 6) + 3] & 255) << 16) | ((charInfos[(i * 6) + 4] & 255) << 8)) | ((charInfos[(i * 6) + 5] & 255) << 0);
    }
}
