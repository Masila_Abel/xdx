package net.reduls.sanmoku;

import java.util.ArrayList;
import java.util.List;
import net.reduls.sanmoku.dic.Matrix;
import net.reduls.sanmoku.dic.PartsOfSpeech;
import net.reduls.sanmoku.dic.Unknown;
import net.reduls.sanmoku.dic.ViterbiNode;
import net.reduls.sanmoku.dic.WordDic;
import net.reduls.sanmoku.dic.WordDic.Callback;

public final class Tagger {
    private static final ArrayList<ViterbiNode> BOS_NODES = new ArrayList(1);

    private static final class MakeLattice implements Callback {
        private boolean empty = true;
        private int i;
        private final ArrayList<ArrayList<ViterbiNode>> nodesAry;
        private ArrayList<ViterbiNode> prevs;

        public MakeLattice(ArrayList<ArrayList<ViterbiNode>> arrayList) {
            this.nodesAry = arrayList;
        }

        public void call(ViterbiNode viterbiNode) {
            this.empty = false;
            if (viterbiNode.isSpace()) {
                ((ArrayList) this.nodesAry.get(this.i + viterbiNode.length())).addAll(this.prevs);
            } else {
                ((ArrayList) this.nodesAry.get(this.i + viterbiNode.length())).add(Tagger.setMincostNode(viterbiNode, this.prevs));
            }
        }

        public boolean isEmpty() {
            return this.empty;
        }

        public void set(int i) {
            this.i = i;
            this.prevs = (ArrayList) this.nodesAry.get(i);
            this.empty = true;
        }
    }

    static {
        BOS_NODES.add(ViterbiNode.makeBOSEOS());
    }

    public static List<Morpheme> parse(String str) {
        return parse(str, new ArrayList(str.length() / 2));
    }

    public static List<Morpheme> parse(String str, List<Morpheme> list) {
        for (ViterbiNode parseImpl = parseImpl(str); parseImpl != null; parseImpl = parseImpl.prev) {
            list.add(new Morpheme(str.substring(parseImpl.start, parseImpl.start + parseImpl.length()), PartsOfSpeech.get(parseImpl.posId()), parseImpl.start, parseImpl.morphemeId));
        }
        return list;
    }

    public static ViterbiNode parseImpl(String str) {
        int length = str.length();
        ArrayList arrayList = new ArrayList(length + 1);
        arrayList.add(BOS_NODES);
        for (int i = 1; i <= length; i++) {
            arrayList.add(new ArrayList());
        }
        Callback makeLattice = new MakeLattice(arrayList);
        for (int i2 = 0; i2 < length; i2++) {
            if (!((ArrayList) arrayList.get(i2)).isEmpty()) {
                makeLattice.set(i2);
                WordDic.search(str, i2, makeLattice);
                Unknown.search(str, i2, makeLattice);
                if (i2 > 0) {
                    ((ArrayList) arrayList.get(i2)).clear();
                }
            }
        }
        ViterbiNode viterbiNode = setMincostNode(ViterbiNode.makeBOSEOS(), (ArrayList) arrayList.get(length)).prev;
        ViterbiNode viterbiNode2 = null;
        while (viterbiNode.prev != null) {
            ViterbiNode viterbiNode3 = viterbiNode.prev;
            viterbiNode.prev = viterbiNode2;
            viterbiNode2 = viterbiNode;
            viterbiNode = viterbiNode3;
        }
        return viterbiNode2;
    }

    private static ViterbiNode setMincostNode(ViterbiNode viterbiNode, ArrayList<ViterbiNode> arrayList) {
        ViterbiNode viterbiNode2 = (ViterbiNode) arrayList.get(0);
        viterbiNode.prev = viterbiNode2;
        int linkCost = viterbiNode2.cost + Matrix.linkCost(viterbiNode2.posId(), viterbiNode.posId());
        for (int i = 1; i < arrayList.size(); i++) {
            viterbiNode2 = (ViterbiNode) arrayList.get(i);
            int linkCost2 = viterbiNode2.cost + Matrix.linkCost(viterbiNode2.posId(), viterbiNode.posId());
            if (linkCost2 < linkCost) {
                viterbiNode.prev = viterbiNode2;
                linkCost = linkCost2;
            }
        }
        viterbiNode.cost += linkCost;
        return viterbiNode;
    }

    public static List<String> wakati(String str) {
        return wakati(str, new ArrayList(str.length() / 1));
    }

    public static List<String> wakati(String str, List<String> list) {
        for (ViterbiNode parseImpl = parseImpl(str); parseImpl != null; parseImpl = parseImpl.prev) {
            list.add(str.substring(parseImpl.start, parseImpl.start + parseImpl.length()));
        }
        return list;
    }
}
