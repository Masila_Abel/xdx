package net.reduls.sanmoku.util;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.Closeable;
import java.io.DataInput;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public final class Misc {
    public static void close(Closeable closeable) {
        try {
            closeable.close();
        } catch (IOException e) {
            throw new AssertionError(e.getMessage());
        }
    }

    public static InputStream openDictionaryData(String str) {
        return Misc.class.getResourceAsStream("/net/reduls/sanmoku/dicdata/" + str);
    }

    public static BufferedReader openDictionaryDataAsBR(String str) {
        try {
            return new BufferedReader(new InputStreamReader(openDictionaryData(str), "UTF-8"), 80960);
        } catch (IOException e) {
            throw new AssertionError(e.getMessage());
        }
    }

    public static DataInputStream openDictionaryDataAsDIS(String str) {
        return new DataInputStream(new BufferedInputStream(openDictionaryData(str), 80960));
    }

    public static byte readByte(DataInput dataInput) {
        try {
            return dataInput.readByte();
        } catch (IOException e) {
            throw new AssertionError(e.getMessage());
        }
    }

    public static byte[] readBytesFromFile(String str, int i) {
        Object openDictionaryDataAsDIS = openDictionaryDataAsDIS(str);
        byte[] bArr = new byte[(readInt(openDictionaryDataAsDIS) * i)];
        try {
            openDictionaryDataAsDIS.readFully(bArr, 0, bArr.length);
            return bArr;
        } catch (Throwable e) {
            throw new RuntimeException(e);
        }
    }

    public static byte[] readBytesFromFile(String str, int i, int i2) {
        byte[] bArr = new byte[(i * i2)];
        try {
            openDictionaryDataAsDIS(str).readFully(bArr, 0, bArr.length);
            return bArr;
        } catch (Throwable e) {
            throw new RuntimeException(e);
        }
    }

    public static char readChar(DataInput dataInput) {
        try {
            return dataInput.readChar();
        } catch (IOException e) {
            throw new AssertionError(e.getMessage());
        }
    }

    public static int readInt(DataInput dataInput) {
        try {
            return dataInput.readInt();
        } catch (IOException e) {
            throw new AssertionError(e.getMessage());
        }
    }

    public static int readIntFromFile(String str) {
        Object openDictionaryDataAsDIS = openDictionaryDataAsDIS(str);
        int readInt = readInt(openDictionaryDataAsDIS);
        close(openDictionaryDataAsDIS);
        return readInt;
    }

    public static String readLine(BufferedReader bufferedReader) {
        try {
            return bufferedReader.readLine();
        } catch (IOException e) {
            throw new AssertionError(e.getMessage());
        }
    }

    public static long readLong(DataInput dataInput) {
        try {
            return dataInput.readLong();
        } catch (IOException e) {
            throw new AssertionError(e.getMessage());
        }
    }

    public static short readShort(DataInput dataInput) {
        try {
            return dataInput.readShort();
        } catch (IOException e) {
            throw new AssertionError(e.getMessage());
        }
    }
}
