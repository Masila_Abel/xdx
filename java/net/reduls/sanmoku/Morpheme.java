package net.reduls.sanmoku;

public final class Morpheme {
    public final String feature;
    final int morphemeId;
    public final int start;
    public final String surface;

    public Morpheme(String str, String str2, int i, int i2) {
        this.surface = str;
        this.feature = str2;
        this.start = i;
        this.morphemeId = i2;
    }
}
