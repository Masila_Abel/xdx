package com.amusoft.sophiebot;

public class ChatMessage {
    public boolean left;
    public String text;

    public ChatMessage(boolean left, String text) {
        this.left = left;
        this.text = text;
    }

    public ChatMessage(ChatMessage chatMessage) {
    }
}
