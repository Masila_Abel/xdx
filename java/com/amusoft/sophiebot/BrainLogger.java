package com.amusoft.sophiebot;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import java.util.ArrayList;

public class BrainLogger {
    private static BrainLogger instance;
    private Context context;
    private ArrayList<String> logs = new ArrayList();

    public static void setup(Context context) {
        instance = new BrainLogger(context);
    }

    public static BrainLogger getInstance() {
        if (instance != null) {
            return instance;
        }
        throw new UnsupportedOperationException("You forgot to call setup method to inizialize BrainLogger");
    }

    private BrainLogger(Context context) {
        this.context = context;
    }

    public void info(String line) {
        this.logs.add(line);
        notify(line);
    }

    public ArrayList<String> getLogs() {
        return this.logs;
    }

    private void notify(String line) {
        LocalBroadcastManager.getInstance(this.context).sendBroadcast(new Intent(Constants.BROADCAST_ACTION_LOGGER).putExtra(Constants.EXTENDED_LOGGER_INFO, line));
    }
}
