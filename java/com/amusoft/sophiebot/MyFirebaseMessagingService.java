package com.amusoft.sophiebot;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.RingtoneManager;
import android.support.v4.app.NotificationCompat.Builder;
import android.util.Log;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import java.util.HashMap;
import java.util.Map;

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    private static final String TAG = "MyFirebaseMsgService";
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference mFirebaseBackupHistory = this.database.getReference("CHATLOG");
    SharedPreferences mUltiChatSettings;

    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.d(TAG, "From: " + remoteMessage.getFrom());
        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());
            String mUsername = getApplication().getSharedPreferences("SophiePrefs", 0).getString("username", null);
            Map<String, Object> newPost = new HashMap();
            newPost.put("left", Boolean.valueOf(true));
            newPost.put("text", remoteMessage.getNotification().getBody());
            this.mFirebaseBackupHistory.child(mUsername).push().setValue(newPost);
            sendNotification(remoteMessage.getNotification().getBody());
        }
        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
            sendNotification(remoteMessage.getNotification().getBody());
        }
    }

    private void sendNotification(String messageBody) {
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra("notificationBody", messageBody);
        intent.addFlags(67108864);
        ((NotificationManager) getSystemService("notification")).notify(0, new Builder(this).setSmallIcon(R.drawable.ic_bot).setContentTitle("Daily Fact").setContentText(messageBody).setAutoCancel(true).setSound(RingtoneManager.getDefaultUri(2)).setContentIntent(PendingIntent.getActivity(this, 0, intent, 1073741824)).build());
    }
}
