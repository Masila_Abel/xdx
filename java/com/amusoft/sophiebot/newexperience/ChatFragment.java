package com.amusoft.sophiebot.newexperience;

import android.annotation.TargetApi;
import android.content.SharedPreferences;
import android.database.DataSetObserver;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnKeyListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;
import com.amusoft.sophiebot.R;
import com.amusoft.sophiebot.chatroom.ChatMessage;
import com.amusoft.sophiebot.chatroom.FirebaseListAdapter;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.analytics.FirebaseAnalytics.Event;
import com.google.firebase.analytics.FirebaseAnalytics.Param;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import java.util.Random;
import outlander.showcaseview.BuildConfig;

public class ChatFragment extends Fragment {
    private static final String ARG_SECTION_NUMBER = "section_number";
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    EditText inputText;
    ListView listView;
    private FirebaseListAdapter mChatListAdapter;
    private ValueEventListener mConnectedListener;
    private FirebaseAnalytics mFirebaseAnalytics;
    DatabaseReference mFirebaseRef = this.database.getReference("chat");
    private String mUsername;
    View rootView;

    public static ChatFragment newInstance(int sectionNumber) {
        ChatFragment fragment = new ChatFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    @TargetApi(17)
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.rootView = inflater.inflate(R.layout.activity_main_newexperience, container, false);
        this.mFirebaseAnalytics = FirebaseAnalytics.getInstance(getActivity().getApplicationContext());
        setupUsername();
        this.inputText = (EditText) this.rootView.findViewById(R.id.chat_editText);
        this.inputText.setOnKeyListener(new OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() != 0 || keyCode != 66) {
                    return false;
                }
                String question = ChatFragment.this.inputText.getText().toString();
                ChatFragment.this.sendMessage();
                ChatFragment.this.inputText.setText(BuildConfig.FLAVOR);
                Bundle bundle = new Bundle();
                bundle.putString(Param.ITEM_ID, "ASKED QUESTION");
                bundle.putString(Param.ITEM_NAME, "ASKED QUESTION");
                bundle.putString(Param.CONTENT_TYPE, "TEXT");
                ChatFragment.this.mFirebaseAnalytics.logEvent(Event.SELECT_CONTENT, bundle);
                return true;
            }
        });
        Toolbar toolbar = (Toolbar) this.rootView.findViewById(R.id.toolbar);
        if (toolbar != null) {
            toolbar.setTitle("Forum");
            toolbar.setTextAlignment(4);
            toolbar.setTitleTextColor(getResources().getColor(R.color.icons));
        }
        return this.rootView;
    }

    public void onStart() {
        super.onStart();
        this.mChatListAdapter = new FirebaseListAdapter(this.mFirebaseRef, getActivity(), this.mUsername) {
            public void cleanup() {
                super.cleanup();
            }
        };
        this.listView = (ListView) this.rootView.findViewById(R.id.chat_listView);
        this.listView.setAdapter(this.mChatListAdapter);
        this.mChatListAdapter.registerDataSetObserver(new DataSetObserver() {
            public void onChanged() {
                super.onChanged();
                ChatFragment.this.listView.setSelection(ChatFragment.this.mChatListAdapter.getCount() - 1);
            }
        });
        this.mConnectedListener = this.mFirebaseRef.getRoot().child(".info/connected").addValueEventListener(new ValueEventListener() {
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (((Boolean) dataSnapshot.getValue()).booleanValue()) {
                    Toast.makeText(ChatFragment.this.getActivity().getApplicationContext(), "Connected", 0).show();
                } else {
                    Toast.makeText(ChatFragment.this.getActivity().getApplicationContext(), "Disconnected", 0).show();
                }
            }

            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }

    public void onStop() {
        super.onStop();
        this.mFirebaseRef.getRoot().child(".info/connected").removeEventListener(this.mConnectedListener);
        this.mChatListAdapter.cleanup();
    }

    private void setupUsername() {
        SharedPreferences prefs = getActivity().getApplication().getSharedPreferences("ChatPrefs", 0);
        this.mUsername = prefs.getString("username", null);
        if (this.mUsername == null) {
            this.mUsername = "Client" + new Random().nextInt(100000);
            prefs.edit().putString("username", this.mUsername).commit();
        }
    }

    private void sendMessage() {
        EditText inputText = (EditText) this.rootView.findViewById(R.id.chat_editText);
        String input = inputText.getText().toString();
        if (!input.equals(BuildConfig.FLAVOR)) {
            ChatMessage chat = new ChatMessage(input, this.mUsername);
            this.mFirebaseRef = this.database.getReference("chat");
            this.mFirebaseRef.push().setValue(chat);
            inputText.setText(BuildConfig.FLAVOR);
        }
    }
}
