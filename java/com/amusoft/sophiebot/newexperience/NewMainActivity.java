package com.amusoft.sophiebot.newexperience;

import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.Toolbar.OnMenuItemClickListener;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import com.amusoft.sophiebot.Constants;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.analytics.FirebaseAnalytics.Event;
import com.google.firebase.analytics.FirebaseAnalytics.Param;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;
import outlander.showcaseview.R;

public class NewMainActivity extends AppCompatActivity {
    static final int SET_FEEDBACK = 1;
    static final int SET_SOUNDS = 0;
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    private FirebaseAnalytics mFirebaseAnalytics;
    DatabaseReference mFirebaseRef = this.database.getReference("Feedback");
    private SectionsPagerAdapter mSectionsPagerAdapter;
    SharedPreferences mUltiChatSettings;
    private ViewPager mViewPager;
    Toolbar toolbar;

    public class SectionsPagerAdapter extends FragmentPagerAdapter {
        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        public Fragment getItem(int position) {
            switch (position) {
                case R.styleable.View_android_theme /*0*/:
                    return MainFragment.newInstance(0);
                case NewMainActivity.SET_FEEDBACK /*1*/:
                    return ChatFragment.newInstance(NewMainActivity.SET_FEEDBACK);
                default:
                    return MainFragment.newInstance(0);
            }
        }

        public int getCount() {
            return 2;
        }

        public CharSequence getPageTitle(int position) {
            switch (position) {
                case R.styleable.View_android_theme /*0*/:
                    return "SOPHIE";
                case NewMainActivity.SET_FEEDBACK /*1*/:
                    return "FORUM";
                default:
                    return null;
            }
        }
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (VERSION.SDK_INT >= 11) {
            getWindow().setFlags(8192, 8192);
        }
        setContentView(com.amusoft.sophiebot.R.layout.activity_new_main);
        this.mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        this.mUltiChatSettings = getSharedPreferences(Constants.PREF_SOUND, 0);
        setToolBar();
    }

    private void setToolBar() {
        this.toolbar = (Toolbar) findViewById(com.amusoft.sophiebot.R.id.toolbarnav);
        if (this.toolbar != null) {
            setSupportActionBar(this.toolbar);
            setUpActionbar();
            getOverflowMenu();
            this.mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
            this.mViewPager = (ViewPager) findViewById(com.amusoft.sophiebot.R.id.container);
            this.mViewPager.setAdapter(this.mSectionsPagerAdapter);
            this.toolbar.setCollapsible(true);
            this.toolbar.setBackgroundColor(getResources().getColor(com.amusoft.sophiebot.R.color.primary));
            this.toolbar.setTitleTextColor(getResources().getColor(com.amusoft.sophiebot.R.color.icons));
            this.toolbar.inflateMenu(com.amusoft.sophiebot.R.menu.main);
            this.toolbar.setOnMenuItemClickListener(new OnMenuItemClickListener() {
                public boolean onMenuItemClick(MenuItem menuItem) {
                    int id = menuItem.getItemId();
                    if (id == com.amusoft.sophiebot.R.id.action_tutorial) {
                        NewMainActivity.this.getApplication().getSharedPreferences("SophiePrefs", 0).edit().remove("username").commit();
                        Bundle bundle = new Bundle();
                        bundle.putString(Param.ITEM_ID, "SHOW TUTORIAL");
                        bundle.putString(Param.ITEM_NAME, "SHOW TUTORIAL");
                        bundle.putString(Param.CONTENT_TYPE, "TEXT");
                        NewMainActivity.this.mFirebaseAnalytics.logEvent(Event.SELECT_CONTENT, bundle);
                        NewMainActivity.this.finish();
                        NewMainActivity.this.overridePendingTransition(0, 0);
                        NewMainActivity.this.startActivity(NewMainActivity.this.getIntent());
                        NewMainActivity.this.overridePendingTransition(0, 0);
                    }
                    if (id == com.amusoft.sophiebot.R.id.action_invite) {
                        Intent sendIntent = new Intent();
                        sendIntent.setAction("android.intent.action.SEND");
                        sendIntent.putExtra("android.intent.extra.TEXT", "Hi check out this really cool app from \n https://play.google.com/store/apps/details?id=com.amusoft.sophiebot");
                        sendIntent.setType("text/plain");
                        NewMainActivity.this.startActivity(sendIntent);
                        bundle = new Bundle();
                        bundle.putString(Param.ITEM_ID, "SHARED APP");
                        bundle.putString(Param.ITEM_NAME, "APP SHARED");
                        bundle.putString(Param.CONTENT_TYPE, "LINK");
                        NewMainActivity.this.mFirebaseAnalytics.logEvent(Event.SELECT_CONTENT, bundle);
                    }
                    if (id == com.amusoft.sophiebot.R.id.sound_setting) {
                        NewMainActivity.this.showDialog(0);
                        bundle = new Bundle();
                        bundle.putString(Param.ITEM_ID, "GONE FORUM");
                        bundle.putString(Param.ITEM_NAME, "CLICK ON FORUM");
                        bundle.putString(Param.CONTENT_TYPE, "TEXT");
                        NewMainActivity.this.mFirebaseAnalytics.logEvent(Event.SELECT_CONTENT, bundle);
                    }
                    return false;
                }
            });
        }
    }

    private void setUpActionbar() {
        if (getSupportActionBar() != null) {
            ActionBar bar = getSupportActionBar();
            bar.setTitle(getResources().getString(R.string.app_name));
            bar.setHomeButtonEnabled(false);
            bar.setDisplayShowHomeEnabled(false);
            bar.setDisplayHomeAsUpEnabled(false);
            bar.setDisplayShowTitleEnabled(true);
            bar.setNavigationMode(0);
        }
    }

    protected void onResume() {
        super.onResume();
    }

    protected void onPause() {
        super.onPause();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(com.amusoft.sophiebot.R.menu.main, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == com.amusoft.sophiebot.R.id.action_tutorial) {
            getApplication().getSharedPreferences("SophiePrefs", 0).edit().remove("username").commit();
            Bundle bundle = new Bundle();
            bundle.putString(Param.ITEM_ID, "SHOW TUTORIAL");
            bundle.putString(Param.ITEM_NAME, "SHOW TUTORIAL");
            bundle.putString(Param.CONTENT_TYPE, "TEXT");
            this.mFirebaseAnalytics.logEvent(Event.SELECT_CONTENT, bundle);
            finish();
            overridePendingTransition(0, 0);
            startActivity(getIntent());
            overridePendingTransition(0, 0);
        }
        if (id == com.amusoft.sophiebot.R.id.action_invite) {
            Intent sendIntent = new Intent();
            sendIntent.setAction("android.intent.action.SEND");
            sendIntent.putExtra("android.intent.extra.TEXT", "Hi check out this really cool app from \n https://play.google.com/store/apps/details?id=com.amusoft.sophiebot");
            sendIntent.setType("text/plain");
            startActivity(sendIntent);
            bundle = new Bundle();
            bundle.putString(Param.ITEM_ID, "SHARED APP");
            bundle.putString(Param.ITEM_NAME, "APP SHARED");
            bundle.putString(Param.CONTENT_TYPE, "LINK");
            this.mFirebaseAnalytics.logEvent(Event.SELECT_CONTENT, bundle);
        }
        if (id == com.amusoft.sophiebot.R.id.sound_setting) {
            showDialog(0);
            bundle = new Bundle();
            bundle.putString(Param.ITEM_ID, "GONE FORUM");
            bundle.putString(Param.ITEM_NAME, "CLICK ON FORUM");
            bundle.putString(Param.CONTENT_TYPE, "TEXT");
            this.mFirebaseAnalytics.logEvent(Event.SELECT_CONTENT, bundle);
        }
        return super.onOptionsItemSelected(item);
    }

    private void getOverflowMenu() {
        try {
            ViewConfiguration config = ViewConfiguration.get(this);
            Field menuKeyField = ViewConfiguration.class.getDeclaredField("sHasPermanentMenuKey");
            if (menuKeyField != null) {
                menuKeyField.setAccessible(true);
                menuKeyField.setBoolean(config, false);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case R.styleable.View_android_theme /*0*/:
                View layoutset = ((LayoutInflater) getSystemService("layout_inflater")).inflate(com.amusoft.sophiebot.R.layout.dialog_sounds, (ViewGroup) findViewById(com.amusoft.sophiebot.R.id.dia_sounds));
                final CheckBox g = (CheckBox) layoutset.findViewById(com.amusoft.sophiebot.R.id.checkBoxSounds);
                if ((this.mUltiChatSettings.contains(Constants.PREF_SOUND) & this.mUltiChatSettings.getString(Constants.PREF_SOUND, null).equals("true")) != 0) {
                    g.setChecked(true);
                }
                g.setOnCheckedChangeListener(new OnCheckedChangeListener() {
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        Editor editor22;
                        Bundle bundle;
                        if (isChecked) {
                            editor22 = NewMainActivity.this.mUltiChatSettings.edit();
                            editor22.putString(Constants.PREF_SOUND, "true");
                            editor22.commit();
                            g.setChecked(true);
                            bundle = new Bundle();
                            bundle.putString(Param.ITEM_ID, "ENABLED SOUND");
                            bundle.putString(Param.ITEM_NAME, "ENABLED SOUND");
                            bundle.putString(Param.CONTENT_TYPE, "TEXT");
                            NewMainActivity.this.mFirebaseAnalytics.logEvent(Event.SELECT_CONTENT, bundle);
                        } else if (!isChecked) {
                            editor22 = NewMainActivity.this.mUltiChatSettings.edit();
                            editor22.putString(Constants.PREF_SOUND, "false");
                            editor22.commit();
                            g.setChecked(false);
                            bundle = new Bundle();
                            bundle.putString(Param.ITEM_ID, "DISABLED SOUND");
                            bundle.putString(Param.ITEM_NAME, "DISABLED SOUND");
                            bundle.putString(Param.CONTENT_TYPE, "TEXT");
                            NewMainActivity.this.mFirebaseAnalytics.logEvent(Event.SELECT_CONTENT, bundle);
                        }
                    }
                });
                Builder builder2 = new Builder(this);
                builder2.setView(layoutset);
                builder2.setPositiveButton(17039370, new OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        NewMainActivity.this.removeDialog(0);
                    }
                });
                return builder2.create();
            case SET_FEEDBACK /*1*/:
                View layout = ((LayoutInflater) getSystemService("layout_inflater")).inflate(com.amusoft.sophiebot.R.layout.dialogue_feedback, (ViewGroup) findViewById(com.amusoft.sophiebot.R.id.feedlay));
                final EditText whatLike = (EditText) layout.findViewById(com.amusoft.sophiebot.R.id.editWhatLike);
                final EditText whatDontLike = (EditText) layout.findViewById(com.amusoft.sophiebot.R.id.editWhatDontLike);
                final EditText whatSuggest = (EditText) layout.findViewById(com.amusoft.sophiebot.R.id.editWhatSeeNext);
                ((Button) layout.findViewById(com.amusoft.sophiebot.R.id.btndone)).setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {
                        Map<Object, Object> addFeedback = new HashMap();
                        addFeedback.put("Like", whatLike.getText().toString());
                        addFeedback.put("Don't Like", whatDontLike.getText().toString());
                        addFeedback.put("Suggest", whatSuggest.getText().toString());
                        NewMainActivity.this.mFirebaseRef.push().setValue(addFeedback);
                        Bundle bundle = new Bundle();
                        bundle.putString(Param.ITEM_ID, "GAVE FEEDBACK");
                        bundle.putString(Param.ITEM_NAME, "USER SHARED FEEDBACK");
                        bundle.putString(Param.CONTENT_TYPE, "TEXT");
                        NewMainActivity.this.mFirebaseAnalytics.logEvent(Event.SELECT_CONTENT, bundle);
                        NewMainActivity.this.removeDialog(NewMainActivity.SET_FEEDBACK);
                        NewMainActivity.this.finish();
                        NewMainActivity.this.onStop();
                    }
                });
                Builder builder = new Builder(this);
                builder.setView(layout);
                return builder.create();
            default:
                return null;
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        }
        doThat();
        return true;
    }

    private void doThat() {
        SharedPreferences prefs = getApplication().getSharedPreferences("SophiePrefs", 0);
        int feedbackdue = prefs.getInt("feedbackdue", 0);
        if (feedbackdue == 5) {
            showDialog(SET_FEEDBACK);
            int feedbackdue2 = feedbackdue + SET_FEEDBACK;
            prefs.edit().putInt("feedbackdue", feedbackdue).commit();
            feedbackdue = feedbackdue2;
            return;
        }
        feedbackdue2 = feedbackdue + SET_FEEDBACK;
        prefs.edit().putInt("feedbackdue", feedbackdue).commit();
        finish();
        onStop();
        feedbackdue = feedbackdue2;
    }
}
