package com.amusoft.sophiebot.newexperience;

import android.annotation.TargetApi;
import android.app.FragmentManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.speech.tts.TextToSpeech.OnInitListener;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;
import com.amusoft.sophiebot.BrainLoggerDialog;
import com.amusoft.sophiebot.BrainService;
import com.amusoft.sophiebot.ChatArrayAdapter;
import com.amusoft.sophiebot.ChatBotApplication;
import com.amusoft.sophiebot.ChatMessage;
import com.amusoft.sophiebot.Constants;
import com.amusoft.sophiebot.R;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.analytics.FirebaseAnalytics.Event;
import com.google.firebase.analytics.FirebaseAnalytics.Param;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import de.hdodenhof.circleimageview.BuildConfig;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Random;
import outlander.showcaseview.ShowcaseViewBuilder;

public class MainFragment extends Fragment {
    private static final String ARG_SECTION_NUMBER = "section_number";
    private static final String FRAGMENT_DIALOG_LOG_TAG = "BrainLoggerDialog";
    static final int SET_SOUNDS = 1;
    private static ChatArrayAdapter adapter;
    ArrayList<ChatMessage> allmsgs;
    private EditText chatEditText;
    private ListView chatListView;
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    private BrainLoggerDialog dialog;
    private FirebaseAnalytics mFirebaseAnalytics;
    DatabaseReference mFirebaseRef = this.database.getReference("TO ADD");
    DatabaseReference mFirebaseRef2 = this.database.getReference("ALL ASKED");
    private ResponseReceiver mMessageReceiver;
    SharedPreferences mUltiChatSettings;
    private String question;
    View rootView;
    String strTotutor;
    Toolbar toolbar;
    TextToSpeech tts;

    private class ResponseReceiver extends BroadcastReceiver {
        private ResponseReceiver() {
        }

        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equalsIgnoreCase(Constants.BROADCAST_ACTION_BRAIN_STATUS)) {
                switch (intent.getIntExtra(Constants.EXTRA_BRAIN_STATUS, 0)) {
                    case BuildConfig.VERSION_CODE /*-1*/:
                        Toast.makeText(MainFragment.this.getActivity().getApplicationContext(), "brain loading", 0).show();
                        if (MainFragment.this.dialog != null) {
                            MainFragment.this.dialog.show(MainFragment.this.getActivity().getFragmentManager(), MainFragment.FRAGMENT_DIALOG_LOG_TAG);
                            break;
                        }
                        break;
                    case MainFragment.SET_SOUNDS /*1*/:
                        Toast.makeText(MainFragment.this.getActivity().getApplicationContext(), "brain loaded", 0).show();
                        if (MainFragment.this.dialog != null) {
                            MainFragment.this.dialog.setPositiveButtonEnabled(true);
                            break;
                        }
                        break;
                }
            }
            if (intent.getAction().equalsIgnoreCase(Constants.BROADCAST_ACTION_BRAIN_ANSWER)) {
                final String answer = intent.getStringExtra(Constants.EXTRA_BRAIN_ANSWER);
                MainFragment.adapter.add(new ChatMessage(true, answer));
                MainFragment.adapter.notifyDataSetChanged();
                if (answer.contains("I have no answer for that")) {
                    new HashMap().put("Question", MainFragment.this.question);
                    MainFragment.this.mFirebaseRef.push().setValue(MainFragment.this.question);
                }
                if (answer.contains("Our Forum is the best place to get answers about")) {
                    new HashMap().put("Question", MainFragment.this.question);
                    MainFragment.this.mFirebaseRef.push().setValue(MainFragment.this.question);
                }
                if (MainFragment.this.mUltiChatSettings.getString(Constants.PREF_SOUND, null).equals("true")) {
                    MainFragment.this.tts = new TextToSpeech(MainFragment.this.getActivity().getApplicationContext(), new OnInitListener() {
                        public void onInit(int status) {
                            if (status != -1) {
                                MainFragment.this.tts.setLanguage(Locale.UK);
                                MainFragment.this.tts.speak(answer, 0, null);
                            }
                        }
                    });
                }
            }
            if (intent.getAction().equalsIgnoreCase(Constants.BROADCAST_ACTION_LOGGER)) {
                String info = intent.getStringExtra(Constants.EXTENDED_LOGGER_INFO);
                if (info != null) {
                    Log.i("EXTENDED_LOGGER_INFO", info);
                    if (MainFragment.this.dialog != null) {
                        MainFragment.this.dialog.addLine(info);
                    }
                }
            }
        }
    }

    public static MainFragment newInstance(int sectionNumber) {
        MainFragment fragment = new MainFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    @TargetApi(17)
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.rootView = inflater.inflate(R.layout.activity_main_newexperience, container, false);
        this.mFirebaseAnalytics = FirebaseAnalytics.getInstance(getActivity().getApplicationContext());
        this.mUltiChatSettings = getActivity().getSharedPreferences(Constants.PREF_SOUND, 0);
        FragmentManager fm = getActivity().getFragmentManager();
        this.allmsgs = new ArrayList();
        adapter = new ChatArrayAdapter(getActivity().getApplicationContext(), this.allmsgs);
        this.chatListView = (ListView) this.rootView.findViewById(R.id.chat_listView);
        this.chatListView.setAdapter(adapter);
        this.chatEditText = (EditText) this.rootView.findViewById(R.id.chat_editText);
        this.chatEditText.setOnKeyListener(new OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() != 0 || keyCode != 66) {
                    return false;
                }
                MainFragment.this.question = MainFragment.this.chatEditText.getText().toString();
                MainFragment.adapter.add(new ChatMessage(false, MainFragment.this.question));
                MainFragment.this.chatEditText.setText(outlander.showcaseview.BuildConfig.FLAVOR);
                new HashMap().put("Question", MainFragment.this.question);
                MainFragment.this.mFirebaseRef2.push().setValue(MainFragment.this.question);
                Bundle bundle = new Bundle();
                bundle.putString(Param.ITEM_ID, "ASKED QUESTION");
                bundle.putString(Param.ITEM_NAME, "ASKED QUESTION");
                bundle.putString(Param.CONTENT_TYPE, "TEXT");
                MainFragment.this.mFirebaseAnalytics.logEvent(Event.SELECT_CONTENT, bundle);
                Intent brainIntent = new Intent(MainFragment.this.getContext(), BrainService.class);
                brainIntent.setAction(BrainService.ACTION_QUESTION);
                brainIntent.putExtra(BrainService.EXTRA_QUESTION, MainFragment.this.question);
                MainFragment.this.getActivity().startService(brainIntent);
                return true;
            }
        });
        this.toolbar = (Toolbar) this.rootView.findViewById(R.id.toolbar);
        if (this.toolbar != null) {
            this.toolbar.setTitle("Sophie");
            this.toolbar.setTextAlignment(4);
            this.toolbar.setTitleTextColor(getResources().getColor(R.color.icons));
        }
        getActivity().getWindow().setSoftInputMode(2);
        return this.rootView;
    }

    public void onStart() {
        super.onStart();
        SharedPreferences prefs = getActivity().getApplication().getSharedPreferences("SophiePrefs", 0);
        this.strTotutor = prefs.getString("username", null);
        if (this.strTotutor == null) {
            Random r = new Random();
            this.strTotutor = "tutored";
            prefs.edit().putString("username", this.strTotutor).commit();
            setupTutorial();
        }
    }

    private void setupTutorial() {
        final ShowcaseViewBuilder showcaseViewBuilder = ShowcaseViewBuilder.init(getActivity()).setTargetView(this.chatEditText).setBackgroundOverlayColor(-579808563).setRingColor(-860231785).setRingWidth(TypedValue.applyDimension(SET_SOUNDS, 1.0f, getResources().getDisplayMetrics())).setMarkerDrawable(getResources().getDrawable(R.drawable.arrow_up), 3).addCustomView(R.layout.description_view, 48).addCustomView(R.layout.skip_layout, 48).setCustomViewMargin((int) TypedValue.applyDimension(SET_SOUNDS, 2.0f, getResources().getDisplayMetrics()));
        showcaseViewBuilder.show();
        showcaseViewBuilder.setClickListenerOnView(R.id.btn, new OnClickListener() {
            public void onClick(View v) {
                showcaseViewBuilder.hide();
                MainFragment.this.showOverflow();
            }
        });
    }

    private void showOverflow() {
        final ShowcaseViewBuilder showcaseViewBuilder2 = ShowcaseViewBuilder.init(getActivity()).setTargetView(this.toolbar).setBackgroundOverlayColor(-579808563).setRingColor(-860231785).setRingWidth(TypedValue.applyDimension(SET_SOUNDS, 1.0f, getResources().getDisplayMetrics())).addCustomView(R.layout.description_swipe, 80).addCustomView(R.layout.skip_layout, 80).setCustomViewMargin((int) TypedValue.applyDimension(SET_SOUNDS, 1.0f, getResources().getDisplayMetrics()));
        showcaseViewBuilder2.show();
        showcaseViewBuilder2.setClickListenerOnView(R.id.got_it_btn, new OnClickListener() {
            public void onClick(View view) {
                showcaseViewBuilder2.hide();
            }
        });
    }

    public void onResume() {
        super.onResume();
        IntentFilter intentFilter = new IntentFilter(Constants.BROADCAST_ACTION_BRAIN_STATUS);
        intentFilter.addAction(Constants.BROADCAST_ACTION_BRAIN_ANSWER);
        intentFilter.addAction(Constants.BROADCAST_ACTION_LOGGER);
        this.mMessageReceiver = new ResponseReceiver();
        LocalBroadcastManager.getInstance(getActivity().getApplicationContext()).registerReceiver(this.mMessageReceiver, intentFilter);
        if (this.dialog != null && ChatBotApplication.isBrainLoaded()) {
            this.dialog.loadLog();
            this.dialog.setPositiveButtonEnabled(true);
        }
    }

    public void onPause() {
        LocalBroadcastManager.getInstance(getActivity().getApplicationContext()).unregisterReceiver(this.mMessageReceiver);
        super.onPause();
    }
}
