package com.amusoft.sophiebot;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.List;
import outlander.showcaseview.R;

public class ChatArrayAdapter extends ArrayAdapter<ChatMessage> {
    private static final int LEFT_MESSAGE = 0;
    private static final int RIGHT_MESSAGE = 1;
    private List<ChatMessage> chatMessages = new ArrayList();
    private TextView messageTextView;
    private LinearLayout wrapper;

    public ChatArrayAdapter(Context context, ArrayList<ChatMessage> allmsgs) {
        super(context, 17367043, allmsgs);
        this.chatMessages = allmsgs;
    }

    public int getCount() {
        return this.chatMessages.size();
    }

    public ChatMessage getItem(int index) {
        return (ChatMessage) this.chatMessages.get(index);
    }

    public int getViewTypeCount() {
        return 2;
    }

    public int getItemViewType(int position) {
        return ((ChatMessage) this.chatMessages.get(position)).left ? LEFT_MESSAGE : RIGHT_MESSAGE;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        ChatMessage comment = getItem(position);
        int type = getItemViewType(position);
        if (row == null) {
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService("layout_inflater");
            if (type == 0) {
                row = inflater.inflate(R.layout.chat_listitem_left, parent, false);
            }
            if (type == RIGHT_MESSAGE) {
                row = inflater.inflate(R.layout.chat_listitem_right, parent, false);
            }
        }
        this.wrapper = (LinearLayout) row.findViewById(R.id.wrapper);
        this.messageTextView = (TextView) row.findViewById(R.id.text);
        this.messageTextView.setText(comment.text);
        return row;
    }

    public Bitmap decodeToBitmap(byte[] decodedByte) {
        return BitmapFactory.decodeByteArray(decodedByte, LEFT_MESSAGE, decodedByte.length);
    }
}
