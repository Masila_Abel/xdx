package com.amusoft.sophiebot;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.FragmentManager;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.widget.ScrollView;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.Iterator;
import outlander.showcaseview.BuildConfig;

public class BrainLoggerDialog extends DialogFragment {
    private ScrollView myScrollView;
    private boolean shown = false;
    private TextView tv;

    public Dialog onCreateDialog(Bundle savedInstanceState) {
        this.myScrollView = (ScrollView) getActivity().getLayoutInflater().inflate(R.layout.dialog_brain_log, null, false);
        this.tv = (TextView) this.myScrollView.findViewById(R.id.textViewWithScroll);
        if (savedInstanceState != null) {
            String text = savedInstanceState.getString("log");
            TextView textView = this.tv;
            if (text == null) {
                text = BuildConfig.FLAVOR;
            }
            textView.setText(text);
        } else {
            loadLog();
        }
        Builder builder = new Builder(getActivity()).setView(this.myScrollView).setTitle("Sophie").setIcon(R.drawable.ic_bot).setPositiveButton("OK", new OnClickListener() {
            @TargetApi(11)
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        setCancelable(false);
        return builder.create();
    }

    public void onStart() {
        super.onStart();
        if (getDialog() != null) {
            getDialog().getWindow().setLayout(-1, (int) getResources().getDimension(R.dimen.dialog_logger_height));
            ((AlertDialog) getDialog()).getButton(-1).setEnabled(false);
        }
    }

    public void onSaveInstanceState(Bundle outState) {
        outState.putString("log", this.tv.getText().toString());
        super.onSaveInstanceState(outState);
    }

    public void onDestroyView() {
        if (getDialog() != null && getRetainInstance()) {
            getDialog().setDismissMessage(null);
        }
        super.onDestroyView();
    }

    public void show(FragmentManager manager, String tag) {
        if (!this.shown) {
            super.show(manager, tag);
            this.shown = true;
        }
    }

    public void onDismiss(DialogInterface dialog) {
        this.shown = false;
        super.onDismiss(dialog);
    }

    public void clear() {
        if (this.tv != null) {
            this.tv.setText(BuildConfig.FLAVOR);
        }
    }

    public void addLine(String text) {
        if (this.tv != null) {
            this.tv.append(text + "\n");
            this.myScrollView.fullScroll(130);
        }
    }

    public void setPositiveButtonEnabled(boolean enabled) {
        AlertDialog dialog = (AlertDialog) getDialog();
        if (dialog != null) {
            dialog.getButton(-1).setEnabled(enabled);
        }
    }

    public void loadLog() {
        clear();
        Iterator it = ((ArrayList) BrainLogger.getInstance().getLogs().clone()).iterator();
        while (it.hasNext()) {
            addLine((String) it.next());
        }
    }
}
