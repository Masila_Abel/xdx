package com.amusoft.sophiebot;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import java.util.Random;

public class Splash extends ActionBarActivity {
    SharedPreferences mUltiChatSettings;
    private String mUsername;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        this.mUltiChatSettings = getSharedPreferences(Constants.PREF_SOUND, 0);
        if (getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }
        new Handler().postDelayed(new Runnable() {
            public void run() {
                Editor editor22 = Splash.this.mUltiChatSettings.edit();
                editor22.putString(Constants.PREF_SOUND, "false");
                editor22.commit();
                SharedPreferences prefs = Splash.this.getApplication().getSharedPreferences("SophiePrefs", 0);
                Splash.this.mUsername = prefs.getString("username", null);
                if (Splash.this.mUsername == null) {
                    Splash.this.mUsername = "Client" + new Random().nextInt(100000);
                    prefs.edit().putString("username", Splash.this.mUsername).commit();
                }
                Splash.this.startActivity(new Intent(Splash.this.getBaseContext(), MainActivity.class));
                Splash.this.finish();
            }
        }, 3000);
    }

    private void setupUsername() {
        SharedPreferences prefs = getApplication().getSharedPreferences("SophiePrefs", 0);
        this.mUsername = prefs.getString("username", null);
        if (this.mUsername == null) {
            this.mUsername = "Client" + new Random().nextInt(100000);
            prefs.edit().putString("username", this.mUsername).commit();
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_splash, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        return super.onOptionsItemSelected(item);
    }
}
