package com.amusoft.sophiebot;

import android.app.Application;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import com.amusoft.sophiebot.BrainService.LocalBinder;
import com.google.firebase.FirebaseApp;
import com.google.firebase.database.FirebaseDatabase;

public class ChatBotApplication extends Application {
    private static boolean mBound = false;
    private static BrainService mService;
    private ServiceConnection mConnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName className, IBinder service) {
            try {
                ChatBotApplication.mService = ((LocalBinder) service).getService();
                ChatBotApplication.mBound = true;
            } catch (Exception e) {
            }
        }

        public void onServiceDisconnected(ComponentName arg0) {
            ChatBotApplication.mBound = false;
        }
    };

    public void onCreate() {
        super.onCreate();
        if (!FirebaseApp.getApps(this).isEmpty()) {
            FirebaseDatabase.getInstance().setPersistenceEnabled(true);
        }
        BrainLogger.setup(this);
        Intent brainIntent = new Intent(this, BrainService.class);
        brainIntent.setAction(BrainService.ACTION_START);
        startService(brainIntent);
        bindService(brainIntent, this.mConnection, 1);
    }

    public void onTerminate() {
        super.onTerminate();
        if (mBound) {
            unbindService(this.mConnection);
            mBound = false;
        }
    }

    public static boolean isBrainLoaded() {
        if (mBound) {
            return mService.isBrainLoaded();
        }
        return false;
    }
}
