package com.amusoft.sophiebot;

public final class Constants {
    public static final String BROADCAST_ACTION_BRAIN_ANSWER = "com.amusoft.sophie.BROADCAST_ACTION_BRAIN_ANSWER";
    public static final String BROADCAST_ACTION_BRAIN_STATUS = "com.amusoft.sophie.BROADCAST_ACTION_BRAIN_STATUS";
    public static final String BROADCAST_ACTION_LOGGER = "com.amusoft.sophie.BROADCAST_ACTION_LOGGER";
    public static final String EXTENDED_LOGGER_INFO = "icom.amusoft.sophie.LOGGER_INFO";
    public static final String EXTRA_BRAIN_ANSWER = "com.amusoft.sophie.EXTRA_BRAIN_ANSWER";
    public static final String EXTRA_BRAIN_STATUS = "com.amusoft.sophie.BRAIN_STATUS";
    public static final String PREFERENCES = "svysv";
    public static final String PREF_SOUND = "svysv";
    public static final int STATUS_BRAIN_LOADED = 1;
    public static final int STATUS_BRAIN_LOADING = -1;
}
