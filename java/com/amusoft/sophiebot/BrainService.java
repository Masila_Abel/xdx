package com.amusoft.sophiebot;

import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat.Builder;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import com.amusoft.sophiebot.alice.Alice;
import outlander.showcaseview.BuildConfig;

public class BrainService extends Service {
    public static final String ACTION_QUESTION = "BrainService.ACTION_QUESTION";
    public static final String ACTION_START = "BrainService.ACTION_START";
    public static final String ACTION_STOP = "BrainService.ACTION_STOP";
    public static final String EXTRA_QUESTION = "BrainService.EXTRA_QUESTION";
    private static final int NOTIFICATION_ID = 1337;
    private static Alice alice;
    private final IBinder mBinder = new LocalBinder();

    private final class LoadBrainThread extends Thread {
        private LoadBrainThread() {
        }

        public void run() {
            Log.d("BrainService", "LoadBrainThread run()");
            LocalBroadcastManager.getInstance(BrainService.this).sendBroadcast(new Intent(Constants.BROADCAST_ACTION_BRAIN_STATUS).putExtra(Constants.EXTRA_BRAIN_STATUS, -1));
            BrainService.this.showLoadingNotification();
            BrainService.alice = Alice.setup(BrainService.this);
            LocalBroadcastManager.getInstance(BrainService.this).sendBroadcast(new Intent(Constants.BROADCAST_ACTION_BRAIN_STATUS).putExtra(Constants.EXTRA_BRAIN_STATUS, 1));
            BrainService.this.showLoadedNotification();
        }
    }

    public class LocalBinder extends Binder {
        BrainService getService() {
            return BrainService.this;
        }
    }

    public void onCreate() {
        super.onCreate();
        Log.d("BrainService", "onCreate()");
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent == null) {
            Log.d("BrainService", "onStartCommand() null");
            return 1;
        }
        String action = intent.getAction();
        if (action.equalsIgnoreCase(ACTION_QUESTION)) {
            String question = intent.getStringExtra(EXTRA_QUESTION);
            if (question == null) {
                return 1;
            }
            Log.d("BrainService", "onStartCommand() question:" + question);
            String answer = BuildConfig.FLAVOR;
            if (alice != null) {
                answer = alice.processInput(question);
            } else {
                answer = "My brain has not been loaded yet.";
            }
            LocalBroadcastManager.getInstance(this).sendBroadcast(new Intent(Constants.BROADCAST_ACTION_BRAIN_ANSWER).putExtra(Constants.EXTRA_BRAIN_ANSWER, answer));
            return 1;
        } else if (action.equalsIgnoreCase(ACTION_STOP)) {
            Log.d("BrainService", "onStartCommand() ACTION_STOP");
            stopForeground(true);
            stopSelf();
            return 2;
        } else if (!action.equalsIgnoreCase(ACTION_START) || alice != null) {
            return 1;
        } else {
            Log.d("BrainService", "onStartCommand() ACTION_START");
            new LoadBrainThread().start();
            return 1;
        }
    }

    public IBinder onBind(Intent intent) {
        return this.mBinder;
    }

    public void onDestroy() {
        super.onDestroy();
        Log.d("BrainService", "onDestroy()");
    }

    public boolean isBrainLoaded() {
        return alice != null;
    }

    private void showLoadedNotification() {
        Intent openIntent = new Intent(this, MainActivity.class);
        openIntent.setFlags(603979776);
        PendingIntent openPIntent = PendingIntent.getActivity(this, 0, openIntent, 134217728);
        Intent stopIntent = new Intent(this, BrainService.class);
        stopIntent.setAction(ACTION_STOP);
        PendingIntent stopPIntent = PendingIntent.getService(this, 0, stopIntent, 134217728);
        Intent restartIntent = new Intent(this, BrainService.class);
        restartIntent.setAction(ACTION_START);
        PendingIntent restartPIntent = PendingIntent.getService(this, 0, restartIntent, 134217728);
        startForeground(NOTIFICATION_ID, new Builder(this).setSmallIcon(R.drawable.ic_stat_notify_chat).setTicker("Brain Loaded").setContentTitle("Sophie brain").setContentText("Ask me anything").setAutoCancel(false).setContentIntent(openPIntent).setWhen(System.currentTimeMillis()).addAction(R.drawable.ic_stat_stop, "Stop", stopPIntent).build());
    }

    private void showLoadingNotification() {
        Intent openIntent = new Intent(this, MainActivity.class);
        openIntent.setFlags(603979776);
        PendingIntent openPIntent = PendingIntent.getActivity(this, 0, openIntent, 0);
        Intent stopIntent = new Intent(this, BrainService.class);
        stopIntent.setAction(ACTION_STOP);
        startForeground(NOTIFICATION_ID, new Builder(this).setSmallIcon(R.drawable.ic_stat_loading).setTicker("Loading Brain").setContentTitle("Sophie").setContentText("Loading...").setAutoCancel(false).setContentIntent(openPIntent).setWhen(System.currentTimeMillis()).setProgress(0, 0, true).addAction(R.drawable.ic_stat_stop, "Stop", PendingIntent.getService(this, 0, stopIntent, 134217728)).build());
    }
}
