package com.amusoft.sophiebot;

import android.app.IntentService;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import com.amusoft.sophiebot.alice.Alice;

public class LoadBrainService extends IntentService {
    public LoadBrainService() {
        super("LoadBrainService");
    }

    protected void onHandleIntent(Intent workIntent) {
        Alice.setup(this);
        LocalBroadcastManager.getInstance(this).sendBroadcast(new Intent(Constants.BROADCAST_ACTION_BRAIN_STATUS).putExtra(Constants.EXTRA_BRAIN_STATUS, 1));
    }
}
