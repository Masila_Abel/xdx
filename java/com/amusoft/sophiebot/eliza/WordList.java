package com.amusoft.sophiebot.eliza;

import java.util.Vector;

public class WordList extends Vector {
    public void add(String word) {
        addElement(word);
    }

    public void print(int indent) {
        for (int i = 0; i < size(); i++) {
            System.out.print(((String) elementAt(i)) + "  ");
        }
        System.out.println();
    }

    boolean find(String s) {
        for (int i = 0; i < size(); i++) {
            if (s.equals(elementAt(i))) {
                return true;
            }
        }
        return false;
    }
}
