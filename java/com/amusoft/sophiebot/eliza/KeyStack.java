package com.amusoft.sophiebot.eliza;

public class KeyStack {
    Key[] keyStack = new Key[20];
    int keyTop = 0;
    final int stackSize = 20;

    public void print() {
        System.out.println("Key stack " + this.keyTop);
        for (int i = 0; i < this.keyTop; i++) {
            this.keyStack[i].printKey(0);
        }
    }

    public int keyTop() {
        return this.keyTop;
    }

    public void reset() {
        this.keyTop = 0;
    }

    public Key key(int n) {
        if (n < 0 || n >= this.keyTop) {
            return null;
        }
        return this.keyStack[n];
    }

    public void pushKey(Key key) {
        if (key == null) {
            System.out.println("push null key");
            return;
        }
        int i = this.keyTop;
        while (i > 0 && key.rank > this.keyStack[i - 1].rank) {
            this.keyStack[i] = this.keyStack[i - 1];
            i--;
        }
        this.keyStack[i] = key;
        this.keyTop++;
    }
}
