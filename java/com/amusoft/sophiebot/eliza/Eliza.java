package com.amusoft.sophiebot.eliza;

import android.content.Context;
import android.util.Log;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import outlander.showcaseview.BuildConfig;

public class Eliza {
    static final int failure = 1;
    static final int gotoRule = 2;
    static final int success = 0;
    final boolean echoInput = false;
    boolean finished = false;
    String finl = "Goodbye.";
    String initial = "Hello.";
    KeyStack keyStack = new KeyStack();
    KeyList keys = new KeyList();
    DecompList lastDecomp;
    ReasembList lastReasemb;
    Mem mem = new Mem();
    Context parent;
    PrePostList post = new PrePostList();
    PrePostList pre = new PrePostList();
    final boolean printData = false;
    final boolean printInitialFinal = false;
    final boolean printKeys = false;
    final boolean printPrePost = false;
    final boolean printSyns = false;
    WordList quit = new WordList();
    SynList syns = new SynList();

    public Eliza(Context parent) {
        this.parent = parent;
        readDefaultScript();
    }

    public void dispose() {
    }

    public boolean finished() {
        return this.finished;
    }

    public void collect(String s) {
        String[] lines = new String[4];
        if (EString.match(s, "*reasmb: *", lines)) {
            if (this.lastReasemb == null) {
                Log.e("Eliza", "Error: no last reasemb");
            } else {
                this.lastReasemb.add(lines[failure]);
            }
        } else if (EString.match(s, "*decomp: *", lines)) {
            if (this.lastDecomp == null) {
                Log.e("Eliza", "Error: no last decomp");
                return;
            }
            this.lastReasemb = new ReasembList();
            String temp = new String(lines[failure]);
            if (EString.match(temp, "$ *", lines)) {
                this.lastDecomp.add(lines[0], true, this.lastReasemb);
            } else {
                this.lastDecomp.add(temp, false, this.lastReasemb);
            }
        } else if (EString.match(s, "*key: * #*", lines)) {
            this.lastDecomp = new DecompList();
            this.lastReasemb = null;
            int n = 0;
            if (lines[gotoRule].length() != 0) {
                try {
                    n = Integer.parseInt(lines[gotoRule]);
                } catch (NumberFormatException e) {
                    Log.e("Eliza", "Number is wrong in key: " + lines[gotoRule]);
                }
            }
            this.keys.add(lines[failure], n, this.lastDecomp);
        } else if (EString.match(s, "*key: *", lines)) {
            this.lastDecomp = new DecompList();
            this.lastReasemb = null;
            this.keys.add(lines[failure], 0, this.lastDecomp);
        } else if (EString.match(s, "*synon: * *", lines)) {
            WordList words = new WordList();
            words.add(lines[failure]);
            s = lines[gotoRule];
            while (EString.match(s, "* *", lines)) {
                words.add(lines[0]);
                s = lines[failure];
            }
            words.add(s);
            this.syns.add(words);
        } else if (EString.match(s, "*pre: * *", lines)) {
            this.pre.add(lines[failure], lines[gotoRule]);
        } else if (EString.match(s, "*post: * *", lines)) {
            this.post.add(lines[failure], lines[gotoRule]);
        } else if (EString.match(s, "*initial: *", lines)) {
            this.initial = lines[failure];
        } else if (EString.match(s, "*final: *", lines)) {
            this.finl = lines[failure];
        } else if (EString.match(s, "*quit: *", lines)) {
            this.quit.add(" " + lines[failure] + " ");
        } else {
            Log.e("Eliza", "Unrecognized input: " + s);
        }
    }

    public void print() {
    }

    public String processInput(String s) {
        s = EString.compress(EString.translate(EString.translate(EString.translate(s, "ABCDEFGHIJKLMNOPQRSTUVWXYZ", "abcdefghijklmnopqrstuvwxyz"), "@#$%^&*()_-+=~`{[}]|:;<>\\\"", "                          "), ",?!", "..."));
        String[] lines = new String[gotoRule];
        while (EString.match(s, "*.*", lines)) {
            String reply = sentence(lines[0]);
            if (reply != null) {
                return reply;
            }
            s = EString.trim(lines[failure]);
        }
        if (s.length() != 0) {
            reply = sentence(s);
            if (reply != null) {
                return reply;
            }
        }
        String m = this.mem.get();
        if (m != null) {
            return m;
        }
        Key key = this.keys.getKey("xnone");
        if (key != null) {
            reply = decompose(key, s, null);
            if (reply != null) {
                return reply;
            }
        }
        return "I am at a loss for words.";
    }

    public boolean readDefaultScript() {
        clearScript();
        return readScript("eliza.script");
    }

    public boolean readScript(String script) {
        clearScript();
        String[] lines = loadStrings(script);
        if (lines == null || lines.length == 0) {
            Log.e("Eliza", "Cannot load Eliza script!");
            return false;
        }
        for (int i = 0; i < lines.length; i += failure) {
            Log.i("Eliza", "lines[" + i + "]: " + lines[i]);
            collect(lines[i]);
        }
        return true;
    }

    private String[] loadStrings(String script) {
        ArrayList<String> out_list = new ArrayList();
        try {
            this.parent.getAssets().openFd(script);
            InputStream is = this.parent.getAssets().open(script);
            if (is != null) {
                BufferedReader buffreader = new BufferedReader(new InputStreamReader(is));
                String line;
                do {
                    line = buffreader.readLine();
                    if (line != null) {
                        out_list.add(line);
                        continue;
                    }
                } while (line != null);
            }
        } catch (IOException e) {
            Log.e("Eliza", "IOException: " + e);
        }
        return (String[]) out_list.toArray(new String[out_list.size()]);
    }

    void clearScript() {
        this.keys.clear();
        this.syns.clear();
        this.pre.clear();
        this.post.clear();
        this.initial = BuildConfig.FLAVOR;
        this.finl = BuildConfig.FLAVOR;
        this.quit.clear();
        this.keyStack.reset();
    }

    String sentence(String s) {
        s = EString.pad(this.pre.translate(s));
        if (this.quit.find(s)) {
            this.finished = true;
            return this.finl;
        }
        this.keys.buildKeyStack(this.keyStack, s);
        for (int i = 0; i < this.keyStack.keyTop(); i += failure) {
            Key gotoKey = new Key();
            String reply = decompose(this.keyStack.key(i), s, gotoKey);
            if (reply != null) {
                return reply;
            }
            while (gotoKey.key() != null) {
                reply = decompose(gotoKey, s, gotoKey);
                if (reply != null) {
                    return reply;
                }
            }
        }
        return null;
    }

    String decompose(Key key, String s, Key gotoKey) {
        String[] reply = new String[10];
        for (int i = 0; i < key.decomp().size(); i += failure) {
            Decomp d = (Decomp) key.decomp().elementAt(i);
            if (this.syns.matchDecomp(s, d.pattern(), reply)) {
                String rep = assemble(d, reply, gotoKey);
                if (rep != null) {
                    return rep;
                }
                if (gotoKey.key() != null) {
                    return null;
                }
            }
        }
        return null;
    }

    String assemble(Decomp d, String[] reply, Key gotoKey) {
        String[] lines = new String[3];
        d.stepRule();
        String rule = d.nextRule();
        if (EString.match(rule, "goto *", lines)) {
            gotoKey.copy(this.keys.getKey(lines[0]));
            if (gotoKey.key() != null) {
                return null;
            }
            Log.w("Eliza", "Goto rule did not match key: " + lines[0]);
            return null;
        }
        String work = BuildConfig.FLAVOR;
        while (EString.match(rule, "* (#)*", lines)) {
            rule = lines[gotoRule];
            int n = 0;
            try {
                n = Integer.parseInt(lines[failure]) - 1;
            } catch (NumberFormatException e) {
                Log.w("Eliza", "Number is wrong in reassembly rule " + lines[failure]);
            }
            if (n < 0 || n >= reply.length) {
                Log.w("Eliza", "Substitution number is bad " + lines[failure]);
                return null;
            }
            reply[n] = this.post.translate(reply[n]);
            work = work + lines[0] + " " + reply[n];
        }
        work = work + rule;
        if (!d.mem()) {
            return work;
        }
        this.mem.save(work);
        return null;
    }
}
