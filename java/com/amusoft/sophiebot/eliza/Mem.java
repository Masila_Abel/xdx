package com.amusoft.sophiebot.eliza;

public class Mem {
    final int memMax = 20;
    int memTop = 0;
    String[] memory = new String[20];

    public void save(String str) {
        if (this.memTop < 20) {
            String[] strArr = this.memory;
            int i = this.memTop;
            this.memTop = i + 1;
            strArr[i] = new String(str);
        }
    }

    public String get() {
        if (this.memTop == 0) {
            return null;
        }
        String m = this.memory[0];
        for (int i = 0; i < this.memTop - 1; i++) {
            this.memory[i] = this.memory[i + 1];
        }
        this.memTop--;
        return m;
    }
}
