package com.amusoft.sophiebot.alice;

import java.util.Set;
import org.w3c.dom.Node;

public interface AIMLProcessorExtension {
    Set<String> extensionTagSet();

    String recursEval(Node node, ParseState parseState);
}
