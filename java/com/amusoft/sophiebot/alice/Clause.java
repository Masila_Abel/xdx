package com.amusoft.sophiebot.alice;

public class Clause {
    public Boolean affirm;
    public String obj;
    public String pred;
    public String subj;

    public Clause(String s, String p, String o) {
        this(s, p, o, Boolean.valueOf(true));
    }

    public Clause(String s, String p, String o, Boolean affirm) {
        this.subj = s;
        this.pred = p;
        this.obj = o;
        this.affirm = affirm;
    }

    public Clause(Clause clause) {
        this(clause.subj, clause.pred, clause.obj, clause.affirm);
    }
}
