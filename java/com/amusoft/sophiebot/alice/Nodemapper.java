package com.amusoft.sophiebot.alice;

import java.util.ArrayList;
import java.util.HashMap;

public class Nodemapper {
    public Category category = null;
    public int height = MagicNumbers.max_graph_height;
    public String key = null;
    public HashMap<String, Nodemapper> map = null;
    public ArrayList<String> sets;
    public boolean shortCut = false;
    public StarBindings starBindings = null;
    public Nodemapper value = null;
}
