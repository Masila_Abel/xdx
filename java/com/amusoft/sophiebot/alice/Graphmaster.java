package com.amusoft.sophiebot.alice;

import com.amusoft.sophiebot.BrainLogger;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import outlander.showcaseview.BuildConfig;

public class Graphmaster {
    private static boolean DEBUG = false;
    public static boolean enableShortCuts = false;
    public static boolean verbose = false;
    public Bot bot;
    Pattern botPropPattern;
    String botPropRegex;
    public int categoryCnt;
    int leafCnt;
    public int matchCount;
    public String name;
    int naryCnt;
    int nodeCnt;
    long nodeSize;
    public String resultNote;
    public final Nodemapper root;
    int shortCutCnt;
    int singletonCnt;
    public int upgradeCnt;
    public HashSet<String> vocabulary;

    public Graphmaster(Bot bot) {
        this(bot, "brain");
    }

    public Graphmaster(Bot bot, String name) {
        this.matchCount = 0;
        this.upgradeCnt = 0;
        this.resultNote = BuildConfig.FLAVOR;
        this.categoryCnt = 0;
        this.botPropRegex = "<bot name=\"(.*?)\"/>";
        this.botPropPattern = Pattern.compile(this.botPropRegex, 2);
        this.root = new Nodemapper();
        this.bot = bot;
        this.name = name;
        this.vocabulary = new HashSet();
    }

    public static String inputThatTopic(String input, String that, String topic) {
        return input.trim() + " <THAT> " + that.trim() + " <TOPIC> " + topic.trim();
    }

    public String replaceBotProperties(String pattern) {
        if (pattern.contains("<B")) {
            Matcher matcher = this.botPropPattern.matcher(pattern);
            while (matcher.find()) {
                pattern = pattern.replaceFirst("(?i)" + this.botPropRegex, this.bot.properties.get(matcher.group(1).toLowerCase()).toUpperCase());
            }
        }
        return pattern;
    }

    public void addCategory(Category category) {
        addPath(Path.sentenceToPath(replaceBotProperties(inputThatTopic(category.getPattern(), category.getThat(), category.getTopic()))), category);
        this.categoryCnt++;
    }

    boolean thatStarTopicStar(Path path) {
        return Path.pathToSentence(path).trim().equals("<THAT> * <TOPIC> *");
    }

    void addSets(String type, Bot bot, Nodemapper node, String filename) {
        String setName = Utilities.tagTrim(type, "SET").toLowerCase();
        if (bot.setMap.containsKey(setName)) {
            if (node.sets == null) {
                node.sets = new ArrayList();
            }
            if (!node.sets.contains(setName)) {
                node.sets.add(setName);
                return;
            }
            return;
        }
        System.out.println("No AIML Set found for <set>" + setName + "</set> in " + bot.name + " " + filename);
    }

    void addPath(Path path, Category category) {
        addPath(this.root, path, category);
    }

    void addPath(Nodemapper node, Path path, Category category) {
        if (path == null) {
            node.category = category;
            node.height = 0;
        } else if (enableShortCuts && thatStarTopicStar(path)) {
            node.category = category;
            node.height = Math.min(4, node.height);
            node.shortCut = true;
        } else if (NodemapperOperator.containsKey(node, path.word)) {
            if (path.word.startsWith("<SET>")) {
                addSets(path.word, this.bot, node, category.getFilename());
            }
            nextNode = NodemapperOperator.get(node, path.word);
            addPath(nextNode, path.next, category);
            offset = 1;
            if (path.word.equals("#") || path.word.equals("^")) {
                offset = 0;
            }
            node.height = Math.min(nextNode.height + offset, node.height);
        } else {
            nextNode = new Nodemapper();
            if (path.word.startsWith("<SET>")) {
                addSets(path.word, this.bot, node, category.getFilename());
            }
            if (node.key != null) {
                NodemapperOperator.upgrade(node);
                this.upgradeCnt++;
            }
            NodemapperOperator.put(node, path.word, nextNode);
            addPath(nextNode, path.next, category);
            offset = 1;
            if (path.word.equals("#") || path.word.equals("^")) {
                offset = 0;
            }
            node.height = Math.min(nextNode.height + offset, node.height);
        }
    }

    public boolean existsCategory(Category c) {
        return findNode(c) != null;
    }

    public Nodemapper findNode(Category c) {
        return findNode(c.getPattern(), c.getThat(), c.getTopic());
    }

    public Nodemapper findNode(String input, String that, String topic) {
        Nodemapper result = findNode(this.root, Path.sentenceToPath(inputThatTopic(input, that, topic)));
        if (verbose) {
            System.out.println("findNode " + inputThatTopic(input, that, topic) + " " + result);
        }
        return result;
    }

    Nodemapper findNode(Nodemapper node, Path path) {
        if (path != null || node == null) {
            if (Path.pathToSentence(path).trim().equals("<THAT> * <TOPIC> *") && node.shortCut && path.word.equals("<THAT>")) {
                if (!verbose) {
                    return node;
                }
                System.out.println("findNode: shortcut, returning " + node.category.inputThatTopic());
                return node;
            } else if (NodemapperOperator.containsKey(node, path.word)) {
                if (verbose) {
                    System.out.println("findNode: node contains " + path.word);
                }
                return findNode(NodemapperOperator.get(node, path.word.toUpperCase()), path.next);
            } else {
                if (verbose) {
                    System.out.println("findNode: returning null");
                }
                return null;
            }
        } else if (!verbose) {
            return node;
        } else {
            System.out.println("findNode: path is null, returning node " + node.category.inputThatTopic());
            return node;
        }
    }

    public final Nodemapper match(String input, String that, String topic) {
        Nodemapper n;
        try {
            String inputThatTopic = inputThatTopic(input, that, topic);
            n = match(Path.sentenceToPath(inputThatTopic), inputThatTopic);
            if (MagicBooleans.trace_mode) {
                if (n != null) {
                    if (MagicBooleans.trace_mode) {
                        System.out.println("Matched: " + n.category.inputThatTopic() + " " + n.category.getFilename());
                    }
                } else if (MagicBooleans.trace_mode) {
                    System.out.println("No match.");
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            n = null;
        }
        if (MagicBooleans.trace_mode && Chat.matchTrace.length() < MagicNumbers.max_trace_length && n != null) {
            Chat.setMatchTrace(Chat.matchTrace + n.category.inputThatTopic() + "\n");
        }
        return n;
    }

    final Nodemapper match(Path path, String inputThatTopic) {
        try {
            String[] inputStars = new String[MagicNumbers.max_stars];
            String[] thatStars = new String[MagicNumbers.max_stars];
            String[] topicStars = new String[MagicNumbers.max_stars];
            String matchTrace = BuildConfig.FLAVOR;
            Path path2 = path;
            Nodemapper n = match(path2, this.root, inputThatTopic, "inputStar", 0, inputStars, thatStars, topicStars, matchTrace);
            if (n != null) {
                StarBindings sb = new StarBindings();
                int i = 0;
                while (inputStars[i] != null && i < MagicNumbers.max_stars) {
                    sb.inputStars.add(inputStars[i]);
                    i++;
                }
                i = 0;
                while (thatStars[i] != null && i < MagicNumbers.max_stars) {
                    sb.thatStars.add(thatStars[i]);
                    i++;
                }
                i = 0;
                while (topicStars[i] != null && i < MagicNumbers.max_stars) {
                    sb.topicStars.add(topicStars[i]);
                    i++;
                }
                n.starBindings = sb;
            }
            if (n == null) {
                return n;
            }
            n.category.addMatch(inputThatTopic, this.bot);
            return n;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    final Nodemapper match(Path path, Nodemapper node, String inputThatTopic, String starState, int starIndex, String[] inputStars, String[] thatStars, String[] topicStars, String matchTrace) {
        this.matchCount++;
        Nodemapper matchedNode = nullMatch(path, node, matchTrace);
        if (matchedNode != null) {
            return matchedNode;
        }
        if (path.length < node.height) {
            return null;
        }
        matchedNode = dollarMatch(path, node, inputThatTopic, starState, starIndex, inputStars, thatStars, topicStars, matchTrace);
        if (matchedNode != null) {
            return matchedNode;
        }
        matchedNode = sharpMatch(path, node, inputThatTopic, starState, starIndex, inputStars, thatStars, topicStars, matchTrace);
        if (matchedNode != null) {
            return matchedNode;
        }
        matchedNode = underMatch(path, node, inputThatTopic, starState, starIndex, inputStars, thatStars, topicStars, matchTrace);
        if (matchedNode != null) {
            return matchedNode;
        }
        matchedNode = wordMatch(path, node, inputThatTopic, starState, starIndex, inputStars, thatStars, topicStars, matchTrace);
        if (matchedNode != null) {
            return matchedNode;
        }
        matchedNode = setMatch(path, node, inputThatTopic, starState, starIndex, inputStars, thatStars, topicStars, matchTrace);
        if (matchedNode != null) {
            return matchedNode;
        }
        matchedNode = shortCutMatch(path, node, inputThatTopic, starState, starIndex, inputStars, thatStars, topicStars, matchTrace);
        if (matchedNode != null) {
            return matchedNode;
        }
        matchedNode = caretMatch(path, node, inputThatTopic, starState, starIndex, inputStars, thatStars, topicStars, matchTrace);
        if (matchedNode != null) {
            return matchedNode;
        }
        matchedNode = starMatch(path, node, inputThatTopic, starState, starIndex, inputStars, thatStars, topicStars, matchTrace);
        if (matchedNode != null) {
            return matchedNode;
        }
        return null;
    }

    void fail(String mode, String trace) {
    }

    final Nodemapper nullMatch(Path path, Nodemapper node, String matchTrace) {
        if (path == null && node != null && NodemapperOperator.isLeaf(node) && node.category != null) {
            return node;
        }
        fail("null", matchTrace);
        return null;
    }

    final Nodemapper shortCutMatch(Path path, Nodemapper node, String inputThatTopic, String starState, int starIndex, String[] inputStars, String[] thatStars, String[] topicStars, String matchTrace) {
        if (node == null || !node.shortCut || !path.word.equals("<THAT>") || node.category == null) {
            fail("shortCut", matchTrace);
            return null;
        }
        String tail = Path.pathToSentence(path).trim();
        String that = tail.substring(tail.indexOf("<THAT>") + "<THAT>".length(), tail.indexOf("<TOPIC>")).trim();
        String topic = tail.substring(tail.indexOf("<TOPIC>") + "<TOPIC>".length(), tail.length()).trim();
        thatStars[0] = that;
        topicStars[0] = topic;
        return node;
    }

    final Nodemapper wordMatch(Path path, Nodemapper node, String inputThatTopic, String starState, int starIndex, String[] inputStars, String[] thatStars, String[] topicStars, String matchTrace) {
        try {
            String uword = path.word.toUpperCase();
            if (uword.equals("<THAT>")) {
                starIndex = 0;
                starState = "thatStar";
            } else if (uword.equals("<TOPIC>")) {
                starIndex = 0;
                starState = "topicStar";
            }
            matchTrace = matchTrace + "[" + uword + "," + uword + "]";
            if (path != null && NodemapperOperator.containsKey(node, uword)) {
                Nodemapper matchedNode = match(path.next, NodemapperOperator.get(node, uword), inputThatTopic, starState, starIndex, inputStars, thatStars, topicStars, matchTrace);
                if (matchedNode != null) {
                    return matchedNode;
                }
            }
            fail("word", matchTrace);
            return null;
        } catch (Exception ex) {
            System.out.println("wordMatch: " + Path.pathToSentence(path) + ": " + ex);
            ex.printStackTrace();
            return null;
        }
    }

    final Nodemapper dollarMatch(Path path, Nodemapper node, String inputThatTopic, String starState, int starIndex, String[] inputStars, String[] thatStars, String[] topicStars, String matchTrace) {
        String uword = "$" + path.word.toUpperCase();
        if (path != null && NodemapperOperator.containsKey(node, uword)) {
            Nodemapper matchedNode = match(path.next, NodemapperOperator.get(node, uword), inputThatTopic, starState, starIndex, inputStars, thatStars, topicStars, matchTrace);
            if (matchedNode != null) {
                return matchedNode;
            }
        }
        fail("dollar", matchTrace);
        return null;
    }

    final Nodemapper starMatch(Path path, Nodemapper node, String input, String starState, int starIndex, String[] inputStars, String[] thatStars, String[] topicStars, String matchTrace) {
        return wildMatch(path, node, input, starState, starIndex, inputStars, thatStars, topicStars, "*", matchTrace);
    }

    final Nodemapper underMatch(Path path, Nodemapper node, String input, String starState, int starIndex, String[] inputStars, String[] thatStars, String[] topicStars, String matchTrace) {
        return wildMatch(path, node, input, starState, starIndex, inputStars, thatStars, topicStars, "_", matchTrace);
    }

    final Nodemapper caretMatch(Path path, Nodemapper node, String input, String starState, int starIndex, String[] inputStars, String[] thatStars, String[] topicStars, String matchTrace) {
        Nodemapper matchedNode = zeroMatch(path, node, input, starState, starIndex, inputStars, thatStars, topicStars, "^", matchTrace);
        if (matchedNode != null) {
            return matchedNode;
        }
        return wildMatch(path, node, input, starState, starIndex, inputStars, thatStars, topicStars, "^", matchTrace);
    }

    final Nodemapper sharpMatch(Path path, Nodemapper node, String input, String starState, int starIndex, String[] inputStars, String[] thatStars, String[] topicStars, String matchTrace) {
        Nodemapper matchedNode = zeroMatch(path, node, input, starState, starIndex, inputStars, thatStars, topicStars, "#", matchTrace);
        if (matchedNode != null) {
            return matchedNode;
        }
        return wildMatch(path, node, input, starState, starIndex, inputStars, thatStars, topicStars, "#", matchTrace);
    }

    final Nodemapper zeroMatch(Path path, Nodemapper node, String input, String starState, int starIndex, String[] inputStars, String[] thatStars, String[] topicStars, String wildcard, String matchTrace) {
        matchTrace = matchTrace + "[" + wildcard + ",]";
        if (path == null || !NodemapperOperator.containsKey(node, wildcard)) {
            fail("zero " + wildcard, matchTrace);
            return null;
        }
        setStars(this.bot.properties.get(MagicStrings.null_star), starIndex, starState, inputStars, thatStars, topicStars);
        return match(path, NodemapperOperator.get(node, wildcard), input, starState, starIndex + 1, inputStars, thatStars, topicStars, matchTrace);
    }

    final Nodemapper wildMatch(Path path, Nodemapper node, String input, String starState, int starIndex, String[] inputStars, String[] thatStars, String[] topicStars, String wildcard, String matchTrace) {
        if (path.word.equals("<THAT>") || path.word.equals("<TOPIC>")) {
            fail("wild1 " + wildcard, matchTrace);
            return null;
        }
        if (path != null) {
            try {
                if (NodemapperOperator.containsKey(node, wildcard)) {
                    matchTrace = matchTrace + "[" + wildcard + "," + path.word + "]";
                    String currentWord = path.word;
                    String starWords = currentWord + " ";
                    Path pathStart = path.next;
                    Nodemapper nextNode = NodemapperOperator.get(node, wildcard);
                    Nodemapper matchedNode;
                    if (!NodemapperOperator.isLeaf(nextNode) || nextNode.shortCut) {
                        path = pathStart;
                        while (path != null) {
                            if (currentWord.equals("<THAT>") || currentWord.equals("<TOPIC>")) {
                                break;
                            }
                            matchTrace = matchTrace + "[" + wildcard + "," + path.word + "]";
                            matchedNode = match(path, nextNode, input, starState, starIndex + 1, inputStars, thatStars, topicStars, matchTrace);
                            if (matchedNode != null) {
                                setStars(starWords, starIndex, starState, inputStars, thatStars, topicStars);
                                return matchedNode;
                            }
                            currentWord = path.word;
                            starWords = starWords + currentWord + " ";
                            path = path.next;
                        }
                        fail("wild2 " + wildcard, matchTrace);
                        return null;
                    }
                    matchedNode = nextNode;
                    setStars(Path.pathToSentence(path), starIndex, starState, inputStars, thatStars, topicStars);
                    return matchedNode;
                }
            } catch (Exception ex) {
                System.out.println("wildMatch: " + Path.pathToSentence(path) + ": " + ex);
            }
        }
        fail("wild3 " + wildcard, matchTrace);
        return null;
    }

    final Nodemapper setMatch(Path path, Nodemapper node, String input, String starState, int starIndex, String[] inputStars, String[] thatStars, String[] topicStars, String matchTrace) {
        if (DEBUG) {
            System.out.println("Graphmaster.setMatch(path: " + path + ", node: " + node + ", input: " + input + ", starState: " + starState + ", starIndex: " + starIndex + ", inputStars, thatStars, topicStars, matchTrace: " + matchTrace + ", )");
        }
        if (node.sets == null || path.word.equals("<THAT>") || path.word.equals("<TOPIC>")) {
            return null;
        }
        if (DEBUG) {
            System.out.println("in Graphmaster.setMatch, setMatch sets =" + node.sets);
        }
        Iterator it = node.sets.iterator();
        while (it.hasNext()) {
            String setName = (String) it.next();
            if (DEBUG) {
                System.out.println("in Graphmaster.setMatch, setMatch trying type " + setName);
            }
            Nodemapper nextNode = NodemapperOperator.get(node, "<SET>" + setName.toUpperCase() + "</SET>");
            AIMLSet aimlSet = (AIMLSet) this.bot.setMap.get(setName);
            Nodemapper bestMatchedNode = null;
            String currentWord = path.word;
            String starWords = currentWord + " ";
            int length = 1;
            matchTrace = matchTrace + "[<set>" + setName + "</set>," + path.word + "]";
            if (DEBUG) {
                System.out.println("in Graphmaster.setMatch, setMatch starWords =\"" + starWords + "\"");
            }
            for (Path qath = path.next; qath != null && !currentWord.equals("<THAT>") && !currentWord.equals("<TOPIC>") && length <= aimlSet.maxLength; qath = qath.next) {
                if (DEBUG) {
                    System.out.println("in Graphmaster.setMatch, qath.word = " + qath.word);
                }
                String phrase = this.bot.preProcessor.normalize(starWords.trim()).toUpperCase();
                if (DEBUG) {
                    System.out.println("in Graphmaster.setMatch, setMatch trying \"" + phrase + "\" in " + setName);
                }
                if (aimlSet.contains(phrase)) {
                    Nodemapper matchedNode = match(qath, nextNode, input, starState, starIndex + 1, inputStars, thatStars, topicStars, matchTrace);
                    if (matchedNode != null) {
                        setStars(starWords, starIndex, starState, inputStars, thatStars, topicStars);
                        if (DEBUG) {
                            System.out.println("in Graphmaster.setMatch, setMatch found " + phrase + " in " + setName);
                        }
                        bestMatchedNode = matchedNode;
                    }
                }
                length++;
                currentWord = qath.word;
                starWords = starWords + currentWord + " ";
            }
            if (bestMatchedNode != null) {
                return bestMatchedNode;
            }
        }
        fail("set", matchTrace);
        return null;
    }

    public void setStars(String starWords, int starIndex, String starState, String[] inputStars, String[] thatStars, String[] topicStars) {
        if (starIndex < MagicNumbers.max_stars) {
            starWords = starWords.trim();
            if (starState.equals("inputStar")) {
                inputStars[starIndex] = starWords;
            } else if (starState.equals("thatStar")) {
                thatStars[starIndex] = starWords;
            } else if (starState.equals("topicStar")) {
                topicStars[starIndex] = starWords;
            }
        }
    }

    public void printgraph() {
        printgraph(this.root, BuildConfig.FLAVOR);
    }

    void printgraph(Nodemapper node, String partial) {
        if (node == null) {
            System.out.println("Null graph");
            return;
        }
        String template = BuildConfig.FLAVOR;
        if (NodemapperOperator.isLeaf(node) || node.shortCut) {
            template = Category.templateToLine(node.category.getTemplate());
            template = template.substring(0, Math.min(16, template.length()));
            if (node.shortCut) {
                System.out.println(partial + "(" + NodemapperOperator.size(node) + "[" + node.height + "])--<THAT>-->X(1)--*-->X(1)--<TOPIC>-->X(1)--*-->" + template + "...");
            } else {
                System.out.println(partial + "(" + NodemapperOperator.size(node) + "[" + node.height + "]) " + template + "...");
            }
        }
        for (String key : NodemapperOperator.keySet(node)) {
            printgraph(NodemapperOperator.get(node, key), partial + "(" + NodemapperOperator.size(node) + "[" + node.height + "])--" + key + "-->");
        }
    }

    public ArrayList<Category> getCategories() {
        ArrayList<Category> categories = new ArrayList();
        getCategories(this.root, categories);
        return categories;
    }

    void getCategories(Nodemapper node, ArrayList<Category> categories) {
        if (node != null) {
            if ((NodemapperOperator.isLeaf(node) || node.shortCut) && node.category != null) {
                categories.add(node.category);
            }
            for (String key : NodemapperOperator.keySet(node)) {
                getCategories(NodemapperOperator.get(node, key), categories);
            }
        }
    }

    public void nodeStats() {
        this.leafCnt = 0;
        this.nodeCnt = 0;
        this.nodeSize = 0;
        this.singletonCnt = 0;
        this.shortCutCnt = 0;
        this.naryCnt = 0;
        nodeStatsGraph(this.root);
        this.resultNote = this.bot.name + " (" + this.name + "): " + getCategories().size() + " categories " + this.nodeCnt + " nodes " + this.singletonCnt + " singletons " + this.leafCnt + " leaves " + this.shortCutCnt + " shortcuts " + this.naryCnt + " n-ary " + this.nodeSize + " branches " + (((float) this.nodeSize) / ((float) this.nodeCnt)) + " average branching ";
        if (MagicBooleans.trace_mode) {
            System.out.println(this.resultNote);
            BrainLogger.getInstance().info(this.resultNote);
        }
    }

    public void nodeStatsGraph(Nodemapper node) {
        if (node != null) {
            this.nodeCnt++;
            this.nodeSize += (long) NodemapperOperator.size(node);
            if (NodemapperOperator.size(node) == 1) {
                this.singletonCnt++;
            }
            if (NodemapperOperator.isLeaf(node) && !node.shortCut) {
                this.leafCnt++;
            }
            if (NodemapperOperator.size(node) > 1) {
                this.naryCnt++;
            }
            if (node.shortCut) {
                this.shortCutCnt++;
            }
            for (String key : NodemapperOperator.keySet(node)) {
                nodeStatsGraph(NodemapperOperator.get(node, key));
            }
        }
    }

    public HashSet<String> getVocabulary() {
        this.vocabulary = new HashSet();
        getBrainVocabulary(this.root);
        for (String set : this.bot.setMap.keySet()) {
            this.vocabulary.addAll((Collection) this.bot.setMap.get(set));
        }
        return this.vocabulary;
    }

    public void getBrainVocabulary(Nodemapper node) {
        if (node != null) {
            for (String key : NodemapperOperator.keySet(node)) {
                this.vocabulary.add(key);
                getBrainVocabulary(NodemapperOperator.get(node, key));
            }
        }
    }
}
