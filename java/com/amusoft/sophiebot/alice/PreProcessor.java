package com.amusoft.sophiebot.alice;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PreProcessor {
    private static boolean DEBUG = false;
    public int denormalCount = 0;
    public Pattern[] denormalPatterns = new Pattern[MagicNumbers.max_substitutions];
    public String[] denormalSubs = new String[MagicNumbers.max_substitutions];
    public int genderCount = 0;
    public Pattern[] genderPatterns = new Pattern[MagicNumbers.max_substitutions];
    public String[] genderSubs = new String[MagicNumbers.max_substitutions];
    public int normalCount = 0;
    public Pattern[] normalPatterns = new Pattern[MagicNumbers.max_substitutions];
    public String[] normalSubs = new String[MagicNumbers.max_substitutions];
    public int person2Count = 0;
    public Pattern[] person2Patterns = new Pattern[MagicNumbers.max_substitutions];
    public String[] person2Subs = new String[MagicNumbers.max_substitutions];
    public int personCount = 0;
    public Pattern[] personPatterns = new Pattern[MagicNumbers.max_substitutions];
    public String[] personSubs = new String[MagicNumbers.max_substitutions];

    public PreProcessor(Bot bot) {
        this.normalCount = readSubstitutions(bot.config_path + "/normal.txt", this.normalPatterns, this.normalSubs);
        this.denormalCount = readSubstitutions(bot.config_path + "/denormal.txt", this.denormalPatterns, this.denormalSubs);
        this.personCount = readSubstitutions(bot.config_path + "/person.txt", this.personPatterns, this.personSubs);
        this.person2Count = readSubstitutions(bot.config_path + "/person2.txt", this.person2Patterns, this.person2Subs);
        this.genderCount = readSubstitutions(bot.config_path + "/gender.txt", this.genderPatterns, this.genderSubs);
        if (MagicBooleans.trace_mode) {
            System.out.println("Preprocessor: " + this.normalCount + " norms " + this.personCount + " persons " + this.person2Count + " person2 ");
        }
    }

    public String normalize(String request) {
        if (DEBUG) {
            System.out.println("PreProcessor.normalize(request: " + request + ")");
        }
        String result = substitute(request, this.normalPatterns, this.normalSubs, this.normalCount).replaceAll("(\r\n|\n\r|\r|\n)", " ");
        if (DEBUG) {
            System.out.println("PreProcessor.normalize() returning: " + result);
        }
        return result;
    }

    public String denormalize(String request) {
        return substitute(request, this.denormalPatterns, this.denormalSubs, this.denormalCount);
    }

    public String person(String input) {
        return substitute(input, this.personPatterns, this.personSubs, this.personCount);
    }

    public String person2(String input) {
        return substitute(input, this.person2Patterns, this.person2Subs, this.person2Count);
    }

    public String gender(String input) {
        return substitute(input, this.genderPatterns, this.genderSubs, this.genderCount);
    }

    String substitute(String request, Pattern[] patterns, String[] subs, int count) {
        String result = " " + request + " ";
        int i = 0;
        while (i < count) {
            int index = i;
            try {
                String replacement = subs[i];
                Matcher m = patterns[i].matcher(result);
                if (m.find()) {
                    result = m.replaceAll(replacement);
                }
                i++;
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println("Request " + request + " Result " + result + " at " + index + " " + patterns[index] + " " + subs[index]);
            }
        }
        while (result.contains("  ")) {
            result = result.replace("  ", " ");
        }
        result = result.trim();
        return result.trim();
    }

    public int readSubstitutionsFromInputStream(InputStream in, Pattern[] patterns, String[] subs) {
        BufferedReader br = new BufferedReader(new InputStreamReader(in));
        int subCount = 0;
        while (true) {
            try {
                String strLine = br.readLine();
                if (strLine == null) {
                    break;
                }
                strLine = strLine.trim();
                if (!strLine.startsWith(MagicStrings.text_comment_mark)) {
                    Matcher matcher = Pattern.compile("\"(.*?)\",\"(.*?)\"", 32).matcher(strLine);
                    if (matcher.find() && subCount < MagicNumbers.max_substitutions) {
                        subs[subCount] = matcher.group(2);
                        patterns[subCount] = Pattern.compile(Pattern.quote(matcher.group(1)), 2);
                        subCount++;
                    }
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        return subCount;
    }

    int readSubstitutions(String filename, Pattern[] patterns, String[] subs) {
        try {
            if (!new File(filename).exists()) {
                return 0;
            }
            FileInputStream fstream = new FileInputStream(filename);
            int subCount = readSubstitutionsFromInputStream(fstream, patterns, subs);
            fstream.close();
            return subCount;
        } catch (Exception e) {
            System.err.println("Error: " + e.getMessage());
            return 0;
        }
    }

    public String[] sentenceSplit(String line) {
        String[] result = line.replace("\u3002", ".").replace("\uff1f", "?").replace("\uff01", "!").split("[\\.!\\?]");
        for (int i = 0; i < result.length; i++) {
            result[i] = result[i].trim();
        }
        return result;
    }

    public void normalizeFile(String infile, String outfile) {
        Exception ex;
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(infile)));
            BufferedWriter bw = new BufferedWriter(new FileWriter(outfile));
            while (true) {
                try {
                    String strLine = br.readLine();
                    if (strLine != null) {
                        strLine = strLine.trim();
                        if (strLine.length() > 0) {
                            String norm = normalize(strLine).toUpperCase();
                            String[] sentences = sentenceSplit(norm);
                            if (sentences.length > 1) {
                                for (String s : sentences) {
                                    System.out.println(norm + "-->" + s);
                                }
                            }
                            for (String sentence : sentences) {
                                String sentence2 = sentence2.trim();
                                if (sentence2.length() > 0) {
                                    bw.write(sentence2);
                                    bw.newLine();
                                }
                            }
                        }
                    } else {
                        bw.close();
                        br.close();
                        return;
                    }
                } catch (Exception e) {
                    ex = e;
                    BufferedWriter bufferedWriter = bw;
                }
            }
        } catch (Exception e2) {
            ex = e2;
            ex.printStackTrace();
        }
    }
}
