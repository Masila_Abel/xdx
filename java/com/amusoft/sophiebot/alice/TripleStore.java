package com.amusoft.sophiebot.alice;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class TripleStore {
    public Bot bot;
    public Chat chatSession;
    public int idCnt = 0;
    public HashMap<String, Triple> idTriple = new HashMap();
    public String name = "unknown";
    public HashMap<String, HashSet<String>> objectTriples = new HashMap();
    public HashMap<String, HashSet<String>> predicateTriples = new HashMap();
    public HashMap<String, HashSet<String>> subjectTriples = new HashMap();
    public HashMap<String, String> tripleStringId = new HashMap();

    public class Triple {
        public String id;
        public String object;
        public String predicate;
        public String subject;

        public Triple(String s, String p, String o) {
            Bot bot = TripleStore.this.bot;
            if (bot != null) {
                s = bot.preProcessor.normalize(s);
                p = bot.preProcessor.normalize(p);
                o = bot.preProcessor.normalize(o);
            }
            if (s != null && p != null && o != null) {
                this.subject = s;
                this.predicate = p;
                this.object = o;
                StringBuilder append = new StringBuilder().append(TripleStore.this.name);
                int i = TripleStore.this.idCnt;
                TripleStore.this.idCnt = i + 1;
                this.id = append.append(i).toString();
            }
        }
    }

    public TripleStore(String name, Chat chatSession) {
        this.name = name;
        this.chatSession = chatSession;
        this.bot = chatSession.bot;
    }

    public String mapTriple(Triple triple) {
        String id = triple.id;
        this.idTriple.put(id, triple);
        String s = triple.subject;
        String p = triple.predicate;
        String o = triple.object;
        s = s.toUpperCase();
        p = p.toUpperCase();
        o = o.toUpperCase();
        String tripleString = (s + ":" + p + ":" + o).toUpperCase();
        if (this.tripleStringId.keySet().contains(tripleString)) {
            return (String) this.tripleStringId.get(tripleString);
        }
        HashSet<String> existingTriples;
        this.tripleStringId.put(tripleString, id);
        if (this.subjectTriples.containsKey(s)) {
            existingTriples = (HashSet) this.subjectTriples.get(s);
        } else {
            existingTriples = new HashSet();
        }
        existingTriples.add(id);
        this.subjectTriples.put(s, existingTriples);
        if (this.predicateTriples.containsKey(p)) {
            existingTriples = (HashSet) this.predicateTriples.get(p);
        } else {
            existingTriples = new HashSet();
        }
        existingTriples.add(id);
        this.predicateTriples.put(p, existingTriples);
        if (this.objectTriples.containsKey(o)) {
            existingTriples = (HashSet) this.objectTriples.get(o);
        } else {
            existingTriples = new HashSet();
        }
        existingTriples.add(id);
        this.objectTriples.put(o, existingTriples);
        return id;
    }

    public String unMapTriple(Triple triple) {
        String id = MagicStrings.undefined_triple;
        String s = triple.subject;
        String p = triple.predicate;
        String o = triple.object;
        s = s.toUpperCase();
        p = p.toUpperCase();
        o = o.toUpperCase();
        String tripleString = s + ":" + p + ":" + o;
        System.out.println("unMapTriple " + tripleString);
        tripleString = tripleString.toUpperCase();
        triple = (Triple) this.idTriple.get(this.tripleStringId.get(tripleString));
        System.out.println("unMapTriple " + triple);
        if (triple == null) {
            return MagicStrings.undefined_triple;
        }
        HashSet<String> existingTriples;
        id = triple.id;
        this.idTriple.remove(id);
        this.tripleStringId.remove(tripleString);
        if (this.subjectTriples.containsKey(s)) {
            existingTriples = (HashSet) this.subjectTriples.get(s);
        } else {
            existingTriples = new HashSet();
        }
        existingTriples.remove(id);
        this.subjectTriples.put(s, existingTriples);
        if (this.predicateTriples.containsKey(p)) {
            existingTriples = (HashSet) this.predicateTriples.get(p);
        } else {
            existingTriples = new HashSet();
        }
        existingTriples.remove(id);
        this.predicateTriples.put(p, existingTriples);
        if (this.objectTriples.containsKey(o)) {
            existingTriples = (HashSet) this.objectTriples.get(o);
        } else {
            existingTriples = new HashSet();
        }
        existingTriples.remove(id);
        this.objectTriples.put(o, existingTriples);
        return id;
    }

    public Set<String> allTriples() {
        return new HashSet(this.idTriple.keySet());
    }

    public String addTriple(String subject, String predicate, String object) {
        if (subject == null || predicate == null || object == null) {
            return MagicStrings.undefined_triple;
        }
        return mapTriple(new Triple(subject, predicate, object));
    }

    public String deleteTriple(String subject, String predicate, String object) {
        if (subject == null || predicate == null || object == null) {
            return MagicStrings.undefined_triple;
        }
        if (MagicBooleans.trace_mode) {
            System.out.println("Deleting " + subject + " " + predicate + " " + object);
        }
        return unMapTriple(new Triple(subject, predicate, object));
    }

    public void printTriples() {
        for (String x : this.idTriple.keySet()) {
            Triple triple = (Triple) this.idTriple.get(x);
            System.out.println(x + ":" + triple.subject + ":" + triple.predicate + ":" + triple.object);
        }
    }

    HashSet<String> emptySet() {
        return new HashSet();
    }

    public HashSet<String> getTriples(String s, String p, String o) {
        Set<String> subjectSet;
        Set<String> predicateSet;
        Set<String> objectSet;
        if (MagicBooleans.trace_mode) {
            System.out.println("TripleStore: getTriples [" + this.idTriple.size() + "] " + s + ":" + p + ":" + o);
        }
        if (s == null || s.startsWith("?")) {
            subjectSet = allTriples();
        } else {
            s = s.toUpperCase();
            if (this.subjectTriples.containsKey(s)) {
                subjectSet = (Set) this.subjectTriples.get(s);
            } else {
                subjectSet = emptySet();
            }
        }
        if (p == null || p.startsWith("?")) {
            predicateSet = allTriples();
        } else {
            p = p.toUpperCase();
            if (this.predicateTriples.containsKey(p)) {
                predicateSet = (Set) this.predicateTriples.get(p);
            } else {
                predicateSet = emptySet();
            }
        }
        if (o == null || o.startsWith("?")) {
            objectSet = allTriples();
        } else {
            o = o.toUpperCase();
            if (this.objectTriples.containsKey(o)) {
                objectSet = (Set) this.objectTriples.get(o);
            } else {
                objectSet = emptySet();
            }
        }
        Set<String> resultSet = new HashSet(subjectSet);
        resultSet.retainAll(predicateSet);
        resultSet.retainAll(objectSet);
        return new HashSet(resultSet);
    }

    public HashSet<String> getSubjects(HashSet<String> triples) {
        HashSet<String> resultSet = new HashSet();
        Iterator it = triples.iterator();
        while (it.hasNext()) {
            resultSet.add(((Triple) this.idTriple.get((String) it.next())).subject);
        }
        return resultSet;
    }

    public HashSet<String> getPredicates(HashSet<String> triples) {
        HashSet<String> resultSet = new HashSet();
        Iterator it = triples.iterator();
        while (it.hasNext()) {
            resultSet.add(((Triple) this.idTriple.get((String) it.next())).predicate);
        }
        return resultSet;
    }

    public HashSet<String> getObjects(HashSet<String> triples) {
        HashSet<String> resultSet = new HashSet();
        Iterator it = triples.iterator();
        while (it.hasNext()) {
            resultSet.add(((Triple) this.idTriple.get((String) it.next())).object);
        }
        return resultSet;
    }

    public String formatAIMLTripleList(HashSet<String> triples) {
        String result = MagicStrings.default_list_item;
        Iterator it = triples.iterator();
        while (it.hasNext()) {
            result = ((String) it.next()) + " " + result;
        }
        return result.trim();
    }

    public String getSubject(String id) {
        if (this.idTriple.containsKey(id)) {
            return ((Triple) this.idTriple.get(id)).subject;
        }
        return "Unknown subject";
    }

    public String getPredicate(String id) {
        if (this.idTriple.containsKey(id)) {
            return ((Triple) this.idTriple.get(id)).predicate;
        }
        return "Unknown predicate";
    }

    public String getObject(String id) {
        if (this.idTriple.containsKey(id)) {
            return ((Triple) this.idTriple.get(id)).object;
        }
        return "Unknown object";
    }

    public String stringTriple(String id) {
        Triple triple = (Triple) this.idTriple.get(id);
        return id + " " + triple.subject + " " + triple.predicate + " " + triple.object;
    }

    public void printAllTriples() {
        for (String id : this.idTriple.keySet()) {
            System.out.println(stringTriple(id));
        }
    }

    public HashSet<Tuple> select(HashSet<String> vars, HashSet<String> visibleVars, ArrayList<Clause> clauses) {
        HashSet<Tuple> result = new HashSet();
        try {
            result = selectFromRemainingClauses(new Tuple(vars, visibleVars), clauses);
            if (MagicBooleans.trace_mode) {
                Iterator it = result.iterator();
                while (it.hasNext()) {
                    System.out.println(((Tuple) it.next()).printTuple());
                }
            }
        } catch (Exception ex) {
            System.out.println("Something went wrong with select " + visibleVars);
            ex.printStackTrace();
        }
        return result;
    }

    public Clause adjustClause(Tuple tuple, Clause clause) {
        String value;
        Set vars = tuple.getVars();
        String subj = clause.subj;
        String pred = clause.pred;
        String obj = clause.obj;
        Clause newClause = new Clause(clause);
        if (vars.contains(subj)) {
            value = tuple.getValue(subj);
            if (!value.equals(MagicStrings.unbound_variable)) {
                newClause.subj = value;
            }
        }
        if (vars.contains(pred)) {
            value = tuple.getValue(pred);
            if (!value.equals(MagicStrings.unbound_variable)) {
                newClause.pred = value;
            }
        }
        if (vars.contains(obj)) {
            value = tuple.getValue(obj);
            if (!value.equals(MagicStrings.unbound_variable)) {
                newClause.obj = value;
            }
        }
        return newClause;
    }

    public Tuple bindTuple(Tuple partial, String triple, Clause clause) {
        Tuple tuple = new Tuple(partial);
        if (clause.subj.startsWith("?")) {
            tuple.bind(clause.subj, getSubject(triple));
        }
        if (clause.pred.startsWith("?")) {
            tuple.bind(clause.pred, getPredicate(triple));
        }
        if (clause.obj.startsWith("?")) {
            tuple.bind(clause.obj, getObject(triple));
        }
        return tuple;
    }

    public HashSet<Tuple> selectFromSingleClause(Tuple partial, Clause clause, Boolean affirm) {
        HashSet<Tuple> result = new HashSet();
        HashSet<String> triples = getTriples(clause.subj, clause.pred, clause.obj);
        if (affirm.booleanValue()) {
            Iterator it = triples.iterator();
            while (it.hasNext()) {
                result.add(bindTuple(partial, (String) it.next(), clause));
            }
        } else if (triples.size() == 0) {
            result.add(partial);
        }
        return result;
    }

    public HashSet<Tuple> selectFromRemainingClauses(Tuple partial, ArrayList<Clause> clauses) {
        HashSet<Tuple> result = new HashSet();
        Clause clause = adjustClause(partial, (Clause) clauses.get(0));
        HashSet<Tuple> tuples = selectFromSingleClause(partial, clause, clause.affirm);
        if (clauses.size() <= 1) {
            return tuples;
        }
        ArrayList<Clause> remainingClauses = new ArrayList(clauses);
        remainingClauses.remove(0);
        Iterator it = tuples.iterator();
        while (it.hasNext()) {
            result.addAll(selectFromRemainingClauses((Tuple) it.next(), remainingClauses));
        }
        return result;
    }
}
