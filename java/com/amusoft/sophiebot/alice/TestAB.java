package com.amusoft.sophiebot.alice;

import com.amusoft.sophiebot.alice.utils.IOUtils;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import outlander.showcaseview.BuildConfig;

public class TestAB {
    public static String sample_file = "sample.random.txt";

    public static void testChat(Bot bot, boolean doWrites, boolean traceMode) {
        Chat chatSession = new Chat(bot, doWrites);
        bot.brain.nodeStats();
        MagicBooleans.trace_mode = traceMode;
        String textLine = BuildConfig.FLAVOR;
        while (true) {
            textLine = IOUtils.readInputTextLine("Human");
            if (textLine == null || textLine.length() < 1) {
                textLine = MagicStrings.null_input;
            }
            if (textLine.equals("q")) {
                System.exit(0);
            } else if (textLine.equals("wq")) {
                bot.writeQuit();
                System.exit(0);
            } else if (textLine.equals("sc")) {
                sraixCache("c:/ab/data/sraixdata6.txt", chatSession);
            } else if (textLine.equals("iqtest")) {
                try {
                    new ChatTest(bot).testMultisentenceRespond();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            } else if (textLine.equals("ab")) {
                testAB(bot, sample_file);
            } else {
                String request = textLine;
                if (MagicBooleans.trace_mode) {
                    System.out.println("STATE=" + request + ":THAT=" + ((History) chatSession.thatHistory.get(0)).get(0) + ":TOPIC=" + chatSession.predicates.get("topic"));
                }
                String response = chatSession.multisentenceRespond(request);
                while (response.contains("&lt;")) {
                    response = response.replace("&lt;", "<");
                }
                while (response.contains("&gt;")) {
                    response = response.replace("&gt;", ">");
                }
                IOUtils.writeOutputTextLine("Robot", response);
            }
        }
    }

    public static void testBotChat() {
        Bot bot = new Bot("alice");
        System.out.println(bot.brain.upgradeCnt + " brain upgrades");
        String request = "Hello.  How are you?  What is your name?  Tell me about yourself.";
        String response = new Chat(bot).multisentenceRespond(request);
        System.out.println("Human: " + request);
        System.out.println("Robot: " + response);
    }

    public static void runTests(Bot bot, boolean traceMode) {
        MagicBooleans.qa_test_mode = true;
        Chat chatSession = new Chat(bot, false);
        bot.brain.nodeStats();
        MagicBooleans.trace_mode = traceMode;
        IOUtils testInput = new IOUtils(MagicStrings.root_path + "/data/lognormal-500.txt", "read");
        IOUtils testOutput = new IOUtils(MagicStrings.root_path + "/data/lognormal-500-out.txt", "write");
        String textLine = testInput.readLine();
        int i = 1;
        System.out.print(0);
        while (textLine != null) {
            if (textLine == null || textLine.length() < 1) {
                textLine = MagicStrings.null_input;
            }
            if (textLine.equals("q")) {
                System.exit(0);
            } else if (textLine.equals("wq")) {
                bot.writeQuit();
                System.exit(0);
            } else if (textLine.equals("ab")) {
                testAB(bot, sample_file);
            } else if (textLine.equals(MagicStrings.null_input)) {
                testOutput.writeLine(BuildConfig.FLAVOR);
            } else if (textLine.startsWith("#")) {
                testOutput.writeLine(textLine);
            } else {
                String request = textLine;
                if (MagicBooleans.trace_mode) {
                    System.out.println("STATE=" + request + ":THAT=" + ((History) chatSession.thatHistory.get(0)).get(0) + ":TOPIC=" + chatSession.predicates.get("topic"));
                }
                String response = chatSession.multisentenceRespond(request);
                while (response.contains("&lt;")) {
                    response = response.replace("&lt;", "<");
                }
                while (response.contains("&gt;")) {
                    response = response.replace("&gt;", ">");
                }
                testOutput.writeLine("Robot: " + response);
            }
            textLine = testInput.readLine();
            System.out.print(".");
            if (i % 10 == 0) {
                System.out.print(" ");
            }
            if (i % 100 == 0) {
                System.out.println(BuildConfig.FLAVOR);
                System.out.print(i + " ");
            }
            i++;
        }
        testInput.close();
        testOutput.close();
        System.out.println(BuildConfig.FLAVOR);
    }

    public static void testAB(Bot bot, String sampleFile) {
        MagicBooleans.trace_mode = true;
        AB ab = new AB(bot, sampleFile);
        ab.ab();
        System.out.println("Begin Pattern Suggestor Terminal Interaction");
        ab.terminalInteraction();
    }

    public static void testShortCuts() {
    }

    public static void sraixCache(String filename, Chat chatSession) {
        MagicBooleans.cache_sraix = true;
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(filename)));
            int count = 0;
            while (true) {
                String strLine = br.readLine();
                int count2;
                if (strLine != null) {
                    count2 = count + 1;
                    if (count < 650000) {
                        System.out.println("Human: " + strLine);
                        System.out.println("Robot: " + chatSession.multisentenceRespond(strLine));
                        count = count2;
                    } else {
                        return;
                    }
                }
                count2 = count;
                return;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
