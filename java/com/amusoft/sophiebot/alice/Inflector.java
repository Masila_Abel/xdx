package com.amusoft.sophiebot.alice;

import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import outlander.showcaseview.BuildConfig;

public class Inflector {
    protected static final Inflector INSTANCE = new Inflector();
    private LinkedList<Rule> plurals = new LinkedList();
    private LinkedList<Rule> singulars = new LinkedList();
    private final Set<String> uncountables = new HashSet();

    protected class Rule {
        protected final String expression;
        protected final Pattern expressionPattern;
        protected final String replacement;

        protected Rule(String expression, String replacement) {
            this.expression = expression;
            if (replacement == null) {
                replacement = BuildConfig.FLAVOR;
            }
            this.replacement = replacement;
            this.expressionPattern = Pattern.compile(this.expression, 2);
        }

        protected String apply(String input) {
            Matcher matcher = this.expressionPattern.matcher(input);
            if (matcher.find()) {
                return matcher.replaceAll(this.replacement);
            }
            return null;
        }

        public int hashCode() {
            return this.expression.hashCode();
        }

        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (obj != null && obj.getClass() == getClass()) {
                if (this.expression.equalsIgnoreCase(((Rule) obj).expression)) {
                    return true;
                }
            }
            return false;
        }

        public String toString() {
            return this.expression + ", " + this.replacement;
        }
    }

    public static final Inflector getInstance() {
        return INSTANCE;
    }

    public Inflector() {
        initialize();
    }

    protected Inflector(Inflector original) {
        this.plurals.addAll(original.plurals);
        this.singulars.addAll(original.singulars);
        this.uncountables.addAll(original.uncountables);
    }

    public Inflector clone() {
        return new Inflector(this);
    }

    public String pluralize(Object word) {
        if (word == null) {
            return null;
        }
        String wordStr = word.toString().trim();
        if (wordStr.length() == 0 || isUncountable(wordStr)) {
            return wordStr;
        }
        Iterator it = this.plurals.iterator();
        while (it.hasNext()) {
            String result = ((Rule) it.next()).apply(wordStr);
            if (result != null) {
                return result;
            }
        }
        return wordStr;
    }

    public String pluralize(Object word, int count) {
        if (word == null) {
            return null;
        }
        if (count == 1 || count == -1) {
            return word.toString();
        }
        return pluralize(word);
    }

    public String singularize(Object word) {
        if (word == null) {
            return null;
        }
        String wordStr = word.toString().trim();
        if (wordStr.length() == 0 || isUncountable(wordStr)) {
            return wordStr;
        }
        Iterator it = this.singulars.iterator();
        while (it.hasNext()) {
            String result = ((Rule) it.next()).apply(wordStr);
            if (result != null) {
                return result;
            }
        }
        return wordStr;
    }

    public String lowerCamelCase(String lowerCaseAndUnderscoredWord, char... delimiterChars) {
        return camelCase(lowerCaseAndUnderscoredWord, false, delimiterChars);
    }

    public String upperCamelCase(String lowerCaseAndUnderscoredWord, char... delimiterChars) {
        return camelCase(lowerCaseAndUnderscoredWord, true, delimiterChars);
    }

    public String camelCase(String lowerCaseAndUnderscoredWord, boolean uppercaseFirstLetter, char... delimiterChars) {
        int i = 0;
        if (lowerCaseAndUnderscoredWord == null) {
            return null;
        }
        lowerCaseAndUnderscoredWord = lowerCaseAndUnderscoredWord.trim();
        if (lowerCaseAndUnderscoredWord.length() == 0) {
            return BuildConfig.FLAVOR;
        }
        if (uppercaseFirstLetter) {
            String result = lowerCaseAndUnderscoredWord;
            if (delimiterChars != null) {
                int length = delimiterChars.length;
                while (i < length) {
                    result = result.replace(delimiterChars[i], '_');
                    i++;
                }
            }
            return replaceAllWithUppercase(result, "(^|_)(.)", 2);
        } else if (lowerCaseAndUnderscoredWord.length() < 2) {
            return lowerCaseAndUnderscoredWord;
        } else {
            return BuildConfig.FLAVOR + Character.toLowerCase(lowerCaseAndUnderscoredWord.charAt(0)) + camelCase(lowerCaseAndUnderscoredWord, true, delimiterChars).substring(1);
        }
    }

    public String underscore(String camelCaseWord, char... delimiterChars) {
        if (camelCaseWord == null) {
            return null;
        }
        String result = camelCaseWord.trim();
        if (result.length() == 0) {
            return BuildConfig.FLAVOR;
        }
        result = result.replaceAll("([A-Z]+)([A-Z][a-z])", "$1_$2").replaceAll("([a-z\\d])([A-Z])", "$1_$2").replace('-', '_');
        if (delimiterChars != null) {
            for (char delimiterChar : delimiterChars) {
                result = result.replace(delimiterChar, '_');
            }
        }
        return result.toLowerCase();
    }

    public String capitalize(String words) {
        if (words == null) {
            return null;
        }
        String result = words.trim();
        if (result.length() == 0) {
            return BuildConfig.FLAVOR;
        }
        if (result.length() == 1) {
            return result.toUpperCase();
        }
        return BuildConfig.FLAVOR + Character.toUpperCase(result.charAt(0)) + result.substring(1).toLowerCase();
    }

    public String humanize(String lowerCaseAndUnderscoredWords, String... removableTokens) {
        if (lowerCaseAndUnderscoredWords == null) {
            return null;
        }
        String result = lowerCaseAndUnderscoredWords.trim();
        if (result.length() == 0) {
            return BuildConfig.FLAVOR;
        }
        result = result.replaceAll("_id$", BuildConfig.FLAVOR);
        if (removableTokens != null) {
            for (String removableToken : removableTokens) {
                result = result.replaceAll(removableToken, BuildConfig.FLAVOR);
            }
        }
        return capitalize(result.replaceAll("_+", " "));
    }

    public String titleCase(String words, String... removableTokens) {
        return replaceAllWithUppercase(humanize(words, removableTokens), "\\b([a-z])", 1);
    }

    public String ordinalize(int number) {
        int remainder = number % 100;
        String numberStr = Integer.toString(number);
        if (11 <= number && number <= 13) {
            return numberStr + "th";
        }
        remainder = number % 10;
        if (remainder == 1) {
            return numberStr + "st";
        }
        if (remainder == 2) {
            return numberStr + "nd";
        }
        if (remainder == 3) {
            return numberStr + "rd";
        }
        return numberStr + "th";
    }

    public boolean isUncountable(String word) {
        if (word == null) {
            return false;
        }
        return this.uncountables.contains(word.trim().toLowerCase());
    }

    public Set<String> getUncountables() {
        return this.uncountables;
    }

    public void addPluralize(String rule, String replacement) {
        this.plurals.addFirst(new Rule(rule, replacement));
    }

    public void addSingularize(String rule, String replacement) {
        this.singulars.addFirst(new Rule(rule, replacement));
    }

    public void addIrregular(String singular, String plural) {
        String singularRemainder = singular.length() > 1 ? singular.substring(1) : BuildConfig.FLAVOR;
        String pluralRemainder = plural.length() > 1 ? plural.substring(1) : BuildConfig.FLAVOR;
        addPluralize("(" + singular.charAt(0) + ")" + singularRemainder + "$", "$1" + pluralRemainder);
        addSingularize("(" + plural.charAt(0) + ")" + pluralRemainder + "$", "$1" + singularRemainder);
    }

    public void addUncountable(String... words) {
        if (words != null && words.length != 0) {
            for (String word : words) {
                if (word != null) {
                    this.uncountables.add(word.trim().toLowerCase());
                }
            }
        }
    }

    protected static String replaceAllWithUppercase(String input, String regex, int groupNumberToUppercase) {
        Matcher matcher = Pattern.compile(regex).matcher(input);
        StringBuffer sb = new StringBuffer();
        while (matcher.find()) {
            matcher.appendReplacement(sb, matcher.group(groupNumberToUppercase).toUpperCase());
        }
        matcher.appendTail(sb);
        return sb.toString();
    }

    public void clear() {
        this.uncountables.clear();
        this.plurals.clear();
        this.singulars.clear();
    }

    protected void initialize() {
        addPluralize("$", "s");
        addPluralize("s$", "s");
        addPluralize("(ax|test)is$", "$1es");
        addPluralize("(octop|vir)us$", "$1i");
        addPluralize("(octop|vir)i$", "$1i");
        addPluralize("(alias|status)$", "$1es");
        addPluralize("(bu)s$", "$1ses");
        addPluralize("(buffal|tomat)o$", "$1oes");
        addPluralize("([ti])um$", "$1a");
        addPluralize("([ti])a$", "$1a");
        addPluralize("sis$", "ses");
        addPluralize("(?:([^f])fe|([lr])f)$", "$1$2ves");
        addPluralize("(hive)$", "$1s");
        addPluralize("([^aeiouy]|qu)y$", "$1ies");
        addPluralize("(x|ch|ss|sh)$", "$1es");
        addPluralize("(matr|vert|ind)ix|ex$", "$1ices");
        addPluralize("([m|l])ouse$", "$1ice");
        addPluralize("([m|l])ice$", "$1ice");
        addPluralize("^(ox)$", "$1en");
        addPluralize("(quiz)$", "$1zes");
        addPluralize("(people|men|children|sexes|moves|stadiums)$", "$1");
        addPluralize("(oxen|octopi|viri|aliases|quizzes)$", "$1");
        addSingularize("s$", BuildConfig.FLAVOR);
        addSingularize("(s|si|u)s$", "$1s");
        addSingularize("(n)ews$", "$1ews");
        addSingularize("([ti])a$", "$1um");
        addSingularize("((a)naly|(b)a|(d)iagno|(p)arenthe|(p)rogno|(s)ynop|(t)he)ses$", "$1$2sis");
        addSingularize("(^analy)ses$", "$1sis");
        addSingularize("(^analy)sis$", "$1sis");
        addSingularize("([^f])ves$", "$1fe");
        addSingularize("(hive)s$", "$1");
        addSingularize("(tive)s$", "$1");
        addSingularize("([lr])ves$", "$1f");
        addSingularize("([^aeiouy]|qu)ies$", "$1y");
        addSingularize("(s)eries$", "$1eries");
        addSingularize("(m)ovies$", "$1ovie");
        addSingularize("(x|ch|ss|sh)es$", "$1");
        addSingularize("([m|l])ice$", "$1ouse");
        addSingularize("(bus)es$", "$1");
        addSingularize("(o)es$", "$1");
        addSingularize("(shoe)s$", "$1");
        addSingularize("(cris|ax|test)is$", "$1is");
        addSingularize("(cris|ax|test)es$", "$1is");
        addSingularize("(octop|vir)i$", "$1us");
        addSingularize("(octop|vir)us$", "$1us");
        addSingularize("(alias|status)es$", "$1");
        addSingularize("(alias|status)$", "$1");
        addSingularize("^(ox)en", "$1");
        addSingularize("(vert|ind)ices$", "$1ex");
        addSingularize("(matr)ices$", "$1ix");
        addSingularize("(quiz)zes$", "$1");
        addIrregular("person", "people");
        addIrregular("man", "men");
        addIrregular("child", "children");
        addIrregular("sex", "sexes");
        addIrregular("move", "moves");
        addIrregular("stadium", "stadiums");
        addUncountable("equipment", "information", "rice", "money", "species", "series", "fish", "sheep");
    }
}
