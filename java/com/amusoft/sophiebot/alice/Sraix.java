package com.amusoft.sophiebot.alice;

import com.amusoft.sophiebot.alice.utils.CalendarUtils;
import com.amusoft.sophiebot.alice.utils.NetworkUtils;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.json.JSONArray;
import org.json.JSONObject;
import outlander.showcaseview.BuildConfig;

public class Sraix {
    public static HashMap<String, String> custIdMap = new HashMap();
    public static String custid = "1";

    public static String sraix(Chat chatSession, String input, String defaultResponse, String hint, String host, String botid, String apiKey, String limit) {
        String response;
        if (!MagicBooleans.enable_network_connection) {
            response = MagicStrings.sraix_failed;
        } else if (host == null || botid == null) {
            response = sraixPannous(input, hint, chatSession);
        } else {
            response = sraixPandorabots(input, chatSession, host, botid);
        }
        System.out.println("Sraix: response = " + response + " defaultResponse = " + defaultResponse);
        if (!response.equals(MagicStrings.sraix_failed)) {
            return response;
        }
        if (chatSession != null && defaultResponse == null) {
            return AIMLProcessor.respond(MagicStrings.sraix_failed, "nothing", "nothing", chatSession);
        }
        if (defaultResponse != null) {
            return defaultResponse;
        }
        return response;
    }

    public static String sraixPandorabots(String input, Chat chatSession, String host, String botid) {
        String responseContent = pandorabotsRequest(input, host, botid);
        if (responseContent == null) {
            return MagicStrings.sraix_failed;
        }
        return pandorabotsResponse(responseContent, chatSession, host, botid);
    }

    public static String pandorabotsRequest(String input, String host, String botid) {
        try {
            custid = "0";
            String key = host + ":" + botid;
            if (custIdMap.containsKey(key)) {
                custid = (String) custIdMap.get(key);
            }
            String spec = NetworkUtils.spec(host, botid, custid, input);
            if (MagicBooleans.trace_mode) {
                System.out.println("Spec = " + spec);
            }
            return NetworkUtils.responseContent(spec);
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public static String pandorabotsResponse(String sraixResponse, Chat chatSession, String host, String botid) {
        String botResponse = MagicStrings.sraix_failed;
        try {
            int n1 = sraixResponse.indexOf("<that>");
            int n2 = sraixResponse.indexOf("</that>");
            if (n2 > n1) {
                botResponse = sraixResponse.substring("<that>".length() + n1, n2);
            }
            n1 = sraixResponse.indexOf("custid=");
            if (n1 > 0) {
                custid = sraixResponse.substring("custid=\"".length() + n1, sraixResponse.length());
                n2 = custid.indexOf("\"");
                if (n2 > 0) {
                    custid = custid.substring(0, n2);
                } else {
                    custid = "0";
                }
                custIdMap.put(host + ":" + botid, custid);
            }
            if (botResponse.endsWith(".")) {
                return botResponse.substring(0, botResponse.length() - 1);
            }
            return botResponse;
        } catch (Exception ex) {
            ex.printStackTrace();
            return botResponse;
        }
    }

    public static String sraixPannous(String input, String hint, Chat chatSession) {
        String rawInput = input;
        if (hint == null) {
            try {
                hint = MagicStrings.sraix_no_hint;
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println("Sraix '" + input + "' failed");
            }
        }
        input = (" " + input + " ").replace(" point ", ".").replace(" rparen ", ")").replace(" lparen ", "(").replace(" slash ", "/").replace(" star ", "*").replace(" dash ", "-").trim().replace(" ", "+");
        int offset = CalendarUtils.timeZoneOffset();
        String locationString = BuildConfig.FLAVOR;
        if (Chat.locationKnown) {
            locationString = "&location=" + Chat.latitude + "," + Chat.longitude;
        }
        String url = "https://ask.pannous.com/api?input=" + input + "&locale=en_US&timeZone=" + offset + locationString + "&login=" + MagicStrings.pannous_login + "&ip=" + NetworkUtils.localIPAddress() + "&botid=0&key=" + MagicStrings.pannous_api_key + "&exclude=Dialogues,ChatBot&out=json&clientFeatures=show-images,reminder,say&debug=true";
        MagicBooleans.trace("in Sraix.sraixPannous, url: '" + url + "'");
        String page = NetworkUtils.responseContent(url);
        String text = BuildConfig.FLAVOR;
        String imgRef = BuildConfig.FLAVOR;
        String urlRef = BuildConfig.FLAVOR;
        if (page == null || page.length() == 0) {
            text = MagicStrings.sraix_failed;
            return MagicStrings.sraix_failed;
        }
        int i;
        JSONArray outputJson = new JSONObject(page).getJSONArray("output");
        if (outputJson.length() == 0) {
            text = MagicStrings.sraix_failed;
        } else {
            JSONArray arr;
            JSONObject actions = outputJson.getJSONObject(0).getJSONObject("actions");
            Object obj;
            JSONObject sObj;
            if (actions.has("reminder")) {
                obj = actions.get("reminder");
                if (obj instanceof JSONObject) {
                    if (MagicBooleans.trace_mode) {
                        System.out.println("Found JSON Object");
                    }
                    sObj = (JSONObject) obj;
                    String date = sObj.getString("date").substring(0, "2012-10-24T14:32".length());
                    if (MagicBooleans.trace_mode) {
                        System.out.println("date=" + date);
                    }
                    String duration = sObj.getString("duration");
                    if (MagicBooleans.trace_mode) {
                        System.out.println("duration=" + duration);
                    }
                    Matcher m = Pattern.compile("(.*)-(.*)-(.*)T(.*):(.*)").matcher(date);
                    String year = BuildConfig.FLAVOR;
                    String month = BuildConfig.FLAVOR;
                    String day = BuildConfig.FLAVOR;
                    String hour = BuildConfig.FLAVOR;
                    String minute = BuildConfig.FLAVOR;
                    if (m.matches()) {
                        year = m.group(1);
                        month = String.valueOf(Integer.parseInt(m.group(2)) - 1);
                        day = m.group(3);
                        hour = m.group(4);
                        text = "<year>" + year + "</year>" + "<month>" + month + "</month>" + "<day>" + day + "</day>" + "<hour>" + hour + "</hour>" + "<minute>" + m.group(5) + "</minute>" + "<duration>" + duration + "</duration>";
                    } else {
                        text = MagicStrings.schedule_error;
                    }
                }
            } else if (!(!actions.has("say") || hint.equals(MagicStrings.sraix_pic_hint) || hint.equals(MagicStrings.sraix_shopping_hint))) {
                MagicBooleans.trace("in Sraix.sraixPannous, found say action");
                obj = actions.get("say");
                if (obj instanceof JSONObject) {
                    sObj = (JSONObject) obj;
                    text = sObj.getString("text");
                    if (sObj.has("moreText")) {
                        arr = sObj.getJSONArray("moreText");
                        for (i = 0; i < arr.length(); i++) {
                            text = text + " " + arr.getString(i);
                        }
                    }
                } else {
                    text = obj.toString();
                }
            }
            if (actions.has("show") && !text.contains("Wolfram") && actions.getJSONObject("show").has("images")) {
                MagicBooleans.trace("in Sraix.sraixPannous, found show action");
                arr = actions.getJSONObject("show").getJSONArray("images");
                imgRef = arr.getString((int) (((double) arr.length()) * Math.random()));
                if (imgRef.startsWith("//")) {
                    imgRef = "http:" + imgRef;
                }
                imgRef = "<a href=\"" + imgRef + "\"><img src=\"" + imgRef + "\"/></a>";
            }
            if (hint.equals(MagicStrings.sraix_shopping_hint) && actions.has("open") && actions.getJSONObject("open").has("url")) {
                urlRef = "<oob><url>" + actions.getJSONObject("open").getString("url") + "</oob></url>";
            }
        }
        if (hint.equals(MagicStrings.sraix_event_hint) && !text.startsWith("<year>")) {
            return MagicStrings.sraix_failed;
        }
        if (text.equals(MagicStrings.sraix_failed)) {
            return AIMLProcessor.respond(MagicStrings.sraix_failed, "nothing", "nothing", chatSession);
        }
        String[] sentences = text.replace("&#39;", "'").replace("&apos;", "'").replaceAll("\\[(.*)\\]", BuildConfig.FLAVOR).split("\\. ");
        String clippedPage = sentences[0];
        for (i = 1; i < sentences.length; i++) {
            if (clippedPage.length() < 500) {
                clippedPage = clippedPage + ". " + sentences[i];
            }
        }
        clippedPage = (clippedPage + " " + imgRef + " " + urlRef).trim();
        log(rawInput, clippedPage);
        return clippedPage;
    }

    public static void log(String pattern, String template) {
        System.out.println("Logging " + pattern);
        template = template.trim();
        if (MagicBooleans.cache_sraix) {
            try {
                if (!template.contains("<year>") && !template.contains("No facilities")) {
                    template = template.replace("\n", "\\#Newline").replace(",", MagicStrings.aimlif_split_char_name).replaceAll("<a(.*)</a>", BuildConfig.FLAVOR).trim();
                    if (template.length() > 0) {
                        BufferedWriter fbw = new BufferedWriter(new FileWriter("c:/ab/bots/sraixcache/aimlif/sraixcache.aiml.csv", true));
                        fbw.write("0," + pattern + ",*,*," + template + ",sraixcache.aiml");
                        fbw.newLine();
                        fbw.close();
                    }
                }
            } catch (Exception e) {
                System.out.println("Error: " + e.getMessage());
            }
        }
    }
}
