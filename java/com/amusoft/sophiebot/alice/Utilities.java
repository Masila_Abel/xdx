package com.amusoft.sophiebot.alice;

import com.amusoft.sophiebot.alice.utils.CalendarUtils;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.Character.UnicodeBlock;
import java.util.HashSet;
import outlander.showcaseview.BuildConfig;

public class Utilities {
    public static String fixCSV(String line) {
        while (line.endsWith(";")) {
            line = line.substring(0, line.length() - 1);
        }
        if (line.startsWith("\"")) {
            line = line.substring(1, line.length());
        }
        if (line.endsWith("\"")) {
            line = line.substring(0, line.length() - 1);
        }
        return line.replaceAll("\"\"", "\"");
    }

    public static String tagTrim(String xmlExpression, String tagName) {
        String stag = "<" + tagName + ">";
        String etag = "</" + tagName + ">";
        if (xmlExpression.length() < (stag + etag).length()) {
            return xmlExpression;
        }
        xmlExpression = xmlExpression.substring(stag.length());
        return xmlExpression.substring(0, xmlExpression.length() - etag.length());
    }

    public static HashSet<String> stringSet(String... strings) {
        HashSet<String> set = new HashSet();
        for (String s : strings) {
            set.add(s);
        }
        return set;
    }

    public static String getFileFromInputStream(InputStream in) {
        BufferedReader br = new BufferedReader(new InputStreamReader(in));
        String contents = BuildConfig.FLAVOR;
        while (true) {
            try {
                String strLine = br.readLine();
                if (strLine == null) {
                    break;
                } else if (!strLine.startsWith(MagicStrings.text_comment_mark)) {
                    if (strLine.length() == 0) {
                        contents = contents + "\n";
                    } else {
                        contents = contents + strLine + "\n";
                    }
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        return contents.trim();
    }

    public static String getFile(String filename) {
        String contents = BuildConfig.FLAVOR;
        try {
            InputStream fstream = Alice.getContext().getAssets().open(filename);
            contents = getFileFromInputStream(fstream);
            fstream.close();
            return contents;
        } catch (IOException e) {
            System.err.println("Error: " + e.getMessage());
            return contents;
        }
    }

    public static String getCopyrightFromInputStream(InputStream in) {
        BufferedReader br = new BufferedReader(new InputStreamReader(in));
        String copyright = BuildConfig.FLAVOR;
        while (true) {
            try {
                String strLine = br.readLine();
                if (strLine == null) {
                    break;
                } else if (strLine.length() == 0) {
                    copyright = copyright + "\n";
                } else {
                    copyright = copyright + "<!-- " + strLine + " -->\n";
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        return copyright;
    }

    public static String getCopyright(Bot bot, String AIMLFilename) {
        String copyright = BuildConfig.FLAVOR;
        String year = CalendarUtils.year();
        String date = CalendarUtils.date();
        try {
            String[] splitCopyright = getFile(bot.config_path + "/copyright.txt").split("\n");
            copyright = BuildConfig.FLAVOR;
            for (String str : splitCopyright) {
                copyright = copyright + "<!-- " + str + " -->\n";
            }
            copyright = copyright.replace("[url]", bot.properties.get("url")).replace("[date]", date).replace("[YYYY]", year).replace("[version]", bot.properties.get("version")).replace("[botname]", bot.name.toUpperCase()).replace("[filename]", AIMLFilename).replace("[botmaster]", bot.properties.get("botmaster")).replace("[organization]", bot.properties.get("organization"));
        } catch (Exception e) {
            System.err.println("Error: " + e.getMessage());
        }
        return copyright + "<!--  -->\n";
    }

    public static String getPannousAPIKey(Bot bot) {
        String apiKey = getFile(bot.config_path + "/pannous-apikey.txt");
        if (apiKey.equals(BuildConfig.FLAVOR)) {
            return MagicStrings.pannous_api_key;
        }
        return apiKey;
    }

    public static String getPannousLogin(Bot bot) {
        String login = getFile(bot.config_path + "/pannous-login.txt");
        if (login.equals(BuildConfig.FLAVOR)) {
            return MagicStrings.pannous_login;
        }
        return login;
    }

    public static boolean isCharCJK(char c) {
        if (UnicodeBlock.of(c) == UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS || UnicodeBlock.of(c) == UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS_EXTENSION_A || UnicodeBlock.of(c) == UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS_EXTENSION_B || UnicodeBlock.of(c) == UnicodeBlock.CJK_COMPATIBILITY_FORMS || UnicodeBlock.of(c) == UnicodeBlock.CJK_COMPATIBILITY_IDEOGRAPHS || UnicodeBlock.of(c) == UnicodeBlock.CJK_RADICALS_SUPPLEMENT || UnicodeBlock.of(c) == UnicodeBlock.CJK_SYMBOLS_AND_PUNCTUATION || UnicodeBlock.of(c) == UnicodeBlock.ENCLOSED_CJK_LETTERS_AND_MONTHS) {
            return true;
        }
        return false;
    }
}
