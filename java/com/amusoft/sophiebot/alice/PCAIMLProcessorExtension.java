package com.amusoft.sophiebot.alice;

import java.util.Set;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import outlander.showcaseview.BuildConfig;

public class PCAIMLProcessorExtension implements AIMLProcessorExtension {
    public Set<String> extensionTagNames = Utilities.stringSet("contactid", "multipleids", "displayname", "dialnumber", "emailaddress", "contactbirthday", "addinfo");

    public Set<String> extensionTagSet() {
        return this.extensionTagNames;
    }

    private String newContact(Node node, ParseState ps) {
        NodeList childList = node.getChildNodes();
        String emailAddress = "unknown";
        String displayName = "unknown";
        String dialNumber = "unknown";
        String emailType = "unknown";
        String phoneType = "unknown";
        String birthday = "unknown";
        for (int i = 0; i < childList.getLength(); i++) {
            if (childList.item(i).getNodeName().equals("birthday")) {
                birthday = AIMLProcessor.evalTagContent(childList.item(i), ps, null);
            }
            if (childList.item(i).getNodeName().equals("phonetype")) {
                phoneType = AIMLProcessor.evalTagContent(childList.item(i), ps, null);
            }
            if (childList.item(i).getNodeName().equals("emailtype")) {
                emailType = AIMLProcessor.evalTagContent(childList.item(i), ps, null);
            }
            if (childList.item(i).getNodeName().equals("dialnumber")) {
                dialNumber = AIMLProcessor.evalTagContent(childList.item(i), ps, null);
            }
            if (childList.item(i).getNodeName().equals("displayname")) {
                displayName = AIMLProcessor.evalTagContent(childList.item(i), ps, null);
            }
            if (childList.item(i).getNodeName().equals("emailaddress")) {
                emailAddress = AIMLProcessor.evalTagContent(childList.item(i), ps, null);
            }
        }
        System.out.println("Adding new contact " + displayName + " " + phoneType + " " + dialNumber + " " + emailType + " " + emailAddress + " " + birthday);
        Contact contact = new Contact(displayName, phoneType, dialNumber, emailType, emailAddress, birthday);
        return BuildConfig.FLAVOR;
    }

    private String contactId(Node node, ParseState ps) {
        return Contact.contactId(AIMLProcessor.evalTagContent(node, ps, null));
    }

    private String multipleIds(Node node, ParseState ps) {
        return Contact.multipleIds(AIMLProcessor.evalTagContent(node, ps, null));
    }

    private String displayName(Node node, ParseState ps) {
        return Contact.displayName(AIMLProcessor.evalTagContent(node, ps, null));
    }

    private String dialNumber(Node node, ParseState ps) {
        NodeList childList = node.getChildNodes();
        String id = "unknown";
        String type = "unknown";
        for (int i = 0; i < childList.getLength(); i++) {
            if (childList.item(i).getNodeName().equals("id")) {
                id = AIMLProcessor.evalTagContent(childList.item(i), ps, null);
            }
            if (childList.item(i).getNodeName().equals("type")) {
                type = AIMLProcessor.evalTagContent(childList.item(i), ps, null);
            }
        }
        return Contact.dialNumber(type, id);
    }

    private String emailAddress(Node node, ParseState ps) {
        NodeList childList = node.getChildNodes();
        String id = "unknown";
        String type = "unknown";
        for (int i = 0; i < childList.getLength(); i++) {
            if (childList.item(i).getNodeName().equals("id")) {
                id = AIMLProcessor.evalTagContent(childList.item(i), ps, null);
            }
            if (childList.item(i).getNodeName().equals("type")) {
                type = AIMLProcessor.evalTagContent(childList.item(i), ps, null);
            }
        }
        return Contact.emailAddress(type, id);
    }

    private String contactBirthday(Node node, ParseState ps) {
        return Contact.birthday(AIMLProcessor.evalTagContent(node, ps, null));
    }

    public String recursEval(Node node, ParseState ps) {
        try {
            String nodeName = node.getNodeName();
            if (nodeName.equals("contactid")) {
                return contactId(node, ps);
            }
            if (nodeName.equals("multipleids")) {
                return multipleIds(node, ps);
            }
            if (nodeName.equals("dialnumber")) {
                return dialNumber(node, ps);
            }
            if (nodeName.equals("addinfo")) {
                return newContact(node, ps);
            }
            if (nodeName.equals("displayname")) {
                return displayName(node, ps);
            }
            if (nodeName.equals("emailaddress")) {
                return emailAddress(node, ps);
            }
            if (nodeName.equals("contactbirthday")) {
                return contactBirthday(node, ps);
            }
            return AIMLProcessor.genericXML(node, ps);
        } catch (Exception ex) {
            ex.printStackTrace();
            return BuildConfig.FLAVOR;
        }
    }
}
