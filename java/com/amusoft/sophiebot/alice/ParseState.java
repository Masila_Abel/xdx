package com.amusoft.sophiebot.alice;

public class ParseState {
    public Chat chatSession;
    public int depth;
    public String input;
    public Nodemapper leaf;
    public StarBindings starBindings;
    public String that;
    public String topic;
    public Predicates vars = new Predicates();

    public ParseState(int depth, Chat chatSession, String input, String that, String topic, Nodemapper leaf) {
        this.chatSession = chatSession;
        this.input = input;
        this.that = that;
        this.topic = topic;
        this.leaf = leaf;
        this.depth = depth;
        this.starBindings = leaf.starBindings;
    }
}
