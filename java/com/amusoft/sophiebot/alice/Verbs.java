package com.amusoft.sophiebot.alice;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class Verbs {
    public static HashSet<String> allVerbs = new HashSet();
    public static HashMap<String, String> be2been = new HashMap();
    public static HashMap<String, String> be2being = new HashMap();
    public static HashMap<String, String> be2is = new HashMap();
    public static HashMap<String, String> be2was = new HashMap();
    static Set<String> bing = Utilities.stringSet("ab", "at", "op", "el", "in", "ur", "op", "er", "un", "in", "it", "et", "ut", "im", "id", "ol", "ig");
    static Set<String> es = Utilities.stringSet("sh", "ch", "th", "ss", "x");
    static Set<String> ies = Utilities.stringSet("ly", "ry", "ny", "fy", "dy", "py");
    public static HashSet<String> irregular = new HashSet();
    static Set<String> notBing = Utilities.stringSet("der", "eat", "ber", "ain", "sit", "ait", "uit", "eet", "ter", "lop", "ver", "wer", "aim", "oid", "eel", "out", "oin", "fer", "vel", "mit");
    static Set<String> ring = Utilities.stringSet("be", "me", "re", "se", "ve", "de", "le", "ce", "ze", "ke", "te", "ge", "ne", "pe", "ue");

    public static String endsWith(String verb, Set<String> endings) {
        for (String x : endings) {
            if (verb.endsWith(x)) {
                return x;
            }
        }
        return null;
    }

    public static String is(String verb) {
        if (irregular.contains(verb)) {
            return (String) be2is.get(verb);
        }
        if (verb.endsWith("go")) {
            return verb + "es";
        }
        if (endsWith(verb, es) != null) {
            return verb + "es";
        }
        if (endsWith(verb, ies) != null) {
            return verb.substring(0, verb.length() - 1) + "ies";
        }
        return verb + "s";
    }

    public static String was(String verb) {
        verb = verb.trim();
        if (verb.equals("admit")) {
            return "admitted";
        }
        if (verb.equals("commit")) {
            return "committed";
        }
        if (verb.equals("die")) {
            return "died";
        }
        if (verb.equals("agree")) {
            return "agreed";
        }
        if (verb.endsWith("efer")) {
            return verb + "red";
        }
        if (irregular.contains(verb)) {
            return (String) be2was.get(verb);
        }
        if (endsWith(verb, ies) != null) {
            return verb.substring(0, verb.length() - 1) + "ied";
        }
        if (endsWith(verb, ring) != null) {
            return verb + "d";
        }
        String ending = endsWith(verb, bing);
        if (ending == null || endsWith(verb, notBing) != null) {
            return verb + "ed";
        }
        return verb + ending.substring(1, 2) + "ed";
    }

    public static String being(String verb) {
        if (irregular.contains(verb)) {
            return (String) be2being.get(verb);
        }
        if (verb.equals("admit")) {
            return "admitting";
        }
        if (verb.equals("commit")) {
            return "committing";
        }
        if (verb.equals("quit")) {
            return "quitting";
        }
        if (verb.equals("die")) {
            return "dying";
        }
        if (verb.equals("lie")) {
            return "lying";
        }
        if (verb.endsWith("efer")) {
            return verb + "ring";
        }
        if (endsWith(verb, ring) != null) {
            return verb.substring(0, verb.length() - 1) + "ing";
        }
        String ending = endsWith(verb, bing);
        if (ending == null || endsWith(verb, notBing) != null) {
            return verb + "ing";
        }
        return verb + ending.substring(1, 2) + "ing";
    }

    public static String been(String verb) {
        if (irregular.contains(verb)) {
            return (String) be2been.get(verb);
        }
        return was(verb);
    }

    public static void getIrregulars() {
        for (String x : Utilities.getFile("c:/ab/data/irrverbs.txt").split("\n")) {
            String[] triple = x.toLowerCase().split(",");
            if (triple.length == 5) {
                irregular.add(triple[0]);
                allVerbs.add(triple[0]);
                be2was.put(triple[0], triple[1]);
                be2been.put(triple[0], triple[2]);
                be2is.put(triple[0], triple[3]);
                be2being.put(triple[0], triple[4]);
            }
        }
    }

    public static void makeVerbSetsMaps(Bot bot) {
        getIrregulars();
        for (String verb : Utilities.getFile("c:/ab/data/verb300.txt").split("\n")) {
            String verb2;
            allVerbs.add(verb2);
        }
        AIMLSet be = new AIMLSet("be", bot);
        AIMLSet is = new AIMLSet("is", bot);
        AIMLSet aIMLSet = new AIMLSet("was", bot);
        AIMLSet been = new AIMLSet("been", bot);
        AIMLSet being = new AIMLSet("being", bot);
        AIMLMap aIMLMap = new AIMLMap("is2be", bot);
        AIMLMap be2is = new AIMLMap("be2is", bot);
        aIMLMap = new AIMLMap("was2be", bot);
        AIMLMap be2was = new AIMLMap("be2was", bot);
        AIMLMap been2be = new AIMLMap("been2be", bot);
        AIMLMap be2been = new AIMLMap("be2been", bot);
        AIMLMap be2being = new AIMLMap("be2being", bot);
        AIMLMap being2be = new AIMLMap("being2be", bot);
        Iterator it = allVerbs.iterator();
        while (it.hasNext()) {
            verb2 = (String) it.next();
            String beForm = verb2;
            String isForm = is(verb2);
            String wasForm = was(verb2);
            String beenForm = been(verb2);
            String beingForm = being(verb2);
            System.out.println(verb2 + "," + isForm + "," + wasForm + "," + beingForm + "," + beenForm);
            be.add(beForm);
            is.add(isForm);
            aIMLSet.add(wasForm);
            been.add(beenForm);
            being.add(beingForm);
            be2is.put(beForm, isForm);
            aIMLMap.put(isForm, beForm);
            be2was.put(beForm, wasForm);
            aIMLMap.put(wasForm, beForm);
            be2been.put(beForm, beenForm);
            been2be.put(beenForm, beForm);
            be2being.put(beForm, beingForm);
            being2be.put(beingForm, beForm);
        }
        be.writeAIMLSet();
        is.writeAIMLSet();
        aIMLSet.writeAIMLSet();
        been.writeAIMLSet();
        being.writeAIMLSet();
        be2is.writeAIMLMap();
        aIMLMap.writeAIMLMap();
        be2was.writeAIMLMap();
        aIMLMap.writeAIMLMap();
        be2been.writeAIMLMap();
        been2be.writeAIMLMap();
        be2being.writeAIMLMap();
        being2be.writeAIMLMap();
    }
}
