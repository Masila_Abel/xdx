package com.amusoft.sophiebot.alice;

import java.text.DecimalFormat;
import outlander.showcaseview.BuildConfig;
import outlander.showcaseview.R;

public class EnglishNumberToWords {
    private static final String[] numNames = new String[]{BuildConfig.FLAVOR, " one", " two", " three", " four", " five", " six", " seven", " eight", " nine", " ten", " eleven", " twelve", " thirteen", " fourteen", " fifteen", " sixteen", " seventeen", " eighteen", " nineteen"};
    private static final String[] tensNames = new String[]{BuildConfig.FLAVOR, " ten", " twenty", " thirty", " forty", " fifty", " sixty", " seventy", " eighty", " ninety"};

    private EnglishNumberToWords() {
    }

    private static String convertLessThanOneThousand(int number) {
        String soFar;
        if (number % 100 < 20) {
            soFar = numNames[number % 100];
            number /= 100;
        } else {
            soFar = numNames[number % 10];
            number /= 10;
            soFar = tensNames[number % 10] + soFar;
            number /= 10;
        }
        return number == 0 ? soFar : numNames[number] + " hundred" + soFar;
    }

    public static String convert(long number) {
        if (number == 0) {
            return "zero";
        }
        String tradBillions;
        String tradMillions;
        String tradHundredThousands;
        String snumber = Long.toString(number);
        snumber = new DecimalFormat("000000000000").format(number);
        int billions = Integer.parseInt(snumber.substring(0, 3));
        int millions = Integer.parseInt(snumber.substring(3, 6));
        int hundredThousands = Integer.parseInt(snumber.substring(6, 9));
        int thousands = Integer.parseInt(snumber.substring(9, 12));
        switch (billions) {
            case R.styleable.View_android_theme /*0*/:
                tradBillions = BuildConfig.FLAVOR;
                break;
            case R.styleable.View_android_focusable /*1*/:
                tradBillions = convertLessThanOneThousand(billions) + " billion ";
                break;
            default:
                tradBillions = convertLessThanOneThousand(billions) + " billion ";
                break;
        }
        String result = tradBillions;
        switch (millions) {
            case R.styleable.View_android_theme /*0*/:
                tradMillions = BuildConfig.FLAVOR;
                break;
            case R.styleable.View_android_focusable /*1*/:
                tradMillions = convertLessThanOneThousand(millions) + " million ";
                break;
            default:
                tradMillions = convertLessThanOneThousand(millions) + " million ";
                break;
        }
        result = result + tradMillions;
        switch (hundredThousands) {
            case R.styleable.View_android_theme /*0*/:
                tradHundredThousands = BuildConfig.FLAVOR;
                break;
            case R.styleable.View_android_focusable /*1*/:
                tradHundredThousands = "one thousand ";
                break;
            default:
                tradHundredThousands = convertLessThanOneThousand(hundredThousands) + " thousand ";
                break;
        }
        result = result + tradHundredThousands;
        return (result + convertLessThanOneThousand(thousands)).replaceAll("^\\s+", BuildConfig.FLAVOR).replaceAll("\\b\\s{2,}\\b", " ");
    }

    public static void makeSetMap(Bot bot) {
        AIMLSet numberName = new AIMLSet("numbername", bot);
        AIMLMap name2number = new AIMLMap("name2number", bot);
        for (int i = 0; i < 10000; i++) {
            String name = convert((long) i).trim();
            String number = BuildConfig.FLAVOR + i;
            numberName.add(name);
            name2number.put(name, number);
            if (i == 1000) {
                System.out.println("Name2number(" + name + ")=" + number);
            }
        }
        numberName.writeAIMLSet();
        name2number.writeAIMLMap();
    }

    public static void main(String[] args) {
        System.out.println("*** " + convert(0));
        System.out.println("*** " + convert(1));
        System.out.println("*** " + convert(16));
        System.out.println("*** " + convert(100));
        System.out.println("*** " + convert(118));
        System.out.println("*** " + convert(200));
        System.out.println("*** " + convert(219));
        System.out.println("*** " + convert(800));
        System.out.println("*** " + convert(801));
        System.out.println("*** " + convert(1316));
        System.out.println("*** " + convert(1000000));
        System.out.println("*** " + convert(2000000));
        System.out.println("*** " + convert(3000200));
        System.out.println("*** " + convert(700000));
        System.out.println("*** " + convert(9000000));
        System.out.println("*** " + convert(9001000));
        System.out.println("*** " + convert(123456789));
        System.out.println("*** " + convert(2147483647L));
        System.out.println("*** " + convert(3000000010L));
    }
}
