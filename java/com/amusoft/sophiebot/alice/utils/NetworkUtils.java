package com.amusoft.sophiebot.alice.utils;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.URI;
import java.net.URLEncoder;
import java.util.Enumeration;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import outlander.showcaseview.BuildConfig;

public class NetworkUtils {
    public static String localIPAddress() {
        try {
            Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces();
            while (en.hasMoreElements()) {
                Enumeration<InetAddress> enumIpAddr = ((NetworkInterface) en.nextElement()).getInetAddresses();
                while (enumIpAddr.hasMoreElements()) {
                    InetAddress inetAddress = (InetAddress) enumIpAddr.nextElement();
                    if (!inetAddress.isLoopbackAddress()) {
                        String ipAddress = inetAddress.getHostAddress().toString();
                        int p = ipAddress.indexOf("%");
                        if (p > 0) {
                            return ipAddress.substring(0, p);
                        }
                        return ipAddress;
                    }
                }
            }
        } catch (SocketException ex) {
            ex.printStackTrace();
        }
        return "127.0.0.1";
    }

    public static String responseContent(String url) throws Exception {
        HttpClient client = new DefaultHttpClient();
        HttpGet request = new HttpGet();
        request.setURI(new URI(url));
        BufferedReader inb = new BufferedReader(new InputStreamReader(client.execute(request).getEntity().getContent()));
        StringBuilder sb = new StringBuilder(BuildConfig.FLAVOR);
        String NL = System.getProperty("line.separator");
        while (true) {
            String line = inb.readLine();
            if (line != null) {
                sb.append(line).append(NL);
            } else {
                inb.close();
                return sb.toString();
            }
        }
    }

    public static String responseContentUri(URI uri) throws Exception {
        HttpClient client = new DefaultHttpClient();
        HttpPost request = new HttpPost();
        request.setURI(uri);
        BufferedReader inb = new BufferedReader(new InputStreamReader(client.execute(request).getEntity().getContent()));
        StringBuilder sb = new StringBuilder(BuildConfig.FLAVOR);
        String NL = System.getProperty("line.separator");
        while (true) {
            String line = inb.readLine();
            if (line != null) {
                sb.append(line).append(NL);
            } else {
                inb.close();
                return sb.toString();
            }
        }
    }

    public static String spec(String host, String botid, String custid, String input) {
        String spec = BuildConfig.FLAVOR;
        try {
            if (custid.equals("0")) {
                spec = String.format("%s?botid=%s&input=%s", new Object[]{"http://" + host + "/pandora/talk-xml", botid, URLEncoder.encode(input, "UTF-8")});
            } else {
                spec = String.format("%s?botid=%s&custid=%s&input=%s", new Object[]{"http://" + host + "/pandora/talk-xml", botid, custid, URLEncoder.encode(input, "UTF-8")});
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        System.out.println(spec);
        return spec;
    }
}
