package com.amusoft.sophiebot.alice.utils;

import org.joda.time.Days;
import org.joda.time.Hours;
import org.joda.time.Months;
import org.joda.time.Years;
import org.joda.time.chrono.GregorianChronology;
import org.joda.time.chrono.LenientChronology;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

public class IntervalUtils {
    public static void test() {
        System.out.println("Hours = " + getHoursBetween("12:00:00.00", "23:59:59.00", "HH:mm:ss.SS"));
        System.out.println("Years = " + getYearsBetween("August 2, 1960", "January 30, 2013", "MMMMMMMMM dd, yyyy"));
    }

    public static int getHoursBetween(String date1, String date2, String format) {
        try {
            DateTimeFormatter fmt = DateTimeFormat.forPattern(format).withChronology(LenientChronology.getInstance(GregorianChronology.getInstance()));
            return Hours.hoursBetween(fmt.parseDateTime(date1), fmt.parseDateTime(date2)).getHours();
        } catch (Exception ex) {
            ex.printStackTrace();
            return 0;
        }
    }

    public static int getYearsBetween(String date1, String date2, String format) {
        try {
            DateTimeFormatter fmt = DateTimeFormat.forPattern(format).withChronology(LenientChronology.getInstance(GregorianChronology.getInstance()));
            return Years.yearsBetween(fmt.parseDateTime(date1), fmt.parseDateTime(date2)).getYears();
        } catch (Exception ex) {
            ex.printStackTrace();
            return 0;
        }
    }

    public static int getMonthsBetween(String date1, String date2, String format) {
        try {
            DateTimeFormatter fmt = DateTimeFormat.forPattern(format).withChronology(LenientChronology.getInstance(GregorianChronology.getInstance()));
            return Months.monthsBetween(fmt.parseDateTime(date1), fmt.parseDateTime(date2)).getMonths();
        } catch (Exception ex) {
            ex.printStackTrace();
            return 0;
        }
    }

    public static int getDaysBetween(String date1, String date2, String format) {
        try {
            DateTimeFormatter fmt = DateTimeFormat.forPattern(format).withChronology(LenientChronology.getInstance(GregorianChronology.getInstance()));
            return Days.daysBetween(fmt.parseDateTime(date1), fmt.parseDateTime(date2)).getDays();
        } catch (Exception ex) {
            ex.printStackTrace();
            return 0;
        }
    }
}
