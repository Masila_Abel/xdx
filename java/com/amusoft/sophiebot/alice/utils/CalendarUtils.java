package com.amusoft.sophiebot.alice.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class CalendarUtils {
    public static String formatTime(String formatString, long msSinceEpoch) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(formatString);
        dateFormat.setCalendar(Calendar.getInstance());
        return dateFormat.format(new Date(msSinceEpoch));
    }

    public static int timeZoneOffset() {
        Calendar cal = Calendar.getInstance();
        return (cal.get(15) + cal.get(16)) / 60000;
    }

    public static String year() {
        return String.valueOf(Calendar.getInstance().get(1));
    }

    public static String date() {
        Calendar cal = Calendar.getInstance();
        int year = cal.get(1);
        SimpleDateFormat dateFormat = new SimpleDateFormat("MMMMMMMMM dd, yyyy");
        dateFormat.setCalendar(cal);
        return dateFormat.format(cal.getTime());
    }

    public static String date(String jformat, String locale, String timezone) {
        if (jformat == null) {
            jformat = "EEE MMM dd HH:mm:ss zzz yyyy";
        }
        if (locale == null) {
            locale = Locale.US.getISO3Country();
        }
        if (timezone == null) {
            timezone = TimeZone.getDefault().getDisplayName();
        }
        String dateAsString = new Date().toString();
        try {
            dateAsString = new SimpleDateFormat(jformat).format(new Date());
        } catch (Exception ex) {
            System.out.println("CalendarUtils.date Bad date: Format = " + jformat + " Locale = " + locale + " Timezone = " + timezone);
            ex.printStackTrace();
        }
        return dateAsString;
    }
}
