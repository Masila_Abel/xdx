package com.amusoft.sophiebot.alice.utils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import outlander.showcaseview.BuildConfig;

public class IOUtils {
    BufferedReader reader;
    BufferedWriter writer;

    public IOUtils(String filePath, String mode) {
        try {
            if (mode.equals("read")) {
                this.reader = new BufferedReader(new FileReader(filePath));
            } else if (mode.equals("write")) {
                new File(filePath).delete();
                this.writer = new BufferedWriter(new FileWriter(filePath, true));
            }
        } catch (IOException e) {
            System.err.println("error: " + e);
        }
    }

    public String readLine() {
        String result = null;
        try {
            result = this.reader.readLine();
        } catch (IOException e) {
            System.err.println("error: " + e);
        }
        return result;
    }

    public void writeLine(String line) {
        try {
            this.writer.write(line);
            this.writer.newLine();
        } catch (IOException e) {
            System.err.println("error: " + e);
        }
    }

    public void close() {
        try {
            if (this.reader != null) {
                this.reader.close();
            }
            if (this.writer != null) {
                this.writer.close();
            }
        } catch (IOException e) {
            System.err.println("error: " + e);
        }
    }

    public static void writeOutputTextLine(String prompt, String text) {
        System.out.println(prompt + ": " + text);
    }

    public static String readInputTextLine() {
        return readInputTextLine(null);
    }

    public static String readInputTextLine(String prompt) {
        if (prompt != null) {
            System.out.print(prompt + ": ");
        }
        String textLine = null;
        try {
            textLine = new BufferedReader(new InputStreamReader(System.in)).readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return textLine;
    }

    public static File[] listFiles(File dir) {
        return dir.listFiles();
    }

    public static String system(String evaluatedContents, String failedString) {
        try {
            BufferedReader buffrdr = new BufferedReader(new InputStreamReader(Runtime.getRuntime().exec(evaluatedContents).getInputStream()));
            String result = BuildConfig.FLAVOR;
            String str = BuildConfig.FLAVOR;
            while (true) {
                str = buffrdr.readLine();
                if (str == null) {
                    return result;
                }
                result = result + str + "\n";
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return failedString;
        }
    }

    public static String evalScript(String engineName, String script) throws Exception {
        return BuildConfig.FLAVOR;
    }
}
