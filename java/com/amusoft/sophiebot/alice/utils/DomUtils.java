package com.amusoft.sophiebot.alice.utils;

import com.amusoft.sophiebot.alice.Alice;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.io.StringWriter;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;

public class DomUtils {
    public static Node parseFile(String fileName) throws Exception {
        Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new InputSource(new StringReader(getFileFromAssets(fileName))));
        doc.getDocumentElement().normalize();
        return doc.getDocumentElement();
    }

    private static String getFileFromAssets(String fileName) {
        StringBuilder buf = new StringBuilder();
        try {
            BufferedReader in = new BufferedReader(new InputStreamReader(Alice.getContext().getAssets().open(fileName), "UTF-8"));
            while (true) {
                String str = in.readLine();
                if (str == null) {
                    break;
                }
                buf.append(str);
            }
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return buf.toString();
    }

    public static Node parseString(String string) throws Exception {
        Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new ByteArrayInputStream(string.getBytes("UTF-16")));
        doc.getDocumentElement().normalize();
        return doc.getDocumentElement();
    }

    public static String nodeToString(Node node) {
        StringWriter sw = new StringWriter();
        try {
            Transformer t = TransformerFactory.newInstance().newTransformer();
            t.setOutputProperty("omit-xml-declaration", "yes");
            t.setOutputProperty("indent", "no");
            t.transform(new DOMSource(node), new StreamResult(sw));
        } catch (TransformerException e) {
            System.out.println("nodeToString Transformer Exception");
        }
        return sw.toString();
    }
}
