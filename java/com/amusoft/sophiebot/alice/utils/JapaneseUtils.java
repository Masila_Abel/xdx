package com.amusoft.sophiebot.alice.utils;

import com.amusoft.sophiebot.alice.AIMLProcessor;
import com.amusoft.sophiebot.alice.MagicBooleans;
import com.amusoft.sophiebot.alice.MagicStrings;
import net.reduls.sanmoku.Morpheme;
import net.reduls.sanmoku.Tagger;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import outlander.showcaseview.BuildConfig;

public class JapaneseUtils {
    public static String tokenizeFragment(String fragment) {
        String result = BuildConfig.FLAVOR;
        for (Morpheme e : Tagger.parse(fragment)) {
            result = result + e.surface + " ";
        }
        return result.trim();
    }

    public static String tokenizeSentence(String sentence) {
        if (!MagicBooleans.jp_tokenize) {
            return sentence;
        }
        String str = BuildConfig.FLAVOR;
        str = tokenizeXML(sentence);
        while (str.contains("$ ")) {
            str = str.replace("$ ", "$");
        }
        while (str.contains("  ")) {
            str = str.replace("  ", " ");
        }
        while (str.contains("anon ")) {
            str = str.replace("anon ", "anon");
        }
        return str.trim();
    }

    public static String tokenizeXML(String xmlExpression) {
        String response = MagicStrings.template_failed;
        try {
            response = recursEval(DomUtils.parseString("<sentence>" + xmlExpression + "</sentence>"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return AIMLProcessor.trimTag(response, "sentence");
    }

    private static String recursEval(Node node) {
        try {
            String nodeName = node.getNodeName();
            if (nodeName.equals("#text")) {
                return tokenizeFragment(node.getNodeValue());
            }
            if (nodeName.equals("sentence")) {
                return evalTagContent(node);
            }
            return genericXML(node);
        } catch (Exception ex) {
            ex.printStackTrace();
            return "JP Morph Error";
        }
    }

    public static String genericXML(Node node) {
        return unevaluatedXML(evalTagContent(node), node);
    }

    public static String evalTagContent(Node node) {
        String result = BuildConfig.FLAVOR;
        try {
            NodeList childList = node.getChildNodes();
            for (int i = 0; i < childList.getLength(); i++) {
                result = result + recursEval(childList.item(i));
            }
        } catch (Exception ex) {
            System.out.println("Something went wrong with evalTagContent");
            ex.printStackTrace();
        }
        return result;
    }

    private static String unevaluatedXML(String result, Node node) {
        String nodeName = node.getNodeName();
        String attributes = BuildConfig.FLAVOR;
        if (node.hasAttributes()) {
            NamedNodeMap XMLAttributes = node.getAttributes();
            for (int i = 0; i < XMLAttributes.getLength(); i++) {
                attributes = attributes + " " + XMLAttributes.item(i).getNodeName() + "=\"" + XMLAttributes.item(i).getNodeValue() + "\"";
            }
        }
        if (result.equals(BuildConfig.FLAVOR)) {
            return " <" + nodeName + attributes + "/> ";
        }
        return " <" + nodeName + attributes + ">" + result + "</" + nodeName + "> ";
    }
}
