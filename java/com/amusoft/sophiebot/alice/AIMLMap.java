package com.amusoft.sophiebot.alice;

import com.amusoft.sophiebot.BrainLogger;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;

public class AIMLMap extends HashMap<String, String> {
    Bot bot;
    String botid;
    String host;
    Inflector inflector = new Inflector();
    boolean isExternal = false;
    public String mapName;

    public AIMLMap(String name, Bot bot) {
        this.bot = bot;
        this.mapName = name;
    }

    public String get(String key) {
        if (this.mapName.equals(MagicStrings.map_successor)) {
            try {
                return String.valueOf(Integer.parseInt(key) + 1);
            } catch (Exception e) {
                return MagicStrings.default_map;
            }
        } else if (this.mapName.equals(MagicStrings.map_predecessor)) {
            try {
                return String.valueOf(Integer.parseInt(key) - 1);
            } catch (Exception e2) {
                return MagicStrings.default_map;
            }
        } else if (this.mapName.equals("singular")) {
            return this.inflector.singularize(key).toLowerCase();
        } else {
            if (this.mapName.equals("plural")) {
                return this.inflector.pluralize(key).toLowerCase();
            }
            String value;
            if (this.isExternal && MagicBooleans.enable_external_sets) {
                String response = Sraix.sraix(null, this.mapName.toUpperCase() + " " + key, MagicStrings.default_map, null, this.host, this.botid, null, "0");
                System.out.println("External " + this.mapName + "(" + key + ")=" + response);
                value = response;
            } else {
                value = (String) super.get(key);
            }
            if (value == null) {
                return MagicStrings.default_map;
            }
            return value;
        }
    }

    public String put(String key, String value) {
        return (String) super.put(key, value);
    }

    public void writeAIMLMap() {
        System.out.println("Writing AIML Map " + this.mapName);
        try {
            BufferedWriter out = new BufferedWriter(new FileWriter(this.bot.maps_path + "/" + this.mapName + ".txt"));
            for (String p : keySet()) {
                String p2 = p2.trim();
                out.write(p2 + ":" + get(p2).trim());
                out.newLine();
            }
            out.close();
        } catch (Exception e) {
            System.err.println("Error: " + e.getMessage());
        }
    }

    public int readAIMLMapFromInputStream(InputStream in, Bot bot) {
        int cnt = 0;
        BufferedReader br = new BufferedReader(new InputStreamReader(in));
        while (true) {
            try {
                String strLine = br.readLine();
                if (strLine == null || strLine.length() <= 0) {
                    break;
                }
                String[] splitLine = strLine.split(":");
                if (splitLine.length >= 2) {
                    cnt++;
                    if (!strLine.startsWith(MagicStrings.remote_map_key)) {
                        put(splitLine[0].toUpperCase(), splitLine[1]);
                    } else if (splitLine.length >= 3) {
                        this.host = splitLine[1];
                        this.botid = splitLine[2];
                        this.isExternal = true;
                        System.out.println("Created external map at " + this.host + " " + this.botid);
                    }
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        return cnt;
    }

    public int readAIMLMap(Bot bot) {
        int cnt = 0;
        if (MagicBooleans.trace_mode) {
            System.out.println("Reading AIML Map " + bot.maps_path + "/" + this.mapName + ".txt");
            BrainLogger.getInstance().info("Reading AIML Map " + bot.maps_path + "/" + this.mapName + ".txt");
        }
        try {
            InputStream fstream = Alice.getContext().getAssets().open(bot.maps_path + "/" + this.mapName + ".txt");
            cnt = readAIMLMapFromInputStream(fstream, bot);
            fstream.close();
            return cnt;
        } catch (IOException e) {
            System.err.println(bot.maps_path + "/" + this.mapName + ".txt Error: " + e.getMessage());
            e.printStackTrace();
            return cnt;
        }
    }
}
