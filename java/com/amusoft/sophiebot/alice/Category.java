package com.amusoft.sophiebot.alice;

import java.util.Comparator;
import outlander.showcaseview.BuildConfig;

public class Category {
    public static Comparator<Category> ACTIVATION_COMPARATOR = new Comparator<Category>() {
        public int compare(Category c1, Category c2) {
            return c2.getActivationCnt() - c1.getActivationCnt();
        }
    };
    public static Comparator<Category> CATEGORY_NUMBER_COMPARATOR = new Comparator<Category>() {
        public int compare(Category c1, Category c2) {
            return c1.getCategoryNumber() - c2.getCategoryNumber();
        }
    };
    public static Comparator<Category> PATTERN_COMPARATOR = new Comparator<Category>() {
        public int compare(Category c1, Category c2) {
            return String.CASE_INSENSITIVE_ORDER.compare(c1.inputThatTopic(), c2.inputThatTopic());
        }
    };
    public static int categoryCnt = 0;
    private int activationCnt;
    private int categoryNumber;
    private String filename;
    private AIMLSet matches;
    private String pattern;
    private String template;
    private String that;
    private String topic;
    public String validationMessage;

    public AIMLSet getMatches(Bot bot) {
        if (this.matches != null) {
            return this.matches;
        }
        return new AIMLSet("No Matches", bot);
    }

    public int getActivationCnt() {
        return this.activationCnt;
    }

    public int getCategoryNumber() {
        return this.categoryNumber;
    }

    public String getPattern() {
        if (this.pattern == null) {
            return "*";
        }
        return this.pattern;
    }

    public String getThat() {
        if (this.that == null) {
            return "*";
        }
        return this.that;
    }

    public String getTopic() {
        if (this.topic == null) {
            return "*";
        }
        return this.topic;
    }

    public String getTemplate() {
        if (this.template == null) {
            return BuildConfig.FLAVOR;
        }
        return this.template;
    }

    public String getFilename() {
        if (this.filename == null) {
            return MagicStrings.unknown_aiml_file;
        }
        return this.filename;
    }

    public void incrementActivationCnt() {
        this.activationCnt++;
    }

    public void setActivationCnt(int cnt) {
        this.activationCnt = cnt;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public void setTemplate(String template) {
        this.template = template;
    }

    public void setPattern(String pattern) {
        this.pattern = pattern;
    }

    public void setThat(String that) {
        this.that = that;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String inputThatTopic() {
        return Graphmaster.inputThatTopic(this.pattern, this.that, this.topic);
    }

    public void addMatch(String input, Bot bot) {
        if (this.matches == null) {
            this.matches = new AIMLSet(inputThatTopic().replace("*", "STAR").replace("_", "UNDERSCORE").replace(" ", "-").replace("<THAT>", "THAT").replace("<TOPIC>", "TOPIC"), bot);
        }
        this.matches.add(input);
    }

    public static String templateToLine(String template) {
        return template.replaceAll("(\r\n|\n\r|\r|\n)", "\\#Newline").replaceAll(MagicStrings.aimlif_split_char, MagicStrings.aimlif_split_char_name);
    }

    private static String lineToTemplate(String line) {
        return line.replaceAll("\\#Newline", "\n").replaceAll(MagicStrings.aimlif_split_char_name, MagicStrings.aimlif_split_char);
    }

    public static Category IFToCategory(String IF) {
        String[] split = IF.split(MagicStrings.aimlif_split_char);
        return new Category(Integer.parseInt(split[0]), split[1], split[2], split[3], lineToTemplate(split[4]), split[5]);
    }

    public static String categoryToIF(Category category) {
        String c = MagicStrings.aimlif_split_char;
        return category.getActivationCnt() + c + category.getPattern() + c + category.getThat() + c + category.getTopic() + c + templateToLine(category.getTemplate()) + c + category.getFilename();
    }

    public static String categoryToAIML(Category category) {
        String topicStart = BuildConfig.FLAVOR;
        String topicEnd = BuildConfig.FLAVOR;
        String thatStatement = BuildConfig.FLAVOR;
        String result = BuildConfig.FLAVOR;
        String pattern = category.getPattern();
        if (pattern.contains("<SET>") || pattern.contains("<BOT")) {
            String[] splitPattern = pattern.split(" ");
            String rpattern = BuildConfig.FLAVOR;
            for (String w : splitPattern) {
                String w2;
                if (w2.startsWith("<SET>") || w2.startsWith("<BOT") || w2.startsWith("NAME=")) {
                    w2 = w2.toLowerCase();
                }
                rpattern = rpattern + " " + w2;
            }
            pattern = rpattern.trim();
        }
        String property = System.getProperty("line.separator");
        property = "\n";
        try {
            if (!category.getTopic().equals("*")) {
                topicStart = "<topic name=\"" + category.getTopic() + "\">" + property;
                topicEnd = "</topic>" + property;
            }
            if (!category.getThat().equals("*")) {
                thatStatement = "<that>" + category.getThat() + "</that>";
            }
            result = topicStart + "<category><pattern>" + pattern + "</pattern>" + thatStatement + property + "<template>" + category.getTemplate() + "</template>" + property + "</category>" + topicEnd;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return result;
    }

    public boolean validPatternForm(String pattern) {
        if (pattern.length() < 1) {
            this.validationMessage += "Zero length. ";
            return false;
        }
        for (int i = 0; i < pattern.split(" ").length; i++) {
        }
        return true;
    }

    public boolean validate() {
        this.validationMessage = BuildConfig.FLAVOR;
        if (!validPatternForm(this.pattern)) {
            this.validationMessage += "Badly formatted <pattern>";
            return false;
        } else if (!validPatternForm(this.that)) {
            this.validationMessage += "Badly formatted <that>";
            return false;
        } else if (!validPatternForm(this.topic)) {
            this.validationMessage += "Badly formatted <topic>";
            return false;
        } else if (!AIMLProcessor.validTemplate(this.template)) {
            this.validationMessage += "Badly formatted <template>";
            return false;
        } else if (this.filename.endsWith(".aiml")) {
            return true;
        } else {
            this.validationMessage += "Filename suffix should be .aiml";
            return false;
        }
    }

    public Category(int activationCnt, String pattern, String that, String topic, String template, String filename) {
        this.validationMessage = BuildConfig.FLAVOR;
        if (MagicBooleans.fix_excel_csv) {
            pattern = Utilities.fixCSV(pattern);
            that = Utilities.fixCSV(that);
            topic = Utilities.fixCSV(topic);
            template = Utilities.fixCSV(template);
            filename = Utilities.fixCSV(filename);
        }
        this.pattern = pattern.trim().toUpperCase();
        this.that = that.trim().toUpperCase();
        this.topic = topic.trim().toUpperCase();
        this.template = template.replace("& ", " and ");
        this.filename = filename;
        this.activationCnt = activationCnt;
        this.matches = null;
        int i = categoryCnt;
        categoryCnt = i + 1;
        this.categoryNumber = i;
    }

    public Category(int activationCnt, String patternThatTopic, String template, String filename) {
        this(activationCnt, patternThatTopic.substring(0, patternThatTopic.indexOf("<THAT>")), patternThatTopic.substring(patternThatTopic.indexOf("<THAT>") + "<THAT>".length(), patternThatTopic.indexOf("<TOPIC>")), patternThatTopic.substring(patternThatTopic.indexOf("<TOPIC>") + "<TOPIC>".length(), patternThatTopic.length()), template, filename);
    }
}
