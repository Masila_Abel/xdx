package com.amusoft.sophiebot.alice;

import com.amusoft.sophiebot.BrainLogger;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashSet;
import java.util.Iterator;
import java.util.regex.Pattern;

public class AIMLSet extends HashSet<String> {
    Bot bot;
    String botid;
    String host;
    private HashSet<String> inCache = new HashSet();
    boolean isExternal = false;
    int maxLength = 1;
    private HashSet<String> outCache = new HashSet();
    public String setName;

    public AIMLSet(String name, Bot bot) {
        this.bot = bot;
        this.setName = name.toLowerCase();
        if (this.setName.equals(MagicStrings.natural_number_set_name)) {
            this.maxLength = 1;
        }
    }

    public boolean contains(String s) {
        if (this.isExternal && MagicBooleans.enable_external_sets) {
            if (this.inCache.contains(s)) {
                return true;
            }
            if (this.outCache.contains(s)) {
                return false;
            }
            if (s.split(" ").length > this.maxLength) {
                return false;
            }
            if (Sraix.sraix(null, MagicStrings.set_member_string + this.setName.toUpperCase() + " " + s, "false", null, this.host, this.botid, null, "0").equals("true")) {
                this.inCache.add(s);
                return true;
            }
            this.outCache.add(s);
            return false;
        } else if (this.setName.equals(MagicStrings.natural_number_set_name)) {
            return Boolean.valueOf(Pattern.compile("[0-9]+").matcher(s).matches()).booleanValue();
        } else {
            return super.contains(s);
        }
    }

    public void writeAIMLSet() {
        System.out.println("Writing AIML Set " + this.setName);
        try {
            BufferedWriter out = new BufferedWriter(new FileWriter(this.bot.sets_path + "/" + this.setName + ".txt"));
            Iterator it = iterator();
            while (it.hasNext()) {
                out.write(((String) it.next()).trim());
                out.newLine();
            }
            out.close();
        } catch (Exception e) {
            System.err.println("Error: " + e.getMessage());
        }
    }

    public int readAIMLSetFromInputStream(InputStream in, Bot bot) {
        BufferedReader br = new BufferedReader(new InputStreamReader(in));
        int cnt = 0;
        while (true) {
            try {
                String strLine = br.readLine();
                if (strLine == null || strLine.length() <= 0) {
                    break;
                }
                cnt++;
                if (strLine.startsWith("external")) {
                    String[] splitLine = strLine.split(":");
                    if (splitLine.length >= 4) {
                        this.host = splitLine[1];
                        this.botid = splitLine[2];
                        this.maxLength = Integer.parseInt(splitLine[3]);
                        this.isExternal = true;
                        System.out.println("Created external set at " + this.host + " " + this.botid);
                    }
                } else {
                    strLine = strLine.toUpperCase().trim();
                    int length = strLine.split(" ").length;
                    if (length > this.maxLength) {
                        this.maxLength = length;
                    }
                    add(strLine.trim());
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        return cnt;
    }

    public int readAIMLSet(Bot bot) {
        int cnt = 0;
        if (MagicBooleans.trace_mode) {
            System.out.println("Reading AIML Set " + bot.sets_path + "/" + this.setName + ".txt");
            BrainLogger.getInstance().info("Reading AIML Set " + bot.sets_path + "/" + this.setName + ".txt");
        }
        try {
            InputStream fstream = Alice.getContext().getAssets().open(bot.sets_path + "/" + this.setName + ".txt");
            cnt = readAIMLSetFromInputStream(fstream, bot);
            fstream.close();
            return cnt;
        } catch (IOException e) {
            System.err.println(bot.sets_path + "/" + this.setName + ".txt Error: " + e.getMessage());
            e.printStackTrace();
            return cnt;
        }
    }
}
