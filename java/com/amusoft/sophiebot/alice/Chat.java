package com.amusoft.sophiebot.alice;

import com.amusoft.sophiebot.alice.utils.IOUtils;
import com.amusoft.sophiebot.alice.utils.JapaneseUtils;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import outlander.showcaseview.BuildConfig;

public class Chat {
    public static String latitude;
    public static boolean locationKnown = false;
    public static String longitude;
    public static String matchTrace = BuildConfig.FLAVOR;
    public Bot bot;
    public String customerId;
    public boolean doWrites;
    public History<String> inputHistory;
    public Predicates predicates;
    public History<String> requestHistory;
    public History<String> responseHistory;
    public History<History> thatHistory;
    public TripleStore tripleStore;

    public Chat(Bot bot) {
        this(bot, true, "0");
    }

    public Chat(Bot bot, boolean doWrites) {
        this(bot, doWrites, "0");
    }

    public Chat(Bot bot, boolean doWrites, String customerId) {
        this.customerId = MagicStrings.default_Customer_id;
        this.thatHistory = new History("that");
        this.requestHistory = new History("request");
        this.responseHistory = new History("response");
        this.inputHistory = new History("input");
        this.predicates = new Predicates();
        this.tripleStore = new TripleStore("anon", this);
        this.customerId = customerId;
        this.bot = bot;
        this.doWrites = doWrites;
        History<String> contextThatHistory = new History();
        contextThatHistory.add(MagicStrings.default_that);
        this.thatHistory.add(contextThatHistory);
        addPredicates();
        addTriples();
        this.predicates.put("topic", MagicStrings.default_topic);
        this.predicates.put("jsenabled", MagicStrings.js_enabled);
        if (MagicBooleans.trace_mode) {
            System.out.println("Chat Session Created for bot " + bot.name);
        }
    }

    void addPredicates() {
        try {
            this.predicates.getPredicateDefaults(this.bot.config_path + "/predicates.txt");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    int addTriples() {
        int tripleCnt = 0;
        if (MagicBooleans.trace_mode) {
            System.out.println("Loading Triples from " + this.bot.config_path + "/triples.txt");
        }
        File f = new File(this.bot.config_path + "/triples.txt");
        if (f.exists()) {
            try {
                InputStream is = new FileInputStream(f);
                BufferedReader br = new BufferedReader(new InputStreamReader(is));
                while (true) {
                    String strLine = br.readLine();
                    if (strLine == null) {
                        break;
                    }
                    String[] triple = strLine.split(":");
                    if (triple.length >= 3) {
                        this.tripleStore.addTriple(triple[0], triple[1], triple[2]);
                        tripleCnt++;
                    }
                }
                is.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        if (MagicBooleans.trace_mode) {
            System.out.println("Loaded " + tripleCnt + " triples");
        }
        return tripleCnt;
    }

    public void chat() {
        Exception ex;
        try {
            BufferedWriter bw = new BufferedWriter(new FileWriter(this.bot.log_path + "/log_" + this.customerId + ".txt", true));
            BufferedWriter bufferedWriter;
            try {
                String request = "SET PREDICATES";
                String response = multisentenceRespond(request);
                while (!request.equals("quit")) {
                    System.out.print("Human: ");
                    request = IOUtils.readInputTextLine();
                    response = multisentenceRespond(request);
                    System.out.println("Robot: " + response);
                    bw.write("Human: " + request);
                    bw.newLine();
                    bw.write("Robot: " + response);
                    bw.newLine();
                    bw.flush();
                }
                bw.close();
                bufferedWriter = bw;
            } catch (Exception e) {
                ex = e;
                bufferedWriter = bw;
                ex.printStackTrace();
            }
        } catch (Exception e2) {
            ex = e2;
            ex.printStackTrace();
        }
    }

    String respond(String input, String that, String topic, History contextThatHistory) {
        boolean repetition = true;
        int i = 0;
        while (i < MagicNumbers.repetition_count) {
            if (this.inputHistory.get(i) == null || !input.toUpperCase().equals(((String) this.inputHistory.get(i)).toUpperCase())) {
                repetition = false;
            }
            i++;
        }
        if (input.equals(MagicStrings.null_input)) {
            repetition = false;
        }
        this.inputHistory.add(input);
        if (repetition) {
            input = MagicStrings.repetition_detected;
        }
        String response = AIMLProcessor.respond(input, that, topic, this);
        String normResponse = this.bot.preProcessor.normalize(response);
        if (MagicBooleans.jp_tokenize) {
            normResponse = JapaneseUtils.tokenizeSentence(normResponse);
        }
        String[] sentences = this.bot.preProcessor.sentenceSplit(normResponse);
        for (String that2 : sentences) {
            if (that2.trim().equals(BuildConfig.FLAVOR)) {
                that2 = MagicStrings.default_that;
            }
            contextThatHistory.add(that2);
        }
        return response.trim() + "  ";
    }

    String respond(String input, History<String> contextThatHistory) {
        String that;
        History hist = (History) this.thatHistory.get(0);
        if (hist == null) {
            that = MagicStrings.default_that;
        } else {
            that = hist.getString(0);
        }
        return respond(input, that, this.predicates.get("topic"), contextThatHistory);
    }

    public String multisentenceRespond(String request) {
        String response = BuildConfig.FLAVOR;
        matchTrace = BuildConfig.FLAVOR;
        try {
            String[] sentences = this.bot.preProcessor.sentenceSplit(JapaneseUtils.tokenizeSentence(this.bot.preProcessor.normalize(request)));
            History<String> contextThatHistory = new History("contextThat");
            for (String respond : sentences) {
                AIMLProcessor.trace_count = 0;
                response = response + "  " + respond(respond, contextThatHistory);
            }
            this.requestHistory.add(request);
            this.responseHistory.add(response);
            this.thatHistory.add(contextThatHistory);
            response = response.replaceAll("[\n]+", "\n").trim();
            if (this.doWrites) {
                this.bot.writeLearnfIFCategories();
            }
            return response;
        } catch (Exception ex) {
            ex.printStackTrace();
            return MagicStrings.error_bot_response;
        }
    }

    public static void setMatchTrace(String newMatchTrace) {
        matchTrace = newMatchTrace;
    }
}
