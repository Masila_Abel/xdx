package com.amusoft.sophiebot.alice;

import junit.framework.TestCase;

public class InflectorTest extends TestCase {
    public void testPluralize() throws Exception {
        Inflector inflector = new Inflector();
        pairs = new String[3][];
        pairs[0] = new String[]{"dog", "dogs"};
        pairs[1] = new String[]{"person", "people"};
        pairs[2] = new String[]{"cats", "cats"};
        for (int i = 0; i < pairs.length; i++) {
            String singular = pairs[i][0];
            assertEquals("Pluralize " + pairs[0][0], pairs[i][1], inflector.pluralize(singular));
        }
    }

    public void testSingularize() throws Exception {
    }
}
