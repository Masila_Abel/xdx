package com.amusoft.sophiebot.alice;

import com.amusoft.sophiebot.BrainLogger;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import outlander.showcaseview.BuildConfig;

public class Bot {
    public String aiml_path;
    public String aimlif_path;
    public String bot_name_path;
    public final Graphmaster brain;
    public String config_path;
    public Graphmaster learnGraph;
    public Graphmaster learnfGraph;
    public String log_path;
    public HashMap<String, AIMLMap> mapMap;
    public String maps_path;
    public String name;
    public final PreProcessor preProcessor;
    public HashSet<String> pronounSet;
    public final Properties properties;
    public HashMap<String, AIMLSet> setMap;
    public String sets_path;

    public void setAllPaths(String root, String name) {
        this.bot_name_path = name;
        if (MagicBooleans.trace_mode) {
            System.out.println("Name = " + name + " Path = " + this.bot_name_path);
        }
        this.aiml_path = this.bot_name_path + "/aiml";
        this.aimlif_path = this.bot_name_path + "/aimlif";
        this.config_path = this.bot_name_path + "/config";
        this.log_path = this.bot_name_path + "/logs";
        this.sets_path = this.bot_name_path + "/sets";
        this.maps_path = this.bot_name_path + "/maps";
        if (MagicBooleans.trace_mode) {
            System.out.println(this.bot_name_path);
            System.out.println(this.aiml_path);
            System.out.println(this.aimlif_path);
            System.out.println(this.config_path);
            System.out.println(this.log_path);
            System.out.println(this.sets_path);
            System.out.println(this.maps_path);
        }
    }

    public Bot() {
        this(MagicStrings.default_bot);
    }

    public Bot(String name) {
        this(name, MagicStrings.root_path);
    }

    public Bot(String name, String path) {
        this(name, path, "auto");
    }

    public Bot(String name, String path, String action) {
        this.properties = new Properties();
        this.name = MagicStrings.default_bot_name;
        this.setMap = new HashMap();
        this.mapMap = new HashMap();
        this.pronounSet = new HashSet();
        this.bot_name_path = "alice";
        this.aimlif_path = this.bot_name_path + "/aimlif";
        this.aiml_path = this.bot_name_path + "/aiml";
        this.config_path = this.bot_name_path + "/config";
        this.log_path = this.bot_name_path + "/log";
        this.sets_path = this.bot_name_path + "/sets";
        this.maps_path = this.bot_name_path + "/maps";
        this.name = name;
        setAllPaths(path, name);
        this.brain = new Graphmaster(this);
        this.learnfGraph = new Graphmaster(this, "learnf");
        this.learnGraph = new Graphmaster(this, "learn");
        this.preProcessor = new PreProcessor(this);
        addProperties();
        int cnt = addAIMLSets();
        if (MagicBooleans.trace_mode) {
            System.out.println("Loaded " + cnt + " set elements.");
        }
        cnt = addAIMLMaps();
        if (MagicBooleans.trace_mode) {
            System.out.println("Loaded " + cnt + " map elements");
        }
        this.pronounSet = getPronouns();
        this.setMap.put(MagicStrings.natural_number_set_name, new AIMLSet(MagicStrings.natural_number_set_name, this));
        this.mapMap.put(MagicStrings.map_successor, new AIMLMap(MagicStrings.map_successor, this));
        this.mapMap.put(MagicStrings.map_predecessor, new AIMLMap(MagicStrings.map_predecessor, this));
        this.mapMap.put(MagicStrings.map_singular, new AIMLMap(MagicStrings.map_singular, this));
        this.mapMap.put(MagicStrings.map_plural, new AIMLMap(MagicStrings.map_plural, this));
        MagicStrings.pannous_api_key = Utilities.getPannousAPIKey(this);
        MagicStrings.pannous_login = Utilities.getPannousLogin(this);
        if (action.equals("aiml2csv")) {
            addCategoriesFromAIML();
        } else {
            if (action.equals("csv2aiml")) {
                addCategoriesFromAIMLIF();
            } else {
                if (action.equals("chat-app")) {
                    if (MagicBooleans.trace_mode) {
                        System.out.println("Loading only AIMLIF files");
                    }
                    cnt = addCategoriesFromAIMLIF();
                } else {
                    addCategoriesFromAIMLIF();
                    if (this.brain.getCategories().size() == 0) {
                        System.out.println("No AIMLIF Files found.  Looking for AIML");
                        cnt = addCategoriesFromAIML();
                    }
                }
            }
        }
        this.brain.addCategory(new Category(0, "PROGRAM VERSION", "*", "*", MagicStrings.program_name_version, "update.aiml"));
        this.brain.nodeStats();
        this.learnfGraph.nodeStats();
    }

    HashSet<String> getPronouns() {
        HashSet<String> pronounSet = new HashSet();
        String[] splitPronouns = Utilities.getFile(this.config_path + "/pronouns.txt").split("\n");
        for (String trim : splitPronouns) {
            String p = trim.trim();
            if (p.length() > 0) {
                pronounSet.add(p);
            }
        }
        if (MagicBooleans.trace_mode) {
            System.out.println("Read pronouns: " + pronounSet);
        }
        return pronounSet;
    }

    void addMoreCategories(String file, ArrayList<Category> moreCategories) {
        if (!file.contains(MagicStrings.deleted_aiml_file)) {
            Iterator it;
            if (file.contains(MagicStrings.learnf_aiml_file)) {
                if (MagicBooleans.trace_mode) {
                    System.out.println("Reading Learnf file");
                }
                it = moreCategories.iterator();
                while (it.hasNext()) {
                    Category c = (Category) it.next();
                    this.brain.addCategory(c);
                    this.learnfGraph.addCategory(c);
                }
                return;
            }
            it = moreCategories.iterator();
            while (it.hasNext()) {
                this.brain.addCategory((Category) it.next());
            }
        }
    }

    int addCategoriesFromAIML() {
        Timer timer = new Timer();
        timer.start();
        int cnt = 0;
        try {
            String[] listOfFiles = Alice.getContext().getAssets().list(this.aiml_path);
            if (MagicBooleans.trace_mode) {
                System.out.println("Loading AIML files from " + this.aiml_path);
                BrainLogger.getInstance().info("Loading AIML files from " + this.aiml_path);
            }
            for (String file : listOfFiles) {
                if (file.endsWith(".aiml") || file.endsWith(".AIML")) {
                    if (MagicBooleans.trace_mode) {
                        System.out.println(file);
                        BrainLogger.getInstance().info("Reading AIML category " + file);
                    }
                    try {
                        ArrayList<Category> moreCategories = AIMLProcessor.AIMLToCategories(this.aiml_path, file);
                        addMoreCategories(file, moreCategories);
                        cnt += moreCategories.size();
                    } catch (Exception iex) {
                        System.out.println("Problem loading " + file);
                        iex.printStackTrace();
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (MagicBooleans.trace_mode) {
            System.out.println("Loaded " + cnt + " categories in " + timer.elapsedTimeSecs() + " sec");
            BrainLogger.getInstance().info("Loaded " + cnt + " categories in " + timer.elapsedTimeSecs() + " sec");
        }
        return cnt;
    }

    public int addCategoriesFromAIMLIF() {
        Timer timer = new Timer();
        timer.start();
        int cnt = 0;
        try {
            String[] listOfFiles = Alice.getContext().getAssets().list(this.aimlif_path);
            if (MagicBooleans.trace_mode) {
                System.out.println("Loading AIML files from " + this.aimlif_path);
            }
            for (String file : listOfFiles) {
                if (file.endsWith(MagicStrings.aimlif_file_suffix) || file.endsWith(MagicStrings.aimlif_file_suffix.toUpperCase())) {
                    if (MagicBooleans.trace_mode) {
                        System.out.println(file);
                    }
                    try {
                        ArrayList<Category> moreCategories = readIFCategories(this.aimlif_path + "/" + file);
                        cnt += moreCategories.size();
                        addMoreCategories(file, moreCategories);
                    } catch (Exception iex) {
                        System.out.println("Problem loading " + file);
                        iex.printStackTrace();
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (MagicBooleans.trace_mode) {
            System.out.println("Loaded " + cnt + " categories in " + timer.elapsedTimeSecs() + " sec");
        }
        return cnt;
    }

    public void writeQuit() {
        writeAIMLIFFiles();
        writeAIMLFiles();
    }

    public int readCertainIFCategories(Graphmaster graph, String fileName) {
        int cnt = 0;
        try {
            Alice.getContext().getAssets().open(this.aimlif_path + "/" + fileName + MagicStrings.aimlif_file_suffix);
            ArrayList<Category> certainCategories = readIFCategories(this.aimlif_path + "/" + fileName + MagicStrings.aimlif_file_suffix);
            Iterator it = certainCategories.iterator();
            while (it.hasNext()) {
                graph.addCategory((Category) it.next());
            }
            cnt = certainCategories.size();
            System.out.println("readCertainIFCategories " + cnt + " categories from " + fileName + MagicStrings.aimlif_file_suffix);
        } catch (IOException e) {
            System.out.println("Problem loading " + fileName);
            e.printStackTrace();
        }
        return cnt;
    }

    public void writeCertainIFCategories(Graphmaster graph, String file) {
        if (MagicBooleans.trace_mode) {
            System.out.println("writeCertainIFCaegories " + file + " size= " + graph.getCategories().size());
        }
        writeIFCategories(graph.getCategories(), file + MagicStrings.aimlif_file_suffix);
    }

    public void writeLearnfIFCategories() {
        writeCertainIFCategories(this.learnfGraph, MagicStrings.learnf_aiml_file);
    }

    public void writeIFCategories(ArrayList<Category> cats, String filename) {
        IOException ex;
        FileNotFoundException ex2;
        Throwable th;
        BufferedWriter bw = null;
        if (new File(this.aimlif_path).exists()) {
            try {
                BufferedWriter bw2 = new BufferedWriter(new FileWriter(this.aimlif_path + "/" + filename));
                try {
                    Iterator it = cats.iterator();
                    while (it.hasNext()) {
                        bw2.write(Category.categoryToIF((Category) it.next()));
                        bw2.newLine();
                    }
                    if (bw2 != null) {
                        try {
                            bw2.flush();
                            bw2.close();
                        } catch (IOException ex3) {
                            ex3.printStackTrace();
                            bw = bw2;
                            return;
                        }
                    }
                    bw = bw2;
                } catch (FileNotFoundException e) {
                    ex2 = e;
                    bw = bw2;
                    try {
                        ex2.printStackTrace();
                        if (bw != null) {
                            try {
                                bw.flush();
                                bw.close();
                            } catch (IOException ex32) {
                                ex32.printStackTrace();
                            }
                        }
                    } catch (Throwable th2) {
                        th = th2;
                        if (bw != null) {
                            try {
                                bw.flush();
                                bw.close();
                            } catch (IOException ex322) {
                                ex322.printStackTrace();
                            }
                        }
                        throw th;
                    }
                } catch (IOException e2) {
                    ex322 = e2;
                    bw = bw2;
                    ex322.printStackTrace();
                    if (bw != null) {
                        try {
                            bw.flush();
                            bw.close();
                        } catch (IOException ex3222) {
                            ex3222.printStackTrace();
                        }
                    }
                } catch (Throwable th3) {
                    th = th3;
                    bw = bw2;
                    if (bw != null) {
                        bw.flush();
                        bw.close();
                    }
                    throw th;
                }
            } catch (FileNotFoundException e3) {
                ex2 = e3;
                ex2.printStackTrace();
                if (bw != null) {
                    bw.flush();
                    bw.close();
                }
            } catch (IOException e4) {
                ex3222 = e4;
                ex3222.printStackTrace();
                if (bw != null) {
                    bw.flush();
                    bw.close();
                }
            }
        }
    }

    public void writeAIMLIFFiles() {
        BufferedWriter bw;
        if (MagicBooleans.trace_mode) {
            System.out.println("writeAIMLIFFiles");
        }
        HashMap<String, BufferedWriter> fileMap = new HashMap();
        this.brain.addCategory(new Category(0, "BRAIN BUILD", "*", "*", new Date().toString(), "update.aiml"));
        ArrayList<Category> brainCategories = this.brain.getCategories();
        Collections.sort(brainCategories, Category.CATEGORY_NUMBER_COMPARATOR);
        Iterator it = brainCategories.iterator();
        while (it.hasNext()) {
            Category c = (Category) it.next();
            try {
                String fileName = c.getFilename();
                if (fileMap.containsKey(fileName)) {
                    bw = (BufferedWriter) fileMap.get(fileName);
                } else {
                    bw = new BufferedWriter(new FileWriter(this.aimlif_path + "/" + fileName + MagicStrings.aimlif_file_suffix));
                    fileMap.put(fileName, bw);
                }
                bw.write(Category.categoryToIF(c));
                bw.newLine();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        for (Object aSet : fileMap.keySet()) {
            bw = (BufferedWriter) fileMap.get(aSet);
            if (bw != null) {
                try {
                    bw.flush();
                    bw.close();
                } catch (IOException ex2) {
                    ex2.printStackTrace();
                }
            }
        }
        new File(this.aimlif_path).setLastModified(new Date().getTime());
    }

    public void writeAIMLFiles() {
        if (MagicBooleans.trace_mode) {
            System.out.println("writeAIMLFiles");
        }
        HashMap<String, BufferedWriter> fileMap = new HashMap();
        this.brain.addCategory(new Category(0, "BRAIN BUILD", "*", "*", new Date().toString(), "update.aiml"));
        ArrayList<Category> brainCategories = this.brain.getCategories();
        Collections.sort(brainCategories, Category.CATEGORY_NUMBER_COMPARATOR);
        Iterator it = brainCategories.iterator();
        while (it.hasNext()) {
            BufferedWriter bw;
            Category c = (Category) it.next();
            if (!c.getFilename().equals(MagicStrings.null_aiml_file)) {
                try {
                    String fileName = c.getFilename();
                    if (fileMap.containsKey(fileName)) {
                        bw = (BufferedWriter) fileMap.get(fileName);
                    } else {
                        String copyright = Utilities.getCopyright(this, fileName);
                        bw = new BufferedWriter(new FileWriter(this.aiml_path + "/" + fileName));
                        fileMap.put(fileName, bw);
                        bw.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<aiml>\n");
                        bw.write(copyright);
                    }
                    bw.write(Category.categoryToAIML(c) + "\n");
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }
        for (Object aSet : fileMap.keySet()) {
            bw = (BufferedWriter) fileMap.get(aSet);
            if (bw != null) {
                try {
                    bw.write("</aiml>\n");
                    bw.flush();
                    bw.close();
                } catch (IOException ex2) {
                    ex2.printStackTrace();
                }
            }
        }
        new File(this.aiml_path).setLastModified(new Date().getTime());
    }

    void addProperties() {
        try {
            this.properties.getProperties(this.config_path + "/properties.txt");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public ArrayList<Category> readIFCategories(String filename) {
        ArrayList<Category> categories = new ArrayList();
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(filename)));
            while (true) {
                String strLine = br.readLine();
                if (strLine == null) {
                    break;
                }
                try {
                    categories.add(Category.IFToCategory(strLine));
                } catch (Exception e) {
                    System.out.println("Invalid AIMLIF in " + filename + " line " + strLine);
                }
            }
            br.close();
        } catch (Exception e2) {
            System.err.println("Error: " + e2.getMessage());
        }
        return categories;
    }

    int addAIMLSets() {
        int cnt = 0;
        new Timer().start();
        try {
            String[] listOfFiles = Alice.getContext().getAssets().list(this.sets_path);
            if (MagicBooleans.trace_mode) {
                System.out.println("Loading AIML Sets files from " + this.sets_path);
                BrainLogger.getInstance().info("Loading AIML Sets files from " + this.sets_path);
            }
            for (String file : listOfFiles) {
                if (file.endsWith(".txt") || file.endsWith(".TXT")) {
                    if (MagicBooleans.trace_mode) {
                        System.out.println(file);
                    }
                    String setName = file.substring(0, file.length() - ".txt".length());
                    if (MagicBooleans.trace_mode) {
                        System.out.println("Read AIML Set " + setName);
                    }
                    AIMLSet aimlSet = new AIMLSet(setName, this);
                    cnt += aimlSet.readAIMLSet(this);
                    this.setMap.put(setName, aimlSet);
                }
            }
        } catch (IOException e) {
            System.out.println("addAIMLSets: " + this.sets_path + " does not exist.");
            e.printStackTrace();
        }
        return cnt;
    }

    int addAIMLMaps() {
        int cnt = 0;
        new Timer().start();
        try {
            String[] listOfFiles = Alice.getContext().getAssets().list(this.maps_path);
            if (MagicBooleans.trace_mode) {
                System.out.println("Loading AIML Map files from " + this.maps_path);
                BrainLogger.getInstance().info("Loading AIML Map files from " + this.maps_path);
            }
            for (String file : listOfFiles) {
                if (file.endsWith(".txt") || file.endsWith(".TXT")) {
                    if (MagicBooleans.trace_mode) {
                        System.out.println(file);
                    }
                    String mapName = file.substring(0, file.length() - ".txt".length());
                    if (MagicBooleans.trace_mode) {
                        System.out.println("Read AIML Map " + mapName);
                    }
                    AIMLMap aimlMap = new AIMLMap(mapName, this);
                    cnt += aimlMap.readAIMLMap(this);
                    this.mapMap.put(mapName, aimlMap);
                }
            }
        } catch (IOException e) {
            System.out.println("addAIMLMaps: " + this.maps_path + " does not exist.");
            e.printStackTrace();
        }
        return cnt;
    }

    public void deleteLearnfCategories() {
        Iterator it = this.learnfGraph.getCategories().iterator();
        while (it.hasNext()) {
            Category c = (Category) it.next();
            Nodemapper n = this.brain.findNode(c);
            System.out.println("Found node " + n + " for " + c.inputThatTopic());
            if (n != null) {
                n.category = null;
            }
        }
        this.learnfGraph = new Graphmaster(this);
    }

    public void deleteLearnCategories() {
        Iterator it = this.learnGraph.getCategories().iterator();
        while (it.hasNext()) {
            Category c = (Category) it.next();
            Nodemapper n = this.brain.findNode(c);
            System.out.println("Found node " + n + " for " + c.inputThatTopic());
            if (n != null) {
                n.category = null;
            }
        }
        this.learnGraph = new Graphmaster(this);
    }

    public void shadowChecker() {
        shadowChecker(this.brain.root);
    }

    void shadowChecker(Nodemapper node) {
        if (NodemapperOperator.isLeaf(node)) {
            String input = this.brain.replaceBotProperties(node.category.getPattern()).replace("*", "XXX").replace("_", "XXX").replace("^", BuildConfig.FLAVOR).replace("#", BuildConfig.FLAVOR);
            String that = node.category.getThat().replace("*", "XXX").replace("_", "XXX").replace("^", BuildConfig.FLAVOR).replace("#", BuildConfig.FLAVOR);
            String topic = node.category.getTopic().replace("*", "XXX").replace("_", "XXX").replace("^", BuildConfig.FLAVOR).replace("#", BuildConfig.FLAVOR);
            input = instantiateSets(input);
            System.out.println("shadowChecker: input=" + input);
            Nodemapper match = this.brain.match(input, that, topic);
            if (match != node) {
                System.out.println(BuildConfig.FLAVOR + Graphmaster.inputThatTopic(input, that, topic));
                System.out.println("MATCHED:     " + match.category.inputThatTopic());
                System.out.println("SHOULD MATCH:" + node.category.inputThatTopic());
                return;
            }
            return;
        }
        for (String key : NodemapperOperator.keySet(node)) {
            shadowChecker(NodemapperOperator.get(node, key));
        }
    }

    public String instantiateSets(String pattern) {
        String[] splitPattern = pattern.split(" ");
        pattern = BuildConfig.FLAVOR;
        for (String x : splitPattern) {
            String x2;
            if (x2.startsWith("<SET>")) {
                if (((AIMLSet) this.setMap.get(AIMLProcessor.trimTag(x2, "SET"))) != null) {
                    x2 = "FOUNDITEM";
                } else {
                    x2 = "NOTFOUND";
                }
            }
            pattern = pattern + " " + x2;
        }
        return pattern.trim();
    }
}
