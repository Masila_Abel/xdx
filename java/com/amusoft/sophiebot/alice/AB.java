package com.amusoft.sophiebot.alice;

import com.amusoft.sophiebot.alice.utils.IOUtils;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import outlander.showcaseview.BuildConfig;

public class AB {
    static int leafPatternCnt = 0;
    public static int limit = 500000;
    static int starPatternCnt = 0;
    public Bot alice;
    public Bot bot;
    public final Graphmaster deletedGraph;
    public boolean filter_atomic_mode;
    public boolean filter_wild_mode;
    public final Graphmaster inputGraph;
    public String logfile;
    public boolean offer_alice_responses;
    AIMLSet passed;
    public final Graphmaster patternGraph;
    public int runCompletedCnt;
    public boolean shuffle_mode = true;
    public boolean sort_mode;
    public ArrayList<Category> suggestedCategories;
    AIMLSet testSet;

    public AB(Bot bot, String sampleFile) {
        boolean z;
        if (this.shuffle_mode) {
            z = false;
        } else {
            z = true;
        }
        this.sort_mode = z;
        this.filter_atomic_mode = false;
        this.filter_wild_mode = false;
        this.offer_alice_responses = true;
        this.logfile = MagicStrings.root_path + "/data/" + MagicStrings.ab_sample_file;
        MagicStrings.ab_sample_file = sampleFile;
        this.logfile = MagicStrings.root_path + "/data/" + MagicStrings.ab_sample_file;
        System.out.println("AB with sample file " + this.logfile);
        this.bot = bot;
        this.inputGraph = new Graphmaster(bot, "input");
        this.deletedGraph = new Graphmaster(bot, "deleted");
        this.patternGraph = new Graphmaster(bot, "pattern");
        Iterator it = bot.brain.getCategories().iterator();
        while (it.hasNext()) {
            this.patternGraph.addCategory((Category) it.next());
        }
        this.suggestedCategories = new ArrayList();
        this.passed = new AIMLSet("passed", bot);
        this.testSet = new AIMLSet("1000", bot);
        readDeletedIFCategories();
    }

    public void productivity(int runCompletedCnt, Timer timer) {
        float time = timer.elapsedTimeMins();
        System.out.println("Completed " + runCompletedCnt + " in " + time + " min. Productivity " + (((float) runCompletedCnt) / time) + " cat/min");
    }

    public void readDeletedIFCategories() {
        this.bot.readCertainIFCategories(this.deletedGraph, MagicStrings.deleted_aiml_file);
        if (MagicBooleans.trace_mode) {
            System.out.println("--- DELETED CATEGORIES -- read " + this.deletedGraph.getCategories().size() + " deleted categories");
        }
    }

    public void writeDeletedIFCategories() {
        System.out.println("--- DELETED CATEGORIES -- write");
        this.bot.writeCertainIFCategories(this.deletedGraph, MagicStrings.deleted_aiml_file);
        System.out.println("--- DELETED CATEGORIES -- write " + this.deletedGraph.getCategories().size() + " deleted categories");
    }

    public void saveCategory(String pattern, String template, String filename) {
        Category c = new Category(0, pattern, "*", "*", template, filename);
        if (c.validate()) {
            this.bot.brain.addCategory(c);
            this.bot.writeAIMLIFFiles();
            this.runCompletedCnt++;
            return;
        }
        System.out.println("Invalid Category " + c.validationMessage);
    }

    public void deleteCategory(Category c) {
        c.setFilename(MagicStrings.deleted_aiml_file);
        c.setTemplate(MagicStrings.deleted_template);
        this.deletedGraph.addCategory(c);
        System.out.println("--- bot.writeDeletedIFCategories()");
        writeDeletedIFCategories();
    }

    public void skipCategory(Category c) {
    }

    public void abwq() {
        Timer timer = new Timer();
        timer.start();
        classifyInputs(this.logfile);
        System.out.println(timer.elapsedTimeSecs() + " classifying inputs");
        this.bot.writeQuit();
    }

    public void graphInputs(String filename) {
        int count = 0;
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(filename)));
            while (true) {
                String strLine = br.readLine();
                if (strLine == null || count >= limit) {
                    break;
                }
                Category c = new Category(0, strLine, "*", "*", "nothing", MagicStrings.unknown_aiml_file);
                Nodemapper node = this.inputGraph.findNode(c);
                if (node == null) {
                    this.inputGraph.addCategory(c);
                    c.incrementActivationCnt();
                } else {
                    node.category.incrementActivationCnt();
                }
                count++;
            }
            br.close();
        } catch (Exception e) {
            System.err.println("Error: " + e.getMessage());
        }
    }

    public void findPatterns() {
        findPatterns(this.inputGraph.root, BuildConfig.FLAVOR);
        System.out.println(leafPatternCnt + " Leaf Patterns " + starPatternCnt + " Star Patterns");
    }

    void findPatterns(Nodemapper node, String partialPatternThatTopic) {
        Category c;
        if (NodemapperOperator.isLeaf(node) && node.category.getActivationCnt() > MagicNumbers.node_activation_cnt) {
            leafPatternCnt++;
            try {
                String categoryPatternThatTopic = BuildConfig.FLAVOR;
                if (node.shortCut) {
                    categoryPatternThatTopic = partialPatternThatTopic + " <THAT> * <TOPIC> *";
                } else {
                    categoryPatternThatTopic = partialPatternThatTopic;
                }
                c = new Category(0, categoryPatternThatTopic, MagicStrings.blank_template, MagicStrings.unknown_aiml_file);
                if (!(this.bot.brain.existsCategory(c) || this.deletedGraph.existsCategory(c))) {
                    this.patternGraph.addCategory(c);
                    this.suggestedCategories.add(c);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (NodemapperOperator.size(node) > MagicNumbers.node_size) {
            starPatternCnt++;
            try {
                c = new Category(0, partialPatternThatTopic + " * <THAT> * <TOPIC> *", MagicStrings.blank_template, MagicStrings.unknown_aiml_file);
                if (!(this.bot.brain.existsCategory(c) || this.deletedGraph.existsCategory(c))) {
                    this.patternGraph.addCategory(c);
                    this.suggestedCategories.add(c);
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        for (String key : NodemapperOperator.keySet(node)) {
            findPatterns(NodemapperOperator.get(node, key), partialPatternThatTopic + " " + key);
        }
    }

    public void classifyInputs(String filename) {
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(filename)));
            int count = 0;
            while (true) {
                String strLine = br.readLine();
                if (strLine == null || count >= limit) {
                    break;
                } else if (strLine != null) {
                    if (strLine.startsWith("Human: ")) {
                        strLine = strLine.substring("Human: ".length(), strLine.length());
                    }
                    String[] sentences = this.bot.preProcessor.sentenceSplit(strLine);
                    for (String sentence : sentences) {
                        if (sentence.length() > 0) {
                            Nodemapper match = this.patternGraph.match(sentence, "unknown", "unknown");
                            if (match == null) {
                                System.out.println(sentence + " null match");
                            } else {
                                match.category.incrementActivationCnt();
                            }
                            count++;
                            if (count % 10000 == 0) {
                                System.out.println(count);
                            }
                        }
                    }
                }
            }
            System.out.println("Finished classifying " + count + " inputs");
            br.close();
        } catch (Exception e) {
            System.err.println("Error: " + e.getMessage());
            e.printStackTrace();
        }
    }

    public void ab() {
        String logFile = this.logfile;
        MagicBooleans.trace_mode = false;
        MagicBooleans.enable_external_sets = false;
        if (this.offer_alice_responses) {
            this.alice = new Bot("alice");
        }
        Timer timer = new Timer();
        this.bot.brain.nodeStats();
        if (this.bot.brain.getCategories().size() < MagicNumbers.brain_print_size) {
            this.bot.brain.printgraph();
        }
        timer.start();
        System.out.println("Graphing inputs");
        graphInputs(logFile);
        System.out.println(timer.elapsedTimeSecs() + " seconds Graphing inputs");
        this.inputGraph.nodeStats();
        if (this.inputGraph.getCategories().size() < MagicNumbers.brain_print_size) {
            this.inputGraph.printgraph();
        }
        timer.start();
        System.out.println("Finding Patterns");
        findPatterns();
        System.out.println(this.suggestedCategories.size() + " suggested categories");
        System.out.println(timer.elapsedTimeSecs() + " seconds finding patterns");
        timer.start();
        this.patternGraph.nodeStats();
        if (this.patternGraph.getCategories().size() < MagicNumbers.brain_print_size) {
            this.patternGraph.printgraph();
        }
        System.out.println("Classifying Inputs from " + logFile);
        classifyInputs(logFile);
        System.out.println(timer.elapsedTimeSecs() + " classifying inputs");
    }

    public ArrayList<Category> nonZeroActivationCount(ArrayList<Category> suggestedCategories) {
        ArrayList<Category> result = new ArrayList();
        Iterator it = suggestedCategories.iterator();
        while (it.hasNext()) {
            Category c = (Category) it.next();
            if (c.getActivationCnt() > 0) {
                result.add(c);
            }
        }
        return result;
    }

    public void terminalInteraction() {
        ArrayList<Category> browserCategories;
        boolean firstInteraction = true;
        String alicetemplate = null;
        Timer timer = new Timer();
        this.sort_mode = !this.shuffle_mode;
        Collections.sort(this.suggestedCategories, Category.ACTIVATION_COMPARATOR);
        ArrayList<Category> topSuggestCategories = new ArrayList();
        int i = 0;
        while (i < 10000 && i < this.suggestedCategories.size()) {
            topSuggestCategories.add(this.suggestedCategories.get(i));
            i++;
        }
        this.suggestedCategories = topSuggestCategories;
        if (this.shuffle_mode) {
            Collections.shuffle(this.suggestedCategories);
        }
        timer = new Timer();
        timer.start();
        this.runCompletedCnt = 0;
        ArrayList<Category> filteredAtomicCategories = new ArrayList();
        ArrayList<Category> filteredWildCategories = new ArrayList();
        Iterator it = this.suggestedCategories.iterator();
        while (it.hasNext()) {
            Category c = (Category) it.next();
            if (c.getPattern().contains("*")) {
                filteredWildCategories.add(c);
            } else {
                filteredAtomicCategories.add(c);
            }
        }
        if (this.filter_atomic_mode) {
            browserCategories = filteredAtomicCategories;
        } else if (this.filter_wild_mode) {
            browserCategories = filteredWildCategories;
        } else {
            browserCategories = this.suggestedCategories;
        }
        Iterator it2 = nonZeroActivationCount(browserCategories).iterator();
        while (it2.hasNext()) {
            c = (Category) it2.next();
            try {
                ArrayList arrayList = new ArrayList(c.getMatches(this.bot));
                Collections.shuffle(arrayList);
                int sampleSize = Math.min(MagicNumbers.displayed_input_sample_size, c.getMatches(this.bot).size());
                for (i = 0; i < sampleSize; i++) {
                    System.out.println(BuildConfig.FLAVOR + arrayList.get(i));
                }
                System.out.println("[" + c.getActivationCnt() + "] " + c.inputThatTopic());
                if (this.offer_alice_responses) {
                    Nodemapper node = this.alice.brain.findNode(c);
                    if (node != null) {
                        alicetemplate = node.category.getTemplate();
                        String displayAliceTemplate = alicetemplate.replace("\n", " ");
                        if (displayAliceTemplate.length() > 200) {
                            displayAliceTemplate = displayAliceTemplate.substring(0, 200);
                        }
                        System.out.println("ALICE: " + displayAliceTemplate);
                    } else {
                        alicetemplate = null;
                    }
                }
                String textLine = BuildConfig.FLAVOR + IOUtils.readInputTextLine();
                if (firstInteraction) {
                    timer.start();
                    firstInteraction = false;
                }
                productivity(this.runCompletedCnt, timer);
                terminalInteractionStep(this.bot, BuildConfig.FLAVOR, textLine, c, alicetemplate);
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println("Returning to Category Browser");
            }
        }
        System.out.println("No more samples");
        this.bot.writeAIMLFiles();
        this.bot.writeAIMLIFFiles();
    }

    public void terminalInteractionStep(Bot bot, String request, String textLine, Category c, String alicetemplate) {
        if (textLine.contains("<pattern>")) {
            if (textLine.contains("</pattern>")) {
                int index = textLine.indexOf("<pattern>") + "<pattern>".length();
                int jndex = textLine.indexOf("</pattern>");
                int kndex = jndex + "</pattern>".length();
                if (index < jndex) {
                    String pattern = textLine.substring(index, jndex);
                    c.setPattern(pattern);
                    textLine = textLine.substring(kndex, textLine.length());
                    System.out.println("Got pattern = " + pattern + " template = " + textLine);
                }
            }
        }
        String botThinks = BuildConfig.FLAVOR;
        for (String p : new String[]{"he", "she", "it", "we", "they"}) {
            if (textLine.contains("<" + p + ">")) {
                textLine = textLine.replace("<" + p + ">", BuildConfig.FLAVOR);
                botThinks = "<think><set name=\"" + p + "\"><set name=\"topic\"><star/></set></set></think>";
            }
        }
        if (textLine.equals("q")) {
            System.exit(0);
            return;
        }
        if (textLine.equals("wq")) {
            bot.writeQuit();
            System.exit(0);
            return;
        }
        if (!textLine.equals("skip")) {
            if (!textLine.equals(BuildConfig.FLAVOR)) {
                if (!textLine.equals("s")) {
                    if (!textLine.equals("pass")) {
                        if (textLine.equals("a")) {
                            String filename;
                            String template = alicetemplate;
                            if (template.contains("<sr")) {
                                filename = MagicStrings.reductions_update_aiml_file;
                            } else {
                                filename = MagicStrings.personality_aiml_file;
                            }
                            saveCategory(c.getPattern(), template, filename);
                            return;
                        }
                        if (textLine.equals("d")) {
                            deleteCategory(c);
                            return;
                        }
                        if (textLine.equals("x")) {
                            saveCategory(c.getPattern(), ("<sraix services=\"pannous\">" + c.getPattern().replace("*", "<star/>") + "</sraix>") + botThinks, MagicStrings.sraix_aiml_file);
                            return;
                        }
                        if (textLine.equals("p")) {
                            saveCategory(c.getPattern(), ("<srai>" + MagicStrings.inappropriate_filter + "</srai>") + botThinks, MagicStrings.inappropriate_aiml_file);
                            return;
                        }
                        if (textLine.equals("f")) {
                            saveCategory(c.getPattern(), ("<srai>" + MagicStrings.profanity_filter + "</srai>") + botThinks, MagicStrings.profanity_aiml_file);
                            return;
                        }
                        if (textLine.equals("i")) {
                            saveCategory(c.getPattern(), ("<srai>" + MagicStrings.insult_filter + "</srai>") + botThinks, MagicStrings.insult_aiml_file);
                            return;
                        }
                        if (!textLine.contains("<srai>")) {
                            if (!textLine.contains("<sr/>")) {
                                if (textLine.contains("<oob>")) {
                                    saveCategory(c.getPattern(), textLine + botThinks, MagicStrings.oob_aiml_file);
                                    return;
                                }
                                if (textLine.contains("<set name") || botThinks.length() > 0) {
                                    saveCategory(c.getPattern(), textLine + botThinks, MagicStrings.predicates_aiml_file);
                                    return;
                                }
                                if (textLine.contains("<get name")) {
                                    if (!textLine.contains("<get name=\"name")) {
                                        saveCategory(c.getPattern(), textLine + botThinks, MagicStrings.predicates_aiml_file);
                                        return;
                                    }
                                }
                                saveCategory(c.getPattern(), textLine + botThinks, MagicStrings.personality_aiml_file);
                                return;
                            }
                        }
                        saveCategory(c.getPattern(), textLine + botThinks, MagicStrings.reductions_update_aiml_file);
                        return;
                    }
                }
                this.passed.add(request);
                AIMLSet difference = new AIMLSet("difference", bot);
                difference.addAll(this.testSet);
                difference.removeAll(this.passed);
                difference.writeAIMLSet();
                this.passed.writeAIMLSet();
                return;
            }
        }
        skipCategory(c);
    }
}
