package com.amusoft.sophiebot.alice;

import com.amusoft.sophiebot.alice.TripleStore.Triple;
import com.amusoft.sophiebot.alice.utils.CalendarUtils;
import com.amusoft.sophiebot.alice.utils.DomUtils;
import com.amusoft.sophiebot.alice.utils.IOUtils;
import com.amusoft.sophiebot.alice.utils.IntervalUtils;
import com.amusoft.sophiebot.alice.utils.JapaneseUtils;
import com.google.firebase.analytics.FirebaseAnalytics.Param;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import outlander.showcaseview.BuildConfig;

public class AIMLProcessor {
    private static boolean DEBUG = false;
    public static AIMLProcessorExtension extension;
    public static int repeatCount = 0;
    public static int sraiCount = 0;
    public static int trace_count = 0;

    private static void categoryProcessor(Node n, ArrayList<Category> categories, String topic, String aimlFile, String language) {
        NodeList children = n.getChildNodes();
        String pattern = "*";
        String that = "*";
        String template = BuildConfig.FLAVOR;
        for (int j = 0; j < children.getLength(); j++) {
            Node m = children.item(j);
            String mName = m.getNodeName();
            if (!mName.equals("#text")) {
                if (mName.equals("pattern")) {
                    pattern = DomUtils.nodeToString(m);
                } else if (mName.equals("that")) {
                    that = DomUtils.nodeToString(m);
                } else if (mName.equals("topic")) {
                    topic = DomUtils.nodeToString(m);
                } else if (mName.equals("template")) {
                    template = DomUtils.nodeToString(m);
                } else {
                    System.out.println("categoryProcessor: unexpected " + mName + " in " + DomUtils.nodeToString(m));
                }
            }
        }
        pattern = trimTag(pattern, "pattern");
        that = trimTag(that, "that");
        topic = trimTag(topic, "topic");
        pattern = cleanPattern(pattern);
        that = cleanPattern(that);
        topic = cleanPattern(topic);
        template = trimTag(template, "template");
        if (MagicBooleans.jp_tokenize) {
            pattern = JapaneseUtils.tokenizeSentence(pattern);
            that = JapaneseUtils.tokenizeSentence(that);
            topic = JapaneseUtils.tokenizeSentence(topic);
        }
        Category c = new Category(0, pattern, that, topic, template, aimlFile);
        if (template == null || template.length() == 0) {
            System.out.println("Category " + c.inputThatTopic() + " discarded due to blank or missing <template>.");
        } else {
            categories.add(c);
        }
    }

    public static String cleanPattern(String pattern) {
        return pattern.replaceAll("(\r\n|\n\r|\r|\n)", " ").replaceAll("  ", " ").trim();
    }

    public static String trimTag(String s, String tagName) {
        String stag = "<" + tagName + ">";
        String etag = "</" + tagName + ">";
        if (s.startsWith(stag) && s.endsWith(etag)) {
            s = s.substring(stag.length());
            s = s.substring(0, s.length() - etag.length());
        }
        return s.trim();
    }

    public static ArrayList<Category> AIMLToCategories(String directory, String aimlFile) {
        try {
            int i;
            ArrayList<Category> arrayList = new ArrayList();
            Node root = DomUtils.parseFile(directory + "/" + aimlFile);
            String language = MagicStrings.default_language;
            if (root.hasAttributes()) {
                NamedNodeMap XMLAttributes = root.getAttributes();
                for (i = 0; i < XMLAttributes.getLength(); i++) {
                    if (XMLAttributes.item(i).getNodeName().equals("language")) {
                        language = XMLAttributes.item(i).getNodeValue();
                    }
                }
            }
            NodeList nodelist = root.getChildNodes();
            for (i = 0; i < nodelist.getLength(); i++) {
                Node n = nodelist.item(i);
                if (n.getNodeName().equals("category")) {
                    categoryProcessor(n, arrayList, "*", aimlFile, language);
                } else if (n.getNodeName().equals("topic")) {
                    String topic = n.getAttributes().getNamedItem("name").getTextContent();
                    NodeList children = n.getChildNodes();
                    for (int j = 0; j < children.getLength(); j++) {
                        Node m = children.item(j);
                        if (m.getNodeName().equals("category")) {
                            categoryProcessor(m, arrayList, topic, aimlFile, language);
                        }
                    }
                }
            }
            return arrayList;
        } catch (Exception ex) {
            System.out.println("AIMLToCategories: " + ex);
            ex.printStackTrace();
            return null;
        }
    }

    public static int checkForRepeat(String input, Chat chatSession) {
        if (input.equals(chatSession.inputHistory.get(1))) {
            return 1;
        }
        return 0;
    }

    public static String respond(String input, String that, String topic, Chat chatSession) {
        return respond(input, that, topic, chatSession, 0);
    }

    public static String respond(String input, String that, String topic, Chat chatSession, int srCnt) {
        MagicBooleans.trace("input: " + input + ", that: " + that + ", topic: " + topic + ", chatSession: " + chatSession + ", srCnt: " + srCnt);
        if (input == null || input.length() == 0) {
            input = MagicStrings.null_input;
        }
        sraiCount = srCnt;
        String response = MagicStrings.default_bot_response;
        try {
            Nodemapper leaf = chatSession.bot.brain.match(input, that, topic);
            if (leaf == null) {
                return response;
            }
            response = evalTemplate(leaf.category.getTemplate(), new ParseState(0, chatSession, input, that, topic, leaf));
            return response;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private static String capitalizeString(String string) {
        char[] chars = string.toLowerCase().toCharArray();
        boolean found = false;
        int i = 0;
        while (i < chars.length) {
            if (!found && Character.isLetter(chars[i])) {
                chars[i] = Character.toUpperCase(chars[i]);
                found = true;
            } else if (Character.isWhitespace(chars[i])) {
                found = false;
            }
            i++;
        }
        return String.valueOf(chars);
    }

    private static String explode(String input) {
        String result = BuildConfig.FLAVOR;
        for (int i = 0; i < input.length(); i++) {
            result = result + " " + input.charAt(i);
        }
        while (result.contains("  ")) {
            result = result.replace("  ", " ");
        }
        return result.trim();
    }

    public static String evalTagContent(Node node, ParseState ps, Set<String> ignoreAttributes) {
        String result = BuildConfig.FLAVOR;
        try {
            NodeList childList = node.getChildNodes();
            for (int i = 0; i < childList.getLength(); i++) {
                Node child = childList.item(i);
                if (ignoreAttributes == null || !ignoreAttributes.contains(child.getNodeName())) {
                    result = result + recursEval(child, ps);
                }
            }
        } catch (Exception ex) {
            System.out.println("Something went wrong with evalTagContent");
            ex.printStackTrace();
        }
        return result;
    }

    public static String genericXML(Node node, ParseState ps) {
        return unevaluatedXML(evalTagContent(node, ps, null), node, ps);
    }

    private static String unevaluatedXML(String resultIn, Node node, ParseState ps) {
        String nodeName = node.getNodeName();
        String attributes = BuildConfig.FLAVOR;
        if (node.hasAttributes()) {
            NamedNodeMap XMLAttributes = node.getAttributes();
            for (int i = 0; i < XMLAttributes.getLength(); i++) {
                attributes = attributes + " " + XMLAttributes.item(i).getNodeName() + "=\"" + XMLAttributes.item(i).getNodeValue() + "\"";
            }
        }
        String result = "<" + nodeName + attributes + "/>";
        if (resultIn.equals(BuildConfig.FLAVOR)) {
            return result;
        }
        return "<" + nodeName + attributes + ">" + resultIn + "</" + nodeName + ">";
    }

    private static String srai(Node node, ParseState ps) {
        sraiCount++;
        if (sraiCount > MagicNumbers.max_recursion_count || ps.depth > MagicNumbers.max_recursion_depth) {
            return MagicStrings.too_much_recursion;
        }
        String response = MagicStrings.default_bot_response;
        try {
            String result = JapaneseUtils.tokenizeSentence(ps.chatSession.bot.preProcessor.normalize(evalTagContent(node, ps, null).trim().replaceAll("(\r\n|\n\r|\r|\n)", " ")));
            String topic = ps.chatSession.predicates.get("topic");
            if (MagicBooleans.trace_mode) {
                System.out.println(trace_count + ". <srai>" + result + "</srai> from " + ps.leaf.category.inputThatTopic() + " topic=" + topic + ") ");
                trace_count++;
            }
            Nodemapper leaf = ps.chatSession.bot.brain.match(result, ps.that, topic);
            if (leaf == null) {
                return response;
            }
            response = evalTemplate(leaf.category.getTemplate(), new ParseState(ps.depth + 1, ps.chatSession, ps.input, ps.that, topic, leaf));
            return response.trim();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private static String getAttributeOrTagValue(Node node, ParseState ps, String attributeName) {
        String result = BuildConfig.FLAVOR;
        Node m = node.getAttributes().getNamedItem(attributeName);
        if (m != null) {
            return m.getNodeValue();
        }
        NodeList childList = node.getChildNodes();
        result = null;
        for (int i = 0; i < childList.getLength(); i++) {
            Node child = childList.item(i);
            if (child.getNodeName().equals(attributeName)) {
                result = evalTagContent(child, ps, null);
            }
        }
        return result;
    }

    private static String sraix(Node node, ParseState ps) {
        HashSet<String> attributeNames = Utilities.stringSet("botid", "host");
        String host = getAttributeOrTagValue(node, ps, "host");
        String botid = getAttributeOrTagValue(node, ps, "botid");
        String hint = getAttributeOrTagValue(node, ps, "hint");
        String limit = getAttributeOrTagValue(node, ps, "limit");
        String defaultResponse = getAttributeOrTagValue(node, ps, "default");
        return Sraix.sraix(ps.chatSession, evalTagContent(node, ps, attributeNames), defaultResponse, hint, host, botid, null, limit);
    }

    private static String map(Node node, ParseState ps) {
        String result = MagicStrings.default_map;
        HashSet<String> attributeNames = Utilities.stringSet("name");
        String mapName = getAttributeOrTagValue(node, ps, "name");
        String contents = evalTagContent(node, ps, attributeNames).trim();
        if (mapName == null) {
            return "<map>" + contents + "</map>";
        }
        AIMLMap map = (AIMLMap) ps.chatSession.bot.mapMap.get(mapName);
        if (map != null) {
            result = map.get(contents.toUpperCase());
        }
        if (result == null) {
            result = MagicStrings.default_map;
        }
        return result.trim();
    }

    private static String set(Node node, ParseState ps) {
        HashSet<String> attributeNames = Utilities.stringSet("name", "var");
        String predicateName = getAttributeOrTagValue(node, ps, "name");
        String varName = getAttributeOrTagValue(node, ps, "var");
        String result = evalTagContent(node, ps, attributeNames).trim().replaceAll("(\r\n|\n\r|\r|\n)", " ");
        String value = result.trim();
        if (predicateName != null) {
            ps.chatSession.predicates.put(predicateName, result);
            MagicBooleans.trace("Set predicate " + predicateName + " to " + result + " in " + ps.leaf.category.inputThatTopic());
        }
        if (varName != null) {
            ps.vars.put(varName, result);
            MagicBooleans.trace("Set var " + varName + " to " + value + " in " + ps.leaf.category.inputThatTopic());
        }
        if (ps.chatSession.bot.pronounSet.contains(predicateName)) {
            return predicateName;
        }
        return result;
    }

    private static String get(Node node, ParseState ps) {
        String result = MagicStrings.default_get;
        String predicateName = getAttributeOrTagValue(node, ps, "name");
        String varName = getAttributeOrTagValue(node, ps, "var");
        String tupleName = getAttributeOrTagValue(node, ps, "tuple");
        if (predicateName != null) {
            return ps.chatSession.predicates.get(predicateName).trim();
        }
        if (varName != null && tupleName != null) {
            return tupleGet(tupleName, varName);
        }
        if (varName != null) {
            return ps.vars.get(varName).trim();
        }
        return result;
    }

    public static String tupleGet(String tupleName, String varName) {
        String result = MagicStrings.default_get;
        Tuple tuple = (Tuple) Tuple.tupleMap.get(tupleName);
        if (tuple == null) {
            return MagicStrings.default_get;
        }
        return tuple.getValue(varName);
    }

    private static String bot(Node node, ParseState ps) {
        String result = MagicStrings.default_property;
        String propertyName = getAttributeOrTagValue(node, ps, "name");
        if (propertyName != null) {
            return ps.chatSession.bot.properties.get(propertyName).trim();
        }
        return result;
    }

    private static String date(Node node, ParseState ps) {
        return CalendarUtils.date(getAttributeOrTagValue(node, ps, "jformat"), getAttributeOrTagValue(node, ps, "locale"), getAttributeOrTagValue(node, ps, "timezone"));
    }

    private static String interval(Node node, ParseState ps) {
        String style = getAttributeOrTagValue(node, ps, "style");
        String jformat = getAttributeOrTagValue(node, ps, "jformat");
        String from = getAttributeOrTagValue(node, ps, "from");
        String to = getAttributeOrTagValue(node, ps, "to");
        if (style == null) {
            style = "years";
        }
        if (jformat == null) {
            jformat = "MMMMMMMMM dd, yyyy";
        }
        if (from == null) {
            from = "January 1, 1970";
        }
        if (to == null) {
            to = CalendarUtils.date(jformat, null, null);
        }
        String result = "unknown";
        if (style.equals("years")) {
            result = BuildConfig.FLAVOR + IntervalUtils.getYearsBetween(from, to, jformat);
        }
        if (style.equals("months")) {
            result = BuildConfig.FLAVOR + IntervalUtils.getMonthsBetween(from, to, jformat);
        }
        if (style.equals("days")) {
            result = BuildConfig.FLAVOR + IntervalUtils.getDaysBetween(from, to, jformat);
        }
        if (style.equals("hours")) {
            return BuildConfig.FLAVOR + IntervalUtils.getHoursBetween(from, to, jformat);
        }
        return result;
    }

    private static int getIndexValue(Node node, ParseState ps) {
        String value = getAttributeOrTagValue(node, ps, "index");
        if (value == null) {
            return 0;
        }
        try {
            return Integer.parseInt(value) - 1;
        } catch (Exception ex) {
            ex.printStackTrace();
            return 0;
        }
    }

    private static String inputStar(Node node, ParseState ps) {
        String result = BuildConfig.FLAVOR;
        int index = getIndexValue(node, ps);
        if (ps.starBindings.inputStars.star(index) == null) {
            return BuildConfig.FLAVOR;
        }
        return ps.starBindings.inputStars.star(index).trim();
    }

    private static String thatStar(Node node, ParseState ps) {
        int index = getIndexValue(node, ps);
        if (ps.starBindings.thatStars.star(index) == null) {
            return BuildConfig.FLAVOR;
        }
        return ps.starBindings.thatStars.star(index).trim();
    }

    private static String topicStar(Node node, ParseState ps) {
        int index = getIndexValue(node, ps);
        if (ps.starBindings.topicStars.star(index) == null) {
            return BuildConfig.FLAVOR;
        }
        return ps.starBindings.topicStars.star(index).trim();
    }

    private static String id(Node node, ParseState ps) {
        return ps.chatSession.customerId;
    }

    private static String size(Node node, ParseState ps) {
        return String.valueOf(ps.chatSession.bot.brain.getCategories().size());
    }

    private static String vocabulary(Node node, ParseState ps) {
        return String.valueOf(ps.chatSession.bot.brain.getVocabulary().size());
    }

    private static String program(Node node, ParseState ps) {
        return MagicStrings.program_name_version;
    }

    private static String that(Node node, ParseState ps) {
        int index = 0;
        int jndex = 0;
        String value = getAttributeOrTagValue(node, ps, "index");
        if (value != null) {
            try {
                String[] spair = value.split(",");
                index = Integer.parseInt(spair[0]) - 1;
                jndex = Integer.parseInt(spair[1]) - 1;
                System.out.println("That index=" + index + "," + jndex);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        String that = MagicStrings.unknown_history_item;
        History hist = (History) ps.chatSession.thatHistory.get(index);
        if (hist != null) {
            that = (String) hist.get(jndex);
        }
        return that.trim();
    }

    private static String input(Node node, ParseState ps) {
        return ps.chatSession.inputHistory.getString(getIndexValue(node, ps));
    }

    private static String request(Node node, ParseState ps) {
        return ps.chatSession.requestHistory.getString(getIndexValue(node, ps)).trim();
    }

    private static String response(Node node, ParseState ps) {
        return ps.chatSession.responseHistory.getString(getIndexValue(node, ps)).trim();
    }

    private static String system(Node node, ParseState ps) {
        return IOUtils.system(evalTagContent(node, ps, Utilities.stringSet("timeout")), MagicStrings.system_failed);
    }

    private static String think(Node node, ParseState ps) {
        evalTagContent(node, ps, null);
        return BuildConfig.FLAVOR;
    }

    private static String explode(Node node, ParseState ps) {
        return explode(evalTagContent(node, ps, null));
    }

    private static String normalize(Node node, ParseState ps) {
        return ps.chatSession.bot.preProcessor.normalize(evalTagContent(node, ps, null));
    }

    private static String denormalize(Node node, ParseState ps) {
        return ps.chatSession.bot.preProcessor.denormalize(evalTagContent(node, ps, null));
    }

    private static String uppercase(Node node, ParseState ps) {
        return evalTagContent(node, ps, null).toUpperCase();
    }

    private static String lowercase(Node node, ParseState ps) {
        return evalTagContent(node, ps, null).toLowerCase();
    }

    private static String formal(Node node, ParseState ps) {
        return capitalizeString(evalTagContent(node, ps, null));
    }

    private static String sentence(Node node, ParseState ps) {
        String result = evalTagContent(node, ps, null);
        if (result.length() > 1) {
            return result.substring(0, 1).toUpperCase() + result.substring(1, result.length());
        }
        return BuildConfig.FLAVOR;
    }

    private static String person(Node node, ParseState ps) {
        String result;
        if (node.hasChildNodes()) {
            result = evalTagContent(node, ps, null);
        } else {
            result = ps.starBindings.inputStars.star(0);
        }
        return ps.chatSession.bot.preProcessor.person(" " + result + " ").trim();
    }

    private static String person2(Node node, ParseState ps) {
        String result;
        if (node.hasChildNodes()) {
            result = evalTagContent(node, ps, null);
        } else {
            result = ps.starBindings.inputStars.star(0);
        }
        return ps.chatSession.bot.preProcessor.person2(" " + result + " ").trim();
    }

    private static String gender(Node node, ParseState ps) {
        return ps.chatSession.bot.preProcessor.gender(" " + evalTagContent(node, ps, null) + " ").trim();
    }

    private static String random(Node node, ParseState ps) {
        NodeList childList = node.getChildNodes();
        ArrayList<Node> liList = new ArrayList();
        String setName = getAttributeOrTagValue(node, ps, "set");
        for (int i = 0; i < childList.getLength(); i++) {
            if (childList.item(i).getNodeName().equals("li")) {
                liList.add(childList.item(i));
            }
        }
        int index = (int) (Math.random() * ((double) liList.size()));
        if (MagicBooleans.qa_test_mode) {
            index = 0;
        }
        return evalTagContent((Node) liList.get(index), ps, null);
    }

    private static String unevaluatedAIML(Node node, ParseState ps) {
        return unevaluatedXML(learnEvalTagContent(node, ps), node, ps);
    }

    private static String recursLearn(Node node, ParseState ps) {
        String nodeName = node.getNodeName();
        if (nodeName.equals("#text")) {
            return node.getNodeValue();
        }
        if (nodeName.equals("eval")) {
            return evalTagContent(node, ps, null);
        }
        return unevaluatedAIML(node, ps);
    }

    private static String learnEvalTagContent(Node node, ParseState ps) {
        String result = BuildConfig.FLAVOR;
        NodeList childList = node.getChildNodes();
        for (int i = 0; i < childList.getLength(); i++) {
            result = result + recursLearn(childList.item(i), ps);
        }
        return result;
    }

    private static String learn(Node node, ParseState ps) {
        NodeList childList = node.getChildNodes();
        String pattern = BuildConfig.FLAVOR;
        String that = "*";
        String template = BuildConfig.FLAVOR;
        for (int i = 0; i < childList.getLength(); i++) {
            if (childList.item(i).getNodeName().equals("category")) {
                Category c;
                NodeList grandChildList = childList.item(i).getChildNodes();
                for (int j = 0; j < grandChildList.getLength(); j++) {
                    if (grandChildList.item(j).getNodeName().equals("pattern")) {
                        pattern = recursLearn(grandChildList.item(j), ps);
                    } else if (grandChildList.item(j).getNodeName().equals("that")) {
                        that = recursLearn(grandChildList.item(j), ps);
                    } else if (grandChildList.item(j).getNodeName().equals("template")) {
                        template = recursLearn(grandChildList.item(j), ps);
                    }
                }
                pattern = pattern.substring("<pattern>".length(), pattern.length() - "</pattern>".length());
                if (MagicBooleans.trace_mode) {
                    System.out.println("Learn Pattern = " + pattern);
                }
                if (template.length() >= "<template></template>".length()) {
                    template = template.substring("<template>".length(), template.length() - "</template>".length());
                }
                if (that.length() >= "<that></that>".length()) {
                    that = that.substring("<that>".length(), that.length() - "</that>".length());
                }
                pattern = pattern.toUpperCase().replaceAll("\n", " ").replaceAll("[ ]+", " ");
                that = that.toUpperCase().replaceAll("\n", " ").replaceAll("[ ]+", " ");
                if (MagicBooleans.trace_mode) {
                    System.out.println("Learn Pattern = " + pattern);
                    System.out.println("Learn That = " + that);
                    System.out.println("Learn Template = " + template);
                }
                if (node.getNodeName().equals("learn")) {
                    c = new Category(0, pattern, that, "*", template, MagicStrings.null_aiml_file);
                    ps.chatSession.bot.learnGraph.addCategory(c);
                } else {
                    c = new Category(0, pattern, that, "*", template, MagicStrings.learnf_aiml_file);
                    ps.chatSession.bot.learnfGraph.addCategory(c);
                }
                ps.chatSession.bot.brain.addCategory(c);
            }
        }
        return BuildConfig.FLAVOR;
    }

    private static String loopCondition(Node node, ParseState ps) {
        boolean loop = true;
        String result = BuildConfig.FLAVOR;
        while (loop && 0 < MagicNumbers.max_loops) {
            String loopResult = condition(node, ps);
            if (loopResult.trim().equals(MagicStrings.too_much_recursion)) {
                return MagicStrings.too_much_recursion;
            }
            if (loopResult.contains("<loop/>")) {
                loopResult = loopResult.replace("<loop/>", BuildConfig.FLAVOR);
                loop = true;
            } else {
                loop = false;
            }
            result = result + loopResult;
        }
        if (0 >= MagicNumbers.max_loops) {
            result = MagicStrings.too_much_looping;
        }
        return result;
    }

    private static String condition(Node node, ParseState ps) {
        int i;
        String value;
        String result = BuildConfig.FLAVOR;
        NodeList childList = node.getChildNodes();
        ArrayList<Node> liList = new ArrayList();
        HashSet<String> attributeNames = Utilities.stringSet("name", "var", Param.VALUE);
        String predicate = getAttributeOrTagValue(node, ps, "name");
        String varName = getAttributeOrTagValue(node, ps, "var");
        for (i = 0; i < childList.getLength(); i++) {
            if (childList.item(i).getNodeName().equals("li")) {
                liList.add(childList.item(i));
            }
        }
        if (liList.size() == 0) {
            value = getAttributeOrTagValue(node, ps, Param.VALUE);
            if (!(value == null || predicate == null || !ps.chatSession.predicates.get(predicate).equalsIgnoreCase(value))) {
                return evalTagContent(node, ps, attributeNames);
            }
        }
        if (liList.size() == 0) {
            value = getAttributeOrTagValue(node, ps, Param.VALUE);
            if (!(value == null || varName == null || !ps.vars.get(varName).equalsIgnoreCase(value))) {
                return evalTagContent(node, ps, attributeNames);
            }
        }
        for (i = 0; i < liList.size() && result.equals(BuildConfig.FLAVOR); i++) {
            Node n = (Node) liList.get(i);
            String liPredicate = predicate;
            String liVarName = varName;
            if (liPredicate == null) {
                liPredicate = getAttributeOrTagValue(n, ps, "name");
            }
            if (liVarName == null) {
                liVarName = getAttributeOrTagValue(n, ps, "var");
            }
            value = getAttributeOrTagValue(n, ps, Param.VALUE);
            if (value == null) {
                return evalTagContent(n, ps, attributeNames);
            }
            if (liPredicate != null && value != null && (ps.chatSession.predicates.get(liPredicate).equalsIgnoreCase(value) || (ps.chatSession.predicates.containsKey(liPredicate) && value.equals("*")))) {
                return evalTagContent(n, ps, attributeNames);
            }
            if (liVarName != null && value != null && (ps.vars.get(liVarName).equalsIgnoreCase(value) || (ps.vars.containsKey(liPredicate) && value.equals("*")))) {
                return evalTagContent(n, ps, attributeNames);
            }
        }
        return BuildConfig.FLAVOR;
    }

    public static boolean evalTagForLoop(Node node) {
        NodeList childList = node.getChildNodes();
        for (int i = 0; i < childList.getLength(); i++) {
            if (childList.item(i).getNodeName().equals("loop")) {
                return true;
            }
        }
        return false;
    }

    private static String deleteTriple(Node node, ParseState ps) {
        return ps.chatSession.tripleStore.deleteTriple(getAttributeOrTagValue(node, ps, "subj"), getAttributeOrTagValue(node, ps, "pred"), getAttributeOrTagValue(node, ps, "obj"));
    }

    private static String addTriple(Node node, ParseState ps) {
        return ps.chatSession.tripleStore.addTriple(getAttributeOrTagValue(node, ps, "subj"), getAttributeOrTagValue(node, ps, "pred"), getAttributeOrTagValue(node, ps, "obj"));
    }

    public static String uniq(Node node, ParseState ps) {
        HashSet<String> vars = new HashSet();
        HashSet<String> visibleVars = new HashSet();
        String subj = "?subject";
        String pred = "?predicate";
        String obj = "?object";
        NodeList childList = node.getChildNodes();
        for (int j = 0; j < childList.getLength(); j++) {
            Node childNode = childList.item(j);
            String contents = evalTagContent(childNode, ps, null);
            if (childNode.getNodeName().equals("subj")) {
                subj = contents;
            } else if (childNode.getNodeName().equals("pred")) {
                pred = contents;
            } else if (childNode.getNodeName().equals("obj")) {
                obj = contents;
            }
            if (contents.startsWith("?")) {
                visibleVars.add(contents);
                vars.add(contents);
            }
        }
        Tuple partial = new Tuple(vars, visibleVars);
        Clause clause = new Clause(subj, pred, obj);
        HashSet<Tuple> tuples = ps.chatSession.tripleStore.selectFromSingleClause(partial, clause, Boolean.valueOf(true));
        String tupleList = BuildConfig.FLAVOR;
        Iterator it = tuples.iterator();
        while (it.hasNext()) {
            tupleList = ((Tuple) it.next()).name + " " + tupleList;
        }
        tupleList = tupleList.trim();
        if (tupleList.length() == 0) {
            tupleList = "NIL";
        }
        String var = BuildConfig.FLAVOR;
        it = visibleVars.iterator();
        while (it.hasNext()) {
            var = (String) it.next();
        }
        return tupleGet(firstWord(tupleList), var);
    }

    public static String select(Node node, ParseState ps) {
        ArrayList<Clause> clauses = new ArrayList();
        NodeList childList = node.getChildNodes();
        HashSet<String> vars = new HashSet();
        HashSet<String> visibleVars = new HashSet();
        for (int i = 0; i < childList.getLength(); i++) {
            Node childNode = childList.item(i);
            if (childNode.getNodeName().equals("vars")) {
                for (String var : evalTagContent(childNode, ps, null).split(" ")) {
                    String var2 = var2.trim();
                    if (var2.length() > 0) {
                        visibleVars.add(var2);
                    }
                }
            } else if (childNode.getNodeName().equals("q") || childNode.getNodeName().equals("notq")) {
                Boolean affirm = Boolean.valueOf(!childNode.getNodeName().equals("notq"));
                NodeList grandChildList = childNode.getChildNodes();
                String subj = null;
                String pred = null;
                String obj = null;
                for (int j = 0; j < grandChildList.getLength(); j++) {
                    Node grandChildNode = grandChildList.item(j);
                    String contents = evalTagContent(grandChildNode, ps, null);
                    if (grandChildNode.getNodeName().equals("subj")) {
                        subj = contents;
                    } else if (grandChildNode.getNodeName().equals("pred")) {
                        pred = contents;
                    } else if (grandChildNode.getNodeName().equals("obj")) {
                        obj = contents;
                    }
                    if (contents.startsWith("?")) {
                        vars.add(contents);
                    }
                }
                clauses.add(new Clause(subj, pred, obj, affirm));
            }
        }
        HashSet<Tuple> tuples = ps.chatSession.tripleStore.select(vars, visibleVars, clauses);
        String result = BuildConfig.FLAVOR;
        Iterator it = tuples.iterator();
        while (it.hasNext()) {
            result = ((Tuple) it.next()).name + " " + result;
        }
        result = result.trim();
        if (result.length() == 0) {
            return "NIL";
        }
        return result;
    }

    public static String subject(Node node, ParseState ps) {
        String id = evalTagContent(node, ps, null);
        TripleStore ts = ps.chatSession.tripleStore;
        String subject = "unknown";
        if (ts.idTriple.containsKey(id)) {
            return ((Triple) ts.idTriple.get(id)).subject;
        }
        return subject;
    }

    public static String predicate(Node node, ParseState ps) {
        String id = evalTagContent(node, ps, null);
        TripleStore ts = ps.chatSession.tripleStore;
        if (ts.idTriple.containsKey(id)) {
            return ((Triple) ts.idTriple.get(id)).predicate;
        }
        return "unknown";
    }

    public static String object(Node node, ParseState ps) {
        String id = evalTagContent(node, ps, null);
        TripleStore ts = ps.chatSession.tripleStore;
        if (ts.idTriple.containsKey(id)) {
            return ((Triple) ts.idTriple.get(id)).object;
        }
        return "unknown";
    }

    public static String javascript(Node node, ParseState ps) {
        String result = MagicStrings.bad_javascript;
        try {
            result = IOUtils.evalScript("JavaScript", evalTagContent(node, ps, null));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        MagicBooleans.trace("in AIMLProcessor.javascript, returning result: " + result);
        return result;
    }

    public static String firstWord(String sentence) {
        String content;
        if (sentence == null) {
            content = BuildConfig.FLAVOR;
        } else {
            content = sentence;
        }
        content = content.trim();
        if (content.contains(" ")) {
            return content.substring(0, content.indexOf(" "));
        }
        if (content.length() > 0) {
            return content;
        }
        return MagicStrings.default_list_item;
    }

    public static String restWords(String sentence) {
        String content;
        if (sentence == null) {
            content = BuildConfig.FLAVOR;
        } else {
            content = sentence;
        }
        content = content.trim();
        if (content.contains(" ")) {
            return content.substring(content.indexOf(" ") + 1, content.length());
        }
        return MagicStrings.default_list_item;
    }

    public static String first(Node node, ParseState ps) {
        return firstWord(evalTagContent(node, ps, null));
    }

    public static String rest(Node node, ParseState ps) {
        return restWords(ps.chatSession.bot.preProcessor.normalize(evalTagContent(node, ps, null)));
    }

    public static String resetlearnf(Node node, ParseState ps) {
        ps.chatSession.bot.deleteLearnfCategories();
        return "Deleted Learnf Categories";
    }

    public static String resetlearn(Node node, ParseState ps) {
        ps.chatSession.bot.deleteLearnCategories();
        return "Deleted Learn Categories";
    }

    private static String recursEval(Node node, ParseState ps) {
        try {
            String nodeName = node.getNodeName();
            if (nodeName.equals("#text")) {
                return node.getNodeValue();
            }
            if (nodeName.equals("#comment")) {
                return BuildConfig.FLAVOR;
            }
            if (nodeName.equals("template")) {
                return evalTagContent(node, ps, null);
            }
            if (nodeName.equals("random")) {
                return random(node, ps);
            }
            if (nodeName.equals("condition")) {
                return loopCondition(node, ps);
            }
            if (nodeName.equals("srai")) {
                return srai(node, ps);
            }
            if (nodeName.equals("sr")) {
                return respond(ps.starBindings.inputStars.star(0), ps.that, ps.topic, ps.chatSession, sraiCount);
            }
            if (nodeName.equals("sraix")) {
                return sraix(node, ps);
            }
            if (nodeName.equals("set")) {
                return set(node, ps);
            }
            if (nodeName.equals("get")) {
                return get(node, ps);
            }
            if (nodeName.equals("map")) {
                return map(node, ps);
            }
            if (nodeName.equals("bot")) {
                return bot(node, ps);
            }
            if (nodeName.equals("id")) {
                return id(node, ps);
            }
            if (nodeName.equals("size")) {
                return size(node, ps);
            }
            if (nodeName.equals("vocabulary")) {
                return vocabulary(node, ps);
            }
            if (nodeName.equals("program")) {
                return program(node, ps);
            }
            if (nodeName.equals("date")) {
                return date(node, ps);
            }
            if (nodeName.equals("interval")) {
                return interval(node, ps);
            }
            if (nodeName.equals("think")) {
                return think(node, ps);
            }
            if (nodeName.equals("system")) {
                return system(node, ps);
            }
            if (nodeName.equals("explode")) {
                return explode(node, ps);
            }
            if (nodeName.equals("normalize")) {
                return normalize(node, ps);
            }
            if (nodeName.equals("denormalize")) {
                return denormalize(node, ps);
            }
            if (nodeName.equals("uppercase")) {
                return uppercase(node, ps);
            }
            if (nodeName.equals("lowercase")) {
                return lowercase(node, ps);
            }
            if (nodeName.equals("formal")) {
                return formal(node, ps);
            }
            if (nodeName.equals("sentence")) {
                return sentence(node, ps);
            }
            if (nodeName.equals("person")) {
                return person(node, ps);
            }
            if (nodeName.equals("person2")) {
                return person2(node, ps);
            }
            if (nodeName.equals("gender")) {
                return gender(node, ps);
            }
            if (nodeName.equals("star")) {
                return inputStar(node, ps);
            }
            if (nodeName.equals("thatstar")) {
                return thatStar(node, ps);
            }
            if (nodeName.equals("topicstar")) {
                return topicStar(node, ps);
            }
            if (nodeName.equals("that")) {
                return that(node, ps);
            }
            if (nodeName.equals("input")) {
                return input(node, ps);
            }
            if (nodeName.equals("request")) {
                return request(node, ps);
            }
            if (nodeName.equals("response")) {
                return response(node, ps);
            }
            if (nodeName.equals("learn") || nodeName.equals("learnf")) {
                return learn(node, ps);
            }
            if (nodeName.equals("addtriple")) {
                return addTriple(node, ps);
            }
            if (nodeName.equals("deletetriple")) {
                return deleteTriple(node, ps);
            }
            if (nodeName.equals("javascript")) {
                return javascript(node, ps);
            }
            if (nodeName.equals("select")) {
                return select(node, ps);
            }
            if (nodeName.equals("uniq")) {
                return uniq(node, ps);
            }
            if (nodeName.equals("first")) {
                return first(node, ps);
            }
            if (nodeName.equals("rest")) {
                return rest(node, ps);
            }
            if (nodeName.equals("resetlearnf")) {
                return resetlearnf(node, ps);
            }
            if (nodeName.equals("resetlearn")) {
                return resetlearn(node, ps);
            }
            if (extension == null || !extension.extensionTagSet().contains(nodeName)) {
                return genericXML(node, ps);
            }
            return extension.recursEval(node, ps);
        } catch (Exception ex) {
            ex.printStackTrace();
            return BuildConfig.FLAVOR;
        }
    }

    public static String evalTemplate(String template, ParseState ps) {
        String response = MagicStrings.template_failed;
        try {
            response = recursEval(DomUtils.parseString("<template>" + template + "</template>"), ps);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }

    public static boolean validTemplate(String template) {
        MagicBooleans.trace("AIMLProcessor.validTemplate(template: " + template + ")");
        try {
            template = "<template>" + template + "</template>";
            DomUtils.parseString(template);
            return true;
        } catch (Exception e) {
            System.out.println("Invalid Template " + template);
            return false;
        }
    }
}
