package com.amusoft.sophiebot.alice;

import android.content.Context;
import com.amusoft.sophiebot.BrainLogger;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import outlander.showcaseview.BuildConfig;

public class Alice {
    private static Bot bot;
    private static Context context;
    private static Alice instance;

    public static Alice setup(Context context) {
        instance = new Alice(context, new String[0]);
        return instance;
    }

    public static Alice getInstance() {
        if (instance != null) {
            return instance;
        }
        throw new UnsupportedOperationException("You forgot to call setup method to inizialize Alice brain");
    }

    private Alice(Context context, String[] args) {
        context = context;
        MagicStrings.setRootPath();
        AIMLProcessor.extension = new PCAIMLProcessorExtension();
        mainFunction(args);
    }

    public static Context getContext() {
        return context;
    }

    public static void mainFunction(String[] args) {
        String botName = "alice-it";
        MagicBooleans.jp_tokenize = false;
        MagicBooleans.trace_mode = true;
        String action = "chat";
        if (MagicBooleans.trace_mode) {
            System.out.println(MagicStrings.program_name_version);
            BrainLogger.getInstance().info(MagicStrings.program_name_version);
        }
        for (String s : args) {
            String[] splitArg = s.split("=");
            if (splitArg.length >= 2) {
                String option = splitArg[0];
                String value = splitArg[1];
                if (option.equals("bot")) {
                    botName = value;
                }
                if (option.equals("action")) {
                    action = value;
                }
                if (option.equals("trace")) {
                    MagicBooleans.trace_mode = value.equals("true");
                }
                if (option.equals("morph")) {
                    MagicBooleans.jp_tokenize = value.equals("true");
                }
            }
        }
        if (MagicBooleans.trace_mode) {
            System.out.println("Working Directory = " + MagicStrings.root_path);
        }
        Graphmaster.enableShortCuts = true;
        bot = new Bot(botName, MagicStrings.root_path, action);
        if (MagicBooleans.make_verbs_sets_maps) {
            Verbs.makeVerbSetsMaps(bot);
        }
        if (bot.brain.getCategories().size() < MagicNumbers.brain_print_size) {
            bot.brain.printgraph();
        }
        if (MagicBooleans.trace_mode) {
            System.out.println("Action = '" + action + "'");
        } else if (action.equals("ab")) {
            TestAB.testAB(bot, TestAB.sample_file);
        } else if (action.equals("aiml2csv") || action.equals("csv2aiml")) {
            convert(bot, action);
        } else if (action.equals("abwq")) {
            new AB(bot, TestAB.sample_file).abwq();
        } else if (action.equals("test")) {
            TestAB.runTests(bot, MagicBooleans.trace_mode);
        } else if (action.equals("shadow")) {
            MagicBooleans.trace_mode = false;
            bot.shadowChecker();
        } else if (action.equals("iqtest")) {
            try {
                new ChatTest(bot).testMultisentenceRespond();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } else {
            System.out.println("Unrecognized action " + action);
        }
    }

    public static void convert(Bot bot, String action) {
        if (action.equals("aiml2csv")) {
            bot.writeAIMLIFFiles();
        } else if (action.equals("csv2aiml")) {
            bot.writeAIMLFiles();
        }
    }

    public String processInput(String request) {
        String response = BuildConfig.FLAVOR;
        Chat chatSession = new Chat(bot, false);
        bot.brain.nodeStats();
        response = chatSession.multisentenceRespond(request);
        while (response.contains("&lt;")) {
            response = response.replace("&lt;", "<");
        }
        while (response.contains("&gt;")) {
            response = response.replace("&gt;", ">");
        }
        return response;
    }

    public static void getGloss(Bot bot, String filename) {
        System.out.println("getGloss");
        try {
            if (new File(filename).exists()) {
                FileInputStream fstream = new FileInputStream(filename);
                getGlossFromInputStream(bot, fstream);
                fstream.close();
            }
        } catch (Exception e) {
            System.err.println("Error: " + e.getMessage());
        }
    }

    public static void getGlossFromInputStream(Bot bot, InputStream in) {
        String gloss;
        Exception ex;
        System.out.println("getGlossFromInputStream");
        BufferedReader br = new BufferedReader(new InputStreamReader(in));
        int cnt = 0;
        int filecnt = 0;
        HashMap<String, String> def = new HashMap();
        String word = null;
        String gloss2 = null;
        while (true) {
            try {
                String strLine = br.readLine();
                if (strLine == null) {
                    break;
                }
                if (strLine.contains("<entry word")) {
                    word = strLine.substring(strLine.indexOf("<entry word=\"") + "<entry word=\"".length(), strLine.indexOf("#")).replaceAll("_", " ");
                    System.out.println(word);
                    gloss = gloss2;
                } else {
                    if (strLine.contains("<gloss>")) {
                        try {
                            gloss = strLine.replaceAll("<gloss>", BuildConfig.FLAVOR).replaceAll("</gloss>", BuildConfig.FLAVOR).trim();
                            System.out.println(gloss);
                        } catch (Exception e) {
                            ex = e;
                        }
                    } else {
                        gloss = gloss2;
                    }
                }
                if (word == null || gloss == null) {
                    gloss2 = gloss;
                } else {
                    String definition;
                    word = word.toLowerCase().trim();
                    if (gloss.length() > 2) {
                        gloss = gloss.substring(0, 1).toUpperCase() + gloss.substring(1, gloss.length());
                    }
                    if (def.keySet().contains(word)) {
                        definition = ((String) def.get(word)) + "; " + gloss;
                    } else {
                        definition = gloss;
                    }
                    def.put(word, definition);
                    word = null;
                    gloss2 = null;
                }
            } catch (Exception e2) {
                ex = e2;
                gloss = gloss2;
            }
        }
        bot.brain.addCategory(new Category(0, "WNDEF *", "*", "*", "unknown", "wndefs" + 0 + ".aiml"));
        gloss = gloss2;
        for (String word2 : def.keySet()) {
            gloss = ((String) def.get(word2)) + ".";
            cnt++;
            if (cnt % 5000 == 0) {
                filecnt++;
            }
            Category c = new Category(0, "WNDEF " + word2, "*", "*", gloss, "wndefs" + filecnt + ".aiml");
            System.out.println(cnt + " " + filecnt + " " + c.inputThatTopic() + ":" + c.getTemplate() + ":" + c.getFilename());
            Nodemapper node = bot.brain.findNode(c);
            if (node != null) {
                node.category.setTemplate(node.category.getTemplate() + "," + gloss);
            }
            bot.brain.addCategory(c);
        }
        return;
        ex.printStackTrace();
    }

    public static void sraixCache(String filename, Chat chatSession) {
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(filename)));
            int count = 0;
            while (true) {
                String strLine = br.readLine();
                int count2;
                if (strLine != null) {
                    count2 = count + 1;
                    if (count < 1000) {
                        System.out.println("Human: " + strLine);
                        System.out.println("Robot: " + chatSession.multisentenceRespond(strLine));
                        count = count2;
                    } else {
                        return;
                    }
                }
                count2 = count;
                return;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
