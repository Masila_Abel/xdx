package com.amusoft.sophiebot.alice;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import outlander.showcaseview.BuildConfig;

public class Tuple extends HashMap<String, String> {
    public static int index = 0;
    public static HashMap<String, Tuple> tupleMap = new HashMap();
    String name;
    public HashSet<String> visibleVars;

    public boolean equals(Object o) {
        Tuple tuple = (Tuple) o;
        if (this.visibleVars.size() != tuple.visibleVars.size()) {
            return false;
        }
        Iterator it = this.visibleVars.iterator();
        while (it.hasNext()) {
            String x = (String) it.next();
            if (!tuple.visibleVars.contains(x)) {
                return false;
            }
            if (get(x) != null && !((String) get(x)).equals(tuple.get(x))) {
                return false;
            }
        }
        if (values().contains(MagicStrings.unbound_variable)) {
            return false;
        }
        if (tuple.values().contains(MagicStrings.unbound_variable)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int result = 1;
        Iterator it = this.visibleVars.iterator();
        while (it.hasNext()) {
            String x = (String) it.next();
            result = (result * 31) + x.hashCode();
            if (get(x) != null) {
                result = (result * 31) + ((String) get(x)).hashCode();
            }
        }
        return result;
    }

    public Tuple(HashSet<String> varSet, HashSet<String> visibleVars, Tuple tuple) {
        Iterator it;
        this.visibleVars = new HashSet();
        if (visibleVars != null) {
            this.visibleVars.addAll(visibleVars);
        }
        if (varSet == null && tuple != null) {
            for (String key : tuple.keySet()) {
                put(key, tuple.get(key));
            }
            this.visibleVars.addAll(tuple.visibleVars);
        }
        if (varSet != null) {
            it = varSet.iterator();
            while (it.hasNext()) {
                put((String) it.next(), MagicStrings.unbound_variable);
            }
        }
        this.name = "tuple" + index;
        index++;
        tupleMap.put(this.name, this);
    }

    public Tuple(Tuple tuple) {
        this(null, null, tuple);
    }

    public Tuple(HashSet<String> varSet, HashSet<String> visibleVars) {
        this(varSet, visibleVars, null);
    }

    public Set<String> getVars() {
        return keySet();
    }

    public String printVars() {
        String result = BuildConfig.FLAVOR;
        for (String x : getVars()) {
            if (this.visibleVars.contains(x)) {
                result = result + " " + x;
            } else {
                result = result + " [" + x + "]";
            }
        }
        return result;
    }

    public String getValue(String var) {
        String result = (String) get(var);
        if (result == null) {
            return MagicStrings.default_get;
        }
        return result;
    }

    public void bind(String var, String value) {
        if (get(var) == null || ((String) get(var)).equals(MagicStrings.unbound_variable)) {
            put(var, value);
        } else {
            System.out.println(var + " already bound to " + ((String) get(var)));
        }
    }

    public String printTuple() {
        String result = "\n";
        for (String x : keySet()) {
            result = result + x + "=" + ((String) get(x)) + "\n";
        }
        return result.trim();
    }
}
