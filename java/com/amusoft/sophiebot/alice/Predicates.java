package com.amusoft.sophiebot.alice;

import com.amusoft.sophiebot.alice.utils.JapaneseUtils;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;

public class Predicates extends HashMap<String, String> {
    public String put(String key, String value) {
        if (MagicBooleans.jp_tokenize && key.equals("topic")) {
            value = JapaneseUtils.tokenizeSentence(value);
        }
        if (key.equals("topic") && value.length() == 0) {
            value = MagicStrings.default_get;
        }
        if (value.equals(MagicStrings.too_much_recursion)) {
            value = MagicStrings.default_list_item;
        }
        return (String) super.put(key, value);
    }

    public String get(String key) {
        String result = (String) super.get(key);
        if (result == null) {
            return MagicStrings.default_get;
        }
        return result;
    }

    public void getPredicateDefaultsFromInputStream(InputStream in) {
        BufferedReader br = new BufferedReader(new InputStreamReader(in));
        while (true) {
            try {
                String strLine = br.readLine();
                if (strLine == null) {
                    return;
                }
                if (strLine.contains(":")) {
                    put(strLine.substring(0, strLine.indexOf(":")), strLine.substring(strLine.indexOf(":") + 1));
                }
            } catch (Exception ex) {
                ex.printStackTrace();
                return;
            }
        }
    }

    public void getPredicateDefaults(String filename) {
        try {
            if (new File(filename).exists()) {
                FileInputStream fstream = new FileInputStream(filename);
                getPredicateDefaultsFromInputStream(fstream);
                fstream.close();
            }
        } catch (Exception e) {
            System.err.println("Error: " + e.getMessage());
        }
    }
}
