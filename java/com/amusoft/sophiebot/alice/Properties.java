package com.amusoft.sophiebot.alice;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;

public class Properties extends HashMap<String, String> {
    public String get(String key) {
        String result = (String) super.get(key);
        if (result == null) {
            return MagicStrings.default_property;
        }
        return result;
    }

    public int getPropertiesFromInputStream(InputStream in) {
        int cnt = 0;
        BufferedReader br = new BufferedReader(new InputStreamReader(in));
        while (true) {
            try {
                String strLine = br.readLine();
                if (strLine == null) {
                    break;
                } else if (strLine.contains(":")) {
                    put(strLine.substring(0, strLine.indexOf(":")), strLine.substring(strLine.indexOf(":") + 1));
                    cnt++;
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        return cnt;
    }

    public int getProperties(String filename) {
        if (MagicBooleans.trace_mode) {
            System.out.println("Get Properties: " + filename);
        }
        try {
            if (!new File(filename).exists()) {
                return 0;
            }
            if (MagicBooleans.trace_mode) {
                System.out.println("Exists: " + filename);
            }
            FileInputStream fstream = new FileInputStream(filename);
            int cnt = getPropertiesFromInputStream(fstream);
            fstream.close();
            return cnt;
        } catch (Exception e) {
            System.err.println("Error: " + e.getMessage());
            return 0;
        }
    }
}
