package com.amusoft.sophiebot.chatroom;

import android.app.Activity;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.support.v4.app.NotificationCompat.Builder;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.amusoft.sophiebot.R;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public abstract class FirebaseListAdapter extends ArrayAdapter<ChatMessage> {
    Activity activity;
    private LayoutInflater mInflater;
    private List<String> mKeys = new ArrayList();
    private ChildEventListener mListener = this.mRef.addChildEventListener(new ChildEventListener() {
        public void onChildAdded(DataSnapshot dataSnapshot, String previousChildName) {
            Map<String, Object> newPost = (Map) dataSnapshot.getValue();
            try {
                FirebaseListAdapter.this.model = new ChatMessage(newPost.get("message").toString(), newPost.get("author").toString());
            } catch (Exception e) {
            }
            String key = dataSnapshot.getKey();
            if (previousChildName == null) {
                FirebaseListAdapter.this.mModels.add(0, FirebaseListAdapter.this.model);
                FirebaseListAdapter.this.mKeys.add(0, key);
            } else {
                int nextIndex = FirebaseListAdapter.this.mKeys.indexOf(previousChildName) + 1;
                if (nextIndex == FirebaseListAdapter.this.mModels.size()) {
                    FirebaseListAdapter.this.mModels.add(FirebaseListAdapter.this.model);
                    FirebaseListAdapter.this.mKeys.add(key);
                } else {
                    FirebaseListAdapter.this.mModels.add(nextIndex, FirebaseListAdapter.this.model);
                    FirebaseListAdapter.this.mKeys.add(nextIndex, key);
                    FirebaseListAdapter.this.showNotification(FirebaseListAdapter.this.model);
                }
            }
            FirebaseListAdapter.this.notifyDataSetChanged();
        }

        public void onChildChanged(DataSnapshot dataSnapshot, String s) {
            String key = dataSnapshot.getKey();
            Map<String, Object> newPost = (Map) dataSnapshot.getValue();
            ChatMessage newModel = new ChatMessage(newPost.get("message").toString(), newPost.get("author").toString());
            FirebaseListAdapter.this.mModels.set(FirebaseListAdapter.this.mKeys.indexOf(key), newModel);
            FirebaseListAdapter.this.notifyDataSetChanged();
        }

        public void onChildRemoved(DataSnapshot dataSnapshot) {
            int index = FirebaseListAdapter.this.mKeys.indexOf(dataSnapshot.getKey());
            FirebaseListAdapter.this.mKeys.remove(index);
            FirebaseListAdapter.this.mModels.remove(index);
            FirebaseListAdapter.this.notifyDataSetChanged();
        }

        public void onChildMoved(DataSnapshot dataSnapshot, String previousChildName) {
            String key = dataSnapshot.getKey();
            Map<String, Object> newPost = (Map) dataSnapshot.getValue();
            ChatMessage newModel = new ChatMessage(newPost.get("message").toString(), newPost.get("author").toString());
            int index = FirebaseListAdapter.this.mKeys.indexOf(key);
            FirebaseListAdapter.this.mModels.remove(index);
            FirebaseListAdapter.this.mKeys.remove(index);
            if (previousChildName == null) {
                FirebaseListAdapter.this.mModels.add(0, newModel);
                FirebaseListAdapter.this.mKeys.add(0, key);
            } else {
                int nextIndex = FirebaseListAdapter.this.mKeys.indexOf(previousChildName) + 1;
                if (nextIndex == FirebaseListAdapter.this.mModels.size()) {
                    FirebaseListAdapter.this.mModels.add(newModel);
                    FirebaseListAdapter.this.mKeys.add(key);
                } else {
                    FirebaseListAdapter.this.mModels.add(nextIndex, newModel);
                    FirebaseListAdapter.this.mKeys.add(nextIndex, key);
                }
            }
            FirebaseListAdapter.this.notifyDataSetChanged();
        }

        public void onCancelled(DatabaseError databaseError) {
            Log.e("FirebaseListAdapter", "Listen was cancelled, no more updates will occur");
        }
    });
    private List<ChatMessage> mModels = new ArrayList();
    DatabaseReference mRef;
    private String mUsername;
    private ChatMessage model;
    private ViewGroup viewGroup;
    private LinearLayout wrapper;

    public FirebaseListAdapter(DatabaseReference mRef, Activity activity, String mUsername) {
        super(activity, 17367043);
        this.activity = activity;
        this.mRef = mRef;
        this.mUsername = mUsername;
        this.mInflater = activity.getLayoutInflater();
    }

    private void showNotification(ChatMessage model) {
        Intent intent = new Intent(getContext(), ChatActivity.class);
        intent.addFlags(67108864);
        ((NotificationManager) getContext().getSystemService("notification")).notify(0, new Builder(getContext()).setSmallIcon(R.drawable.ic_bot).setContentTitle("New Message in Forum").setContentText(model.getMessage()).setAutoCancel(true).setSound(RingtoneManager.getDefaultUri(2)).setContentIntent(PendingIntent.getActivity(getContext(), 0, intent, 1073741824)).build());
    }

    public void cleanup() {
        this.mRef.removeEventListener(this.mListener);
        this.mModels.clear();
        this.mKeys.clear();
    }

    public int getCount() {
        return this.mModels.size();
    }

    public ChatMessage getItem(int i) {
        return (ChatMessage) this.mModels.get(i);
    }

    public long getItemId(int i) {
        return (long) i;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        ChatMessage comment = getItem(position);
        int type = getItemViewType(position);
        String author = comment.getAuthor();
        Context context = getContext();
        this.activity.getApplicationContext();
        LayoutInflater inflater = (LayoutInflater) context.getSystemService("layout_inflater");
        if (author != null && author.contains(this.mUsername)) {
            row = inflater.inflate(R.layout.chat_listitem_right, parent, false);
            ((TextView) row.findViewById(outlander.showcaseview.R.id.text)).setText(comment.getMessage());
        } else if (author == null || !author.contains("Client")) {
            row = inflater.inflate(R.layout.chat_listitem_left, parent, false);
            ((TextView) row.findViewById(outlander.showcaseview.R.id.text)).setText(this.mUsername + "\n" + comment.getMessage());
        } else {
            row = inflater.inflate(R.layout.chat_listitem_left, parent, false);
            ((TextView) row.findViewById(outlander.showcaseview.R.id.text)).setText(comment.getMessage());
        }
        this.wrapper = (LinearLayout) row.findViewById(R.id.wrapper);
        return row;
    }
}
