package com.amusoft.sophiebot.chatroom;

public class ChatMessage {
    private String author;
    public String message;

    private ChatMessage() {
    }

    public ChatMessage(String message, String author) {
        this.message = message;
        this.author = author;
    }

    public String getMessage() {
        return this.message;
    }

    public String getAuthor() {
        return this.author;
    }
}
