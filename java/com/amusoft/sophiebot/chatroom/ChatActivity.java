package com.amusoft.sophiebot.chatroom;

import android.content.Intent;
import android.database.DataSetObserver;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnKeyListener;
import android.view.ViewConfiguration;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;
import com.amusoft.sophiebot.MainActivity;
import com.amusoft.sophiebot.R;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.analytics.FirebaseAnalytics.Param;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import java.lang.reflect.Field;
import outlander.showcaseview.BuildConfig;

public class ChatActivity extends AppCompatActivity {
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    EditText inputText;
    ListView listView;
    private FirebaseListAdapter mChatListAdapter;
    private ValueEventListener mConnectedListener;
    private FirebaseAnalytics mFirebaseAnalytics;
    DatabaseReference mFirebaseRef = this.database.getReference("chat");
    private String mUsername;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        setupUsername();
        setToolBar();
        getOverflowMenu();
        this.inputText = (EditText) findViewById(R.id.chat_editText);
        this.inputText.setOnKeyListener(new OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() != 0 || keyCode != 66) {
                    return false;
                }
                String question = ChatActivity.this.inputText.getText().toString();
                ChatActivity.this.sendMessage();
                ChatActivity.this.inputText.setText(BuildConfig.FLAVOR);
                Bundle bundle = new Bundle();
                bundle.putString(Param.ITEM_ID, "ASKED QUESTION FORUM");
                bundle.putString(Param.ITEM_NAME, "ASKED QUESTION");
                bundle.putString(Param.CONTENT_TYPE, "TEXT");
                ChatActivity.this.mFirebaseAnalytics.logEvent("ASKED QUESTION FORUM", bundle);
                return true;
            }
        });
    }

    public void onStart() {
        super.onStart();
        this.mChatListAdapter = new FirebaseListAdapter(this.mFirebaseRef, this, this.mUsername) {
            public void cleanup() {
                super.cleanup();
            }
        };
        this.listView = (ListView) findViewById(R.id.chat_listView);
        this.listView.setAdapter(this.mChatListAdapter);
        this.mChatListAdapter.registerDataSetObserver(new DataSetObserver() {
            public void onChanged() {
                super.onChanged();
                ChatActivity.this.listView.setSelection(ChatActivity.this.mChatListAdapter.getCount() - 1);
            }
        });
        this.mConnectedListener = this.mFirebaseRef.getRoot().child(".info/connected").addValueEventListener(new ValueEventListener() {
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (((Boolean) dataSnapshot.getValue()).booleanValue()) {
                    Toast.makeText(ChatActivity.this.getApplicationContext(), "Connected", 0).show();
                } else {
                    Toast.makeText(ChatActivity.this.getApplicationContext(), "Disconnected", 0).show();
                }
            }

            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }

    public void onStop() {
        super.onStop();
        this.mFirebaseRef.getRoot().child(".info/connected").removeEventListener(this.mConnectedListener);
        this.mChatListAdapter.cleanup();
    }

    private void setupUsername() {
        this.mUsername = getApplication().getSharedPreferences("SophiePrefs", 0).getString("username", null);
    }

    private void sendMessage() {
        EditText inputText = (EditText) findViewById(R.id.chat_editText);
        String input = inputText.getText().toString();
        if (!input.equals(BuildConfig.FLAVOR)) {
            ChatMessage chat = new ChatMessage(input, this.mUsername);
            this.mFirebaseRef = this.database.getReference("chat");
            this.mFirebaseRef.push().setValue(chat);
            inputText.setText(BuildConfig.FLAVOR);
        }
    }

    private void setToolBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            setUpActionbar();
            getOverflowMenu();
            toolbar.setBackgroundColor(getResources().getColor(R.color.primary));
            toolbar.setTitleTextColor(getResources().getColor(R.color.icons));
        }
    }

    private void setUpActionbar() {
        if (getSupportActionBar() != null) {
            ActionBar bar = getSupportActionBar();
            bar.setTitle(getResources().getString(outlander.showcaseview.R.string.app_name));
            bar.setHomeButtonEnabled(true);
            bar.setDisplayShowHomeEnabled(true);
            bar.setDisplayHomeAsUpEnabled(true);
            bar.setDisplayShowTitleEnabled(true);
            bar.setNavigationMode(0);
        }
    }

    private void getOverflowMenu() {
        try {
            ViewConfiguration config = ViewConfiguration.get(this);
            Field menuKeyField = ViewConfiguration.class.getDeclaredField("sHasPermanentMenuKey");
            if (menuKeyField != null) {
                menuKeyField.setAccessible(true);
                menuKeyField.setBoolean(config, false);
                menuKeyField.isSynthetic();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_about, menu);
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() != 16908332) {
            return super.onOptionsItemSelected(item);
        }
        Bundle bundle = new Bundle();
        bundle.putString(Param.ITEM_ID, "BACK TO SOPHIE");
        bundle.putString(Param.ITEM_NAME, "BACK TO SOPHIE");
        bundle.putString(Param.CONTENT_TYPE, "TEXT");
        this.mFirebaseAnalytics.logEvent("BACK TO SOPHIE", bundle);
        startActivity(new Intent(getApplicationContext(), MainActivity.class));
        return true;
    }
}
