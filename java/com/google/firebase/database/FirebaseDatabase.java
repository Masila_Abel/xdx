package com.google.firebase.database;

import com.google.android.gms.internal.zzahi;
import com.google.android.gms.internal.zzaho;
import com.google.android.gms.internal.zzahq;
import com.google.android.gms.internal.zzahr;
import com.google.android.gms.internal.zzahs;
import com.google.android.gms.internal.zzalj;
import com.google.android.gms.internal.zzall;
import com.google.android.gms.internal.zzalm;
import com.google.firebase.FirebaseApp;
import com.google.firebase.database.Logger.Level;
import java.util.HashMap;
import java.util.Map;

public class FirebaseDatabase {
    private static final Map<String, FirebaseDatabase> aPe = new HashMap();
    private final FirebaseApp aPf;
    private final zzahr aPg;
    private final zzahi aPh;
    private zzahq aPi;

    private FirebaseDatabase(FirebaseApp firebaseApp, zzahr com_google_android_gms_internal_zzahr, zzahi com_google_android_gms_internal_zzahi) {
        this.aPf = firebaseApp;
        this.aPg = com_google_android_gms_internal_zzahr;
        this.aPh = com_google_android_gms_internal_zzahi;
    }

    public static FirebaseDatabase getInstance() {
        return getInstance(FirebaseApp.getInstance());
    }

    public static synchronized FirebaseDatabase getInstance(FirebaseApp firebaseApp) {
        FirebaseDatabase firebaseDatabase;
        synchronized (FirebaseDatabase.class) {
            if (!aPe.containsKey(firebaseApp.getName())) {
                String databaseUrl = firebaseApp.getOptions().getDatabaseUrl();
                if (databaseUrl == null) {
                    throw new DatabaseException("Failed to get FirebaseDatabase instance: FirebaseApp object has no DatabaseURL in its FirebaseOptions object.");
                }
                zzalj zzsi = zzall.zzsi(databaseUrl);
                if (zzsi.aPq.isEmpty()) {
                    zzahi com_google_android_gms_internal_zzahi = new zzahi();
                    if (!firebaseApp.zzckb()) {
                        com_google_android_gms_internal_zzahi.zzsa(firebaseApp.getName());
                    }
                    com_google_android_gms_internal_zzahi.zze(firebaseApp);
                    aPe.put(firebaseApp.getName(), new FirebaseDatabase(firebaseApp, zzsi.aPg, com_google_android_gms_internal_zzahi));
                } else {
                    String valueOf = String.valueOf(zzsi.aPq.toString());
                    throw new DatabaseException(new StringBuilder((String.valueOf(databaseUrl).length() + 114) + String.valueOf(valueOf).length()).append("Configured Database URL '").append(databaseUrl).append("' is invalid. It should point to the root of a Firebase Database but it includes a path: ").append(valueOf).toString());
                }
            }
            firebaseDatabase = (FirebaseDatabase) aPe.get(firebaseApp.getName());
        }
        return firebaseDatabase;
    }

    public static String getSdkVersion() {
        return "3.0.0";
    }

    private synchronized void zzcmp() {
        if (this.aPi == null) {
            this.aPi = zzahs.zza(this.aPh, this.aPg, this);
        }
    }

    private void zzrh(String str) {
        if (this.aPi != null) {
            throw new DatabaseException(new StringBuilder(String.valueOf(str).length() + 77).append("Calls to ").append(str).append("() must be made before any other usage of FirebaseDatabase instance.").toString());
        }
    }

    public FirebaseApp getApp() {
        return this.aPf;
    }

    public DatabaseReference getReference() {
        zzcmp();
        return new DatabaseReference(this.aPi, zzaho.zzcqw());
    }

    public DatabaseReference getReference(String str) {
        zzcmp();
        if (str == null) {
            throw new NullPointerException("Can't pass null for argument 'pathString' in FirebaseDatabase.getReference()");
        }
        zzalm.zzso(str);
        return new DatabaseReference(this.aPi, new zzaho(str));
    }

    public DatabaseReference getReferenceFromUrl(String str) {
        zzcmp();
        if (str == null) {
            throw new NullPointerException("Can't pass null for argument 'url' in FirebaseDatabase.getReferenceFromUrl()");
        }
        zzalj zzsi = zzall.zzsi(str);
        if (zzsi.aPg.aQT.equals(this.aPi.zzcrf().aQT)) {
            return new DatabaseReference(this.aPi, zzsi.aPq);
        }
        String valueOf = String.valueOf(getReference().toString());
        throw new DatabaseException(new StringBuilder((String.valueOf(str).length() + 93) + String.valueOf(valueOf).length()).append("Invalid URL (").append(str).append(") passed to getReference().  URL was expected to match configured Database URL: ").append(valueOf).toString());
    }

    public void goOffline() {
        zzcmp();
        zzahs.zzk(this.aPi);
    }

    public void goOnline() {
        zzcmp();
        zzahs.zzl(this.aPi);
    }

    public void purgeOutstandingWrites() {
        zzcmp();
        this.aPi.zzs(new Runnable(this) {
            final /* synthetic */ FirebaseDatabase aPj;

            {
                this.aPj = r1;
            }

            public void run() {
                this.aPj.aPi.purgeOutstandingWrites();
            }
        });
    }

    public synchronized void setLogLevel(Level level) {
        zzrh("setLogLevel");
        this.aPh.setLogLevel(level);
    }

    public synchronized void setPersistenceEnabled(boolean z) {
        zzrh("setPersistenceEnabled");
        this.aPh.setPersistenceEnabled(z);
    }
}
