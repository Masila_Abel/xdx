package com.google.firebase.database;

import com.google.android.gms.internal.zzahe;
import com.google.android.gms.internal.zzahj;
import com.google.android.gms.internal.zzaho;
import com.google.android.gms.internal.zzahq;
import com.google.android.gms.internal.zzaie;
import com.google.android.gms.internal.zzaih;
import com.google.android.gms.internal.zzaji;
import com.google.android.gms.internal.zzajj;
import com.google.android.gms.internal.zzajw;
import com.google.android.gms.internal.zzajx;
import com.google.android.gms.internal.zzakb;
import com.google.android.gms.internal.zzakc;
import com.google.android.gms.internal.zzakf;
import com.google.android.gms.internal.zzakj;
import com.google.android.gms.internal.zzakl;
import com.google.android.gms.internal.zzakm;
import com.google.android.gms.internal.zzakn;
import com.google.android.gms.internal.zzakp;
import com.google.android.gms.internal.zzakq;
import com.google.android.gms.internal.zzall;
import com.google.android.gms.internal.zzalm;

public class Query {
    static final /* synthetic */ boolean $assertionsDisabled = (!Query.class.desiredAssertionStatus());
    protected final zzahq aPi;
    protected final zzaho aPq;
    protected final zzaji aPt;
    private final boolean aPu;

    Query(zzahq com_google_android_gms_internal_zzahq, zzaho com_google_android_gms_internal_zzaho) {
        this.aPi = com_google_android_gms_internal_zzahq;
        this.aPq = com_google_android_gms_internal_zzaho;
        this.aPt = zzaji.aXC;
        this.aPu = false;
    }

    Query(zzahq com_google_android_gms_internal_zzahq, zzaho com_google_android_gms_internal_zzaho, zzaji com_google_android_gms_internal_zzaji, boolean z) throws DatabaseException {
        this.aPi = com_google_android_gms_internal_zzahq;
        this.aPq = com_google_android_gms_internal_zzaho;
        this.aPt = com_google_android_gms_internal_zzaji;
        this.aPu = z;
        zzall.zzb(com_google_android_gms_internal_zzaji.isValid(), "Validation of queries failed.");
    }

    private Query zza(zzakj com_google_android_gms_internal_zzakj, String str) {
        zzalm.zzsr(str);
        if (!com_google_android_gms_internal_zzakj.zzcuu() && !com_google_android_gms_internal_zzakj.isEmpty()) {
            throw new IllegalArgumentException("Can only use simple values for startAt()");
        } else if (this.aPt.zzcti()) {
            throw new IllegalArgumentException("Can't call startAt() or equalTo() multiple times");
        } else {
            zzaji zza = this.aPt.zza(com_google_android_gms_internal_zzakj, str != null ? zzajx.zzsc(str) : null);
            zzb(zza);
            zza(zza);
            if ($assertionsDisabled || zza.isValid()) {
                return new Query(this.aPi, this.aPq, zza, this.aPu);
            }
            throw new AssertionError();
        }
    }

    private void zza(final zzahj com_google_android_gms_internal_zzahj) {
        zzaih.zzcsb().zzk(com_google_android_gms_internal_zzahj);
        this.aPi.zzs(new Runnable(this) {
            final /* synthetic */ Query aPw;

            public void run() {
                this.aPw.aPi.zze(com_google_android_gms_internal_zzahj);
            }
        });
    }

    private void zza(zzaji com_google_android_gms_internal_zzaji) {
        if (com_google_android_gms_internal_zzaji.zzctq().equals(zzakf.zzcvn())) {
            zzakj zzctj;
            String str = "You must use startAt(String value), endAt(String value) or equalTo(String value) in combination with orderByKey(). Other type of values or using the version with 2 parameters is not supported";
            if (com_google_android_gms_internal_zzaji.zzcti()) {
                zzctj = com_google_android_gms_internal_zzaji.zzctj();
                if (!(com_google_android_gms_internal_zzaji.zzctk() == zzajx.zzcun() && (zzctj instanceof zzakp))) {
                    throw new IllegalArgumentException(str);
                }
            }
            if (com_google_android_gms_internal_zzaji.zzctl()) {
                zzctj = com_google_android_gms_internal_zzaji.zzctm();
                if (com_google_android_gms_internal_zzaji.zzctn() != zzajx.zzcuo() || !(zzctj instanceof zzakp)) {
                    throw new IllegalArgumentException(str);
                }
            }
        } else if (!com_google_android_gms_internal_zzaji.zzctq().equals(zzakm.zzcvr())) {
        } else {
            if ((com_google_android_gms_internal_zzaji.zzcti() && !zzakn.zzp(com_google_android_gms_internal_zzaji.zzctj())) || (com_google_android_gms_internal_zzaji.zzctl() && !zzakn.zzp(com_google_android_gms_internal_zzaji.zzctm()))) {
                throw new IllegalArgumentException("When using orderByPriority(), values provided to startAt(), endAt(), or equalTo() must be valid priorities.");
            }
        }
    }

    private Query zzb(zzakj com_google_android_gms_internal_zzakj, String str) {
        zzalm.zzsr(str);
        if (com_google_android_gms_internal_zzakj.zzcuu() || com_google_android_gms_internal_zzakj.isEmpty()) {
            zzajx zzsc = str != null ? zzajx.zzsc(str) : null;
            if (this.aPt.zzctl()) {
                throw new IllegalArgumentException("Can't call endAt() or equalTo() multiple times");
            }
            zzaji zzb = this.aPt.zzb(com_google_android_gms_internal_zzakj, zzsc);
            zzb(zzb);
            zza(zzb);
            if ($assertionsDisabled || zzb.isValid()) {
                return new Query(this.aPi, this.aPq, zzb, this.aPu);
            }
            throw new AssertionError();
        }
        throw new IllegalArgumentException("Can only use simple values for endAt()");
    }

    private void zzb(final zzahj com_google_android_gms_internal_zzahj) {
        zzaih.zzcsb().zzi(com_google_android_gms_internal_zzahj);
        this.aPi.zzs(new Runnable(this) {
            final /* synthetic */ Query aPw;

            public void run() {
                this.aPw.aPi.zzf(com_google_android_gms_internal_zzahj);
            }
        });
    }

    private void zzb(zzaji com_google_android_gms_internal_zzaji) {
        if (com_google_android_gms_internal_zzaji.zzcti() && com_google_android_gms_internal_zzaji.zzctl() && com_google_android_gms_internal_zzaji.zzcto() && !com_google_android_gms_internal_zzaji.zzctp()) {
            throw new IllegalArgumentException("Can't combine startAt(), endAt() and limit(). Use limitToFirst() or limitToLast() instead");
        }
    }

    private void zzcms() {
        if (this.aPt.zzcti()) {
            throw new IllegalArgumentException("Can't call equalTo() and startAt() combined");
        } else if (this.aPt.zzctl()) {
            throw new IllegalArgumentException("Can't call equalTo() and endAt() combined");
        }
    }

    private void zzcmt() {
        if (this.aPu) {
            throw new IllegalArgumentException("You can't combine multiple orderBy calls!");
        }
    }

    public ChildEventListener addChildEventListener(ChildEventListener childEventListener) {
        zzb(new zzahe(this.aPi, childEventListener, zzcmv()));
        return childEventListener;
    }

    public void addListenerForSingleValueEvent(final ValueEventListener valueEventListener) {
        zzb(new zzaie(this.aPi, new ValueEventListener(this) {
            final /* synthetic */ Query aPw;

            public void onCancelled(DatabaseError databaseError) {
                valueEventListener.onCancelled(databaseError);
            }

            public void onDataChange(DataSnapshot dataSnapshot) {
                this.aPw.removeEventListener((ValueEventListener) this);
                valueEventListener.onDataChange(dataSnapshot);
            }
        }, zzcmv()));
    }

    public ValueEventListener addValueEventListener(ValueEventListener valueEventListener) {
        zzb(new zzaie(this.aPi, valueEventListener, zzcmv()));
        return valueEventListener;
    }

    public Query endAt(double d) {
        return endAt(d, null);
    }

    public Query endAt(double d, String str) {
        return zzb(new zzakb(Double.valueOf(d), zzakn.zzcvs()), str);
    }

    public Query endAt(String str) {
        return endAt(str, null);
    }

    public Query endAt(String str, String str2) {
        return zzb(str != null ? new zzakp(str, zzakn.zzcvs()) : zzakc.zzcvg(), str2);
    }

    public Query endAt(boolean z) {
        return endAt(z, null);
    }

    public Query endAt(boolean z, String str) {
        return zzb(new zzajw(Boolean.valueOf(z), zzakn.zzcvs()), str);
    }

    public Query equalTo(double d) {
        zzcms();
        return startAt(d).endAt(d);
    }

    public Query equalTo(double d, String str) {
        zzcms();
        return startAt(d, str).endAt(d, str);
    }

    public Query equalTo(String str) {
        zzcms();
        return startAt(str).endAt(str);
    }

    public Query equalTo(String str, String str2) {
        zzcms();
        return startAt(str, str2).endAt(str, str2);
    }

    public Query equalTo(boolean z) {
        zzcms();
        return startAt(z).endAt(z);
    }

    public Query equalTo(boolean z, String str) {
        zzcms();
        return startAt(z, str).endAt(z, str);
    }

    public DatabaseReference getRef() {
        return new DatabaseReference(this.aPi, zzcmu());
    }

    public void keepSynced(final boolean z) {
        if (this.aPq.isEmpty() || !this.aPq.zzcqz().equals(zzajx.zzcuq())) {
            this.aPi.zzs(new Runnable(this) {
                final /* synthetic */ Query aPw;

                public void run() {
                    this.aPw.aPi.zza(this.aPw.zzcmv(), z);
                }
            });
            return;
        }
        throw new DatabaseException("Can't call keepSynced() on .info paths.");
    }

    public Query limitToFirst(int i) {
        if (i <= 0) {
            throw new IllegalArgumentException("Limit must be a positive integer!");
        } else if (!this.aPt.zzcto()) {
            return new Query(this.aPi, this.aPq, this.aPt.zzaex(i), this.aPu);
        } else {
            throw new IllegalArgumentException("Can't call limitToLast on query with previously set limit!");
        }
    }

    public Query limitToLast(int i) {
        if (i <= 0) {
            throw new IllegalArgumentException("Limit must be a positive integer!");
        } else if (!this.aPt.zzcto()) {
            return new Query(this.aPi, this.aPq, this.aPt.zzaey(i), this.aPu);
        } else {
            throw new IllegalArgumentException("Can't call limitToLast on query with previously set limit!");
        }
    }

    public Query orderByChild(String str) {
        if (str == null) {
            throw new NullPointerException("Key can't be null");
        } else if (str.equals("$key") || str.equals(".key")) {
            throw new IllegalArgumentException(new StringBuilder(String.valueOf(str).length() + 54).append("Can't use '").append(str).append("' as path, please use orderByKey() instead!").toString());
        } else if (str.equals("$priority") || str.equals(".priority")) {
            throw new IllegalArgumentException(new StringBuilder(String.valueOf(str).length() + 59).append("Can't use '").append(str).append("' as path, please use orderByPriority() instead!").toString());
        } else if (str.equals("$value") || str.equals(".value")) {
            throw new IllegalArgumentException(new StringBuilder(String.valueOf(str).length() + 56).append("Can't use '").append(str).append("' as path, please use orderByValue() instead!").toString());
        } else {
            zzalm.zzsn(str);
            zzcmt();
            zzaho com_google_android_gms_internal_zzaho = new zzaho(str);
            if (com_google_android_gms_internal_zzaho.size() == 0) {
                throw new IllegalArgumentException("Can't use empty path, use orderByValue() instead!");
            }
            return new Query(this.aPi, this.aPq, this.aPt.zza(new zzakl(com_google_android_gms_internal_zzaho)), true);
        }
    }

    public Query orderByKey() {
        zzcmt();
        zzaji zza = this.aPt.zza(zzakf.zzcvn());
        zza(zza);
        return new Query(this.aPi, this.aPq, zza, true);
    }

    public Query orderByPriority() {
        zzcmt();
        zzaji zza = this.aPt.zza(zzakm.zzcvr());
        zza(zza);
        return new Query(this.aPi, this.aPq, zza, true);
    }

    public Query orderByValue() {
        zzcmt();
        return new Query(this.aPi, this.aPq, this.aPt.zza(zzakq.zzcvt()), true);
    }

    public void removeEventListener(ChildEventListener childEventListener) {
        if (childEventListener == null) {
            throw new NullPointerException("listener must not be null");
        }
        zza(new zzahe(this.aPi, childEventListener, zzcmv()));
    }

    public void removeEventListener(ValueEventListener valueEventListener) {
        if (valueEventListener == null) {
            throw new NullPointerException("listener must not be null");
        }
        zza(new zzaie(this.aPi, valueEventListener, zzcmv()));
    }

    public Query startAt(double d) {
        return startAt(d, null);
    }

    public Query startAt(double d, String str) {
        return zza(new zzakb(Double.valueOf(d), zzakn.zzcvs()), str);
    }

    public Query startAt(String str) {
        return startAt(str, null);
    }

    public Query startAt(String str, String str2) {
        return zza(str != null ? new zzakp(str, zzakn.zzcvs()) : zzakc.zzcvg(), str2);
    }

    public Query startAt(boolean z) {
        return startAt(z, null);
    }

    public Query startAt(boolean z, String str) {
        return zza(new zzajw(Boolean.valueOf(z), zzakn.zzcvs()), str);
    }

    public zzaho zzcmu() {
        return this.aPq;
    }

    public zzajj zzcmv() {
        return new zzajj(this.aPq, this.aPt);
    }
}
