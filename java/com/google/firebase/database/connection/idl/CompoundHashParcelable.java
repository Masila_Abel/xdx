package com.google.firebase.database.connection.idl;

import android.os.Parcel;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.internal.zzagp;
import com.google.android.gms.internal.zzagt;
import java.util.ArrayList;
import java.util.List;

class CompoundHashParcelable extends AbstractSafeParcelable {
    public static final zza CREATOR = new zza();
    final List<String> aQA;
    final List<String> aQB;
    final int versionCode;

    public CompoundHashParcelable(int i, List<String> list, List<String> list2) {
        this.versionCode = i;
        this.aQA = list;
        this.aQB = list2;
    }

    public static zzagp zza(CompoundHashParcelable compoundHashParcelable) {
        List arrayList = new ArrayList(compoundHashParcelable.aQA.size());
        for (String zzrp : compoundHashParcelable.aQA) {
            arrayList.add(zzagt.zzrp(zzrp));
        }
        return new zzagp(arrayList, compoundHashParcelable.aQB);
    }

    public static CompoundHashParcelable zza(zzagp com_google_android_gms_internal_zzagp) {
        List<List> zzcnx = com_google_android_gms_internal_zzagp.zzcnx();
        List arrayList = new ArrayList(zzcnx.size());
        for (List zzap : zzcnx) {
            arrayList.add(zzagt.zzap(zzap));
        }
        return new CompoundHashParcelable(1, arrayList, com_google_android_gms_internal_zzagp.zzcny());
    }

    public void writeToParcel(Parcel parcel, int i) {
        zza.zza(this, parcel, i);
    }
}
