package com.google.firebase.database.connection.idl;

import android.content.Context;
import android.os.RemoteException;
import com.google.android.gms.dynamic.zze;
import com.google.android.gms.internal.zzags;
import com.google.android.gms.internal.zzagv;
import com.google.android.gms.internal.zzagw;
import com.google.android.gms.internal.zzagz;
import com.google.firebase.database.connection.idl.zzj.zza;
import java.util.List;
import java.util.Map;

public class zzc implements zzagw {
    private final zzh aSm;

    class AnonymousClass2 extends zza {
        final /* synthetic */ zzagz aSp;

        AnonymousClass2(zzagz com_google_android_gms_internal_zzagz) {
            this.aSp = com_google_android_gms_internal_zzagz;
        }

        public void zzbm(String str, String str2) throws RemoteException {
            this.aSp.zzbm(str, str2);
        }
    }

    private zzc(zzh com_google_firebase_database_connection_idl_zzh) {
        this.aSm = com_google_firebase_database_connection_idl_zzh;
    }

    public static zzc zza(Context context, ConnectionConfig connectionConfig, zzags com_google_android_gms_internal_zzags, zzagw.zza com_google_android_gms_internal_zzagw_zza) {
        return new zzc(IPersistentConnectionImpl.loadDynamic(context, connectionConfig, com_google_android_gms_internal_zzags.zzcoa(), com_google_android_gms_internal_zzags.zzcob(), com_google_android_gms_internal_zzagw_zza));
    }

    private static zzj zza(zzagz com_google_android_gms_internal_zzagz) {
        return new AnonymousClass2(com_google_android_gms_internal_zzagz);
    }

    public void initialize() {
        try {
            this.aSm.initialize();
        } catch (Throwable e) {
            throw new RuntimeException(e);
        }
    }

    public void interrupt(String str) {
        try {
            this.aSm.interrupt(str);
        } catch (Throwable e) {
            throw new RuntimeException(e);
        }
    }

    public boolean isInterrupted(String str) {
        try {
            return this.aSm.isInterrupted(str);
        } catch (Throwable e) {
            throw new RuntimeException(e);
        }
    }

    public void purgeOutstandingWrites() {
        try {
            this.aSm.purgeOutstandingWrites();
        } catch (Throwable e) {
            throw new RuntimeException(e);
        }
    }

    public void refreshAuthToken() {
        try {
            this.aSm.refreshAuthToken();
        } catch (Throwable e) {
            throw new RuntimeException(e);
        }
    }

    public void resume(String str) {
        try {
            this.aSm.resume(str);
        } catch (Throwable e) {
            throw new RuntimeException(e);
        }
    }

    public void shutdown() {
        try {
            this.aSm.shutdown();
        } catch (Throwable e) {
            throw new RuntimeException(e);
        }
    }

    public void zza(List<String> list, zzagz com_google_android_gms_internal_zzagz) {
        try {
            this.aSm.onDisconnectCancel(list, zza(com_google_android_gms_internal_zzagz));
        } catch (Throwable e) {
            throw new RuntimeException(e);
        }
    }

    public void zza(List<String> list, Object obj, zzagz com_google_android_gms_internal_zzagz) {
        try {
            this.aSm.put(list, zze.zzac(obj), zza(com_google_android_gms_internal_zzagz));
        } catch (Throwable e) {
            throw new RuntimeException(e);
        }
    }

    public void zza(List<String> list, Object obj, String str, zzagz com_google_android_gms_internal_zzagz) {
        try {
            this.aSm.compareAndPut(list, zze.zzac(obj), str, zza(com_google_android_gms_internal_zzagz));
        } catch (Throwable e) {
            throw new RuntimeException(e);
        }
    }

    public void zza(List<String> list, Map<String, Object> map) {
        try {
            this.aSm.unlisten(list, zze.zzac(map));
        } catch (Throwable e) {
            throw new RuntimeException(e);
        }
    }

    public void zza(List<String> list, Map<String, Object> map, final zzagv com_google_android_gms_internal_zzagv, Long l, zzagz com_google_android_gms_internal_zzagz) {
        long longValue;
        zzg anonymousClass1 = new zzg.zza(this) {
            final /* synthetic */ zzc aSo;

            public String zzcoe() {
                return com_google_android_gms_internal_zzagv.zzcoe();
            }

            public boolean zzcof() {
                return com_google_android_gms_internal_zzagv.zzcof();
            }

            public CompoundHashParcelable zzcpm() {
                return CompoundHashParcelable.zza(com_google_android_gms_internal_zzagv.zzcog());
            }
        };
        if (l != null) {
            try {
                longValue = l.longValue();
            } catch (Throwable e) {
                throw new RuntimeException(e);
            }
        }
        longValue = -1;
        this.aSm.listen(list, zze.zzac(map), anonymousClass1, longValue, zza(com_google_android_gms_internal_zzagz));
    }

    public void zza(List<String> list, Map<String, Object> map, zzagz com_google_android_gms_internal_zzagz) {
        try {
            this.aSm.merge(list, zze.zzac(map), zza(com_google_android_gms_internal_zzagz));
        } catch (Throwable e) {
            throw new RuntimeException(e);
        }
    }

    public void zzb(List<String> list, Object obj, zzagz com_google_android_gms_internal_zzagz) {
        try {
            this.aSm.onDisconnectPut(list, zze.zzac(obj), zza(com_google_android_gms_internal_zzagz));
        } catch (Throwable e) {
            throw new RuntimeException(e);
        }
    }

    public void zzb(List<String> list, Map<String, Object> map, zzagz com_google_android_gms_internal_zzagz) {
        try {
            this.aSm.onDisconnectMerge(list, zze.zzac(map), zza(com_google_android_gms_internal_zzagz));
        } catch (Throwable e) {
            throw new RuntimeException(e);
        }
    }

    public void zzrq(String str) {
        try {
            this.aSm.refreshAuthToken2(str);
        } catch (Throwable e) {
            throw new RuntimeException(e);
        }
    }
}
