package com.google.firebase.database.connection.idl;

import android.os.Parcel;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.internal.zzagy;
import java.util.List;

class RangeParcelable extends AbstractSafeParcelable {
    public static final zzk CREATOR = new zzk();
    final List<String> aRP;
    final List<String> aRQ;
    final int versionCode;

    public RangeParcelable(int i, List<String> list, List<String> list2) {
        this.versionCode = i;
        this.aRP = list;
        this.aRQ = list2;
    }

    public static zzagy zza(RangeParcelable rangeParcelable, Object obj) {
        return new zzagy(rangeParcelable.aRP, rangeParcelable.aRQ, obj);
    }

    public static RangeParcelable zza(zzagy com_google_android_gms_internal_zzagy) {
        return new RangeParcelable(1, com_google_android_gms_internal_zzagy.zzcpc(), com_google_android_gms_internal_zzagy.zzcpd());
    }

    public void writeToParcel(Parcel parcel, int i) {
        zzk.zza(this, parcel, i);
    }
}
