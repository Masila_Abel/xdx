package com.google.firebase.database.connection.idl;

import android.os.Parcel;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.internal.zzagu;

class HostInfoParcelable extends AbstractSafeParcelable {
    public static final zzd CREATOR = new zzd();
    final String aQT;
    final boolean aQU;
    final int versionCode;
    final String zx;

    public HostInfoParcelable(int i, String str, String str2, boolean z) {
        this.versionCode = i;
        this.aQT = str;
        this.zx = str2;
        this.aQU = z;
    }

    public static zzagu zza(HostInfoParcelable hostInfoParcelable) {
        return new zzagu(hostInfoParcelable.aQT, hostInfoParcelable.zx, hostInfoParcelable.aQU);
    }

    public static HostInfoParcelable zza(zzagu com_google_android_gms_internal_zzagu) {
        return new HostInfoParcelable(1, com_google_android_gms_internal_zzagu.getHost(), com_google_android_gms_internal_zzagu.getNamespace(), com_google_android_gms_internal_zzagu.isSecure());
    }

    public void writeToParcel(Parcel parcel, int i) {
        zzd.zza(this, parcel, i);
    }
}
