package com.google.firebase.database.connection.idl;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zzb;
import java.util.List;
import outlander.showcaseview.R;

public class zza implements Creator<CompoundHashParcelable> {
    static void zza(CompoundHashParcelable compoundHashParcelable, Parcel parcel, int i) {
        int zzcn = zzb.zzcn(parcel);
        zzb.zzc(parcel, 1, compoundHashParcelable.versionCode);
        zzb.zzb(parcel, 2, compoundHashParcelable.aQA, false);
        zzb.zzb(parcel, 3, compoundHashParcelable.aQB, false);
        zzb.zzaj(parcel, zzcn);
    }

    public /* synthetic */ Object createFromParcel(Parcel parcel) {
        return zzwd(parcel);
    }

    public /* synthetic */ Object[] newArray(int i) {
        return zzaet(i);
    }

    public CompoundHashParcelable[] zzaet(int i) {
        return new CompoundHashParcelable[i];
    }

    public CompoundHashParcelable zzwd(Parcel parcel) {
        List list = null;
        int zzcm = com.google.android.gms.common.internal.safeparcel.zza.zzcm(parcel);
        int i = 0;
        List list2 = null;
        while (parcel.dataPosition() < zzcm) {
            int zzcl = com.google.android.gms.common.internal.safeparcel.zza.zzcl(parcel);
            switch (com.google.android.gms.common.internal.safeparcel.zza.zzgm(zzcl)) {
                case R.styleable.View_android_focusable /*1*/:
                    i = com.google.android.gms.common.internal.safeparcel.zza.zzg(parcel, zzcl);
                    break;
                case R.styleable.View_paddingStart /*2*/:
                    list2 = com.google.android.gms.common.internal.safeparcel.zza.zzae(parcel, zzcl);
                    break;
                case R.styleable.View_paddingEnd /*3*/:
                    list = com.google.android.gms.common.internal.safeparcel.zza.zzae(parcel, zzcl);
                    break;
                default:
                    com.google.android.gms.common.internal.safeparcel.zza.zzb(parcel, zzcl);
                    break;
            }
        }
        if (parcel.dataPosition() == zzcm) {
            return new CompoundHashParcelable(i, list2, list);
        }
        throw new com.google.android.gms.common.internal.safeparcel.zza.zza("Overread allowed size end=" + zzcm, parcel);
    }
}
