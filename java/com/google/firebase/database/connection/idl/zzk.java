package com.google.firebase.database.connection.idl;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import java.util.List;
import outlander.showcaseview.R;

public class zzk implements Creator<RangeParcelable> {
    static void zza(RangeParcelable rangeParcelable, Parcel parcel, int i) {
        int zzcn = zzb.zzcn(parcel);
        zzb.zzc(parcel, 1, rangeParcelable.versionCode);
        zzb.zzb(parcel, 2, rangeParcelable.aRP, false);
        zzb.zzb(parcel, 3, rangeParcelable.aRQ, false);
        zzb.zzaj(parcel, zzcn);
    }

    public /* synthetic */ Object createFromParcel(Parcel parcel) {
        return zzwg(parcel);
    }

    public /* synthetic */ Object[] newArray(int i) {
        return zzaew(i);
    }

    public RangeParcelable[] zzaew(int i) {
        return new RangeParcelable[i];
    }

    public RangeParcelable zzwg(Parcel parcel) {
        List list = null;
        int zzcm = zza.zzcm(parcel);
        int i = 0;
        List list2 = null;
        while (parcel.dataPosition() < zzcm) {
            int zzcl = zza.zzcl(parcel);
            switch (zza.zzgm(zzcl)) {
                case R.styleable.View_android_focusable /*1*/:
                    i = zza.zzg(parcel, zzcl);
                    break;
                case R.styleable.View_paddingStart /*2*/:
                    list2 = zza.zzae(parcel, zzcl);
                    break;
                case R.styleable.View_paddingEnd /*3*/:
                    list = zza.zzae(parcel, zzcl);
                    break;
                default:
                    zza.zzb(parcel, zzcl);
                    break;
            }
        }
        if (parcel.dataPosition() == zzcm) {
            return new RangeParcelable(i, list2, list);
        }
        throw new zza.zza("Overread allowed size end=" + zzcm, parcel);
    }
}
