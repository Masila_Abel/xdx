package com.google.firebase.database.connection.idl;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import outlander.showcaseview.R;

public class zzd implements Creator<HostInfoParcelable> {
    static void zza(HostInfoParcelable hostInfoParcelable, Parcel parcel, int i) {
        int zzcn = zzb.zzcn(parcel);
        zzb.zzc(parcel, 1, hostInfoParcelable.versionCode);
        zzb.zza(parcel, 2, hostInfoParcelable.aQT, false);
        zzb.zza(parcel, 3, hostInfoParcelable.zx, false);
        zzb.zza(parcel, 4, hostInfoParcelable.aQU);
        zzb.zzaj(parcel, zzcn);
    }

    public /* synthetic */ Object createFromParcel(Parcel parcel) {
        return zzwf(parcel);
    }

    public /* synthetic */ Object[] newArray(int i) {
        return zzaev(i);
    }

    public HostInfoParcelable[] zzaev(int i) {
        return new HostInfoParcelable[i];
    }

    public HostInfoParcelable zzwf(Parcel parcel) {
        String str = null;
        boolean z = false;
        int zzcm = zza.zzcm(parcel);
        String str2 = null;
        int i = 0;
        while (parcel.dataPosition() < zzcm) {
            int zzcl = zza.zzcl(parcel);
            switch (zza.zzgm(zzcl)) {
                case R.styleable.View_android_focusable /*1*/:
                    i = zza.zzg(parcel, zzcl);
                    break;
                case R.styleable.View_paddingStart /*2*/:
                    str2 = zza.zzq(parcel, zzcl);
                    break;
                case R.styleable.View_paddingEnd /*3*/:
                    str = zza.zzq(parcel, zzcl);
                    break;
                case R.styleable.View_theme /*4*/:
                    z = zza.zzc(parcel, zzcl);
                    break;
                default:
                    zza.zzb(parcel, zzcl);
                    break;
            }
        }
        if (parcel.dataPosition() == zzcm) {
            return new HostInfoParcelable(i, str2, str, z);
        }
        throw new zza.zza("Overread allowed size end=" + zzcm, parcel);
    }
}
