package com.google.firebase.database.connection.idl;

import android.content.Context;
import android.os.RemoteException;
import com.google.android.gms.common.util.DynamiteApi;
import com.google.android.gms.dynamic.zzd;
import com.google.android.gms.dynamic.zze;
import com.google.android.gms.internal.zzagp;
import com.google.android.gms.internal.zzagr;
import com.google.android.gms.internal.zzags;
import com.google.android.gms.internal.zzagv;
import com.google.android.gms.internal.zzagw;
import com.google.android.gms.internal.zzagx;
import com.google.android.gms.internal.zzagy;
import com.google.android.gms.internal.zzagz;
import com.google.android.gms.internal.zzajs;
import com.google.android.gms.internal.zzsb;
import com.google.firebase.database.connection.idl.zzh.zza;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ScheduledExecutorService;

@DynamiteApi
public class IPersistentConnectionImpl extends zza {
    private zzagw aSq;

    class AnonymousClass2 implements zzagz {
        final /* synthetic */ zzj aSt;

        AnonymousClass2(zzj com_google_firebase_database_connection_idl_zzj) {
            this.aSt = com_google_firebase_database_connection_idl_zzj;
        }

        public void zzbm(String str, String str2) {
            try {
                this.aSt.zzbm(str, str2);
            } catch (Throwable e) {
                throw new RuntimeException(e);
            }
        }
    }

    class AnonymousClass3 extends zzi.zza {
        final /* synthetic */ zzagw.zza aSu;

        AnonymousClass3(zzagw.zza com_google_android_gms_internal_zzagw_zza) {
            this.aSu = com_google_android_gms_internal_zzagw_zza;
        }

        public void onDisconnect() {
            this.aSu.onDisconnect();
        }

        public void zza(List<String> list, zzd com_google_android_gms_dynamic_zzd, boolean z, long j) {
            this.aSu.zza(list, zze.zzad(com_google_android_gms_dynamic_zzd), z, IPersistentConnectionImpl.zzcd(j));
        }

        public void zza(List<String> list, List<RangeParcelable> list2, zzd com_google_android_gms_dynamic_zzd, long j) {
            List list3 = (List) zze.zzad(com_google_android_gms_dynamic_zzd);
            List arrayList = new ArrayList(list2.size());
            for (int i = 0; i < list2.size(); i++) {
                arrayList.add(RangeParcelable.zza((RangeParcelable) list2.get(i), list3.get(i)));
            }
            this.aSu.zza(list, arrayList, IPersistentConnectionImpl.zzcd(j));
        }

        public void zzaq(zzd com_google_android_gms_dynamic_zzd) {
            this.aSu.zzbt((Map) zze.zzad(com_google_android_gms_dynamic_zzd));
        }

        public void zzcoh() {
            this.aSu.zzcoh();
        }

        public void zzcr(boolean z) {
            this.aSu.zzcr(z);
        }
    }

    class AnonymousClass4 implements zzagw.zza {
        final /* synthetic */ zzi aSv;

        AnonymousClass4(zzi com_google_firebase_database_connection_idl_zzi) {
            this.aSv = com_google_firebase_database_connection_idl_zzi;
        }

        public void onDisconnect() {
            try {
                this.aSv.onDisconnect();
            } catch (Throwable e) {
                throw new RuntimeException(e);
            }
        }

        public void zza(List<String> list, Object obj, boolean z, Long l) {
            try {
                this.aSv.zza((List) list, zze.zzac(obj), z, IPersistentConnectionImpl.zza(l));
            } catch (Throwable e) {
                throw new RuntimeException(e);
            }
        }

        public void zza(List<String> list, List<zzagy> list2, Long l) {
            List arrayList = new ArrayList(list2.size());
            List arrayList2 = new ArrayList(list2.size());
            for (zzagy com_google_android_gms_internal_zzagy : list2) {
                arrayList.add(RangeParcelable.zza(com_google_android_gms_internal_zzagy));
                arrayList2.add(com_google_android_gms_internal_zzagy.zzcpe());
            }
            try {
                this.aSv.zza((List) list, arrayList, zze.zzac(arrayList2), IPersistentConnectionImpl.zza(l));
            } catch (Throwable e) {
                throw new RuntimeException(e);
            }
        }

        public void zzbt(Map<String, Object> map) {
            try {
                this.aSv.zzaq(zze.zzac(map));
            } catch (Throwable e) {
                throw new RuntimeException(e);
            }
        }

        public void zzcoh() {
            try {
                this.aSv.zzcoh();
            } catch (Throwable e) {
                throw new RuntimeException(e);
            }
        }

        public void zzcr(boolean z) {
            try {
                this.aSv.zzcr(z);
            } catch (Throwable e) {
                throw new RuntimeException(e);
            }
        }
    }

    class AnonymousClass5 extends zze.zza {
        final /* synthetic */ zzagr aSw;

        AnonymousClass5(zzagr com_google_android_gms_internal_zzagr) {
            this.aSw = com_google_android_gms_internal_zzagr;
        }

        public void zza(boolean z, final zzf com_google_firebase_database_connection_idl_zzf) throws RemoteException {
            this.aSw.zza(z, new zzagr.zza(this) {
                final /* synthetic */ AnonymousClass5 aSy;

                public void onError(String str) {
                    try {
                        com_google_firebase_database_connection_idl_zzf.onError(str);
                    } catch (Throwable e) {
                        throw new RuntimeException(e);
                    }
                }

                public void zzro(String str) {
                    try {
                        com_google_firebase_database_connection_idl_zzf.zzro(str);
                    } catch (Throwable e) {
                        throw new RuntimeException(e);
                    }
                }
            });
        }
    }

    class AnonymousClass6 implements zzagr {
        final /* synthetic */ zze aSz;

        AnonymousClass6(zze com_google_firebase_database_connection_idl_zze) {
            this.aSz = com_google_firebase_database_connection_idl_zze;
        }

        public void zza(boolean z, final zzagr.zza com_google_android_gms_internal_zzagr_zza) {
            try {
                this.aSz.zza(z, new zzf.zza(this) {
                    final /* synthetic */ AnonymousClass6 aSB;

                    public void onError(String str) throws RemoteException {
                        com_google_android_gms_internal_zzagr_zza.onError(str);
                    }

                    public void zzro(String str) throws RemoteException {
                        com_google_android_gms_internal_zzagr_zza.zzro(str);
                    }
                });
            } catch (Throwable e) {
                throw new RuntimeException(e);
            }
        }
    }

    public static zzh loadDynamic(Context context, ConnectionConfig connectionConfig, zzagr com_google_android_gms_internal_zzagr, ScheduledExecutorService scheduledExecutorService, zzagw.zza com_google_android_gms_internal_zzagw_zza) {
        try {
            zzh asInterface = zza.asInterface(zzsb.zza(context, zzsb.KL, "com.google.android.gms.firebase_database").zziu("com.google.firebase.database.connection.idl.IPersistentConnectionImpl"));
            asInterface.setup(connectionConfig, zza(com_google_android_gms_internal_zzagr), zze.zzac(scheduledExecutorService), zza(com_google_android_gms_internal_zzagw_zza));
            return asInterface;
        } catch (zzsb.zza e) {
            throw new RuntimeException(e);
        } catch (Throwable e2) {
            throw new RuntimeException(e2);
        }
    }

    private static long zza(Long l) {
        if (l == null) {
            return -1;
        }
        if (l.longValue() != -1) {
            return l.longValue();
        }
        throw new IllegalArgumentException("Tag parameter clashed with NO_TAG value");
    }

    private static zzagr zza(zze com_google_firebase_database_connection_idl_zze) {
        return new AnonymousClass6(com_google_firebase_database_connection_idl_zze);
    }

    private static zzagw.zza zza(zzi com_google_firebase_database_connection_idl_zzi) {
        return new AnonymousClass4(com_google_firebase_database_connection_idl_zzi);
    }

    private static zzagz zza(zzj com_google_firebase_database_connection_idl_zzj) {
        return new AnonymousClass2(com_google_firebase_database_connection_idl_zzj);
    }

    private static zze zza(zzagr com_google_android_gms_internal_zzagr) {
        return new AnonymousClass5(com_google_android_gms_internal_zzagr);
    }

    private static zzi zza(zzagw.zza com_google_android_gms_internal_zzagw_zza) {
        return new AnonymousClass3(com_google_android_gms_internal_zzagw_zza);
    }

    private static Long zzcd(long j) {
        return j == -1 ? null : Long.valueOf(j);
    }

    public void compareAndPut(List<String> list, zzd com_google_android_gms_dynamic_zzd, String str, zzj com_google_firebase_database_connection_idl_zzj) {
        this.aSq.zza(list, zze.zzad(com_google_android_gms_dynamic_zzd), str, zza(com_google_firebase_database_connection_idl_zzj));
    }

    public void initialize() {
        this.aSq.initialize();
    }

    public void interrupt(String str) {
        this.aSq.interrupt(str);
    }

    public boolean isInterrupted(String str) {
        return this.aSq.isInterrupted(str);
    }

    public void listen(List<String> list, zzd com_google_android_gms_dynamic_zzd, final zzg com_google_firebase_database_connection_idl_zzg, long j, zzj com_google_firebase_database_connection_idl_zzj) {
        Long zzcd = zzcd(j);
        List<String> list2 = list;
        this.aSq.zza(list2, (Map) zze.zzad(com_google_android_gms_dynamic_zzd), new zzagv(this) {
            final /* synthetic */ IPersistentConnectionImpl aSs;

            public String zzcoe() {
                try {
                    return com_google_firebase_database_connection_idl_zzg.zzcoe();
                } catch (Throwable e) {
                    throw new RuntimeException(e);
                }
            }

            public boolean zzcof() {
                try {
                    return com_google_firebase_database_connection_idl_zzg.zzcof();
                } catch (Throwable e) {
                    throw new RuntimeException(e);
                }
            }

            public zzagp zzcog() {
                try {
                    return CompoundHashParcelable.zza(com_google_firebase_database_connection_idl_zzg.zzcpm());
                } catch (Throwable e) {
                    throw new RuntimeException(e);
                }
            }
        }, zzcd, zza(com_google_firebase_database_connection_idl_zzj));
    }

    public void merge(List<String> list, zzd com_google_android_gms_dynamic_zzd, zzj com_google_firebase_database_connection_idl_zzj) {
        this.aSq.zza(list, (Map) zze.zzad(com_google_android_gms_dynamic_zzd), zza(com_google_firebase_database_connection_idl_zzj));
    }

    public void onDisconnectCancel(List<String> list, zzj com_google_firebase_database_connection_idl_zzj) {
        this.aSq.zza(list, zza(com_google_firebase_database_connection_idl_zzj));
    }

    public void onDisconnectMerge(List<String> list, zzd com_google_android_gms_dynamic_zzd, zzj com_google_firebase_database_connection_idl_zzj) {
        this.aSq.zzb(list, (Map) zze.zzad(com_google_android_gms_dynamic_zzd), zza(com_google_firebase_database_connection_idl_zzj));
    }

    public void onDisconnectPut(List<String> list, zzd com_google_android_gms_dynamic_zzd, zzj com_google_firebase_database_connection_idl_zzj) {
        this.aSq.zzb(list, zze.zzad(com_google_android_gms_dynamic_zzd), zza(com_google_firebase_database_connection_idl_zzj));
    }

    public void purgeOutstandingWrites() {
        this.aSq.purgeOutstandingWrites();
    }

    public void put(List<String> list, zzd com_google_android_gms_dynamic_zzd, zzj com_google_firebase_database_connection_idl_zzj) {
        this.aSq.zza(list, zze.zzad(com_google_android_gms_dynamic_zzd), zza(com_google_firebase_database_connection_idl_zzj));
    }

    public void refreshAuthToken() {
        this.aSq.refreshAuthToken();
    }

    public void refreshAuthToken2(String str) {
        this.aSq.zzrq(str);
    }

    public void resume(String str) {
        this.aSq.resume(str);
    }

    public void setup(ConnectionConfig connectionConfig, zze com_google_firebase_database_connection_idl_zze, zzd com_google_android_gms_dynamic_zzd, zzi com_google_firebase_database_connection_idl_zzi) {
        ScheduledExecutorService scheduledExecutorService = (ScheduledExecutorService) zze.zzad(com_google_android_gms_dynamic_zzd);
        this.aSq = new zzagx(new zzags(new zzajs(connectionConfig.zzcpk(), connectionConfig.zzcpl()), zza(com_google_firebase_database_connection_idl_zze), scheduledExecutorService, connectionConfig.aQQ, connectionConfig.aSk, connectionConfig.aQS), HostInfoParcelable.zza(connectionConfig.aSh), zza(com_google_firebase_database_connection_idl_zzi));
    }

    public void shutdown() {
        this.aSq.shutdown();
    }

    public void unlisten(List<String> list, zzd com_google_android_gms_dynamic_zzd) {
        this.aSq.zza(list, (Map) zze.zzad(com_google_android_gms_dynamic_zzd));
    }
}
