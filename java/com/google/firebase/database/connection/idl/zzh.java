package com.google.firebase.database.connection.idl;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.dynamic.zzd;
import java.util.List;
import outlander.showcaseview.R;

public interface zzh extends IInterface {

    public static abstract class zza extends Binder implements zzh {

        private static class zza implements zzh {
            private IBinder zzahn;

            zza(IBinder iBinder) {
                this.zzahn = iBinder;
            }

            public IBinder asBinder() {
                return this.zzahn;
            }

            public void compareAndPut(List<String> list, zzd com_google_android_gms_dynamic_zzd, String str, zzj com_google_firebase_database_connection_idl_zzj) throws RemoteException {
                IBinder iBinder = null;
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.firebase.database.connection.idl.IPersistentConnection");
                    obtain.writeStringList(list);
                    obtain.writeStrongBinder(com_google_android_gms_dynamic_zzd != null ? com_google_android_gms_dynamic_zzd.asBinder() : null);
                    obtain.writeString(str);
                    if (com_google_firebase_database_connection_idl_zzj != null) {
                        iBinder = com_google_firebase_database_connection_idl_zzj.asBinder();
                    }
                    obtain.writeStrongBinder(iBinder);
                    this.zzahn.transact(9, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void initialize() throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.firebase.database.connection.idl.IPersistentConnection");
                    this.zzahn.transact(2, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void interrupt(String str) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.firebase.database.connection.idl.IPersistentConnection");
                    obtain.writeString(str);
                    this.zzahn.transact(14, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public boolean isInterrupted(String str) throws RemoteException {
                boolean z = false;
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.firebase.database.connection.idl.IPersistentConnection");
                    obtain.writeString(str);
                    this.zzahn.transact(16, obtain, obtain2, 0);
                    obtain2.readException();
                    if (obtain2.readInt() != 0) {
                        z = true;
                    }
                    obtain2.recycle();
                    obtain.recycle();
                    return z;
                } catch (Throwable th) {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void listen(List<String> list, zzd com_google_android_gms_dynamic_zzd, zzg com_google_firebase_database_connection_idl_zzg, long j, zzj com_google_firebase_database_connection_idl_zzj) throws RemoteException {
                IBinder iBinder = null;
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.firebase.database.connection.idl.IPersistentConnection");
                    obtain.writeStringList(list);
                    obtain.writeStrongBinder(com_google_android_gms_dynamic_zzd != null ? com_google_android_gms_dynamic_zzd.asBinder() : null);
                    obtain.writeStrongBinder(com_google_firebase_database_connection_idl_zzg != null ? com_google_firebase_database_connection_idl_zzg.asBinder() : null);
                    obtain.writeLong(j);
                    if (com_google_firebase_database_connection_idl_zzj != null) {
                        iBinder = com_google_firebase_database_connection_idl_zzj.asBinder();
                    }
                    obtain.writeStrongBinder(iBinder);
                    this.zzahn.transact(5, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void merge(List<String> list, zzd com_google_android_gms_dynamic_zzd, zzj com_google_firebase_database_connection_idl_zzj) throws RemoteException {
                IBinder iBinder = null;
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.firebase.database.connection.idl.IPersistentConnection");
                    obtain.writeStringList(list);
                    obtain.writeStrongBinder(com_google_android_gms_dynamic_zzd != null ? com_google_android_gms_dynamic_zzd.asBinder() : null);
                    if (com_google_firebase_database_connection_idl_zzj != null) {
                        iBinder = com_google_firebase_database_connection_idl_zzj.asBinder();
                    }
                    obtain.writeStrongBinder(iBinder);
                    this.zzahn.transact(10, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void onDisconnectCancel(List<String> list, zzj com_google_firebase_database_connection_idl_zzj) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.firebase.database.connection.idl.IPersistentConnection");
                    obtain.writeStringList(list);
                    obtain.writeStrongBinder(com_google_firebase_database_connection_idl_zzj != null ? com_google_firebase_database_connection_idl_zzj.asBinder() : null);
                    this.zzahn.transact(13, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void onDisconnectMerge(List<String> list, zzd com_google_android_gms_dynamic_zzd, zzj com_google_firebase_database_connection_idl_zzj) throws RemoteException {
                IBinder iBinder = null;
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.firebase.database.connection.idl.IPersistentConnection");
                    obtain.writeStringList(list);
                    obtain.writeStrongBinder(com_google_android_gms_dynamic_zzd != null ? com_google_android_gms_dynamic_zzd.asBinder() : null);
                    if (com_google_firebase_database_connection_idl_zzj != null) {
                        iBinder = com_google_firebase_database_connection_idl_zzj.asBinder();
                    }
                    obtain.writeStrongBinder(iBinder);
                    this.zzahn.transact(12, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void onDisconnectPut(List<String> list, zzd com_google_android_gms_dynamic_zzd, zzj com_google_firebase_database_connection_idl_zzj) throws RemoteException {
                IBinder iBinder = null;
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.firebase.database.connection.idl.IPersistentConnection");
                    obtain.writeStringList(list);
                    obtain.writeStrongBinder(com_google_android_gms_dynamic_zzd != null ? com_google_android_gms_dynamic_zzd.asBinder() : null);
                    if (com_google_firebase_database_connection_idl_zzj != null) {
                        iBinder = com_google_firebase_database_connection_idl_zzj.asBinder();
                    }
                    obtain.writeStrongBinder(iBinder);
                    this.zzahn.transact(11, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void purgeOutstandingWrites() throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.firebase.database.connection.idl.IPersistentConnection");
                    this.zzahn.transact(7, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void put(List<String> list, zzd com_google_android_gms_dynamic_zzd, zzj com_google_firebase_database_connection_idl_zzj) throws RemoteException {
                IBinder iBinder = null;
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.firebase.database.connection.idl.IPersistentConnection");
                    obtain.writeStringList(list);
                    obtain.writeStrongBinder(com_google_android_gms_dynamic_zzd != null ? com_google_android_gms_dynamic_zzd.asBinder() : null);
                    if (com_google_firebase_database_connection_idl_zzj != null) {
                        iBinder = com_google_firebase_database_connection_idl_zzj.asBinder();
                    }
                    obtain.writeStrongBinder(iBinder);
                    this.zzahn.transact(8, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void refreshAuthToken() throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.firebase.database.connection.idl.IPersistentConnection");
                    this.zzahn.transact(4, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void refreshAuthToken2(String str) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.firebase.database.connection.idl.IPersistentConnection");
                    obtain.writeString(str);
                    this.zzahn.transact(17, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void resume(String str) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.firebase.database.connection.idl.IPersistentConnection");
                    obtain.writeString(str);
                    this.zzahn.transact(15, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void setup(ConnectionConfig connectionConfig, zze com_google_firebase_database_connection_idl_zze, zzd com_google_android_gms_dynamic_zzd, zzi com_google_firebase_database_connection_idl_zzi) throws RemoteException {
                IBinder iBinder = null;
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.firebase.database.connection.idl.IPersistentConnection");
                    if (connectionConfig != null) {
                        obtain.writeInt(1);
                        connectionConfig.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    obtain.writeStrongBinder(com_google_firebase_database_connection_idl_zze != null ? com_google_firebase_database_connection_idl_zze.asBinder() : null);
                    obtain.writeStrongBinder(com_google_android_gms_dynamic_zzd != null ? com_google_android_gms_dynamic_zzd.asBinder() : null);
                    if (com_google_firebase_database_connection_idl_zzi != null) {
                        iBinder = com_google_firebase_database_connection_idl_zzi.asBinder();
                    }
                    obtain.writeStrongBinder(iBinder);
                    this.zzahn.transact(1, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void shutdown() throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.firebase.database.connection.idl.IPersistentConnection");
                    this.zzahn.transact(3, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void unlisten(List<String> list, zzd com_google_android_gms_dynamic_zzd) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.firebase.database.connection.idl.IPersistentConnection");
                    obtain.writeStringList(list);
                    obtain.writeStrongBinder(com_google_android_gms_dynamic_zzd != null ? com_google_android_gms_dynamic_zzd.asBinder() : null);
                    this.zzahn.transact(6, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
        }

        public zza() {
            attachInterface(this, "com.google.firebase.database.connection.idl.IPersistentConnection");
        }

        public static zzh asInterface(IBinder iBinder) {
            if (iBinder == null) {
                return null;
            }
            IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.firebase.database.connection.idl.IPersistentConnection");
            return (queryLocalInterface == null || !(queryLocalInterface instanceof zzh)) ? new zza(iBinder) : (zzh) queryLocalInterface;
        }

        public IBinder asBinder() {
            return this;
        }

        public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
            switch (i) {
                case R.styleable.View_android_focusable /*1*/:
                    parcel.enforceInterface("com.google.firebase.database.connection.idl.IPersistentConnection");
                    setup(parcel.readInt() != 0 ? (ConnectionConfig) ConnectionConfig.CREATOR.createFromParcel(parcel) : null, com.google.firebase.database.connection.idl.zze.zza.zzmd(parcel.readStrongBinder()), com.google.android.gms.dynamic.zzd.zza.zzfc(parcel.readStrongBinder()), com.google.firebase.database.connection.idl.zzi.zza.zzmg(parcel.readStrongBinder()));
                    parcel2.writeNoException();
                    return true;
                case R.styleable.View_paddingStart /*2*/:
                    parcel.enforceInterface("com.google.firebase.database.connection.idl.IPersistentConnection");
                    initialize();
                    parcel2.writeNoException();
                    return true;
                case R.styleable.View_paddingEnd /*3*/:
                    parcel.enforceInterface("com.google.firebase.database.connection.idl.IPersistentConnection");
                    shutdown();
                    parcel2.writeNoException();
                    return true;
                case R.styleable.View_theme /*4*/:
                    parcel.enforceInterface("com.google.firebase.database.connection.idl.IPersistentConnection");
                    refreshAuthToken();
                    parcel2.writeNoException();
                    return true;
                case R.styleable.Toolbar_contentInsetStart /*5*/:
                    parcel.enforceInterface("com.google.firebase.database.connection.idl.IPersistentConnection");
                    listen(parcel.createStringArrayList(), com.google.android.gms.dynamic.zzd.zza.zzfc(parcel.readStrongBinder()), com.google.firebase.database.connection.idl.zzg.zza.zzmf(parcel.readStrongBinder()), parcel.readLong(), com.google.firebase.database.connection.idl.zzj.zza.zzmh(parcel.readStrongBinder()));
                    parcel2.writeNoException();
                    return true;
                case R.styleable.Toolbar_contentInsetEnd /*6*/:
                    parcel.enforceInterface("com.google.firebase.database.connection.idl.IPersistentConnection");
                    unlisten(parcel.createStringArrayList(), com.google.android.gms.dynamic.zzd.zza.zzfc(parcel.readStrongBinder()));
                    parcel2.writeNoException();
                    return true;
                case R.styleable.Toolbar_contentInsetLeft /*7*/:
                    parcel.enforceInterface("com.google.firebase.database.connection.idl.IPersistentConnection");
                    purgeOutstandingWrites();
                    parcel2.writeNoException();
                    return true;
                case R.styleable.Toolbar_contentInsetRight /*8*/:
                    parcel.enforceInterface("com.google.firebase.database.connection.idl.IPersistentConnection");
                    put(parcel.createStringArrayList(), com.google.android.gms.dynamic.zzd.zza.zzfc(parcel.readStrongBinder()), com.google.firebase.database.connection.idl.zzj.zza.zzmh(parcel.readStrongBinder()));
                    parcel2.writeNoException();
                    return true;
                case R.styleable.Toolbar_popupTheme /*9*/:
                    parcel.enforceInterface("com.google.firebase.database.connection.idl.IPersistentConnection");
                    compareAndPut(parcel.createStringArrayList(), com.google.android.gms.dynamic.zzd.zza.zzfc(parcel.readStrongBinder()), parcel.readString(), com.google.firebase.database.connection.idl.zzj.zza.zzmh(parcel.readStrongBinder()));
                    parcel2.writeNoException();
                    return true;
                case R.styleable.Toolbar_titleTextAppearance /*10*/:
                    parcel.enforceInterface("com.google.firebase.database.connection.idl.IPersistentConnection");
                    merge(parcel.createStringArrayList(), com.google.android.gms.dynamic.zzd.zza.zzfc(parcel.readStrongBinder()), com.google.firebase.database.connection.idl.zzj.zza.zzmh(parcel.readStrongBinder()));
                    parcel2.writeNoException();
                    return true;
                case R.styleable.Toolbar_subtitleTextAppearance /*11*/:
                    parcel.enforceInterface("com.google.firebase.database.connection.idl.IPersistentConnection");
                    onDisconnectPut(parcel.createStringArrayList(), com.google.android.gms.dynamic.zzd.zza.zzfc(parcel.readStrongBinder()), com.google.firebase.database.connection.idl.zzj.zza.zzmh(parcel.readStrongBinder()));
                    parcel2.writeNoException();
                    return true;
                case R.styleable.Toolbar_titleMargins /*12*/:
                    parcel.enforceInterface("com.google.firebase.database.connection.idl.IPersistentConnection");
                    onDisconnectMerge(parcel.createStringArrayList(), com.google.android.gms.dynamic.zzd.zza.zzfc(parcel.readStrongBinder()), com.google.firebase.database.connection.idl.zzj.zza.zzmh(parcel.readStrongBinder()));
                    parcel2.writeNoException();
                    return true;
                case R.styleable.Toolbar_titleMarginStart /*13*/:
                    parcel.enforceInterface("com.google.firebase.database.connection.idl.IPersistentConnection");
                    onDisconnectCancel(parcel.createStringArrayList(), com.google.firebase.database.connection.idl.zzj.zza.zzmh(parcel.readStrongBinder()));
                    parcel2.writeNoException();
                    return true;
                case R.styleable.Toolbar_titleMarginEnd /*14*/:
                    parcel.enforceInterface("com.google.firebase.database.connection.idl.IPersistentConnection");
                    interrupt(parcel.readString());
                    parcel2.writeNoException();
                    return true;
                case R.styleable.Toolbar_titleMarginTop /*15*/:
                    parcel.enforceInterface("com.google.firebase.database.connection.idl.IPersistentConnection");
                    resume(parcel.readString());
                    parcel2.writeNoException();
                    return true;
                case R.styleable.Toolbar_titleMarginBottom /*16*/:
                    parcel.enforceInterface("com.google.firebase.database.connection.idl.IPersistentConnection");
                    boolean isInterrupted = isInterrupted(parcel.readString());
                    parcel2.writeNoException();
                    parcel2.writeInt(isInterrupted ? 1 : 0);
                    return true;
                case R.styleable.Toolbar_maxButtonHeight /*17*/:
                    parcel.enforceInterface("com.google.firebase.database.connection.idl.IPersistentConnection");
                    refreshAuthToken2(parcel.readString());
                    parcel2.writeNoException();
                    return true;
                case 1598968902:
                    parcel2.writeString("com.google.firebase.database.connection.idl.IPersistentConnection");
                    return true;
                default:
                    return super.onTransact(i, parcel, parcel2, i2);
            }
        }
    }

    void compareAndPut(List<String> list, zzd com_google_android_gms_dynamic_zzd, String str, zzj com_google_firebase_database_connection_idl_zzj) throws RemoteException;

    void initialize() throws RemoteException;

    void interrupt(String str) throws RemoteException;

    boolean isInterrupted(String str) throws RemoteException;

    void listen(List<String> list, zzd com_google_android_gms_dynamic_zzd, zzg com_google_firebase_database_connection_idl_zzg, long j, zzj com_google_firebase_database_connection_idl_zzj) throws RemoteException;

    void merge(List<String> list, zzd com_google_android_gms_dynamic_zzd, zzj com_google_firebase_database_connection_idl_zzj) throws RemoteException;

    void onDisconnectCancel(List<String> list, zzj com_google_firebase_database_connection_idl_zzj) throws RemoteException;

    void onDisconnectMerge(List<String> list, zzd com_google_android_gms_dynamic_zzd, zzj com_google_firebase_database_connection_idl_zzj) throws RemoteException;

    void onDisconnectPut(List<String> list, zzd com_google_android_gms_dynamic_zzd, zzj com_google_firebase_database_connection_idl_zzj) throws RemoteException;

    void purgeOutstandingWrites() throws RemoteException;

    void put(List<String> list, zzd com_google_android_gms_dynamic_zzd, zzj com_google_firebase_database_connection_idl_zzj) throws RemoteException;

    void refreshAuthToken() throws RemoteException;

    void refreshAuthToken2(String str) throws RemoteException;

    void resume(String str) throws RemoteException;

    void setup(ConnectionConfig connectionConfig, zze com_google_firebase_database_connection_idl_zze, zzd com_google_android_gms_dynamic_zzd, zzi com_google_firebase_database_connection_idl_zzi) throws RemoteException;

    void shutdown() throws RemoteException;

    void unlisten(List<String> list, zzd com_google_android_gms_dynamic_zzd) throws RemoteException;
}
