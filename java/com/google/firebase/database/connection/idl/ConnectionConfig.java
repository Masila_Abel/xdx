package com.google.firebase.database.connection.idl;

import android.os.Parcel;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.internal.zzagu;
import com.google.android.gms.internal.zzajv.zza;
import java.util.List;
import outlander.showcaseview.R;

public class ConnectionConfig extends AbstractSafeParcelable {
    public static final zzb CREATOR = new zzb();
    final boolean aQQ;
    final String aQS;
    final HostInfoParcelable aSh;
    final int aSi;
    final List<String> aSj;
    final String aSk;
    final int versionCode;

    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] aSl = new int[zza.values().length];

        static {
            try {
                aSl[zza.aYk.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                aSl[zza.aYl.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                aSl[zza.aYm.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                aSl[zza.aYn.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
            try {
                aSl[zza.aYo.ordinal()] = 5;
            } catch (NoSuchFieldError e5) {
            }
        }
    }

    public ConnectionConfig(int i, HostInfoParcelable hostInfoParcelable, int i2, List<String> list, boolean z, String str, String str2) {
        this.versionCode = i;
        this.aSh = hostInfoParcelable;
        this.aSi = i2;
        this.aSj = list;
        this.aQQ = z;
        this.aSk = str;
        this.aQS = str2;
    }

    public ConnectionConfig(zzagu com_google_android_gms_internal_zzagu, zza com_google_android_gms_internal_zzajv_zza, List<String> list, boolean z, String str, String str2) {
        int i;
        switch (AnonymousClass1.aSl[com_google_android_gms_internal_zzajv_zza.ordinal()]) {
            case R.styleable.View_android_focusable /*1*/:
                i = 1;
                break;
            case R.styleable.View_paddingStart /*2*/:
                i = 2;
                break;
            case R.styleable.View_paddingEnd /*3*/:
                i = 3;
                break;
            case R.styleable.View_theme /*4*/:
                i = 4;
                break;
            default:
                i = 0;
                break;
        }
        this.versionCode = 1;
        this.aSh = HostInfoParcelable.zza(com_google_android_gms_internal_zzagu);
        this.aSi = i;
        this.aSj = list;
        this.aQQ = z;
        this.aSk = str;
        this.aQS = str2;
    }

    public void writeToParcel(Parcel parcel, int i) {
        zzb.zza(this, parcel, i);
    }

    public zza zzcpk() {
        switch (this.aSi) {
            case R.styleable.View_android_theme /*0*/:
                return zza.aYo;
            case R.styleable.View_android_focusable /*1*/:
                return zza.aYk;
            case R.styleable.View_paddingStart /*2*/:
                return zza.aYl;
            case R.styleable.View_paddingEnd /*3*/:
                return zza.aYm;
            case R.styleable.View_theme /*4*/:
                return zza.aYn;
            default:
                return zza.aYo;
        }
    }

    public List<String> zzcpl() {
        return this.aSj;
    }
}
