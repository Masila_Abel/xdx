package com.google.firebase.database.connection.idl;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import java.util.List;
import outlander.showcaseview.R;

public class zzb implements Creator<ConnectionConfig> {
    static void zza(ConnectionConfig connectionConfig, Parcel parcel, int i) {
        int zzcn = com.google.android.gms.common.internal.safeparcel.zzb.zzcn(parcel);
        com.google.android.gms.common.internal.safeparcel.zzb.zzc(parcel, 1, connectionConfig.versionCode);
        com.google.android.gms.common.internal.safeparcel.zzb.zza(parcel, 2, connectionConfig.aSh, i, false);
        com.google.android.gms.common.internal.safeparcel.zzb.zzc(parcel, 3, connectionConfig.aSi);
        com.google.android.gms.common.internal.safeparcel.zzb.zzb(parcel, 4, connectionConfig.aSj, false);
        com.google.android.gms.common.internal.safeparcel.zzb.zza(parcel, 5, connectionConfig.aQQ);
        com.google.android.gms.common.internal.safeparcel.zzb.zza(parcel, 6, connectionConfig.aSk, false);
        com.google.android.gms.common.internal.safeparcel.zzb.zza(parcel, 7, connectionConfig.aQS, false);
        com.google.android.gms.common.internal.safeparcel.zzb.zzaj(parcel, zzcn);
    }

    public /* synthetic */ Object createFromParcel(Parcel parcel) {
        return zzwe(parcel);
    }

    public /* synthetic */ Object[] newArray(int i) {
        return zzaeu(i);
    }

    public ConnectionConfig[] zzaeu(int i) {
        return new ConnectionConfig[i];
    }

    public ConnectionConfig zzwe(Parcel parcel) {
        boolean z = false;
        String str = null;
        int zzcm = zza.zzcm(parcel);
        String str2 = null;
        List list = null;
        int i = 0;
        HostInfoParcelable hostInfoParcelable = null;
        int i2 = 0;
        while (parcel.dataPosition() < zzcm) {
            int zzcl = zza.zzcl(parcel);
            switch (zza.zzgm(zzcl)) {
                case R.styleable.View_android_focusable /*1*/:
                    i2 = zza.zzg(parcel, zzcl);
                    break;
                case R.styleable.View_paddingStart /*2*/:
                    hostInfoParcelable = (HostInfoParcelable) zza.zza(parcel, zzcl, HostInfoParcelable.CREATOR);
                    break;
                case R.styleable.View_paddingEnd /*3*/:
                    i = zza.zzg(parcel, zzcl);
                    break;
                case R.styleable.View_theme /*4*/:
                    list = zza.zzae(parcel, zzcl);
                    break;
                case R.styleable.Toolbar_contentInsetStart /*5*/:
                    z = zza.zzc(parcel, zzcl);
                    break;
                case R.styleable.Toolbar_contentInsetEnd /*6*/:
                    str2 = zza.zzq(parcel, zzcl);
                    break;
                case R.styleable.Toolbar_contentInsetLeft /*7*/:
                    str = zza.zzq(parcel, zzcl);
                    break;
                default:
                    zza.zzb(parcel, zzcl);
                    break;
            }
        }
        if (parcel.dataPosition() == zzcm) {
            return new ConnectionConfig(i2, hostInfoParcelable, i, list, z, str2, str);
        }
        throw new zza.zza("Overread allowed size end=" + zzcm, parcel);
    }
}
