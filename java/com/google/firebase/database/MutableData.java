package com.google.firebase.database;

import com.google.android.gms.internal.zzaho;
import com.google.android.gms.internal.zzahv;
import com.google.android.gms.internal.zzaid;
import com.google.android.gms.internal.zzajx;
import com.google.android.gms.internal.zzake;
import com.google.android.gms.internal.zzaki;
import com.google.android.gms.internal.zzakj;
import com.google.android.gms.internal.zzakk;
import com.google.android.gms.internal.zzakn;
import com.google.android.gms.internal.zzalm;
import com.google.android.gms.internal.zzaln;
import java.util.Iterator;
import java.util.NoSuchElementException;
import outlander.showcaseview.BuildConfig;

public class MutableData {
    private final zzahv aPl;
    private final zzaho aPm;

    private MutableData(zzahv com_google_android_gms_internal_zzahv, zzaho com_google_android_gms_internal_zzaho) {
        this.aPl = com_google_android_gms_internal_zzahv;
        this.aPm = com_google_android_gms_internal_zzaho;
        zzaid.zza(this.aPm, getValue());
    }

    MutableData(zzakj com_google_android_gms_internal_zzakj) {
        this(new zzahv(com_google_android_gms_internal_zzakj), new zzaho(BuildConfig.FLAVOR));
    }

    public MutableData child(String str) {
        zzalm.zzsn(str);
        return new MutableData(this.aPl, this.aPm.zzh(new zzaho(str)));
    }

    public boolean equals(Object obj) {
        return (obj instanceof MutableData) && this.aPl.equals(((MutableData) obj).aPl) && this.aPm.equals(((MutableData) obj).aPm);
    }

    public Iterable<MutableData> getChildren() {
        zzakj zzcmq = zzcmq();
        if (zzcmq.isEmpty() || zzcmq.zzcuu()) {
            return new Iterable<MutableData>(this) {
                final /* synthetic */ MutableData aPn;

                {
                    this.aPn = r1;
                }

                public Iterator<MutableData> iterator() {
                    return new Iterator<MutableData>(this) {
                        final /* synthetic */ AnonymousClass1 aPo;

                        {
                            this.aPo = r1;
                        }

                        public boolean hasNext() {
                            return false;
                        }

                        public /* synthetic */ Object next() {
                            return zzcmr();
                        }

                        public void remove() {
                            throw new UnsupportedOperationException("remove called on immutable collection");
                        }

                        public MutableData zzcmr() {
                            throw new NoSuchElementException();
                        }
                    };
                }
            };
        }
        final Iterator it = zzake.zzm(zzcmq).iterator();
        return new Iterable<MutableData>(this) {
            final /* synthetic */ MutableData aPn;

            public Iterator<MutableData> iterator() {
                return new Iterator<MutableData>(this) {
                    final /* synthetic */ AnonymousClass2 aPp;

                    {
                        this.aPp = r1;
                    }

                    public boolean hasNext() {
                        return it.hasNext();
                    }

                    public /* synthetic */ Object next() {
                        return zzcmr();
                    }

                    public void remove() {
                        throw new UnsupportedOperationException("remove called on immutable collection");
                    }

                    public MutableData zzcmr() {
                        return new MutableData(this.aPp.aPn.aPl, this.aPp.aPn.aPm.zza(((zzaki) it.next()).zzcvq()));
                    }
                };
            }
        };
    }

    public long getChildrenCount() {
        return (long) zzcmq().getChildCount();
    }

    public String getKey() {
        return this.aPm.zzcrc() != null ? this.aPm.zzcrc().asString() : null;
    }

    public Object getPriority() {
        return zzcmq().zzcuv().getValue();
    }

    public Object getValue() {
        return zzcmq().getValue();
    }

    public <T> T getValue(GenericTypeIndicator<T> genericTypeIndicator) {
        return zzaln.zza(zzcmq().getValue(), genericTypeIndicator);
    }

    public <T> T getValue(Class<T> cls) {
        return zzaln.zza(zzcmq().getValue(), cls);
    }

    public boolean hasChild(String str) {
        return !zzcmq().zzao(new zzaho(str)).isEmpty();
    }

    public boolean hasChildren() {
        zzakj zzcmq = zzcmq();
        return (zzcmq.zzcuu() || zzcmq.isEmpty()) ? false : true;
    }

    public void setPriority(Object obj) {
        this.aPl.zzg(this.aPm, zzcmq().zzf(zzakn.zzbr(obj)));
    }

    public void setValue(Object obj) throws DatabaseException {
        zzaid.zza(this.aPm, obj);
        Object zzbv = zzaln.zzbv(obj);
        zzalm.zzbu(zzbv);
        this.aPl.zzg(this.aPm, zzakk.zzbq(zzbv));
    }

    public String toString() {
        zzajx zzcqz = this.aPm.zzcqz();
        String asString = zzcqz != null ? zzcqz.asString() : "<none>";
        String valueOf = String.valueOf(this.aPl.zzcrm().getValue(true));
        return new StringBuilder((String.valueOf(asString).length() + 32) + String.valueOf(valueOf).length()).append("MutableData { key = ").append(asString).append(", value = ").append(valueOf).append(" }").toString();
    }

    zzakj zzcmq() {
        return this.aPl.zzq(this.aPm);
    }
}
