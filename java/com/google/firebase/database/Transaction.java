package com.google.firebase.database;

import com.google.android.gms.internal.zzakj;

public class Transaction {

    public interface Handler {
        Result doTransaction(MutableData mutableData);

        void onComplete(DatabaseError databaseError, boolean z, DataSnapshot dataSnapshot);
    }

    public static class Result {
        private boolean aPA;
        private zzakj aPB;

        private Result(boolean z, zzakj com_google_android_gms_internal_zzakj) {
            this.aPA = z;
            this.aPB = com_google_android_gms_internal_zzakj;
        }

        public boolean isSuccess() {
            return this.aPA;
        }

        public zzakj zzcmq() {
            return this.aPB;
        }
    }

    public static Result abort() {
        return new Result(false, null);
    }

    public static Result success(MutableData mutableData) {
        return new Result(true, mutableData.zzcmq());
    }
}
