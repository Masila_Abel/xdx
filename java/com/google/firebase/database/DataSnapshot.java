package com.google.firebase.database;

import com.google.android.gms.internal.zzaho;
import com.google.android.gms.internal.zzake;
import com.google.android.gms.internal.zzaki;
import com.google.android.gms.internal.zzalm;
import com.google.android.gms.internal.zzaln;
import java.util.Iterator;

public class DataSnapshot {
    private final zzake aOM;
    private final DatabaseReference aON;

    DataSnapshot(DatabaseReference databaseReference, zzake com_google_android_gms_internal_zzake) {
        this.aOM = com_google_android_gms_internal_zzake;
        this.aON = databaseReference;
    }

    public DataSnapshot child(String str) {
        return new DataSnapshot(this.aON.child(str), zzake.zzm(this.aOM.zzcmq().zzao(new zzaho(str))));
    }

    public boolean exists() {
        return !this.aOM.zzcmq().isEmpty();
    }

    public Iterable<DataSnapshot> getChildren() {
        final Iterator it = this.aOM.iterator();
        return new Iterable<DataSnapshot>(this) {
            final /* synthetic */ DataSnapshot aOP;

            public Iterator<DataSnapshot> iterator() {
                return new Iterator<DataSnapshot>(this) {
                    final /* synthetic */ AnonymousClass1 aOQ;

                    {
                        this.aOQ = r1;
                    }

                    public boolean hasNext() {
                        return it.hasNext();
                    }

                    public /* synthetic */ Object next() {
                        return zzcmn();
                    }

                    public void remove() {
                        throw new UnsupportedOperationException("remove called on immutable collection");
                    }

                    public DataSnapshot zzcmn() {
                        zzaki com_google_android_gms_internal_zzaki = (zzaki) it.next();
                        return new DataSnapshot(this.aOQ.aOP.aON.child(com_google_android_gms_internal_zzaki.zzcvq().asString()), zzake.zzm(com_google_android_gms_internal_zzaki.zzcmq()));
                    }
                };
            }
        };
    }

    public long getChildrenCount() {
        return (long) this.aOM.zzcmq().getChildCount();
    }

    public String getKey() {
        return this.aON.getKey();
    }

    public Object getPriority() {
        Object value = this.aOM.zzcmq().zzcuv().getValue();
        return value instanceof Long ? Double.valueOf((double) ((Long) value).longValue()) : value;
    }

    public DatabaseReference getRef() {
        return this.aON;
    }

    public Object getValue() {
        return this.aOM.zzcmq().getValue();
    }

    public <T> T getValue(GenericTypeIndicator<T> genericTypeIndicator) {
        return zzaln.zza(this.aOM.zzcmq().getValue(), genericTypeIndicator);
    }

    public <T> T getValue(Class<T> cls) {
        return zzaln.zza(this.aOM.zzcmq().getValue(), cls);
    }

    public Object getValue(boolean z) {
        return this.aOM.zzcmq().getValue(z);
    }

    public boolean hasChild(String str) {
        if (this.aON.getParent() == null) {
            zzalm.zzso(str);
        } else {
            zzalm.zzsn(str);
        }
        return !this.aOM.zzcmq().zzao(new zzaho(str)).isEmpty();
    }

    public boolean hasChildren() {
        return this.aOM.zzcmq().getChildCount() > 0;
    }

    public String toString() {
        String valueOf = String.valueOf(this.aON.getKey());
        String valueOf2 = String.valueOf(this.aOM.zzcmq().getValue(true));
        return new StringBuilder((String.valueOf(valueOf).length() + 33) + String.valueOf(valueOf2).length()).append("DataSnapshot { key = ").append(valueOf).append(", value = ").append(valueOf2).append(" }").toString();
    }
}
