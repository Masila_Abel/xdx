package com.google.firebase.database;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.Map;
import outlander.showcaseview.BuildConfig;

public class DatabaseError {
    public static final int DATA_STALE = -1;
    public static final int DISCONNECTED = -4;
    public static final int EXPIRED_TOKEN = -6;
    public static final int INVALID_TOKEN = -7;
    public static final int MAX_RETRIES = -8;
    public static final int NETWORK_ERROR = -24;
    public static final int OPERATION_FAILED = -2;
    public static final int OVERRIDDEN_BY_SET = -9;
    public static final int PERMISSION_DENIED = -3;
    public static final int UNAVAILABLE = -10;
    public static final int UNKNOWN_ERROR = -999;
    public static final int USER_CODE_EXCEPTION = -11;
    public static final int WRITE_CANCELED = -25;
    private static final Map<Integer, String> aOR = new HashMap();
    private static final Map<String, Integer> aOS = new HashMap();
    private final int aOT;
    private final String aOU;
    private final String message;

    static {
        aOR.put(Integer.valueOf(DATA_STALE), "The transaction needs to be run again with current data");
        aOR.put(Integer.valueOf(OPERATION_FAILED), "The server indicated that this operation failed");
        aOR.put(Integer.valueOf(PERMISSION_DENIED), "This client does not have permission to perform this operation");
        aOR.put(Integer.valueOf(DISCONNECTED), "The operation had to be aborted due to a network disconnect");
        aOR.put(Integer.valueOf(EXPIRED_TOKEN), "The supplied auth token has expired");
        aOR.put(Integer.valueOf(INVALID_TOKEN), "The supplied auth token was invalid");
        aOR.put(Integer.valueOf(MAX_RETRIES), "The transaction had too many retries");
        aOR.put(Integer.valueOf(OVERRIDDEN_BY_SET), "The transaction was overridden by a subsequent set");
        aOR.put(Integer.valueOf(UNAVAILABLE), "The service is unavailable");
        aOR.put(Integer.valueOf(USER_CODE_EXCEPTION), "User code called from the Firebase Database runloop threw an exception:\n");
        aOR.put(Integer.valueOf(NETWORK_ERROR), "The operation could not be performed due to a network error");
        aOR.put(Integer.valueOf(WRITE_CANCELED), "The write was canceled by the user.");
        aOR.put(Integer.valueOf(UNKNOWN_ERROR), "An unknown error occurred");
        aOS.put("datastale", Integer.valueOf(DATA_STALE));
        aOS.put("failure", Integer.valueOf(OPERATION_FAILED));
        aOS.put("permission_denied", Integer.valueOf(PERMISSION_DENIED));
        aOS.put("disconnected", Integer.valueOf(DISCONNECTED));
        aOS.put("expired_token", Integer.valueOf(EXPIRED_TOKEN));
        aOS.put("invalid_token", Integer.valueOf(INVALID_TOKEN));
        aOS.put("maxretries", Integer.valueOf(MAX_RETRIES));
        aOS.put("overriddenbyset", Integer.valueOf(OVERRIDDEN_BY_SET));
        aOS.put("unavailable", Integer.valueOf(UNAVAILABLE));
        aOS.put("network_error", Integer.valueOf(NETWORK_ERROR));
        aOS.put("write_canceled", Integer.valueOf(WRITE_CANCELED));
    }

    private DatabaseError(int i, String str) {
        this(i, str, null);
    }

    private DatabaseError(int i, String str, String str2) {
        this.aOT = i;
        this.message = str;
        if (str2 == null) {
            str2 = BuildConfig.FLAVOR;
        }
        this.aOU = str2;
    }

    public static DatabaseError fromException(Throwable th) {
        Writer stringWriter = new StringWriter();
        th.printStackTrace(new PrintWriter(stringWriter));
        String valueOf = String.valueOf((String) aOR.get(Integer.valueOf(USER_CODE_EXCEPTION)));
        String valueOf2 = String.valueOf(stringWriter.toString());
        return new DatabaseError(USER_CODE_EXCEPTION, valueOf2.length() != 0 ? valueOf.concat(valueOf2) : new String(valueOf));
    }

    public static DatabaseError zzaer(int i) {
        if (aOR.containsKey(Integer.valueOf(i))) {
            return new DatabaseError(i, (String) aOR.get(Integer.valueOf(i)), null);
        }
        throw new IllegalArgumentException("Invalid Firebase Database error code: " + i);
    }

    public static DatabaseError zzbk(String str, String str2) {
        return zzp(str, str2, null);
    }

    public static DatabaseError zzp(String str, String str2, String str3) {
        Integer num = (Integer) aOS.get(str.toLowerCase());
        Integer valueOf = num == null ? Integer.valueOf(UNKNOWN_ERROR) : num;
        return new DatabaseError(valueOf.intValue(), str2 == null ? (String) aOR.get(valueOf) : str2, str3);
    }

    public static DatabaseError zzrg(String str) {
        return zzbk(str, null);
    }

    public int getCode() {
        return this.aOT;
    }

    public String getDetails() {
        return this.aOU;
    }

    public String getMessage() {
        return this.message;
    }

    public DatabaseException toException() {
        String str = "Firebase Database error: ";
        String valueOf = String.valueOf(this.message);
        return new DatabaseException(valueOf.length() != 0 ? str.concat(valueOf) : new String(str));
    }

    public String toString() {
        String str = "DatabaseError: ";
        String valueOf = String.valueOf(this.message);
        return valueOf.length() != 0 ? str.concat(valueOf) : new String(str);
    }
}
