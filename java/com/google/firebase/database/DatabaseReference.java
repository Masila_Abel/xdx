package com.google.firebase.database;

import com.google.android.gms.internal.zzahf;
import com.google.android.gms.internal.zzahi;
import com.google.android.gms.internal.zzaho;
import com.google.android.gms.internal.zzahq;
import com.google.android.gms.internal.zzahs;
import com.google.android.gms.internal.zzaid;
import com.google.android.gms.internal.zzajx;
import com.google.android.gms.internal.zzakj;
import com.google.android.gms.internal.zzakk;
import com.google.android.gms.internal.zzakn;
import com.google.android.gms.internal.zzali;
import com.google.android.gms.internal.zzalk;
import com.google.android.gms.internal.zzall;
import com.google.android.gms.internal.zzalm;
import com.google.android.gms.internal.zzaln;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.Transaction.Handler;
import java.net.URLEncoder;
import java.util.Map;
import outlander.showcaseview.BuildConfig;

public class DatabaseReference extends Query {
    private static zzahi aOV;

    public interface CompletionListener {
        void onComplete(DatabaseError databaseError, DatabaseReference databaseReference);
    }

    DatabaseReference(zzahq com_google_android_gms_internal_zzahq, zzaho com_google_android_gms_internal_zzaho) {
        super(com_google_android_gms_internal_zzahq, com_google_android_gms_internal_zzaho);
    }

    public static void goOffline() {
        zza(zzcmo());
    }

    public static void goOnline() {
        zzb(zzcmo());
    }

    private Task<Void> zza(final zzakj com_google_android_gms_internal_zzakj, CompletionListener completionListener) {
        zzalm.zzaq(zzcmu());
        final zzali zzb = zzall.zzb(completionListener);
        this.aPi.zzs(new Runnable(this) {
            final /* synthetic */ DatabaseReference aOY;

            public void run() {
                this.aOY.aPi.zza(this.aOY.zzcmu().zza(zzajx.zzcup()), com_google_android_gms_internal_zzakj, (CompletionListener) zzb.zzcwp());
            }
        });
        return (Task) zzb.getFirst();
    }

    private Task<Void> zza(Object obj, zzakj com_google_android_gms_internal_zzakj, CompletionListener completionListener) {
        zzalm.zzaq(zzcmu());
        zzaid.zza(zzcmu(), obj);
        Object zzbv = zzaln.zzbv(obj);
        zzalm.zzbu(zzbv);
        final zzakj zza = zzakk.zza(zzbv, com_google_android_gms_internal_zzakj);
        final zzali zzb = zzall.zzb(completionListener);
        this.aPi.zzs(new Runnable(this) {
            final /* synthetic */ DatabaseReference aOY;

            public void run() {
                this.aOY.aPi.zza(this.aOY.zzcmu(), zza, (CompletionListener) zzb.zzcwp());
            }
        });
        return (Task) zzb.getFirst();
    }

    private Task<Void> zza(final Map<String, Object> map, CompletionListener completionListener) {
        if (map == null) {
            throw new NullPointerException("Can't pass null for argument 'update' in updateChildren()");
        }
        final zzahf zzby = zzahf.zzby(zzalm.zzc(zzcmu(), map));
        final zzali zzb = zzall.zzb(completionListener);
        this.aPi.zzs(new Runnable(this) {
            final /* synthetic */ DatabaseReference aOY;

            public void run() {
                this.aOY.aPi.zza(this.aOY.zzcmu(), zzby, (CompletionListener) zzb.zzcwp(), map);
            }
        });
        return (Task) zzb.getFirst();
    }

    static void zza(zzahi com_google_android_gms_internal_zzahi) {
        zzahs.zzd(com_google_android_gms_internal_zzahi);
    }

    static void zzb(zzahi com_google_android_gms_internal_zzahi) {
        zzahs.zze(com_google_android_gms_internal_zzahi);
    }

    private static synchronized zzahi zzcmo() {
        zzahi com_google_android_gms_internal_zzahi;
        synchronized (DatabaseReference.class) {
            if (aOV == null) {
                aOV = new zzahi();
            }
            com_google_android_gms_internal_zzahi = aOV;
        }
        return com_google_android_gms_internal_zzahi;
    }

    public DatabaseReference child(String str) {
        if (str == null) {
            throw new NullPointerException("Can't pass null for argument 'pathString' in child()");
        }
        if (zzcmu().isEmpty()) {
            zzalm.zzso(str);
        } else {
            zzalm.zzsn(str);
        }
        return new DatabaseReference(this.aPi, zzcmu().zzh(new zzaho(str)));
    }

    public boolean equals(Object obj) {
        return (obj instanceof DatabaseReference) && toString().equals(obj.toString());
    }

    public FirebaseDatabase getDatabase() {
        return this.aPi.getDatabase();
    }

    public String getKey() {
        return zzcmu().isEmpty() ? null : zzcmu().zzcrc().asString();
    }

    public DatabaseReference getParent() {
        zzaho zzcrb = zzcmu().zzcrb();
        return zzcrb != null ? new DatabaseReference(this.aPi, zzcrb) : null;
    }

    public DatabaseReference getRoot() {
        return new DatabaseReference(this.aPi, new zzaho(BuildConfig.FLAVOR));
    }

    public int hashCode() {
        return toString().hashCode();
    }

    public OnDisconnect onDisconnect() {
        zzalm.zzaq(zzcmu());
        return new OnDisconnect(this.aPi, zzcmu());
    }

    public DatabaseReference push() {
        return new DatabaseReference(this.aPi, zzcmu().zza(zzajx.zzsc(zzalk.zzcn(this.aPi.zzcrg()))));
    }

    public Task<Void> removeValue() {
        return setValue(null);
    }

    public void removeValue(CompletionListener completionListener) {
        setValue(null, completionListener);
    }

    public void runTransaction(Handler handler) {
        runTransaction(handler, true);
    }

    public void runTransaction(final Handler handler, final boolean z) {
        if (handler == null) {
            throw new NullPointerException("Can't pass null for argument 'handler' in runTransaction()");
        }
        zzalm.zzaq(zzcmu());
        this.aPi.zzs(new Runnable(this) {
            final /* synthetic */ DatabaseReference aOY;

            public void run() {
                this.aOY.aPi.zza(this.aOY.zzcmu(), handler, z);
            }
        });
    }

    public Task<Void> setPriority(Object obj) {
        return zza(zzakn.zzbr(obj), null);
    }

    public void setPriority(Object obj, CompletionListener completionListener) {
        zza(zzakn.zzbr(obj), completionListener);
    }

    public Task<Void> setValue(Object obj) {
        return zza(obj, zzakn.zzbr(null), null);
    }

    public Task<Void> setValue(Object obj, Object obj2) {
        return zza(obj, zzakn.zzbr(obj2), null);
    }

    public void setValue(Object obj, CompletionListener completionListener) {
        zza(obj, zzakn.zzbr(null), completionListener);
    }

    public void setValue(Object obj, Object obj2, CompletionListener completionListener) {
        zza(obj, zzakn.zzbr(obj2), completionListener);
    }

    public String toString() {
        String valueOf;
        DatabaseReference parent = getParent();
        if (parent == null) {
            return this.aPi.toString();
        }
        try {
            valueOf = String.valueOf(parent.toString());
            String valueOf2 = String.valueOf(URLEncoder.encode(getKey(), "UTF-8").replace("+", "%20"));
            return new StringBuilder((String.valueOf(valueOf).length() + 1) + String.valueOf(valueOf2).length()).append(valueOf).append("/").append(valueOf2).toString();
        } catch (Throwable e) {
            Throwable th = e;
            String str = "Failed to URLEncode key: ";
            valueOf = String.valueOf(getKey());
            throw new DatabaseException(valueOf.length() != 0 ? str.concat(valueOf) : new String(str), th);
        }
    }

    public Task<Void> updateChildren(Map<String, Object> map) {
        return zza((Map) map, null);
    }

    public void updateChildren(Map<String, Object> map, CompletionListener completionListener) {
        zza((Map) map, completionListener);
    }
}
