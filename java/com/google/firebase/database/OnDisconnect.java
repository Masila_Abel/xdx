package com.google.firebase.database;

import com.google.android.gms.internal.zzaho;
import com.google.android.gms.internal.zzahq;
import com.google.android.gms.internal.zzaid;
import com.google.android.gms.internal.zzakj;
import com.google.android.gms.internal.zzakk;
import com.google.android.gms.internal.zzakn;
import com.google.android.gms.internal.zzali;
import com.google.android.gms.internal.zzall;
import com.google.android.gms.internal.zzalm;
import com.google.android.gms.internal.zzaln;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference.CompletionListener;
import java.util.Map;

public class OnDisconnect {
    private zzahq aPi;
    private zzaho aPq;

    OnDisconnect(zzahq com_google_android_gms_internal_zzahq, zzaho com_google_android_gms_internal_zzaho) {
        this.aPi = com_google_android_gms_internal_zzahq;
        this.aPq = com_google_android_gms_internal_zzaho;
    }

    private Task<Void> zza(CompletionListener completionListener) {
        final zzali zzb = zzall.zzb(completionListener);
        this.aPi.zzs(new Runnable(this) {
            final /* synthetic */ OnDisconnect aPr;

            public void run() {
                this.aPr.aPi.zza(this.aPr.aPq, (CompletionListener) zzb.zzcwp());
            }
        });
        return (Task) zzb.getFirst();
    }

    private Task<Void> zza(final Map<String, Object> map, CompletionListener completionListener) {
        final Map zzc = zzalm.zzc(this.aPq, map);
        final zzali zzb = zzall.zzb(completionListener);
        this.aPi.zzs(new Runnable(this) {
            final /* synthetic */ OnDisconnect aPr;

            public void run() {
                this.aPr.aPi.zza(this.aPr.aPq, zzc, (CompletionListener) zzb.zzcwp(), map);
            }
        });
        return (Task) zzb.getFirst();
    }

    private Task<Void> zzb(Object obj, zzakj com_google_android_gms_internal_zzakj, CompletionListener completionListener) {
        zzalm.zzaq(this.aPq);
        zzaid.zza(this.aPq, obj);
        Object zzbv = zzaln.zzbv(obj);
        zzalm.zzbu(zzbv);
        final zzakj zza = zzakk.zza(zzbv, com_google_android_gms_internal_zzakj);
        final zzali zzb = zzall.zzb(completionListener);
        this.aPi.zzs(new Runnable(this) {
            final /* synthetic */ OnDisconnect aPr;

            public void run() {
                this.aPr.aPi.zzb(this.aPr.aPq, zza, (CompletionListener) zzb.zzcwp());
            }
        });
        return (Task) zzb.getFirst();
    }

    public Task<Void> cancel() {
        return zza(null);
    }

    public void cancel(CompletionListener completionListener) {
        zza(completionListener);
    }

    public Task<Void> removeValue() {
        return setValue(null);
    }

    public void removeValue(CompletionListener completionListener) {
        setValue(null, completionListener);
    }

    public Task<Void> setValue(Object obj) {
        return zzb(obj, zzakn.zzcvs(), null);
    }

    public Task<Void> setValue(Object obj, double d) {
        return zzb(obj, zzakn.zzbr(Double.valueOf(d)), null);
    }

    public Task<Void> setValue(Object obj, String str) {
        return zzb(obj, zzakn.zzbr(str), null);
    }

    public void setValue(Object obj, double d, CompletionListener completionListener) {
        zzb(obj, zzakn.zzbr(Double.valueOf(d)), completionListener);
    }

    public void setValue(Object obj, CompletionListener completionListener) {
        zzb(obj, zzakn.zzcvs(), completionListener);
    }

    public void setValue(Object obj, String str, CompletionListener completionListener) {
        zzb(obj, zzakn.zzbr(str), completionListener);
    }

    public void setValue(Object obj, Map map, CompletionListener completionListener) {
        zzb(obj, zzakn.zzbr(map), completionListener);
    }

    public Task<Void> updateChildren(Map<String, Object> map) {
        return zza(map, null);
    }

    public void updateChildren(Map<String, Object> map, CompletionListener completionListener) {
        zza(map, completionListener);
    }
}
