package com.google.firebase.database;

import com.google.android.gms.internal.zzaho;
import com.google.android.gms.internal.zzahq;
import com.google.android.gms.internal.zzake;
import com.google.android.gms.internal.zzakj;

public class zza {
    public static DataSnapshot zza(DatabaseReference databaseReference, zzake com_google_android_gms_internal_zzake) {
        return new DataSnapshot(databaseReference, com_google_android_gms_internal_zzake);
    }

    public static DatabaseReference zza(zzahq com_google_android_gms_internal_zzahq, zzaho com_google_android_gms_internal_zzaho) {
        return new DatabaseReference(com_google_android_gms_internal_zzahq, com_google_android_gms_internal_zzaho);
    }

    public static MutableData zza(zzakj com_google_android_gms_internal_zzakj) {
        return new MutableData(com_google_android_gms_internal_zzakj);
    }
}
