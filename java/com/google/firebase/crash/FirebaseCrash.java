package com.google.firebase.crash;

import android.content.Context;
import android.os.Looper;
import android.support.annotation.Keep;
import android.util.Log;
import com.google.android.gms.dynamic.zze;
import com.google.firebase.FirebaseApp;
import com.google.firebase.crash.internal.FirebaseCrashOptions;
import com.google.firebase.crash.internal.config.flag.Flags;
import com.google.firebase.crash.internal.zza;
import com.google.firebase.crash.internal.zzb;
import com.google.firebase.crash.internal.zzd;
import com.google.firebase.crash.internal.zzg;
import com.google.firebase.crash.internal.zzh;
import com.google.firebase.iid.zzc;
import outlander.showcaseview.BuildConfig;

public class FirebaseCrash {
    private static final String LOG_TAG = FirebaseCrash.class.getSimpleName();
    private static volatile FirebaseCrash aOl;
    private zzd aOj;
    private zza aOk;
    private boolean aiu;

    FirebaseCrash(FirebaseApp firebaseApp, boolean z) {
        this.aiu = z;
        Context applicationContext = firebaseApp.getApplicationContext();
        if (applicationContext == null) {
            Log.w(LOG_TAG, "Application context is missing, disabling api");
            this.aiu = false;
        }
        if (this.aiu) {
            try {
                FirebaseCrashOptions firebaseCrashOptions = new FirebaseCrashOptions(firebaseApp.getOptions().getApplicationId(), firebaseApp.getOptions().getApiKey());
                zzg.zzcmj().zzbq(applicationContext);
                this.aOj = zzg.zzcmj().zzcmk();
                this.aOj.zza(zze.zzac(applicationContext), firebaseCrashOptions);
                this.aOk = new zza(applicationContext);
                zzcmg();
                String str = LOG_TAG;
                String str2 = "FirebaseCrash reporting initialized ";
                String valueOf = String.valueOf(zzg.zzcmj().toString());
                Log.i(str, valueOf.length() != 0 ? str2.concat(valueOf) : new String(str2));
                return;
            } catch (Throwable e) {
                Log.e(LOG_TAG, "Failed to initialize crash reporting", e);
                this.aiu = false;
                return;
            }
        }
        Log.i(LOG_TAG, "Crash reporting is disabled");
    }

    @Keep
    public static FirebaseCrash getInstance(FirebaseApp firebaseApp) {
        Flags.initialize(firebaseApp.getApplicationContext());
        FirebaseCrash firebaseCrash = new FirebaseCrash(firebaseApp, ((Boolean) Flags.aOs.get()).booleanValue());
        synchronized (FirebaseCrash.class) {
            if (aOl == null) {
                aOl = firebaseCrash;
            }
        }
        return firebaseCrash;
    }

    private boolean isEnabled() {
        return this.aiu;
    }

    public static boolean isSingletonInitialized() {
        return aOl != null;
    }

    public static void log(String str) {
        try {
            zzcmd().zzre(str);
        } catch (zzb e) {
            Log.v(LOG_TAG, e.getMessage());
        }
    }

    public static void logcat(int i, String str, String str2) {
        try {
            zzcmd().zzf(i, str, str2);
        } catch (zzb e) {
            Log.v(LOG_TAG, e.getMessage());
        }
    }

    public static void report(Throwable th) {
        try {
            zzcmd().zzf(th);
        } catch (zzb e) {
            Log.v(LOG_TAG, e.getMessage());
        }
    }

    public static FirebaseCrash zzcmd() {
        if (aOl == null) {
            synchronized (FirebaseCrash.class) {
                if (aOl == null) {
                    aOl = getInstance(FirebaseApp.getInstance());
                }
            }
        }
        return aOl;
    }

    private zzd zzcme() {
        return this.aOj;
    }

    private static boolean zzcmf() {
        return Looper.getMainLooper().getThread().getId() == Thread.currentThread().getId();
    }

    private void zzcmg() {
        if (zzcmf()) {
            Thread.setDefaultUncaughtExceptionHandler(new zzh(Thread.getDefaultUncaughtExceptionHandler(), this));
            return;
        }
        throw new RuntimeException("FirebaseCrash reporting may only be initialized on the main thread (preferably in your app's Application.onCreate)");
    }

    private String zzcmh() {
        return zzc.zzcwr().getId();
    }

    public void zzf(int i, String str, String str2) throws zzb {
        if (str2 != null) {
            if (str == null) {
                str = BuildConfig.FLAVOR;
            }
            Log.println(i, str, str2);
            zzre(str2);
        }
    }

    public void zzf(Throwable th) throws zzb {
        if (isEnabled()) {
            zzd zzcme = zzcme();
            if (zzcme != null && th != null) {
                this.aOk.zza(false, System.currentTimeMillis());
                try {
                    zzcme.zzrf(zzcmh());
                    zzcme.zzam(zze.zzac(th));
                    return;
                } catch (Throwable e) {
                    Log.e(LOG_TAG, "report remoting failed", e);
                    return;
                }
            }
            return;
        }
        throw new zzb("Firebase Crash Reporting is disabled.");
    }

    public void zzg(Throwable th) throws zzb {
        if (isEnabled()) {
            zzd zzcme = zzcme();
            if (zzcme != null && th != null) {
                try {
                    this.aOk.zza(true, System.currentTimeMillis());
                    try {
                        Thread.sleep(200);
                    } catch (InterruptedException e) {
                        Thread.currentThread().interrupt();
                    }
                    zzcme.zzrf(zzcmh());
                    zzcme.zzan(zze.zzac(th));
                    return;
                } catch (Throwable e2) {
                    Log.e(LOG_TAG, "report remoting failed", e2);
                    return;
                }
            }
            return;
        }
        throw new zzb("Firebase Crash Reporting is disabled.");
    }

    public void zzre(String str) throws zzb {
        if (isEnabled()) {
            zzd zzcme = zzcme();
            if (zzcme != null && str != null) {
                try {
                    zzcme.log(str);
                    return;
                } catch (Throwable e) {
                    Log.e(LOG_TAG, "log remoting failed", e);
                    return;
                }
            }
            return;
        }
        throw new zzb("Firebase Crash Reporting is disabled.");
    }
}
