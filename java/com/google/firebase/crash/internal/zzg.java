package com.google.firebase.crash.internal;

import android.content.Context;
import com.google.android.gms.common.internal.zzab;
import com.google.android.gms.internal.zzsb;

public class zzg {
    private static zzg aOp;
    private zzsb aOo;

    public static class zza extends Exception {
        private zza(Throwable th) {
            super(th);
        }
    }

    private zzg() {
    }

    public static zzg zzcmj() {
        zzg com_google_firebase_crash_internal_zzg;
        synchronized (zzg.class) {
            if (aOp != null) {
                com_google_firebase_crash_internal_zzg = aOp;
            } else {
                aOp = new zzg();
                com_google_firebase_crash_internal_zzg = aOp;
            }
        }
        return com_google_firebase_crash_internal_zzg;
    }

    public void zzbq(Context context) throws zza {
        synchronized (zzg.class) {
            if (this.aOo != null) {
                return;
            }
            try {
                this.aOo = zzsb.zza(context, zzsb.KK, "com.google.android.gms.crash");
            } catch (com.google.android.gms.internal.zzsb.zza e) {
                throw new zza(e);
            }
        }
    }

    public zzd zzcmk() throws zza {
        zzab.zzy(this.aOo);
        try {
            return com.google.firebase.crash.internal.zzd.zza.zzma(this.aOo.zziu("com.google.firebase.crash.internal.api.FirebaseCrashApiImpl"));
        } catch (com.google.android.gms.internal.zzsb.zza e) {
            throw new zza(e);
        }
    }

    public zze zzcml() throws zza {
        zzab.zzy(this.aOo);
        try {
            return com.google.firebase.crash.internal.zze.zza.zzmb(this.aOo.zziu("com.google.firebase.crash.internal.service.FirebaseCrashReceiverServiceImpl"));
        } catch (com.google.android.gms.internal.zzsb.zza e) {
            throw new zza(e);
        }
    }

    public zzf zzcmm() throws zza {
        zzab.zzy(this.aOo);
        try {
            return com.google.firebase.crash.internal.zzf.zza.zzmc(this.aOo.zziu("com.google.firebase.crash.internal.service.FirebaseCrashSenderServiceImpl"));
        } catch (com.google.android.gms.internal.zzsb.zza e) {
            throw new zza(e);
        }
    }
}
