package com.google.firebase.crash.internal.config.flag;

import android.content.Context;
import android.support.annotation.Keep;
import com.google.android.gms.internal.zztv;
import com.google.android.gms.internal.zztw;
import com.google.android.gms.internal.zztz;
import java.util.concurrent.TimeUnit;

@Keep
public final class Flags {
    public static final zztv<Integer> aOA = zztv.zzb(0, "crash:retry_num_attempts", 12);
    public static final zztv<Integer> aOB = zztv.zzb(0, "crash:batch_size", 5);
    public static final zztv<Long> aOC = zztv.zzb(0, "crash:batch_throttle", TimeUnit.MINUTES.toMillis(5));
    public static final zztv<Integer> aOD = zztv.zzb(0, "crash:frame_depth", 60);
    public static final zztv<Boolean> aOs = zztv.zzb(0, "crash:enabled", Boolean.valueOf(true));
    public static final zztv<String> aOt = zztv.zzc(0, "crash:gateway_url", "https://mobilecrashreporting.googleapis.com/v1/crashes:batchCreate?key=");
    public static final zztv<Integer> aOu = zztv.zzb(0, "crash:log_buffer_capacity", 100);
    public static final zztv<Integer> aOv = zztv.zzb(0, "crash:log_buffer_max_total_size", 32768);
    public static final zztv<Integer> aOw = zztv.zzb(0, "crash:crash_backlog_capacity", 5);
    public static final zztv<Long> aOx = zztv.zzb(0, "crash:crash_backlog_max_age", 604800000);
    public static final zztv<Long> aOy = zztv.zzb(0, "crash:starting_backoff", TimeUnit.SECONDS.toMillis(1));
    public static final zztv<Long> aOz = zztv.zzb(0, "crash:backoff_limit", TimeUnit.MINUTES.toMillis(60));

    private Flags() {
    }

    public static final void initialize(Context context) {
        zztz.zzbet();
        zztw.initialize(context);
    }
}
