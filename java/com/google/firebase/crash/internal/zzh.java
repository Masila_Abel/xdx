package com.google.firebase.crash.internal;

import android.util.Log;
import com.google.firebase.crash.FirebaseCrash;
import java.lang.Thread.UncaughtExceptionHandler;
import outlander.showcaseview.BuildConfig;

public class zzh implements UncaughtExceptionHandler {
    private final FirebaseCrash aOq;
    private final UncaughtExceptionHandler aOr;

    public zzh(UncaughtExceptionHandler uncaughtExceptionHandler, FirebaseCrash firebaseCrash) {
        this.aOq = firebaseCrash;
        this.aOr = uncaughtExceptionHandler;
    }

    public void uncaughtException(Thread thread, Throwable th) {
        Log.e("UncaughtException", BuildConfig.FLAVOR, th);
        try {
            this.aOq.zzg(th);
        } catch (zzb e) {
            try {
                Log.v("UncaughtException", e.getMessage());
            } catch (Throwable e2) {
                Log.e("UncaughtException", "Ouch!  My own exception handler threw an exception.", e2);
            }
        }
        if (this.aOr != null) {
            this.aOr.uncaughtException(thread, th);
        }
    }
}
