package com.google.firebase.crash.internal.service;

import android.app.IntentService;
import android.content.Intent;
import android.os.RemoteException;
import android.support.annotation.Keep;
import android.util.Log;
import com.google.firebase.crash.internal.zze;
import com.google.firebase.crash.internal.zzg;
import com.google.firebase.crash.internal.zzg.zza;

public final class FirebaseCrashReceiverService extends IntentService {
    private static final String LOG_TAG = FirebaseCrashReceiverService.class.getSimpleName();
    private static final String aOE = FirebaseCrashReceiverService.class.getName();
    public static final String aOF = String.valueOf(aOE).concat(".SAVE");
    public static final String aOG = String.valueOf(aOE).concat(".CRASH_REPORT");
    public static final String aOH = String.valueOf(aOE).concat(".CRASH_TIME");
    public static final String aOI = String.valueOf(aOE).concat(".API_KEY");
    public static final String aOJ = String.valueOf(aOE).concat(".IS_FATAL");
    private zze aOK;

    @Keep
    public FirebaseCrashReceiverService() {
        super(FirebaseCrashReceiverService.class.getSimpleName());
    }

    public void onCreate() {
        Throwable e;
        super.onCreate();
        try {
            zzg.zzcmj().zzbq(getApplicationContext());
            this.aOK = zzg.zzcmj().zzcml();
            this.aOK.zzao(com.google.android.gms.dynamic.zze.zzac(this));
            return;
        } catch (RemoteException e2) {
            e = e2;
        } catch (zza e3) {
            e = e3;
        }
        Log.e(LOG_TAG, "Unexpected failure remoting onCreate()", e);
        this.aOK = null;
    }

    public void onDestroy() {
        if (this.aOK != null) {
            try {
                this.aOK.onDestroy();
            } catch (Throwable e) {
                Log.e(LOG_TAG, "Unexpected failure remoting onDestroy()", e);
            }
        }
        super.onDestroy();
    }

    protected void onHandleIntent(Intent intent) {
        if (this.aOK != null) {
            try {
                this.aOK.zzap(com.google.android.gms.dynamic.zze.zzac(intent));
            } catch (Throwable e) {
                Log.e(LOG_TAG, "Unexpected failure remoting onHandleIntent()", e);
            }
        }
    }
}
