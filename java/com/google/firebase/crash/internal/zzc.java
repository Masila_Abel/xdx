package com.google.firebase.crash.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import outlander.showcaseview.R;

public class zzc implements Creator<FirebaseCrashOptions> {
    static void zza(FirebaseCrashOptions firebaseCrashOptions, Parcel parcel, int i) {
        int zzcn = zzb.zzcn(parcel);
        zzb.zzc(parcel, 1, firebaseCrashOptions.mVersionCode);
        zzb.zza(parcel, 2, firebaseCrashOptions.zzcmi(), false);
        zzb.zza(parcel, 3, firebaseCrashOptions.getApiKey(), false);
        zzb.zzaj(parcel, zzcn);
    }

    public /* synthetic */ Object createFromParcel(Parcel parcel) {
        return zzwc(parcel);
    }

    public /* synthetic */ Object[] newArray(int i) {
        return zzaeq(i);
    }

    public FirebaseCrashOptions[] zzaeq(int i) {
        return new FirebaseCrashOptions[i];
    }

    public FirebaseCrashOptions zzwc(Parcel parcel) {
        String str = null;
        int zzcm = zza.zzcm(parcel);
        int i = 0;
        String str2 = null;
        while (parcel.dataPosition() < zzcm) {
            int zzcl = zza.zzcl(parcel);
            switch (zza.zzgm(zzcl)) {
                case R.styleable.View_android_focusable /*1*/:
                    i = zza.zzg(parcel, zzcl);
                    break;
                case R.styleable.View_paddingStart /*2*/:
                    str2 = zza.zzq(parcel, zzcl);
                    break;
                case R.styleable.View_paddingEnd /*3*/:
                    str = zza.zzq(parcel, zzcl);
                    break;
                default:
                    zza.zzb(parcel, zzcl);
                    break;
            }
        }
        if (parcel.dataPosition() == zzcm) {
            return new FirebaseCrashOptions(i, str2, str);
        }
        throw new zza.zza("Overread allowed size end=" + zzcm, parcel);
    }
}
