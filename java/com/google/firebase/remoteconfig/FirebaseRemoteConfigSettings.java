package com.google.firebase.remoteconfig;

public class FirebaseRemoteConfigSettings {
    private final boolean bbh;

    public static class Builder {
        private boolean bbh = false;

        public FirebaseRemoteConfigSettings build() {
            return new FirebaseRemoteConfigSettings();
        }

        public Builder setDeveloperModeEnabled(boolean z) {
            this.bbh = z;
            return this;
        }
    }

    private FirebaseRemoteConfigSettings(Builder builder) {
        this.bbh = builder.bbh;
    }

    public boolean isDeveloperModeEnabled() {
        return this.bbh;
    }
}
