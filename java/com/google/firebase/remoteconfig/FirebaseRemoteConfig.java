package com.google.firebase.remoteconfig;

import android.content.Context;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.XmlResourceParser;
import android.os.AsyncTask;
import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import android.util.Log;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.internal.zzals;
import com.google.android.gms.internal.zzalt;
import com.google.android.gms.internal.zzalu;
import com.google.android.gms.internal.zzalv;
import com.google.android.gms.internal.zzalw;
import com.google.android.gms.internal.zzalx;
import com.google.android.gms.internal.zzaly.zzb;
import com.google.android.gms.internal.zzaly.zzc;
import com.google.android.gms.internal.zzaly.zzd;
import com.google.android.gms.internal.zzaly.zze;
import com.google.android.gms.internal.zzaly.zzf;
import com.google.android.gms.internal.zzapn;
import com.google.android.gms.internal.zzrr;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskCompletionSource;
import com.google.firebase.FirebaseApp;
import com.google.firebase.analytics.FirebaseAnalytics.Param;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings.Builder;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.Executor;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class FirebaseRemoteConfig {
    public static final boolean DEFAULT_VALUE_FOR_BOOLEAN = false;
    public static final byte[] DEFAULT_VALUE_FOR_BYTE_ARRAY = new byte[LAST_FETCH_STATUS_NO_FETCH_YET];
    public static final double DEFAULT_VALUE_FOR_DOUBLE = 0.0d;
    public static final long DEFAULT_VALUE_FOR_LONG = 0;
    public static final String DEFAULT_VALUE_FOR_STRING = "";
    public static final int LAST_FETCH_STATUS_FAILURE = 1;
    public static final int LAST_FETCH_STATUS_NO_FETCH_YET = 0;
    public static final int LAST_FETCH_STATUS_SUCCESS = -1;
    public static final int LAST_FETCH_STATUS_THROTTLED = 2;
    public static final int VALUE_SOURCE_DEFAULT = 1;
    public static final int VALUE_SOURCE_REMOTE = 2;
    public static final int VALUE_SOURCE_STATIC = 0;
    private static FirebaseRemoteConfig baZ;
    private zzalu bba;
    private zzalu bbb;
    private zzalu bbc;
    private zzalx bbd;
    private final ReadWriteLock bbe;
    private final Context mContext;

    static class zza implements Executor {
        zza() {
        }

        public void execute(Runnable runnable) {
            new Thread(runnable).start();
        }
    }

    FirebaseRemoteConfig(Context context) {
        this(context, null, null, null, null);
    }

    private FirebaseRemoteConfig(Context context, zzalu com_google_android_gms_internal_zzalu, zzalu com_google_android_gms_internal_zzalu2, zzalu com_google_android_gms_internal_zzalu3, zzalx com_google_android_gms_internal_zzalx) {
        this.bbe = new ReentrantReadWriteLock(true);
        this.mContext = context;
        if (com_google_android_gms_internal_zzalx != null) {
            this.bbd = com_google_android_gms_internal_zzalx;
        } else {
            this.bbd = new zzalx();
        }
        this.bbd.zzcp(zzeu(this.mContext));
        if (com_google_android_gms_internal_zzalu != null) {
            this.bba = com_google_android_gms_internal_zzalu;
        }
        if (com_google_android_gms_internal_zzalu2 != null) {
            this.bbb = com_google_android_gms_internal_zzalu2;
        }
        if (com_google_android_gms_internal_zzalu3 != null) {
            this.bbc = com_google_android_gms_internal_zzalu3;
        }
    }

    public static FirebaseRemoteConfig getInstance() {
        if (baZ != null) {
            return baZ;
        }
        FirebaseApp instance = FirebaseApp.getInstance();
        if (instance != null) {
            return zzet(instance.getApplicationContext());
        }
        throw new IllegalStateException("FirebaseApp has not been initialized.");
    }

    private static zzalu zza(com.google.android.gms.internal.zzaly.zza com_google_android_gms_internal_zzaly_zza) {
        if (com_google_android_gms_internal_zzaly_zza == null) {
            return null;
        }
        Map hashMap = new HashMap();
        zzd[] com_google_android_gms_internal_zzaly_zzdArr = com_google_android_gms_internal_zzaly_zza.bbu;
        int length = com_google_android_gms_internal_zzaly_zzdArr.length;
        for (int i = LAST_FETCH_STATUS_NO_FETCH_YET; i < length; i += VALUE_SOURCE_DEFAULT) {
            zzd com_google_android_gms_internal_zzaly_zzd = com_google_android_gms_internal_zzaly_zzdArr[i];
            String str = com_google_android_gms_internal_zzaly_zzd.zx;
            Map hashMap2 = new HashMap();
            zzb[] com_google_android_gms_internal_zzaly_zzbArr = com_google_android_gms_internal_zzaly_zzd.bbA;
            int length2 = com_google_android_gms_internal_zzaly_zzbArr.length;
            for (int i2 = LAST_FETCH_STATUS_NO_FETCH_YET; i2 < length2; i2 += VALUE_SOURCE_DEFAULT) {
                zzb com_google_android_gms_internal_zzaly_zzb = com_google_android_gms_internal_zzaly_zzbArr[i2];
                hashMap2.put(com_google_android_gms_internal_zzaly_zzb.zzcb, com_google_android_gms_internal_zzaly_zzb.bbw);
            }
            hashMap.put(str, hashMap2);
        }
        return new zzalu(hashMap, com_google_android_gms_internal_zzaly_zza.timestamp);
    }

    private static zzalx zza(zzc com_google_android_gms_internal_zzaly_zzc) {
        if (com_google_android_gms_internal_zzaly_zzc == null) {
            return null;
        }
        zzalx com_google_android_gms_internal_zzalx = new zzalx();
        com_google_android_gms_internal_zzalx.zzafe(com_google_android_gms_internal_zzaly_zzc.bbx);
        com_google_android_gms_internal_zzalx.zzcw(com_google_android_gms_internal_zzaly_zzc.bby);
        return com_google_android_gms_internal_zzalx;
    }

    private static Map<String, zzals> zza(zzf[] com_google_android_gms_internal_zzaly_zzfArr) {
        Map hashMap = new HashMap();
        if (com_google_android_gms_internal_zzaly_zzfArr != null) {
            int length = com_google_android_gms_internal_zzaly_zzfArr.length;
            for (int i = LAST_FETCH_STATUS_NO_FETCH_YET; i < length; i += VALUE_SOURCE_DEFAULT) {
                zzf com_google_android_gms_internal_zzaly_zzf = com_google_android_gms_internal_zzaly_zzfArr[i];
                hashMap.put(com_google_android_gms_internal_zzaly_zzf.zx, new zzals(com_google_android_gms_internal_zzaly_zzf.resourceId, com_google_android_gms_internal_zzaly_zzf.bbH));
            }
        }
        return hashMap;
    }

    private static long zzb(InputStream inputStream, OutputStream outputStream) throws IOException {
        byte[] bArr = new byte[4096];
        long j = DEFAULT_VALUE_FOR_LONG;
        while (true) {
            int read = inputStream.read(bArr);
            if (read == LAST_FETCH_STATUS_SUCCESS) {
                return j;
            }
            outputStream.write(bArr, LAST_FETCH_STATUS_NO_FETCH_YET, read);
            j += (long) read;
        }
    }

    private void zzc(Map<String, Object> map, String str, boolean z) {
        if (str != null) {
            Object obj = (map == null || map.isEmpty()) ? VALUE_SOURCE_DEFAULT : LAST_FETCH_STATUS_NO_FETCH_YET;
            Map hashMap = new HashMap();
            if (obj == null) {
                for (String str2 : map.keySet()) {
                    Object obj2 = map.get(str2);
                    if (obj2 instanceof String) {
                        hashMap.put(str2, ((String) obj2).getBytes(zzalw.UTF_8));
                    } else if (obj2 instanceof Long) {
                        hashMap.put(str2, ((Long) obj2).toString().getBytes(zzalw.UTF_8));
                    } else if (obj2 instanceof Integer) {
                        hashMap.put(str2, ((Integer) obj2).toString().getBytes(zzalw.UTF_8));
                    } else if (obj2 instanceof Double) {
                        hashMap.put(str2, ((Double) obj2).toString().getBytes(zzalw.UTF_8));
                    } else if (obj2 instanceof Float) {
                        hashMap.put(str2, ((Float) obj2).toString().getBytes(zzalw.UTF_8));
                    } else if (obj2 instanceof byte[]) {
                        hashMap.put(str2, (byte[]) obj2);
                    } else if (obj2 instanceof Boolean) {
                        hashMap.put(str2, ((Boolean) obj2).toString().getBytes(zzalw.UTF_8));
                    } else {
                        throw new IllegalArgumentException("The type of a default value needs to beone of String, Long, Double, Boolean, or byte[].");
                    }
                }
            }
            this.bbe.writeLock().lock();
            if (obj != null) {
                try {
                    if (this.bbc == null || !this.bbc.zztc(str)) {
                        this.bbe.writeLock().unlock();
                        return;
                    } else {
                        this.bbc.zzk(null, str);
                        this.bbc.setTimestamp(System.currentTimeMillis());
                    }
                } catch (Throwable th) {
                    this.bbe.writeLock().unlock();
                }
            } else {
                if (this.bbc == null) {
                    this.bbc = new zzalu(new HashMap(), System.currentTimeMillis());
                }
                this.bbc.zzk(hashMap, str);
                this.bbc.setTimestamp(System.currentTimeMillis());
            }
            if (z) {
                this.bbd.zztd(str);
            }
            zzcxe();
            this.bbe.writeLock().unlock();
        }
    }

    private void zzcxe() {
        this.bbe.readLock().lock();
        try {
            zzalt com_google_android_gms_internal_zzalt = new zzalt(this.mContext, this.bba, this.bbb, this.bbc, this.bbd);
            if (VERSION.SDK_INT >= 11) {
                AsyncTask.SERIAL_EXECUTOR.execute(com_google_android_gms_internal_zzalt);
            } else {
                new zza().execute(com_google_android_gms_internal_zzalt);
            }
            this.bbe.readLock().unlock();
        } catch (Throwable th) {
            this.bbe.readLock().unlock();
        }
    }

    public static FirebaseRemoteConfig zzet(Context context) {
        if (baZ == null) {
            zze zzev = zzev(context);
            if (zzev == null) {
                if (Log.isLoggable("FirebaseRemoteConfig", 3)) {
                    Log.d("FirebaseRemoteConfig", "No persisted config was found. Initializing from scratch.");
                }
                baZ = new FirebaseRemoteConfig(context);
            } else {
                if (Log.isLoggable("FirebaseRemoteConfig", 3)) {
                    Log.d("FirebaseRemoteConfig", "Initializing from persisted config.");
                }
                zzalu zza = zza(zzev.bbB);
                zzalu zza2 = zza(zzev.bbC);
                zzalu zza3 = zza(zzev.bbD);
                zzalx zza4 = zza(zzev.bbE);
                if (zza4 != null) {
                    zza4.zzcd(zza(zzev.bbF));
                }
                baZ = new FirebaseRemoteConfig(context, zza, zza2, zza3, zza4);
            }
        }
        return baZ;
    }

    private long zzeu(Context context) {
        long j = DEFAULT_VALUE_FOR_LONG;
        try {
            return this.mContext.getPackageManager().getPackageInfo(context.getPackageName(), LAST_FETCH_STATUS_NO_FETCH_YET).lastUpdateTime;
        } catch (NameNotFoundException e) {
            String valueOf = String.valueOf(context.getPackageName());
            Log.e("FirebaseRemoteConfig", new StringBuilder(String.valueOf(valueOf).length() + 25).append("Package [").append(valueOf).append("] was not found!").toString());
            return j;
        }
    }

    private static zze zzev(Context context) {
        FileInputStream openFileInput;
        Throwable e;
        FileInputStream fileInputStream;
        if (context == null) {
            return null;
        }
        try {
            openFileInput = context.openFileInput("persisted_config");
            try {
                zzapn zzbd = zzapn.zzbd(zzl(openFileInput));
                zze com_google_android_gms_internal_zzaly_zze = new zze();
                zze com_google_android_gms_internal_zzaly_zze2 = (zze) com_google_android_gms_internal_zzaly_zze.zzb(zzbd);
                if (openFileInput != null) {
                    try {
                        openFileInput.close();
                    } catch (Throwable e2) {
                        Log.e("FirebaseRemoteConfig", "Failed to close persisted config file.", e2);
                    }
                }
                return com_google_android_gms_internal_zzaly_zze;
            } catch (FileNotFoundException e3) {
                e2 = e3;
                fileInputStream = openFileInput;
                try {
                    if (Log.isLoggable("FirebaseRemoteConfig", 3)) {
                        Log.d("FirebaseRemoteConfig", "Persisted config file was not found.", e2);
                    }
                    if (fileInputStream != null) {
                        try {
                            fileInputStream.close();
                        } catch (Throwable e22) {
                            Log.e("FirebaseRemoteConfig", "Failed to close persisted config file.", e22);
                        }
                    }
                    return null;
                } catch (Throwable th) {
                    e22 = th;
                    openFileInput = fileInputStream;
                    if (openFileInput != null) {
                        try {
                            openFileInput.close();
                        } catch (Throwable e4) {
                            Log.e("FirebaseRemoteConfig", "Failed to close persisted config file.", e4);
                        }
                    }
                    throw e22;
                }
            } catch (IOException e5) {
                e22 = e5;
                try {
                    Log.e("FirebaseRemoteConfig", "Cannot initialize from persisted config.", e22);
                    if (openFileInput != null) {
                        try {
                            openFileInput.close();
                        } catch (Throwable e222) {
                            Log.e("FirebaseRemoteConfig", "Failed to close persisted config file.", e222);
                        }
                    }
                    return null;
                } catch (Throwable th2) {
                    e222 = th2;
                    if (openFileInput != null) {
                        openFileInput.close();
                    }
                    throw e222;
                }
            }
        } catch (FileNotFoundException e6) {
            e222 = e6;
            fileInputStream = null;
            if (Log.isLoggable("FirebaseRemoteConfig", 3)) {
                Log.d("FirebaseRemoteConfig", "Persisted config file was not found.", e222);
            }
            if (fileInputStream != null) {
                fileInputStream.close();
            }
            return null;
        } catch (IOException e7) {
            e222 = e7;
            openFileInput = null;
            Log.e("FirebaseRemoteConfig", "Cannot initialize from persisted config.", e222);
            if (openFileInput != null) {
                openFileInput.close();
            }
            return null;
        } catch (Throwable th3) {
            e222 = th3;
            openFileInput = null;
            if (openFileInput != null) {
                openFileInput.close();
            }
            throw e222;
        }
    }

    private static byte[] zzl(InputStream inputStream) throws IOException {
        OutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        zzb(inputStream, byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }

    public boolean activateFetched() {
        this.bbe.writeLock().lock();
        try {
            if (this.bba == null) {
                return DEFAULT_VALUE_FOR_BOOLEAN;
            }
            if (this.bbb == null || this.bbb.getTimestamp() < this.bba.getTimestamp()) {
                long timestamp = this.bba.getTimestamp();
                this.bbb = this.bba;
                this.bbb.setTimestamp(System.currentTimeMillis());
                this.bba = new zzalu(null, timestamp);
                zzcxe();
                this.bbe.writeLock().unlock();
                return true;
            }
            this.bbe.writeLock().unlock();
            return DEFAULT_VALUE_FOR_BOOLEAN;
        } finally {
            this.bbe.writeLock().unlock();
        }
    }

    public Task<Void> fetch() {
        return fetch(43200);
    }

    public Task<Void> fetch(long j) {
        final TaskCompletionSource taskCompletionSource = new TaskCompletionSource();
        this.bbe.writeLock().lock();
        try {
            com.google.android.gms.internal.zzrr.zza.zza com_google_android_gms_internal_zzrr_zza_zza = new com.google.android.gms.internal.zzrr.zza.zza();
            com_google_android_gms_internal_zzrr_zza_zza.zzah(j);
            if (this.bbd.isDeveloperModeEnabled()) {
                com_google_android_gms_internal_zzrr_zza_zza.zzah("_rcn_developer", "true");
            }
            new com.google.android.gms.config.internal.zzb(this.mContext).zza(com_google_android_gms_internal_zzrr_zza_zza.zzawj()).setResultCallback(new ResultCallback<zzrr.zzb>(this) {
                final /* synthetic */ FirebaseRemoteConfig bbg;

                public /* synthetic */ void onResult(@NonNull Result result) {
                    zza((zzrr.zzb) result);
                }

                public void zza(@NonNull zzrr.zzb com_google_android_gms_internal_zzrr_zzb) {
                    this.bbg.zza(taskCompletionSource, com_google_android_gms_internal_zzrr_zzb);
                }
            });
            return taskCompletionSource.getTask();
        } finally {
            this.bbe.writeLock().unlock();
        }
    }

    public boolean getBoolean(String str) {
        return getBoolean(str, "configns:firebase");
    }

    public boolean getBoolean(String str, String str2) {
        Lock lock = true;
        if (str2 == null) {
            return DEFAULT_VALUE_FOR_BOOLEAN;
        }
        this.bbe.readLock().lock();
        try {
            CharSequence str3;
            if (this.bbb != null && this.bbb.zzbt(str, str2)) {
                str3 = new String(this.bbb.zzbu(str, str2), zzalw.UTF_8);
                if (zzalw.Bz.matcher(str3).matches()) {
                    return lock;
                }
                if (zzalw.BA.matcher(str3).matches()) {
                    this.bbe.readLock().unlock();
                    return DEFAULT_VALUE_FOR_BOOLEAN;
                }
            }
            if (this.bbc != null && this.bbc.zzbt(str, str2)) {
                str3 = new String(this.bbc.zzbu(str, str2), zzalw.UTF_8);
                if (zzalw.Bz.matcher(str3).matches()) {
                    this.bbe.readLock().unlock();
                    return true;
                } else if (zzalw.BA.matcher(str3).matches()) {
                    this.bbe.readLock().unlock();
                    return DEFAULT_VALUE_FOR_BOOLEAN;
                }
            }
            this.bbe.readLock().unlock();
            return DEFAULT_VALUE_FOR_BOOLEAN;
        } finally {
            lock = this.bbe.readLock();
            lock.unlock();
        }
    }

    public byte[] getByteArray(String str) {
        return getByteArray(str, "configns:firebase");
    }

    public byte[] getByteArray(String str, String str2) {
        if (str2 == null) {
            return DEFAULT_VALUE_FOR_BYTE_ARRAY;
        }
        this.bbe.readLock().lock();
        byte[] bArr;
        if (this.bbb == null || !this.bbb.zzbt(str, str2)) {
            try {
                if (this.bbc == null || !this.bbc.zzbt(str, str2)) {
                    bArr = DEFAULT_VALUE_FOR_BYTE_ARRAY;
                    this.bbe.readLock().unlock();
                    return bArr;
                }
                bArr = this.bbc.zzbu(str, str2);
                this.bbe.readLock().unlock();
                return bArr;
            } finally {
                this.bbe.readLock().unlock();
            }
        } else {
            bArr = this.bbb.zzbu(str, str2);
            return bArr;
        }
    }

    public double getDouble(String str) {
        return getDouble(str, "configns:firebase");
    }

    public double getDouble(String str, String str2) {
        double d = DEFAULT_VALUE_FOR_DOUBLE;
        if (str2 != null) {
            this.bbe.readLock().lock();
            try {
                if (this.bbb != null && this.bbb.zzbt(str, str2)) {
                    try {
                        d = Double.valueOf(new String(this.bbb.zzbu(str, str2), zzalw.UTF_8)).doubleValue();
                    } catch (NumberFormatException e) {
                    }
                }
                if (this.bbc != null && this.bbc.zzbt(str, str2)) {
                    try {
                        d = Double.valueOf(new String(this.bbc.zzbu(str, str2), zzalw.UTF_8)).doubleValue();
                        this.bbe.readLock().unlock();
                    } catch (NumberFormatException e2) {
                    }
                }
                this.bbe.readLock().unlock();
            } finally {
                this.bbe.readLock().unlock();
            }
        }
        return d;
    }

    public FirebaseRemoteConfigInfo getInfo() {
        zzalv com_google_android_gms_internal_zzalv = new zzalv();
        this.bbe.readLock().lock();
        try {
            com_google_android_gms_internal_zzalv.zzco(this.bba == null ? -1 : this.bba.getTimestamp());
            com_google_android_gms_internal_zzalv.zzafe(this.bbd.getLastFetchStatus());
            com_google_android_gms_internal_zzalv.setConfigSettings(new Builder().setDeveloperModeEnabled(this.bbd.isDeveloperModeEnabled()).build());
            return com_google_android_gms_internal_zzalv;
        } finally {
            this.bbe.readLock().unlock();
        }
    }

    public Set<String> getKeysByPrefix(String str) {
        return getKeysByPrefix(str, "configns:firebase");
    }

    public Set<String> getKeysByPrefix(String str, String str2) {
        this.bbe.readLock().lock();
        try {
            Set<String> treeSet;
            if (this.bbb == null) {
                treeSet = new TreeSet();
            } else {
                treeSet = this.bbb.zzbv(str, str2);
                this.bbe.readLock().unlock();
            }
            return treeSet;
        } finally {
            this.bbe.readLock().unlock();
        }
    }

    public long getLong(String str) {
        return getLong(str, "configns:firebase");
    }

    public long getLong(String str, String str2) {
        long j = DEFAULT_VALUE_FOR_LONG;
        if (str2 != null) {
            this.bbe.readLock().lock();
            try {
                if (this.bbb != null && this.bbb.zzbt(str, str2)) {
                    try {
                        j = Long.valueOf(new String(this.bbb.zzbu(str, str2), zzalw.UTF_8)).longValue();
                    } catch (NumberFormatException e) {
                    }
                }
                if (this.bbc != null && this.bbc.zzbt(str, str2)) {
                    try {
                        j = Long.valueOf(new String(this.bbc.zzbu(str, str2), zzalw.UTF_8)).longValue();
                        this.bbe.readLock().unlock();
                    } catch (NumberFormatException e2) {
                    }
                }
                this.bbe.readLock().unlock();
            } finally {
                this.bbe.readLock().unlock();
            }
        }
        return j;
    }

    public String getString(String str) {
        return getString(str, "configns:firebase");
    }

    public String getString(String str, String str2) {
        if (str2 == null) {
            return DEFAULT_VALUE_FOR_STRING;
        }
        this.bbe.readLock().lock();
        String str3;
        if (this.bbb == null || !this.bbb.zzbt(str, str2)) {
            try {
                if (this.bbc == null || !this.bbc.zzbt(str, str2)) {
                    str3 = DEFAULT_VALUE_FOR_STRING;
                    this.bbe.readLock().unlock();
                    return str3;
                }
                str3 = new String(this.bbc.zzbu(str, str2), zzalw.UTF_8);
                this.bbe.readLock().unlock();
                return str3;
            } finally {
                this.bbe.readLock().unlock();
            }
        } else {
            str3 = new String(this.bbb.zzbu(str, str2), zzalw.UTF_8);
            return str3;
        }
    }

    public FirebaseRemoteConfigValue getValue(String str) {
        return getValue(str, "configns:firebase");
    }

    public FirebaseRemoteConfigValue getValue(String str, String str2) {
        if (str2 == null) {
            return new zzalw(DEFAULT_VALUE_FOR_BYTE_ARRAY, LAST_FETCH_STATUS_NO_FETCH_YET);
        }
        this.bbe.readLock().lock();
        if (this.bbb == null || !this.bbb.zzbt(str, str2)) {
            try {
                zzalw com_google_android_gms_internal_zzalw;
                if (this.bbc == null || !this.bbc.zzbt(str, str2)) {
                    com_google_android_gms_internal_zzalw = new zzalw(DEFAULT_VALUE_FOR_BYTE_ARRAY, LAST_FETCH_STATUS_NO_FETCH_YET);
                    this.bbe.readLock().unlock();
                    return com_google_android_gms_internal_zzalw;
                }
                com_google_android_gms_internal_zzalw = new zzalw(this.bbc.zzbu(str, str2), VALUE_SOURCE_DEFAULT);
                this.bbe.readLock().unlock();
                return com_google_android_gms_internal_zzalw;
            } finally {
                this.bbe.readLock().unlock();
            }
        } else {
            FirebaseRemoteConfigValue com_google_android_gms_internal_zzalw2 = new zzalw(this.bbb.zzbu(str, str2), VALUE_SOURCE_REMOTE);
            return com_google_android_gms_internal_zzalw2;
        }
    }

    public void setConfigSettings(FirebaseRemoteConfigSettings firebaseRemoteConfigSettings) {
        this.bbe.writeLock().lock();
        try {
            boolean isDeveloperModeEnabled = this.bbd.isDeveloperModeEnabled();
            boolean isDeveloperModeEnabled2 = firebaseRemoteConfigSettings == null ? DEFAULT_VALUE_FOR_BOOLEAN : firebaseRemoteConfigSettings.isDeveloperModeEnabled();
            this.bbd.zzcw(isDeveloperModeEnabled2);
            if (isDeveloperModeEnabled != isDeveloperModeEnabled2) {
                zzcxe();
            }
            this.bbe.writeLock().unlock();
        } catch (Throwable th) {
            this.bbe.writeLock().unlock();
        }
    }

    public void setDefaults(int i) {
        setDefaults(i, "configns:firebase");
    }

    public void setDefaults(int i, String str) {
        if (str != null) {
            this.bbe.readLock().lock();
            try {
                if (!(this.bbd == null || this.bbd.zzcxk() == null || this.bbd.zzcxk().get(str) == null)) {
                    zzals com_google_android_gms_internal_zzals = (zzals) this.bbd.zzcxk().get(str);
                    if (i == com_google_android_gms_internal_zzals.zzcxf() && this.bbd.zzcxl() == com_google_android_gms_internal_zzals.zzcxg()) {
                        if (Log.isLoggable("FirebaseRemoteConfig", 3)) {
                            Log.d("FirebaseRemoteConfig", "Skipped setting defaults from resource file as this resource file was already applied.");
                        }
                        this.bbe.readLock().unlock();
                        return;
                    }
                }
                this.bbe.readLock().unlock();
                Map hashMap = new HashMap();
                try {
                    XmlResourceParser xml = this.mContext.getResources().getXml(i);
                    Object obj = null;
                    Object obj2 = null;
                    Object obj3 = null;
                    for (int eventType = xml.getEventType(); eventType != VALUE_SOURCE_DEFAULT; eventType = xml.next()) {
                        if (eventType == VALUE_SOURCE_REMOTE) {
                            obj2 = xml.getName();
                        } else if (eventType == 3) {
                            if (!(!"entry".equals(xml.getName()) || obj == null || obj3 == null)) {
                                hashMap.put(obj, obj3);
                                obj3 = null;
                                obj = null;
                            }
                            obj2 = null;
                        } else if (eventType == 4) {
                            if ("key".equals(obj2)) {
                                obj = xml.getText();
                            } else if (Param.VALUE.equals(obj2)) {
                                obj3 = xml.getText();
                            }
                        }
                    }
                    this.bbd.zza(str, new zzals(i, this.bbd.zzcxl()));
                    zzc(hashMap, str, DEFAULT_VALUE_FOR_BOOLEAN);
                } catch (Throwable e) {
                    Log.e("FirebaseRemoteConfig", "Caught exception while parsing XML resource. Skipping setDefaults.", e);
                }
            } catch (Throwable th) {
                this.bbe.readLock().unlock();
            }
        } else if (Log.isLoggable("FirebaseRemoteConfig", 3)) {
            Log.d("FirebaseRemoteConfig", "namespace cannot be null for setDefaults.");
        }
    }

    public void setDefaults(Map<String, Object> map) {
        setDefaults((Map) map, "configns:firebase");
    }

    public void setDefaults(Map<String, Object> map, String str) {
        zzc(map, str, true);
    }

    @VisibleForTesting
    void zza(TaskCompletionSource<Void> taskCompletionSource, zzrr.zzb com_google_android_gms_internal_zzrr_zzb) {
        if (com_google_android_gms_internal_zzrr_zzb == null || com_google_android_gms_internal_zzrr_zzb.getStatus() == null) {
            this.bbd.zzafe(VALUE_SOURCE_DEFAULT);
            taskCompletionSource.setException(new FirebaseRemoteConfigFetchException());
            zzcxe();
            return;
        }
        int statusCode = com_google_android_gms_internal_zzrr_zzb.getStatus().getStatusCode();
        Map zzawk;
        Map hashMap;
        Map hashMap2;
        switch (statusCode) {
            case -6508:
            case -6506:
                this.bbd.zzafe(LAST_FETCH_STATUS_SUCCESS);
                if (!(this.bba == null || this.bba.zzcxi())) {
                    zzawk = com_google_android_gms_internal_zzrr_zzb.zzawk();
                    hashMap = new HashMap();
                    for (String str : zzawk.keySet()) {
                        hashMap2 = new HashMap();
                        for (String str2 : (Set) zzawk.get(str)) {
                            hashMap2.put(str2, com_google_android_gms_internal_zzrr_zzb.zza(str2, null, str));
                        }
                        hashMap.put(str, hashMap2);
                    }
                    this.bba = new zzalu(hashMap, this.bba.getTimestamp());
                }
                taskCompletionSource.setResult(null);
                zzcxe();
                return;
            case -6505:
                zzawk = com_google_android_gms_internal_zzrr_zzb.zzawk();
                hashMap = new HashMap();
                for (String str3 : zzawk.keySet()) {
                    hashMap2 = new HashMap();
                    for (String str22 : (Set) zzawk.get(str3)) {
                        hashMap2.put(str22, com_google_android_gms_internal_zzrr_zzb.zza(str22, null, str3));
                    }
                    hashMap.put(str3, hashMap2);
                }
                this.bba = new zzalu(hashMap, System.currentTimeMillis());
                this.bbd.zzafe(LAST_FETCH_STATUS_SUCCESS);
                taskCompletionSource.setResult(null);
                zzcxe();
                return;
            case 6500:
            case 6501:
            case 6503:
            case 6504:
                this.bbd.zzafe(VALUE_SOURCE_DEFAULT);
                taskCompletionSource.setException(new FirebaseRemoteConfigFetchException());
                zzcxe();
                return;
            case 6502:
            case 6507:
                this.bbd.zzafe(VALUE_SOURCE_REMOTE);
                taskCompletionSource.setException(new FirebaseRemoteConfigFetchThrottledException(com_google_android_gms_internal_zzrr_zzb.getThrottleEndTimeMillis()));
                zzcxe();
                return;
            default:
                if (com_google_android_gms_internal_zzrr_zzb.getStatus().isSuccess()) {
                    Log.w("FirebaseRemoteConfig", "Unknown (successful) status code: " + statusCode);
                }
                this.bbd.zzafe(VALUE_SOURCE_DEFAULT);
                taskCompletionSource.setException(new FirebaseRemoteConfigFetchException());
                zzcxe();
                return;
        }
    }
}
