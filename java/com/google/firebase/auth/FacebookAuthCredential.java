package com.google.firebase.auth;

import android.support.annotation.NonNull;
import com.google.android.gms.common.internal.zzab;
import com.google.firebase.auth.api.model.VerifyAssertionRequest;

public class FacebookAuthCredential extends AuthCredential {
    private final String atD;

    FacebookAuthCredential(@NonNull String str) {
        this.atD = zzab.zzhr(str);
    }

    public static VerifyAssertionRequest zza(@NonNull FacebookAuthCredential facebookAuthCredential) {
        zzab.zzy(facebookAuthCredential);
        return new VerifyAssertionRequest(null, facebookAuthCredential.getAccessToken(), facebookAuthCredential.getProvider(), null, null);
    }

    public String getAccessToken() {
        return this.atD;
    }

    public String getProvider() {
        return FacebookAuthProvider.PROVIDER_ID;
    }
}
