package com.google.firebase.auth.api.model;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import outlander.showcaseview.R;

public class zzh implements Creator<VerifyAssertionRequest> {
    static void zza(VerifyAssertionRequest verifyAssertionRequest, Parcel parcel, int i) {
        int zzcn = zzb.zzcn(parcel);
        zzb.zzc(parcel, 1, verifyAssertionRequest.mVersionCode);
        zzb.zza(parcel, 2, verifyAssertionRequest.zzclv(), false);
        zzb.zza(parcel, 3, verifyAssertionRequest.zzclw(), false);
        zzb.zza(parcel, 4, verifyAssertionRequest.getIdToken(), false);
        zzb.zza(parcel, 5, verifyAssertionRequest.getAccessToken(), false);
        zzb.zza(parcel, 6, verifyAssertionRequest.getProviderId(), false);
        zzb.zza(parcel, 7, verifyAssertionRequest.getEmail(), false);
        zzb.zza(parcel, 8, verifyAssertionRequest.zzlh(), false);
        zzb.zza(parcel, 9, verifyAssertionRequest.zzclx(), false);
        zzb.zza(parcel, 10, verifyAssertionRequest.zzcly());
        zzb.zzaj(parcel, zzcn);
    }

    public /* synthetic */ Object createFromParcel(Parcel parcel) {
        return zzwb(parcel);
    }

    public /* synthetic */ Object[] newArray(int i) {
        return zzaep(i);
    }

    public VerifyAssertionRequest[] zzaep(int i) {
        return new VerifyAssertionRequest[i];
    }

    public VerifyAssertionRequest zzwb(Parcel parcel) {
        boolean z = false;
        String str = null;
        int zzcm = zza.zzcm(parcel);
        String str2 = null;
        String str3 = null;
        String str4 = null;
        String str5 = null;
        String str6 = null;
        String str7 = null;
        String str8 = null;
        int i = 0;
        while (parcel.dataPosition() < zzcm) {
            int zzcl = zza.zzcl(parcel);
            switch (zza.zzgm(zzcl)) {
                case R.styleable.View_android_focusable /*1*/:
                    i = zza.zzg(parcel, zzcl);
                    break;
                case R.styleable.View_paddingStart /*2*/:
                    str8 = zza.zzq(parcel, zzcl);
                    break;
                case R.styleable.View_paddingEnd /*3*/:
                    str7 = zza.zzq(parcel, zzcl);
                    break;
                case R.styleable.View_theme /*4*/:
                    str6 = zza.zzq(parcel, zzcl);
                    break;
                case R.styleable.Toolbar_contentInsetStart /*5*/:
                    str5 = zza.zzq(parcel, zzcl);
                    break;
                case R.styleable.Toolbar_contentInsetEnd /*6*/:
                    str4 = zza.zzq(parcel, zzcl);
                    break;
                case R.styleable.Toolbar_contentInsetLeft /*7*/:
                    str3 = zza.zzq(parcel, zzcl);
                    break;
                case R.styleable.Toolbar_contentInsetRight /*8*/:
                    str2 = zza.zzq(parcel, zzcl);
                    break;
                case R.styleable.Toolbar_popupTheme /*9*/:
                    str = zza.zzq(parcel, zzcl);
                    break;
                case R.styleable.Toolbar_titleTextAppearance /*10*/:
                    z = zza.zzc(parcel, zzcl);
                    break;
                default:
                    zza.zzb(parcel, zzcl);
                    break;
            }
        }
        if (parcel.dataPosition() == zzcm) {
            return new VerifyAssertionRequest(i, str8, str7, str6, str5, str4, str3, str2, str, z);
        }
        throw new zza.zza("Overread allowed size end=" + zzcm, parcel);
    }
}
