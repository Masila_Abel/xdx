package com.google.firebase.auth.api.model;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.zzab;
import com.google.android.gms.internal.zzafj;
import com.google.android.gms.internal.zzank;

public class VerifyAssertionRequest extends AbstractSafeParcelable {
    public static final Creator<VerifyAssertionRequest> CREATOR = new zzh();
    @zzafj
    private String aNH;
    @zzank("requestUri")
    private String aNT;
    @zzank("idToken")
    private String aNU;
    @zzank("oauthTokenSecret")
    private String aNV;
    @zzank("returnSecureToken")
    private boolean aNW;
    @zzafj
    private String atD;
    @zzafj
    private String cY;
    @zzafj
    @Nullable
    private String dB;
    @zzafj
    public final int mVersionCode;
    @zzank("postBody")
    private String zzbil;

    public VerifyAssertionRequest() {
        this.mVersionCode = 2;
        this.aNW = true;
    }

    VerifyAssertionRequest(int i, String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, boolean z) {
        this.mVersionCode = i;
        this.aNT = str;
        this.aNU = str2;
        this.cY = str3;
        this.atD = str4;
        this.aNH = str5;
        this.dB = str6;
        this.zzbil = str7;
        this.aNV = str8;
        this.aNW = z;
    }

    public VerifyAssertionRequest(@Nullable String str, @Nullable String str2, String str3, @Nullable String str4, @Nullable String str5) {
        this.mVersionCode = 2;
        this.aNT = "http://localhost";
        this.cY = str;
        this.atD = str2;
        this.aNV = str5;
        this.aNW = true;
        if (TextUtils.isEmpty(this.cY) && TextUtils.isEmpty(this.atD)) {
            throw new IllegalArgumentException("Both idToken, and accessToken cannot be null");
        }
        this.aNH = zzab.zzhr(str3);
        this.dB = str4;
        StringBuilder stringBuilder = new StringBuilder();
        if (!TextUtils.isEmpty(this.cY)) {
            stringBuilder.append("id_token").append("=").append(this.cY).append("&");
        }
        if (!TextUtils.isEmpty(this.atD)) {
            stringBuilder.append("access_token").append("=").append(this.atD).append("&");
        }
        if (!TextUtils.isEmpty(this.dB)) {
            stringBuilder.append("identifier").append("=").append(this.dB).append("&");
        }
        if (!TextUtils.isEmpty(this.aNV)) {
            stringBuilder.append("oauth_token_secret").append("=").append(this.aNV).append("&");
        }
        stringBuilder.append("providerId").append("=").append(this.aNH);
        this.zzbil = stringBuilder.toString();
    }

    public String getAccessToken() {
        return this.atD;
    }

    @Nullable
    public String getEmail() {
        return this.dB;
    }

    public String getIdToken() {
        return this.cY;
    }

    public String getProviderId() {
        return this.aNH;
    }

    public void writeToParcel(Parcel parcel, int i) {
        zzh.zza(this, parcel, i);
    }

    public String zzclv() {
        return this.aNT;
    }

    public String zzclw() {
        return this.aNU;
    }

    public String zzclx() {
        return this.aNV;
    }

    public boolean zzcly() {
        return this.aNW;
    }

    public String zzlh() {
        return this.zzbil;
    }
}
