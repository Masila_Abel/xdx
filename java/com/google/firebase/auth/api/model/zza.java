package com.google.firebase.auth.api.model;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zzb;
import outlander.showcaseview.R;

public class zza implements Creator<CreateAuthUriResponse> {
    static void zza(CreateAuthUriResponse createAuthUriResponse, Parcel parcel, int i) {
        int zzcn = zzb.zzcn(parcel);
        zzb.zzc(parcel, 1, createAuthUriResponse.mVersionCode);
        zzb.zza(parcel, 2, createAuthUriResponse.zzclg(), false);
        zzb.zza(parcel, 3, createAuthUriResponse.isRegistered());
        zzb.zza(parcel, 4, createAuthUriResponse.getProviderId(), false);
        zzb.zza(parcel, 5, createAuthUriResponse.zzclh());
        zzb.zza(parcel, 6, createAuthUriResponse.zzcli(), i, false);
        zzb.zzaj(parcel, zzcn);
    }

    public /* synthetic */ Object createFromParcel(Parcel parcel) {
        return zzvu(parcel);
    }

    public /* synthetic */ Object[] newArray(int i) {
        return zzaei(i);
    }

    public CreateAuthUriResponse[] zzaei(int i) {
        return new CreateAuthUriResponse[i];
    }

    public CreateAuthUriResponse zzvu(Parcel parcel) {
        StringList stringList = null;
        boolean z = false;
        int zzcm = com.google.android.gms.common.internal.safeparcel.zza.zzcm(parcel);
        String str = null;
        boolean z2 = false;
        String str2 = null;
        int i = 0;
        while (parcel.dataPosition() < zzcm) {
            int zzcl = com.google.android.gms.common.internal.safeparcel.zza.zzcl(parcel);
            switch (com.google.android.gms.common.internal.safeparcel.zza.zzgm(zzcl)) {
                case R.styleable.View_android_focusable /*1*/:
                    i = com.google.android.gms.common.internal.safeparcel.zza.zzg(parcel, zzcl);
                    break;
                case R.styleable.View_paddingStart /*2*/:
                    str2 = com.google.android.gms.common.internal.safeparcel.zza.zzq(parcel, zzcl);
                    break;
                case R.styleable.View_paddingEnd /*3*/:
                    z2 = com.google.android.gms.common.internal.safeparcel.zza.zzc(parcel, zzcl);
                    break;
                case R.styleable.View_theme /*4*/:
                    str = com.google.android.gms.common.internal.safeparcel.zza.zzq(parcel, zzcl);
                    break;
                case R.styleable.Toolbar_contentInsetStart /*5*/:
                    z = com.google.android.gms.common.internal.safeparcel.zza.zzc(parcel, zzcl);
                    break;
                case R.styleable.Toolbar_contentInsetEnd /*6*/:
                    stringList = (StringList) com.google.android.gms.common.internal.safeparcel.zza.zza(parcel, zzcl, StringList.CREATOR);
                    break;
                default:
                    com.google.android.gms.common.internal.safeparcel.zza.zzb(parcel, zzcl);
                    break;
            }
        }
        if (parcel.dataPosition() == zzcm) {
            return new CreateAuthUriResponse(i, str2, z2, str, z, stringList);
        }
        throw new com.google.android.gms.common.internal.safeparcel.zza.zza("Overread allowed size end=" + zzcm, parcel);
    }
}
