package com.google.firebase.auth.api.model;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import java.util.List;
import outlander.showcaseview.R;

public class zzf implements Creator<ProviderUserInfoList> {
    static void zza(ProviderUserInfoList providerUserInfoList, Parcel parcel, int i) {
        int zzcn = zzb.zzcn(parcel);
        zzb.zzc(parcel, 1, providerUserInfoList.mVersionCode);
        zzb.zzc(parcel, 2, providerUserInfoList.zzclk(), false);
        zzb.zzaj(parcel, zzcn);
    }

    public /* synthetic */ Object createFromParcel(Parcel parcel) {
        return zzvz(parcel);
    }

    public /* synthetic */ Object[] newArray(int i) {
        return zzaen(i);
    }

    public ProviderUserInfoList[] zzaen(int i) {
        return new ProviderUserInfoList[i];
    }

    public ProviderUserInfoList zzvz(Parcel parcel) {
        int zzcm = zza.zzcm(parcel);
        int i = 0;
        List list = null;
        while (parcel.dataPosition() < zzcm) {
            int zzcl = zza.zzcl(parcel);
            switch (zza.zzgm(zzcl)) {
                case R.styleable.View_android_focusable /*1*/:
                    i = zza.zzg(parcel, zzcl);
                    break;
                case R.styleable.View_paddingStart /*2*/:
                    list = zza.zzc(parcel, zzcl, ProviderUserInfo.CREATOR);
                    break;
                default:
                    zza.zzb(parcel, zzcl);
                    break;
            }
        }
        if (parcel.dataPosition() == zzcm) {
            return new ProviderUserInfoList(i, list);
        }
        throw new zza.zza("Overread allowed size end=" + zzcm, parcel);
    }
}
