package com.google.firebase.auth.api.model;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.support.annotation.Nullable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.internal.zzafj;
import com.google.android.gms.internal.zzank;
import java.util.List;

public class CreateAuthUriResponse extends AbstractSafeParcelable {
    public static final Creator<CreateAuthUriResponse> CREATOR = new zza();
    @zzank("authUri")
    private String aNG;
    @zzank("providerId")
    private String aNH;
    @zzank("forExistingProvider")
    private boolean aNI;
    @zzank("allProviders")
    private StringList aNJ;
    @zzank("registered")
    private boolean aa;
    @zzafj
    public final int mVersionCode;

    public CreateAuthUriResponse() {
        this.mVersionCode = 1;
        this.aNJ = StringList.zzclu();
    }

    CreateAuthUriResponse(int i, String str, boolean z, String str2, boolean z2, StringList stringList) {
        this.mVersionCode = i;
        this.aNG = str;
        this.aa = z;
        this.aNH = str2;
        this.aNI = z2;
        this.aNJ = stringList == null ? StringList.zzclu() : StringList.zza(stringList);
    }

    @Nullable
    public List<String> getAllProviders() {
        return this.aNJ.zzclt();
    }

    @Nullable
    public String getProviderId() {
        return this.aNH;
    }

    public boolean isRegistered() {
        return this.aa;
    }

    public void writeToParcel(Parcel parcel, int i) {
        zza.zza(this, parcel, i);
    }

    @Nullable
    public String zzclg() {
        return this.aNG;
    }

    public boolean zzclh() {
        return this.aNI;
    }

    public StringList zzcli() {
        return this.aNJ;
    }
}
