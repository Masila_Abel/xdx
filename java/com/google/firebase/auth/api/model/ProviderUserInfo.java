package com.google.firebase.auth.api.model;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.internal.zzafj;
import com.google.android.gms.internal.zzank;

public class ProviderUserInfo extends AbstractSafeParcelable {
    public static final Creator<ProviderUserInfo> CREATOR = new zze();
    @zzank("photoUrl")
    private String JF;
    @zzank("providerId")
    private String aNH;
    @zzank("federatedId")
    private String aNQ;
    @zzank("displayName")
    private String dC;
    @zzafj
    public final int mVersionCode;

    public ProviderUserInfo() {
        this.mVersionCode = 1;
    }

    ProviderUserInfo(int i, String str, String str2, String str3, String str4) {
        this.mVersionCode = i;
        this.aNQ = str;
        this.dC = str2;
        this.JF = str3;
        this.aNH = str4;
    }

    @Nullable
    public String getDisplayName() {
        return this.dC;
    }

    @Nullable
    public Uri getPhotoUri() {
        return !TextUtils.isEmpty(this.JF) ? Uri.parse(this.JF) : null;
    }

    public String getProviderId() {
        return this.aNH;
    }

    public void writeToParcel(Parcel parcel, int i) {
        zze.zza(this, parcel, i);
    }

    @Nullable
    public String zzckv() {
        return this.JF;
    }

    public String zzclr() {
        return this.aNQ;
    }
}
