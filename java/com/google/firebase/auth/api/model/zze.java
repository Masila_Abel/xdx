package com.google.firebase.auth.api.model;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import outlander.showcaseview.R;

public class zze implements Creator<ProviderUserInfo> {
    static void zza(ProviderUserInfo providerUserInfo, Parcel parcel, int i) {
        int zzcn = zzb.zzcn(parcel);
        zzb.zzc(parcel, 1, providerUserInfo.mVersionCode);
        zzb.zza(parcel, 2, providerUserInfo.zzclr(), false);
        zzb.zza(parcel, 3, providerUserInfo.getDisplayName(), false);
        zzb.zza(parcel, 4, providerUserInfo.zzckv(), false);
        zzb.zza(parcel, 5, providerUserInfo.getProviderId(), false);
        zzb.zzaj(parcel, zzcn);
    }

    public /* synthetic */ Object createFromParcel(Parcel parcel) {
        return zzvy(parcel);
    }

    public /* synthetic */ Object[] newArray(int i) {
        return zzaem(i);
    }

    public ProviderUserInfo[] zzaem(int i) {
        return new ProviderUserInfo[i];
    }

    public ProviderUserInfo zzvy(Parcel parcel) {
        String str = null;
        int zzcm = zza.zzcm(parcel);
        int i = 0;
        String str2 = null;
        String str3 = null;
        String str4 = null;
        while (parcel.dataPosition() < zzcm) {
            int zzcl = zza.zzcl(parcel);
            switch (zza.zzgm(zzcl)) {
                case R.styleable.View_android_focusable /*1*/:
                    i = zza.zzg(parcel, zzcl);
                    break;
                case R.styleable.View_paddingStart /*2*/:
                    str4 = zza.zzq(parcel, zzcl);
                    break;
                case R.styleable.View_paddingEnd /*3*/:
                    str3 = zza.zzq(parcel, zzcl);
                    break;
                case R.styleable.View_theme /*4*/:
                    str2 = zza.zzq(parcel, zzcl);
                    break;
                case R.styleable.Toolbar_contentInsetStart /*5*/:
                    str = zza.zzq(parcel, zzcl);
                    break;
                default:
                    zza.zzb(parcel, zzcl);
                    break;
            }
        }
        if (parcel.dataPosition() == zzcm) {
            return new ProviderUserInfo(i, str4, str3, str2, str);
        }
        throw new zza.zza("Overread allowed size end=" + zzcm, parcel);
    }
}
