package com.google.firebase.auth.api.model;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.support.annotation.Nullable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.internal.zzafj;
import com.google.android.gms.internal.zzank;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class StringList extends AbstractSafeParcelable {
    public static final Creator<StringList> CREATOR = new zzg();
    @zzank("values")
    private List<String> aNS;
    @zzafj
    public final int mVersionCode;

    public StringList() {
        this(null);
    }

    StringList(int i, List<String> list) {
        this.mVersionCode = i;
        if (list == null || list.isEmpty()) {
            this.aNS = Collections.emptyList();
        } else {
            this.aNS = Collections.unmodifiableList(list);
        }
    }

    public StringList(@Nullable List<String> list) {
        this.mVersionCode = 1;
        this.aNS = new ArrayList();
        if (list != null && !list.isEmpty()) {
            this.aNS.addAll(list);
        }
    }

    public static StringList zza(StringList stringList) {
        return new StringList(stringList != null ? stringList.zzclt() : null);
    }

    public static StringList zzclu() {
        return new StringList(null);
    }

    public void writeToParcel(Parcel parcel, int i) {
        zzg.zza(this, parcel, i);
    }

    public List<String> zzclt() {
        return this.aNS;
    }
}
