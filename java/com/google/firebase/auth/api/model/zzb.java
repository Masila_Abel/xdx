package com.google.firebase.auth.api.model;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import outlander.showcaseview.R;

public class zzb implements Creator<GetAccountInfoUser> {
    static void zza(GetAccountInfoUser getAccountInfoUser, Parcel parcel, int i) {
        int zzcn = com.google.android.gms.common.internal.safeparcel.zzb.zzcn(parcel);
        com.google.android.gms.common.internal.safeparcel.zzb.zzc(parcel, 1, getAccountInfoUser.mVersionCode);
        com.google.android.gms.common.internal.safeparcel.zzb.zza(parcel, 2, getAccountInfoUser.getLocalId(), false);
        com.google.android.gms.common.internal.safeparcel.zzb.zza(parcel, 3, getAccountInfoUser.getEmail(), false);
        com.google.android.gms.common.internal.safeparcel.zzb.zza(parcel, 4, getAccountInfoUser.zzclj());
        com.google.android.gms.common.internal.safeparcel.zzb.zza(parcel, 5, getAccountInfoUser.getDisplayName(), false);
        com.google.android.gms.common.internal.safeparcel.zzb.zza(parcel, 6, getAccountInfoUser.zzckv(), false);
        com.google.android.gms.common.internal.safeparcel.zzb.zza(parcel, 7, getAccountInfoUser.zzcll(), i, false);
        com.google.android.gms.common.internal.safeparcel.zzb.zza(parcel, 8, getAccountInfoUser.getPassword(), false);
        com.google.android.gms.common.internal.safeparcel.zzb.zzaj(parcel, zzcn);
    }

    public /* synthetic */ Object createFromParcel(Parcel parcel) {
        return zzvv(parcel);
    }

    public /* synthetic */ Object[] newArray(int i) {
        return zzaej(i);
    }

    public GetAccountInfoUser[] zzaej(int i) {
        return new GetAccountInfoUser[i];
    }

    public GetAccountInfoUser zzvv(Parcel parcel) {
        boolean z = false;
        String str = null;
        int zzcm = zza.zzcm(parcel);
        ProviderUserInfoList providerUserInfoList = null;
        String str2 = null;
        String str3 = null;
        String str4 = null;
        String str5 = null;
        int i = 0;
        while (parcel.dataPosition() < zzcm) {
            int zzcl = zza.zzcl(parcel);
            switch (zza.zzgm(zzcl)) {
                case R.styleable.View_android_focusable /*1*/:
                    i = zza.zzg(parcel, zzcl);
                    break;
                case R.styleable.View_paddingStart /*2*/:
                    str5 = zza.zzq(parcel, zzcl);
                    break;
                case R.styleable.View_paddingEnd /*3*/:
                    str4 = zza.zzq(parcel, zzcl);
                    break;
                case R.styleable.View_theme /*4*/:
                    z = zza.zzc(parcel, zzcl);
                    break;
                case R.styleable.Toolbar_contentInsetStart /*5*/:
                    str3 = zza.zzq(parcel, zzcl);
                    break;
                case R.styleable.Toolbar_contentInsetEnd /*6*/:
                    str2 = zza.zzq(parcel, zzcl);
                    break;
                case R.styleable.Toolbar_contentInsetLeft /*7*/:
                    providerUserInfoList = (ProviderUserInfoList) zza.zza(parcel, zzcl, ProviderUserInfoList.CREATOR);
                    break;
                case R.styleable.Toolbar_contentInsetRight /*8*/:
                    str = zza.zzq(parcel, zzcl);
                    break;
                default:
                    zza.zzb(parcel, zzcl);
                    break;
            }
        }
        if (parcel.dataPosition() == zzcm) {
            return new GetAccountInfoUser(i, str5, str4, z, str3, str2, providerUserInfoList, str);
        }
        throw new zza.zza("Overread allowed size end=" + zzcm, parcel);
    }
}
