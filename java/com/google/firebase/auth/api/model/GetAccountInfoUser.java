package com.google.firebase.auth.api.model;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.internal.zzafj;
import com.google.android.gms.internal.zzank;
import java.util.List;

public class GetAccountInfoUser implements SafeParcelable {
    public static final Creator<GetAccountInfoUser> CREATOR = new zzb();
    @zzank("localId")
    private String GO;
    @zzank("photoUrl")
    private String JF;
    @zzank("emailVerified")
    private boolean aNK;
    @zzank("providerUserInfo")
    private ProviderUserInfoList aNL;
    @zzank("passwordHash")
    private String cL;
    @zzank("email")
    private String dB;
    @zzank("displayName")
    private String dC;
    @zzafj
    public final int mVersionCode;

    public GetAccountInfoUser() {
        this.mVersionCode = 1;
        this.aNL = new ProviderUserInfoList();
    }

    GetAccountInfoUser(int i, String str, String str2, boolean z, String str3, String str4, ProviderUserInfoList providerUserInfoList, String str5) {
        this.mVersionCode = i;
        this.GO = str;
        this.dB = str2;
        this.aNK = z;
        this.dC = str3;
        this.JF = str4;
        this.aNL = providerUserInfoList == null ? ProviderUserInfoList.zzcls() : ProviderUserInfoList.zza(providerUserInfoList);
        this.cL = str5;
    }

    public int describeContents() {
        return 0;
    }

    @Nullable
    public String getDisplayName() {
        return this.dC;
    }

    @Nullable
    public String getEmail() {
        return this.dB;
    }

    @NonNull
    public String getLocalId() {
        return this.GO;
    }

    @Nullable
    public String getPassword() {
        return this.cL;
    }

    @Nullable
    public Uri getPhotoUri() {
        return !TextUtils.isEmpty(this.JF) ? Uri.parse(this.JF) : null;
    }

    public void writeToParcel(Parcel parcel, int i) {
        zzb.zza(this, parcel, i);
    }

    @Nullable
    public String zzckv() {
        return this.JF;
    }

    public boolean zzclj() {
        return this.aNK;
    }

    @NonNull
    public List<ProviderUserInfo> zzclk() {
        return this.aNL.zzclk();
    }

    public ProviderUserInfoList zzcll() {
        return this.aNL;
    }
}
