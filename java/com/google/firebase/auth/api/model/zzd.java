package com.google.firebase.auth.api.model;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import outlander.showcaseview.R;

public class zzd implements Creator<GetTokenResponse> {
    static void zza(GetTokenResponse getTokenResponse, Parcel parcel, int i) {
        int zzcn = zzb.zzcn(parcel);
        zzb.zzc(parcel, 1, getTokenResponse.mVersionCode);
        zzb.zza(parcel, 2, getTokenResponse.zzcln(), false);
        zzb.zza(parcel, 3, getTokenResponse.getAccessToken(), false);
        zzb.zza(parcel, 4, Long.valueOf(getTokenResponse.zzclo()), false);
        zzb.zza(parcel, 5, getTokenResponse.zzclp(), false);
        zzb.zza(parcel, 6, Long.valueOf(getTokenResponse.zzclq()), false);
        zzb.zzaj(parcel, zzcn);
    }

    public /* synthetic */ Object createFromParcel(Parcel parcel) {
        return zzvx(parcel);
    }

    public /* synthetic */ Object[] newArray(int i) {
        return zzael(i);
    }

    public GetTokenResponse[] zzael(int i) {
        return new GetTokenResponse[i];
    }

    public GetTokenResponse zzvx(Parcel parcel) {
        Long l = null;
        int zzcm = zza.zzcm(parcel);
        int i = 0;
        String str = null;
        Long l2 = null;
        String str2 = null;
        String str3 = null;
        while (parcel.dataPosition() < zzcm) {
            int zzcl = zza.zzcl(parcel);
            switch (zza.zzgm(zzcl)) {
                case R.styleable.View_android_focusable /*1*/:
                    i = zza.zzg(parcel, zzcl);
                    break;
                case R.styleable.View_paddingStart /*2*/:
                    str3 = zza.zzq(parcel, zzcl);
                    break;
                case R.styleable.View_paddingEnd /*3*/:
                    str2 = zza.zzq(parcel, zzcl);
                    break;
                case R.styleable.View_theme /*4*/:
                    l2 = zza.zzj(parcel, zzcl);
                    break;
                case R.styleable.Toolbar_contentInsetStart /*5*/:
                    str = zza.zzq(parcel, zzcl);
                    break;
                case R.styleable.Toolbar_contentInsetEnd /*6*/:
                    l = zza.zzj(parcel, zzcl);
                    break;
                default:
                    zza.zzb(parcel, zzcl);
                    break;
            }
        }
        if (parcel.dataPosition() == zzcm) {
            return new GetTokenResponse(i, str3, str2, l2, str, l);
        }
        throw new zza.zza("Overread allowed size end=" + zzcm, parcel);
    }
}
