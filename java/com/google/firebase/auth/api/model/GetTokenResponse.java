package com.google.firebase.auth.api.model;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.zzab;
import com.google.android.gms.common.util.zzh;
import com.google.android.gms.internal.zzafj;
import com.google.android.gms.internal.zzank;

public class GetTokenResponse extends AbstractSafeParcelable {
    public static final Creator<GetTokenResponse> CREATOR = new zzd();
    @zzank("expires_in")
    private Long aNN;
    @zzank("token_type")
    private String aNO;
    @zzank("issued_at")
    private Long aNP;
    @zzank("refresh_token")
    private String aNh;
    @zzank("access_token")
    private String atD;
    @zzafj
    public final int mVersionCode;

    public GetTokenResponse() {
        this.mVersionCode = 1;
        this.aNP = Long.valueOf(System.currentTimeMillis());
    }

    GetTokenResponse(int i, String str, String str2, Long l, String str3, Long l2) {
        this.mVersionCode = i;
        this.aNh = str;
        this.atD = str2;
        this.aNN = l;
        this.aNO = str3;
        this.aNP = l2;
    }

    public String getAccessToken() {
        return this.atD;
    }

    public boolean isValid() {
        return zzh.zzavm().currentTimeMillis() + 300000 < this.aNP.longValue() + (this.aNN.longValue() * 1000);
    }

    public void writeToParcel(Parcel parcel, int i) {
        zzd.zza(this, parcel, i);
    }

    public String zzcln() {
        return this.aNh;
    }

    public long zzclo() {
        return this.aNN == null ? 0 : this.aNN.longValue();
    }

    @Nullable
    public String zzclp() {
        return this.aNO;
    }

    public long zzclq() {
        return this.aNP.longValue();
    }

    public void zzrc(@NonNull String str) {
        this.aNh = zzab.zzhr(str);
    }
}
