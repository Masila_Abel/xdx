package com.google.firebase.auth;

import android.support.annotation.NonNull;
import com.google.android.gms.common.internal.zzab;

public class EmailAuthCredential extends AuthCredential {
    private String cL;
    private String dB;

    EmailAuthCredential(@NonNull String str, @NonNull String str2) {
        this.dB = zzab.zzhr(str);
        this.cL = zzab.zzhr(str2);
    }

    @NonNull
    public String getEmail() {
        return this.dB;
    }

    @NonNull
    public String getPassword() {
        return this.cL;
    }

    @NonNull
    public String getProvider() {
        return EmailAuthProvider.PROVIDER_ID;
    }
}
