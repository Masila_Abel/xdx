package com.google.firebase.auth;

import android.support.annotation.NonNull;
import com.google.android.gms.common.internal.zzab;
import com.google.firebase.auth.api.model.VerifyAssertionRequest;

public class TwitterAuthCredential extends AuthCredential {
    private String aNd;
    private String co;

    TwitterAuthCredential(@NonNull String str, @NonNull String str2) {
        this.co = zzab.zzhr(str);
        this.aNd = zzab.zzhr(str2);
    }

    public static VerifyAssertionRequest zza(@NonNull TwitterAuthCredential twitterAuthCredential) {
        zzab.zzy(twitterAuthCredential);
        return new VerifyAssertionRequest(null, twitterAuthCredential.getToken(), twitterAuthCredential.getProvider(), null, twitterAuthCredential.zzcku());
    }

    public String getProvider() {
        return TwitterAuthProvider.PROVIDER_ID;
    }

    @NonNull
    public String getToken() {
        return this.co;
    }

    @NonNull
    public String zzcku() {
        return this.aNd;
    }
}
