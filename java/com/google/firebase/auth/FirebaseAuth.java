package com.google.firebase.auth;

import android.support.annotation.Keep;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.util.ArrayMap;
import android.util.Log;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.zzab;
import com.google.android.gms.internal.zzafa;
import com.google.android.gms.internal.zzafd;
import com.google.android.gms.internal.zzaff;
import com.google.android.gms.internal.zzafi;
import com.google.android.gms.internal.zzafp;
import com.google.android.gms.internal.zzafr;
import com.google.android.gms.internal.zzaft;
import com.google.android.gms.internal.zzafu;
import com.google.android.gms.internal.zzafx;
import com.google.android.gms.internal.zzafy;
import com.google.android.gms.internal.zzafz;
import com.google.android.gms.internal.zzalq;
import com.google.android.gms.internal.zzamp;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.Tasks;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.api.model.GetTokenResponse;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;

public abstract class FirebaseAuth implements zzalq {
    private static FirebaseAuth aMY;
    private static Map<String, FirebaseAuth> aap = new ArrayMap();
    private FirebaseApp aMS;
    private zzafa aMT;
    private FirebaseUser aMU;
    private zzafy aMV;
    private zzamp aMW;
    private zzafz aMX;
    private List<AuthStateListener> mListeners;

    public interface AuthStateListener {
        void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth);
    }

    class zza implements zzafp {
        final /* synthetic */ FirebaseAuth aNa;

        zza(FirebaseAuth firebaseAuth) {
            this.aNa = firebaseAuth;
        }

        public void zza(@NonNull GetTokenResponse getTokenResponse, @NonNull FirebaseUser firebaseUser) {
            zzab.zzy(getTokenResponse);
            zzab.zzy(firebaseUser);
            firebaseUser.zzrb(this.aNa.aMW.zzch(getTokenResponse));
            this.aNa.zza(firebaseUser, getTokenResponse, true);
            this.aNa.zza(firebaseUser, true, true);
        }
    }

    public FirebaseAuth(FirebaseApp firebaseApp) {
        this(firebaseApp, zza(firebaseApp), new zzafy(firebaseApp.getApplicationContext(), firebaseApp.zzckc(), zzaff.zzcla()));
    }

    FirebaseAuth(FirebaseApp firebaseApp, zzafa com_google_android_gms_internal_zzafa, zzafy com_google_android_gms_internal_zzafy) {
        this.aMS = (FirebaseApp) zzab.zzy(firebaseApp);
        this.aMT = (zzafa) zzab.zzy(com_google_android_gms_internal_zzafa);
        this.aMV = (zzafy) zzab.zzy(com_google_android_gms_internal_zzafy);
        this.mListeners = new CopyOnWriteArrayList();
        this.aMW = zzaff.zzcla();
        this.aMX = zzafz.zzcmc();
        zzckq();
    }

    public static FirebaseAuth getInstance() {
        return zzb(FirebaseApp.getInstance());
    }

    @Keep
    public static FirebaseAuth getInstance(@NonNull FirebaseApp firebaseApp) {
        return zzb(firebaseApp);
    }

    static zzafa zza(FirebaseApp firebaseApp) {
        return zzafi.zza(firebaseApp.getApplicationContext(), new com.google.android.gms.internal.zzafi.zza.zza(firebaseApp.getOptions().getApiKey()).zzcld());
    }

    private static FirebaseAuth zzb(@NonNull FirebaseApp firebaseApp) {
        return zzc(firebaseApp);
    }

    private static synchronized FirebaseAuth zzc(@NonNull FirebaseApp firebaseApp) {
        FirebaseAuth firebaseAuth;
        synchronized (FirebaseAuth.class) {
            firebaseAuth = (FirebaseAuth) aap.get(firebaseApp.zzckc());
            if (firebaseAuth == null) {
                firebaseAuth = new zzaft(firebaseApp);
                firebaseApp.zza((zzalq) firebaseAuth);
                if (aMY == null) {
                    aMY = firebaseAuth;
                }
                aap.put(firebaseApp.zzckc(), firebaseAuth);
            }
        }
        return firebaseAuth;
    }

    public void addAuthStateListener(@NonNull final AuthStateListener authStateListener) {
        this.mListeners.add(authStateListener);
        this.aMX.execute(new Runnable(this) {
            final /* synthetic */ FirebaseAuth aNa;

            public void run() {
                authStateListener.onAuthStateChanged(this.aNa);
            }
        });
    }

    @NonNull
    public Task<AuthResult> createUserWithEmailAndPassword(@NonNull String str, @NonNull String str2) {
        zzab.zzhr(str);
        zzab.zzhr(str2);
        return this.aMT.zza(this.aMS, str, str2, new zza(this));
    }

    @NonNull
    public Task<ProviderQueryResult> fetchProvidersForEmail(@NonNull String str) {
        zzab.zzhr(str);
        return this.aMT.zza(this.aMS, str);
    }

    @Nullable
    public FirebaseUser getCurrentUser() {
        return this.aMU;
    }

    public void removeAuthStateListener(@NonNull AuthStateListener authStateListener) {
        this.mListeners.remove(authStateListener);
    }

    @NonNull
    public Task<Void> sendPasswordResetEmail(@NonNull String str) {
        zzab.zzhr(str);
        return this.aMT.zzb(this.aMS, str);
    }

    @NonNull
    public Task<AuthResult> signInAnonymously() {
        return (this.aMU == null || !this.aMU.isAnonymous()) ? this.aMT.zza(this.aMS, new zza(this)) : Tasks.forResult(new zzafr((zzafu) this.aMU));
    }

    @NonNull
    public Task<AuthResult> signInWithCredential(@NonNull AuthCredential authCredential) {
        zzab.zzy(authCredential);
        if (!EmailAuthCredential.class.isAssignableFrom(authCredential.getClass())) {
            return this.aMT.zza(this.aMS, authCredential, new zza(this));
        }
        EmailAuthCredential emailAuthCredential = (EmailAuthCredential) authCredential;
        return this.aMT.zzb(this.aMS, emailAuthCredential.getEmail(), emailAuthCredential.getPassword(), new zza(this));
    }

    @NonNull
    public Task<AuthResult> signInWithCustomToken(@NonNull String str) {
        zzab.zzhr(str);
        return this.aMT.zza(this.aMS, str, new zza(this));
    }

    @NonNull
    public Task<AuthResult> signInWithEmailAndPassword(@NonNull String str, @NonNull String str2) {
        zzab.zzhr(str);
        zzab.zzhr(str2);
        return this.aMT.zzb(this.aMS, str, str2, new zza(this));
    }

    public void signOut() {
        zzckp();
    }

    @NonNull
    public Task<Void> zza(@NonNull FirebaseUser firebaseUser, @NonNull AuthCredential authCredential) {
        zzab.zzy(firebaseUser);
        zzab.zzy(authCredential);
        if (!EmailAuthCredential.class.isAssignableFrom(authCredential.getClass())) {
            return this.aMT.zza(this.aMS, firebaseUser, authCredential, new zza(this));
        }
        EmailAuthCredential emailAuthCredential = (EmailAuthCredential) authCredential;
        return this.aMT.zza(this.aMS, firebaseUser, emailAuthCredential.getEmail(), emailAuthCredential.getPassword(), new zza(this));
    }

    @NonNull
    public Task<Void> zza(@NonNull FirebaseUser firebaseUser, @NonNull UserProfileChangeRequest userProfileChangeRequest) {
        zzab.zzy(firebaseUser);
        zzab.zzy(userProfileChangeRequest);
        return this.aMT.zza(this.aMS, firebaseUser, userProfileChangeRequest, new zza(this));
    }

    @NonNull
    public Task<AuthResult> zza(@NonNull FirebaseUser firebaseUser, @NonNull String str) {
        zzab.zzhr(str);
        zzab.zzy(firebaseUser);
        return this.aMT.zzd(this.aMS, firebaseUser, str, new zza(this));
    }

    @NonNull
    public Task<GetTokenResult> zza(@Nullable FirebaseUser firebaseUser, boolean z) {
        if (firebaseUser == null) {
            return Tasks.forException(zzafd.zzes(new Status(17495)));
        }
        GetTokenResponse getTokenResponse = (GetTokenResponse) this.aMW.zzf(this.aMU.zzckt(), GetTokenResponse.class);
        return (!getTokenResponse.isValid() || z) ? this.aMT.zza(this.aMS, firebaseUser, getTokenResponse.zzcln(), new zzafp(this) {
            final /* synthetic */ FirebaseAuth aNa;

            {
                this.aNa = r1;
            }

            public void zza(@NonNull GetTokenResponse getTokenResponse, @NonNull FirebaseUser firebaseUser) {
                this.aNa.zza(firebaseUser, getTokenResponse, true);
            }
        }) : Tasks.forResult(new GetTokenResult(getTokenResponse.getAccessToken()));
    }

    public void zza(@Nullable final FirebaseUser firebaseUser) {
        if (firebaseUser != null) {
            String valueOf = String.valueOf(firebaseUser.getUid());
            Log.d("FirebaseAuth", new StringBuilder(String.valueOf(valueOf).length() + 36).append("Notifying listeners about user ( ").append(valueOf).append(" ).").toString());
        } else {
            Log.d("FirebaseAuth", "Notifying listeners about a sign-out event.");
        }
        this.aMX.execute(new Runnable(this) {
            final /* synthetic */ FirebaseAuth aNa;

            public void run() {
                this.aNa.aMS.zza(this.aNa, firebaseUser);
                for (AuthStateListener onAuthStateChanged : this.aNa.mListeners) {
                    onAuthStateChanged.onAuthStateChanged(this.aNa);
                }
            }
        });
    }

    public void zza(@NonNull FirebaseUser firebaseUser, @NonNull GetTokenResponse getTokenResponse, boolean z) {
        Object obj = 1;
        zzab.zzy(firebaseUser);
        zzab.zzy(getTokenResponse);
        if (this.aMU != null) {
            String accessToken = ((GetTokenResponse) this.aMW.zzf(this.aMU.zzckt(), GetTokenResponse.class)).getAccessToken();
            Object obj2 = (!this.aMU.getUid().equalsIgnoreCase(firebaseUser.getUid()) || accessToken == null || accessToken.equals(getTokenResponse.getAccessToken())) ? null : 1;
            obj = obj2;
        }
        if (obj != null) {
            if (this.aMU != null) {
                this.aMU.zzrb(this.aMW.zzch(getTokenResponse));
            }
            zza(this.aMU);
        }
        if (z) {
            this.aMV.zza(firebaseUser, getTokenResponse);
        }
    }

    public void zza(@NonNull FirebaseUser firebaseUser, boolean z, boolean z2) {
        zzab.zzy(firebaseUser);
        if (this.aMU == null) {
            this.aMU = firebaseUser;
        } else {
            this.aMU.zzcn(firebaseUser.isAnonymous());
            this.aMU.zzan(firebaseUser.getProviderData());
        }
        if (z) {
            this.aMV.zze(this.aMU);
        }
        if (z2) {
            zza(this.aMU);
        }
    }

    @NonNull
    public Task<Void> zzb(@NonNull FirebaseUser firebaseUser) {
        zzab.zzy(firebaseUser);
        return this.aMT.zzb(this.aMS, firebaseUser, new zza(this));
    }

    @NonNull
    public Task<AuthResult> zzb(@NonNull FirebaseUser firebaseUser, @NonNull AuthCredential authCredential) {
        zzab.zzy(authCredential);
        zzab.zzy(firebaseUser);
        return this.aMT.zzb(this.aMS, firebaseUser, authCredential, new zza(this));
    }

    @NonNull
    public Task<Void> zzb(@NonNull FirebaseUser firebaseUser, @NonNull String str) {
        zzab.zzy(firebaseUser);
        zzab.zzhr(str);
        return this.aMT.zzb(this.aMS, firebaseUser, str, new zza(this));
    }

    @NonNull
    public Task<Void> zzc(@NonNull final FirebaseUser firebaseUser) {
        zzab.zzy(firebaseUser);
        return this.aMT.zza(firebaseUser, new zzafx(this) {
            final /* synthetic */ FirebaseAuth aNa;

            public void zzckr() {
                if (this.aNa.aMU.getUid().equalsIgnoreCase(firebaseUser.getUid())) {
                    this.aNa.zzckp();
                }
            }
        });
    }

    @NonNull
    public Task<Void> zzc(@NonNull FirebaseUser firebaseUser, @NonNull String str) {
        zzab.zzy(firebaseUser);
        zzab.zzhr(str);
        return this.aMT.zzc(this.aMS, firebaseUser, str, new zza(this));
    }

    public void zzckp() {
        if (this.aMU != null) {
            this.aMV.zzg(this.aMU);
            this.aMU = null;
        }
        this.aMV.zzcmb();
        zza(null);
    }

    protected void zzckq() {
        this.aMU = this.aMV.zzcma();
        if (this.aMU != null) {
            zza(this.aMU, false, true);
            GetTokenResponse zzf = this.aMV.zzf(this.aMU);
            if (zzf != null) {
                zza(this.aMU, zzf, false);
            }
        }
    }
}
