package de.hdodenhof.circleimageview;

public final class R {

    public static final class attr {
        public static final int border_color = 2130772126;
        public static final int border_overlay = 2130772127;
        public static final int border_width = 2130772125;
    }

    public static final class styleable {
        public static final int[] CircleImageView = new int[]{R.attr.border_width, R.attr.border_color, R.attr.border_overlay};
        public static final int CircleImageView_border_color = 1;
        public static final int CircleImageView_border_overlay = 2;
        public static final int CircleImageView_border_width = 0;
    }
}
